<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Casa extends Model
{
//	use SoftDeletes;

    protected $table = 'casa';
    protected $fillable = [
        'id_casa',
        'nb_casa',
        'nb_representante',
        'tx_correo',
        'created_at',
        'updated_at',
        'deleted_at'
	];
}


