<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Correos extends Model
{
//	use SoftDeletes;

    protected $table = 'correos';
    protected $fillable = [
        'id_correo',
        'nb_correo',
        'created_at',
        'updated_at'
	];
}