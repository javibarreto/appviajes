<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Competidor extends Model
{
//	use SoftDeletes;

    protected $table = 'competidor';
    protected $fillable = [
        'id_competidor',
        'nb_competidor',
        'peso',
        'id_casa',
        'created_at',
        'updated_at',
        'deleted_at',
	];
}


