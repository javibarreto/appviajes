<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consultas extends Model
{

	use SoftDeletes;

    protected $table = 'consultas';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'id',
    'codpersona',
    'descripcion',
    'respuesta',
    'fecha',
    'hora',
    'respondido',
    'codpersonaresp'    
	];

    //protected $dates = ['deleted_at'];
    //
}
   