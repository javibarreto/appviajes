<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fmarcasw extends Model
{

	use SoftDeletes;

    protected $table = 'fmarcas';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'idasis',
    'archivo',
    'cargo',
    'cfecha',
    'codperson',
    'device',
    'dniperson',
    'fecha',
    'hora',
    'motivo',
    'origen',
    'verificado',
    'creadodesde',
    'latitud',
    'longitud',
    'ipmarcacion'
	];

    //protected $dates = ['deleted_at'];
    //
}
   