<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Auth\Middleware\Authenticate;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Session;
use DB;
use Redirect;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

 //   use AuthenticatesAndRegistersUsers, ThrottlesLogins;

  

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        $this->middleware('guest', ['except' => 'getLogout']);
      
    }


    //******************************************************************************//
    //ESTA FUNCION BUSCA EN LA TABLA USERS LA INFORMACION DEL USUARIO
    //QUE ESTA ACCESANDO AL SISTEMA PRESENTANDOSE 2 CASOS:

    //  1.- EL USUARIO ES PRIMERA VEZ QUE ACCESA AL SISTEMA POR LO TANTO 
    //      SE INGRESA A LA TABLA USERS ENCRIPTANDO EL CAMPO CLAVERELOJ PROVENIENTE 
    //      DE LA TABLA PERSONAL,SE REALIZA LA ENCRIPTACION DEBIDO A QUE LARAVEL
    //      MANEJA LA AUTENTICACION DEL CAMPO PASSWORD ENCRIPTADO.

    //  2.- EL USUARIO YA HA TENIDO ACCESO AL SISTEMA POR LO TANTO 
    //      SE BUSCA CON EL CODIGO DEL USUARIO LA CLAVERELOJ EN LA TABLA 
    //      PERSONAL, SE ENCRIPTA CLAVERELOJ Y SE ACTUALIZA EL CAMPO 
    //      PASSWORD CON ESTA INFORMACION EN LA TABLA USERS,DICHO PROCESO SE REALIZA 
    //      DE ESTA MANERA PORQUE EN EL SISTEMA NO EXISTE UN MODULO PARA CAMBIAR 
    //      LA CLAVE DE ACCESO POR LO TANTO SIEMPRE SE DEBE TRAER LA INFORMACION DE
    //      LA TABLA PERSONAL 

    //TAMBIEN SE BUSCA SI EL USUARIO QUE ESTA INGRESANDO ES SUPERVISOR EN LA 
    //REALIZADO UNA CONSULTA A LA TABLA PERSONAL EN EL CAMPO psuperv PARA HABILITAR
    //EN EL ADMIN LA OPCION DE APROBAR SOLICITUD

    //LA AUTENTICACION SE REALIZA CON LA FUNCION Auth::attempt QUE RETORNARA 1
    //SI EL USUARIO Y LA CLAVE SON CORRECTOS Y EN CASO CONTRARIO RETORNA 2
     //******************************************************************************//


    public function Iniciar_sesion(Request $request)
    {

        $usuario = DB::table('users')
                ->select(
                    'users.co_usuario',
                    'users.password'
                )
                ->whereNull('users.deleted_at')
                 ->where('users.co_usuario',$request['usuario'])
                ->count();


        if ($usuario == 0) {

            $usuarioclave = DB::table('personal')
                ->select(
                     'personal.clavereloj'  
                )
                
            ->where('personal.codperson',$request['usuario'])
            ->get();


            $clave = trim($usuarioclave[0]->clavereloj);

            //dd($clave);
            //die();


            $password= bcrypt($clave);

            $supervisor = DB::table('personal')
                ->select(
                     'personal.psuperv'  
                )
                ->distinct()
                ->where('personal.psuperv',$request['usuario'])
                ->count();

    
                 if ($supervisor==0){

                    $supervisor="NO";

                 }
                 else{
                    $supervisor="SI";
                 }


            $utimoid = DB::select(DB::raw('select id_usuario from users where id_usuario = (select max(`id_usuario`) from users )'));
                
            if($utimoid==null){
                $idingresar= 1;
            }
            else{

                $ultimo = trim($utimoid[0]->id_usuario);
                $idingresar= $ultimo + 1;
                $idingresar= (int)$idingresar;
            }

            /*$utimoid = user::max('id_usuario'); 
            $idingresar= $utimoid + 1;
            $idingresar= (int)$idingresar;*/

            //dd($idingresar);
            //die();

            $usuarionuevo = User::create(["id_usuario"=>$idingresar,"co_usuario"=>$request['usuario'],"password"=>$password,"supervisor"=>$supervisor,"latitud"=>$request['latitud'],"longitud"=>$request['longitud']]);

           //return('-1');

        }else{

            
            $usuarioclave = DB::table('personal')
                ->select(
                     'personal.clavereloj'  
                )
                
            ->where('personal.codperson',$request['usuario'])
            ->get();


            $clave = trim($usuarioclave[0]->clavereloj);

            //dd($clave);
            //die();


            $password= bcrypt($clave);

            //dd($password);
            //die();

            $supervisor = DB::table('personal')
                ->select(
                     'personal.psuperv'  
                )
                ->distinct()
                ->where('personal.psuperv',$request['usuario'])
                ->count();

    
                 if ($supervisor==0){

                    $supervisor="NO";

                 }
                 else{
                    $supervisor="SI";
                 }


            $usuarioactualizar = User::where('co_usuario',$request['usuario'])->update(["password" => $password,"supervisor"=>$supervisor,"latitud"=>$request['latitud'],"longitud"=>$request['longitud']]);

             
        }


        if(Auth::attempt(['co_usuario'=>$request['usuario'], 'password'=>$request['clave']])){

           
            return('1');
           
        }
                    
            return("-2");
                     
                  
    }


   




}
