<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Casa;
use App\Correos;


class CasaController extends Controller
{

    public function casa(Request $request)
    {

        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.casa.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarCasa(Request $request)
    {
        $casa = Casa::all();
        return response()->json($casa);
    }


    public function CrearCasa(Request $request)
    {

        if ($request->ajax()) {

            $casa = $request['casa'];
            $representante = $request['representante'];
            $correo = trim($request['correo']);


            $nb_casa = DB::table('casa')
            ->select(
                    'casa.nb_casa'  
            )
            ->where('casa.nb_casa',$casa)
            ->count();

                if ($nb_casa != 0) {
                return response()->json(["mensaje" => "Casa Existente"]);
                }; 

            $correo = Casa::create([
                    "nb_casa"  => $casa,
                    "nb_representante"  => $representante,
                    "tx_correo"  => $correo,
            ]);

            $respuesta = array(
                "mensaje"  => "creado"
            );

            return response()->json($respuesta);

        }

    }

    public function editarCasa(Request $request)
    {
        if ($request->ajax()) {

            $id = $request['id'];
            $casa = $request['casa'];
            $representante = $request['representante'];
            $correo = trim($request['correo']);


            $nb_casa = DB::table('casa')
            ->select(
                'casa.nb_casa'
            )
            ->where('casa.nb_casa',$casa)
            ->where('casa.id_casa','<>',$id)
            ->count();

            if ($nb_casa != 0) {
             return response()->json(["mensaje" => "casa Existente"]);
            };


            $casa = Casa::where('id_casa', $id)->update([
                "nb_casa"  => $casa,
                "nb_representante"  => $representante,
                "tx_correo"  => $correo,
            ]);

            return response()->json([
                "mensaje" => "actualizado"
            ]);

        }


    }

    public function eliminarCasa(Request $request)
    {
        if ($request->ajax()) {
            $date = trim(Carbon::now());

            try {
                $id = $request['id'];

                $casa = Casa::where('id_casa', $id)->delete();

                return response()->json([
                    "mensaje" => "eliminado"
                ]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }
    }


    public function obtenerCasa(Request $request,$id)
    {

        if ($request->ajax()) {

            $casa = DB::table('casa')
            ->select(
                    'id_casa',
                    'nb_casa',
                    'nb_representante',
                    'tx_correo'
                    )
            ->where('id_casa', $id)
            ->get();

            return response()->json($casa);

        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
