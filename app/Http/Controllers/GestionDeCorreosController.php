<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Correos;


class GestionDeCorreosController extends Controller
{
    
  


    public function GestionCorreos(Request $request)
    {

     
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.gestiondecorreos.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarcorreos(Request $request)
    {
        $correos = Correos::all();
        return response()->json($correos);
    }


    public function CrearCorreo(Request $request)
    {

        if ($request->ajax()) {

            $correo = $request['correo'];
           
            DB::beginTransaction();

            try {

              $utimoid = DB::select(DB::raw('select id_correo from correos where id_correo = (select max(`id_correo`) from correos )'));

                if($utimoid==null){
                    $idingresar= 1;
                }
                else{

                    $ultimo = trim($utimoid[0]->id_correo);
                    $idingresar= $ultimo + 1;
                    $idingresar= (int)$idingresar;
                } 


                $nb_correo = DB::table('correos')
                ->select(
                     'correos.nb_correo'  
                )
                ->where('correos.nb_correo',$correo)
                ->count();

                 if ($nb_correo != 0) {
                    return response()->json(["mensaje" => "Correo Existente"]);
                 }; 

                $correo = Correos::create([
                     "id_correo"  => $idingresar,
                    "nb_correo"  => $correo,
                ]);

                DB::commit();

                $respuesta = array(
                    "mensaje"  => "creado",
                    "tipo"  => "vacacion",
                    "correo" => $correo,
                );

                return response()->json($respuesta);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

    public function editarcorreo(Request $request)
    {
         
        if ($request->ajax()) {

            $id = $request['id'];
            $correo = trim($request['correo']);

            $nb_correo = DB::table('correos')
            ->select(
                'correos.nb_correo'  
            )
            ->where('correos.nb_correo',$correo)
            ->count();

            if ($nb_correo != 0) {
             return response()->json(["mensaje" => "Correo Existente"]);
            }; 


            $correo = Correos::where('id_correo', $id)->update(['nb_correo' => $correo]);

            return response()->json([
                "mensaje" => "actualizado"
            ]);

        }


    }

    
    public function eliminarcorreo(Request $request)
    {
            
        if ($request->ajax()) {
            $date = trim(Carbon::now());

            try { 
            
                $id = $request['id'];

                $correo = Correos::where('id_correo', $id)->delete();

                return response()->json([
                    "mensaje" => "eliminado"
                ]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }
    }


    public function obtenercorreo(Request $request,$id)
    {

        if ($request->ajax()) {

            $correo = DB::table('correos')
            ->select(
                    'id_correo',
                    'nb_correo'
                    )
            ->where('id_correo', $id)
            ->get();

            return response()->json($correo);

        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
