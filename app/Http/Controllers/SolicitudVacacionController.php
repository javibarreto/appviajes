<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;


class SolicitudVacacionController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";


    public function arregloColumnasSolicitudVacacion()
    {
        return array(
            0 =>  'id',
            1 =>  'fechasoli',
            2 =>  'horasoli',
            3 =>  'fechadesde',
            4 =>  'fechahasta',
            5 =>  'descripcio',
            6 =>  'useraprob',
            7 =>  'observacion',
            8 =>  'drelacionado',
            9 =>  'estatus',
            10 => 'documento',
        );
    }

     public function contarSolicitudVacacion($consulta)
    {
        return $consulta->count();
    }

    public function obtenerSolicitudVacacion($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaSolicitudVacacion($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {
            $consulta->offset($inicio)
                ->limit($limite);
        }

        return $consulta;
    }

    private function ordenarConsultaSolicitudVacacion($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaSolicitudVacacion($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function SolicitudVacacion(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.solicitudincidencia.vacacion.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function SolicitudVacacionEspecifica($buscar,$fe_desde,$fe_hasta,$estatus,$tipovacacion)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        if ($estatus == 'todos') {
            $estatus = '%';
        }

        if ($estatus == '') {
            $estatus = 'fecha';
        } 

        //dd($tipovacacion);
        //die();

        if ($tipovacacion == 'todos') {
            $tipovacacion = '%';
        }


        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();


        if (($tipovacacion=='%')&&($estatus=='%')){

            $consulta = DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.useraprob')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'descripcio as descripcion',
                    'nomperson as aprobadarechazpor',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS aprobadarechazpor'),
                     DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'mediajornadavac as mediajornadavac',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                )
                ->distinct()
                ->orderBy('fechasoli','asc')
                ->where('codpersona',$co_usuario)
                ->where('tiposolicitud',"V")
                ->whereNull('incweb.deleted_at')
                 ->orderBy('id', 'asc');
        }
        else{

            if ($estatus=='%'){

                $consulta = DB::table('incweb')
                    ->leftjoin('personal','personal.codperson', '=', 'incweb.useraprob')
                    ->select(
                        'id as idsolicitud',
                        DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                        'horasoli as horasolicitud',
                        DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                        DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                        'descripcio as descripcion',
                        'nomperson as aprobadarechazpor',
                        //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS aprobadarechazpor'),
                         DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                        'horaaprob as horaaprob',
                        DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                        'horarechaz as horarechaz',
                        'observacion as observacion',
                        'drelacionado  as codigoprocesado',
                        'estatus as estatus',
                        'mediajornadavac as mediajornadavac',
                        'documento as documento',
                         DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                        'horarevi as horarevi',
                        'mediajornadavac as mediajornadavac'
                    )
                    ->distinct()
                    ->orderBy('fechasoli','asc')
                    ->where('codpersona',$co_usuario)
                    ->where('tiposolicitud',"V")
                    ->whereNull('incweb.deleted_at')
                    ->orderBy('id', 'asc');

                if (($estatus!='%')&&($estatus!='fecha')){
                    //dd("pase por aqui".$sucursal);
                    $consulta->where('estatus',$estatus);
                }


                if ($tipovacacion != '%'){
            
                    if($tipovacacion=="Vacaciones"){
                        //dd($tipovacacion);
                        $mediajornada="NO";
                    }
                    if($tipovacacion=="Media Jornada"){
                        $mediajornada="SI";
                    }

                    //dd($mediajornada);
                    //die();
                    //dd("pase por aqui".$codigoincdia);
                    $consulta->where('incweb.mediajornadavac',$mediajornada);
                }

            }  
            else{

                $consulta = DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.useraprob')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'descripcio as descripcion',
                     'nomperson as aprobadarechazpor',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS aprobadarechazpor'),
                     DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'mediajornadavac as mediajornadavac',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                )
                ->distinct()
                ->whereBetween('fechasoli',[$fe_desde,$fe_hasta])
                ->orderBy('fechasoli','asc')
                ->where('codpersona',$co_usuario)
                ->where('tiposolicitud',"V")
                ->whereNull('incweb.deleted_at')
                 ->orderBy('id', 'asc');

                if (($estatus!='%')&&($estatus!='fecha')){
                     //dd("pase por aqui".$sucursal);
                     $consulta->where('estatus',$estatus);
                }


                if ($tipovacacion != '%'){
                    
                    if($tipovacacion=="Vacaciones"){
                        //dd($tipovacacion);
                        $mediajornada="NO";
                    }
                    if($tipovacacion=="Media Jornada"){
                        $mediajornada="SI";
                    }

                    //dd($mediajornada);
                    //die();
                     //dd("pase por aqui".$codigoincdia);
                    $consulta->where('incweb.mediajornadavac',$mediajornada);
                }


            }       

        }    

        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function SolicitudVacacionGeneral()
    {

        //$fecha = Carbon::now();
        //$fecha = $fecha->format('Ymd');
        //dd($fecha);

        $fecha_actual = Carbon::now();

        //LA FECHA DESDE Y LA FECHA HASTA SE ARMA DE ESTA MANERA PARA QUE MUESTREN 
        //LA INFORMACION DE TODO EL MES

        $monthfinal = $fecha_actual->format('m');
        $yearfinal  = $fecha_actual->format('Y');

        if ($monthfinal == 01) {
            $monthdesde = 12;
            $yeardesde  = $yearfinal;

        } else {
            $monthdesde = $monthfinal;
            $yeardesde  = $yearfinal;
        }

        $daydesde = "01";
        $fe_desde = $yeardesde . '-' . $monthdesde . '-' . $daydesde;

        $dayfinal = "31";
        $fe_hasta = $yearfinal . '-' . $monthfinal . '-' . $dayfinal;

        $fechadesde = explode("-",$fe_desde);
        $fechahasta = explode("-",$fe_hasta);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        return DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.useraprob')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'descripcio as descripcion',
                    'nomperson as aprobadarechazpor',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS aprobadarechazpor'),
                     DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'mediajornadavac as mediajornadavac',
                    'documento as documento',
                    DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                    
                )
                ->distinct()
                //->whereBetween('fechasoli',[$fechadesdeformateada,$fechahastaformateada])
                ->where('codpersona',$co_usuario)
                ->where('tiposolicitud',"V")
                ->whereNull('incweb.deleted_at')
                 ->orderBy('id', 'asc');

    }

     private function BuscarEnConsultaFiltroSolicitudVacacion($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    public function ListarSolicitudVacacion(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasSolicitudVacacion();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico


        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_solicitudvacacion  = $request->input('dp_fedesde_solicitudvacacion');
            $dp_fehasta_solicitudvacacion  = $request->input('dp_fehasta_solicitudvacacion');


            $fechadesde = explode("-",$dp_fedesde_solicitudvacacion);
            $fechahasta = explode("-",$dp_fehasta_solicitudvacacion);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

            $estatus=$request->input('estatus');

            $tipovacacion=$request->input('tipovacacion');

            //dd($estatus);
            //die();


            $consulta = self::SolicitudVacacionEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$estatus,$tipovacacion);

        }else{


            $consulta = self::SolicitudVacacionGeneral();
          
        }

         $consulta  = self::buscarEnConsultaSolicitudVacacion($consulta, $buscar, $columns);

        $total = self::contarSolicitudVacacion($consulta);


        //dd($total);
        //die();
        $consulta = self::limitarConsultaSolicitudVacacion($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listasolicitudvacacion = self::obtenerSolicitudVacacion($consulta);

        //dd($listamarcaasistencia);
        //die();

       
        //dd($totaltardanza);
        //die();

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listasolicitudvacacion,

        );

     

        return response()->json($json_data);
    
    }


    public function IngresarSolicitudVacacion(Request $request)
    {

        //die();

        if ($request->ajax()) {

           
            $fechadesde = $request['fechadesde'];

            $fechahasta = $request['fechahasta'];

            $fechadesdecorreo = $request['fechadesde'];

            $fechahastacorreo = $request['fechahasta'];

            $descripcion = trim($request['descripcion']);

            $mediajornada= trim($request['mediajornada']);

            $documento = trim($request['documento']);

            $correosupervisor=trim($request['correosupervisor']);

            DB::beginTransaction();

            try {
                 
                $utimoid = DB::select(DB::raw('select id from incweb where id = (select max(`id`) from incweb )'));

                if($utimoid==null){
                    $idingresar= 1;
                }
                else{

                    $ultimo = trim($utimoid[0]->id);
                    $idingresar= $ultimo + 1;
                    $idingresar= (int)$idingresar;
                }
                
            
                $fechasoli = Carbon::now();
                $fechasoli = $fechasoli->format('Ymd');

                $horasoli = Carbon::now();
                $horasoli = $horasoli->format('H:i:s');

                $usuario_actual = \Auth::User();
                $codpersona = $usuario_actual->co_usuario;

                $buscarsupervisor = DB::table('personal')
                ->select(
                     'personal.psuperv'  
                )
                ->distinct()
                ->where('personal.codperson',$codpersona)
                ->get();

                $supervisor = trim($buscarsupervisor[0]->psuperv);

                //dd($idingresar."  ".$fechasoli."  ".$horasoli."  ".$codpersona."  ".$supervisor);
                //die();

                $estatus="INGRESADO";
                $tiposolicitud="V";
                $codigoconcepto="V";

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudvacacion();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;

                //dd($dispositivoacceso);


                //dd($mediajornada);
                //die();

                if($mediajornada=="NO"){

                    $fechadesde = explode("-",$fechadesde);
                    $fechahasta = explode("-",$fechahasta);

                    $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                    $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

                    $incweb = IncWeb::create([
                    "id"  => $idingresar,
                    "fechasoli"  => $fechasoli,
                    "horasoli"    => $horasoli,
                    "codpersona"    => $codpersona,
                    "codconcepto"        => $codigoconcepto,
                    "fechadesde"     => $fechadesdeformateada,
                    "fechahasta"      => $fechahastaformateada,
                    "descripcio"    => $descripcion,
                    "estatus"       => $estatus,
                    "tiposolicitud"    => $tiposolicitud,
                    "useraprob"  => $supervisor,
                    "mediajornadavac"  => $mediajornada,
                    "creadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                    "documento"    => $documento,
                    ]);
                }
                else{
                    $incweb = IncWeb::create([
                    "id"  => $idingresar,
                    "fechasoli"  => $fechasoli,
                    "horasoli"    => $horasoli,
                    "codpersona"    => $codpersona,
                    "codconcepto"        => $codigoconcepto,
                    "descripcio"    => $descripcion,
                    "estatus"       => $estatus,
                    "tiposolicitud"    => $tiposolicitud,
                    "useraprob"  => $supervisor,
                    "mediajornadavac"  => $mediajornada,
                    "creadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                    "documento"    => $documento,
                    ]);

                }

            
                DB::commit();

                $descripconcepto= self::obtenerdescripcionconceptosolicitudvacacion($codigoconcepto);

                $soligeneradapor= self::obtenernombrepersonasolicitudvacacion();

                $fechadesdecorreo = explode("-",$fechadesdecorreo);
                $fechahastacorreo = explode("-",$fechahastacorreo);

                $fechadesdeformateadacorreo =  $fechadesdecorreo[2]."/".$fechadesdecorreo[1]."/".$fechadesdecorreo[0];
                $fechahastaformateadacorreo =  $fechahastacorreo[2]."/".$fechahastacorreo[1]."/".$fechahastacorreo[0];

                //$correosupervisor= self::obtenercorreosupervisorsolicitudvacacion();

                $respuesta = array(
                    "mensaje"  => "creado",
                    "tipo"  => "vacacion",
                    "id_solicitud" => $idingresar,
                    "fechadesde" => $fechadesdeformateadacorreo,
                    "fechahasta" => $fechahastaformateadacorreo,
                    "soligeneradapor" => $soligeneradapor,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "correosupervisor" => $correosupervisor,
                );

                
                return response()->json($respuesta);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

    public function ObtenerDatosSolicitudVacacion(Request $request, $id)
    {

         $solicitudvacacion = DB::table('incweb')
            ->select(
                    'id',
                    'fechadesde',
                    'fechahasta',
                    'descripcio',
                    'mediajornadavac',
                    'documento'
                    )
            ->where('id', $id)
                    ->whereNull('incweb.deleted_at')
                    ->get();


            return response()->json($solicitudvacacion);
    }

    public function ActualizarSolicitudVacacion(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $fechadesde = $request['fechadesde'];

            $fechahasta = $request['fechahasta'];

            $fechadesdecorreo = $request['fechadesde'];
            
            $fechahastacorreo = $request['fechahasta'];

            $descripcion = trim($request['descripcion']);

            $descripconcepto = "Vacaciones";

            $documento = trim($request['documento']);

            $correosupervisor=trim($request['correosupervisor']);

            $mediajornada= trim($request['mediajornada']);


            DB::beginTransaction();

            try {

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudvacacion();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;

                if($mediajornada=="NO"){

                    $fechadesde = explode("-",$fechadesde);
                    $fechahasta = explode("-",$fechahasta);

                    $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                    $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];
                 
                    $incweb = IncWeb::where('id', $request['id'])
                            ->update([
                                "fechadesde" => $fechadesdeformateada,
                                "fechahasta" => $fechahastaformateada,
                                "descripcio" => $descripcion,
                                "documento" => $documento,
                                "editadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,

                    ]);
                }

                if($mediajornada=="SI"){

                    $incweb = IncWeb::where('id', $request['id'])
                            ->update([
                                "descripcio" => $descripcion,
                                "documento" => $documento,
                                "editadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                    ]);

                }

                DB::commit();

                //$descripconcepto= self::obtenerdescripcionconceptosolicitudvacacion($codigoconcepto);

                $soligeneradapor= self::obtenernombrepersonasolicitudvacacion();

                $fechadesdecorreo = explode("-",$fechadesdecorreo);
                $fechahastacorreo = explode("-",$fechahastacorreo);

                $fechadesdeformateadacorreo =  $fechadesdecorreo[2]."/".$fechadesdecorreo[1]."/".$fechadesdecorreo[0];
                $fechahastaformateadacorreo =  $fechahastacorreo[2]."/".$fechahastacorreo[1]."/".$fechahastacorreo[0];

                //$correosupervisor= self::obtenercorreosupervisorsolicitudvacacion();

                $respuesta = array(
                    "mensaje"  => "actualizado",
                    "tipo"  => "vacacion",
                    "id_solicitud" => $request['id'],
                    "fechadesde" => $fechadesdeformateadacorreo,
                    "fechahasta" => $fechahastaformateadacorreo,
                    "soligeneradapor" => $soligeneradapor,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "correosupervisor" => $correosupervisor,
                );

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "actualizado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

    public function EliminarSolicitudVacacion(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $fecha = trim(Carbon::now());


            DB::beginTransaction();

            try {

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudvacacion();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;

                $correosupervisor=trim($request['correosupervisor']);

                //dd($dispositivoacceso);
                 
                $incweb = IncWeb::where('id', $request['id'])
                        ->update([
                            "estatus" => "ELIMINADO",
                            "eliminadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                            "deleted_at" => $fecha
                ]);

                DB::commit();

                $codigoconcep = DB::table('incweb')
                    ->select('codconcepto')
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $codigoconcepto= $codigoconcep[0]->codconcepto; 

                $descripsoli = DB::table('incweb')
                    ->select('descripcio')
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $descripcion= $descripsoli[0]->descripcio;

                $fechadesdesoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $fechadesde= $fechadesdesoli[0]->fechadesde; 

                $fechahastasoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechahasta'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $fechahasta= $fechahastasoli[0]->fechahasta; 

                $descripconcepto= self::obtenerdescripcionconceptosolicitudvacacion($codigoconcepto);

                $soligeneradapor= self::obtenernombrepersonasolicitudvacacion();

                //$correosupervisor= self::obtenercorreosupervisorsolicitudvacacion();

                $respuesta = array(
                    "mensaje"  => "eliminado",
                    "tipo"  => "vacacion",
                    "id_solicitud" => $request['id'],
                    "fechadesde" => $fechadesde,
                    "fechahasta" => $fechahasta,
                    "soligeneradapor" => $soligeneradapor,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "correosupervisor" => $correosupervisor,
                );

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "eliminado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

    public function ObtenerEstatusSolicitudVacacion(Request $request, $id)
    {

         $estatus = DB::table('incweb')
            ->select(
                    'estatus'
                    )
            ->where('id', $id)
                    ->whereNull('incweb.deleted_at')
                    ->get();


            return response()->json($estatus);
    }

    public function ObtenerEstatusIngresadoSolicitudVacacion(Request $request)
    {
        
        $usuario_actual = \Auth::User();
        $codpersona = $usuario_actual->co_usuario;


        $estatus = DB::table('incweb')
            ->select(
                    'estatus'
                    )
            ->where('estatus',"INGRESADO")
            ->where('codpersona',$codpersona)
            ->where('tiposolicitud',"V")
            ->whereNull('incweb.deleted_at')
            ->count();

        //dd($estatus);
        //die();

        return response()->json($estatus);
    }

    public function ObtenerSolicitudVacacionPendientes(Request $request)
    {
        
        $usuario_actual = \Auth::User();
        $codpersona = $usuario_actual->co_usuario;


        $ingresadas = DB::table('incweb')
            ->select(
                    'estatus'
                    )
            
            ->where('estatus',"INGRESADO")
            ->where('drelacionado',NULL)
            ->where('codpersona',$codpersona)
            ->where('tiposolicitud',"V")
            ->whereNull('incweb.deleted_at')
            ->count();

        $aprobadas = DB::table('incweb')
            ->select(
                    'estatus'
                    )
            
            ->where('estatus',"APROBADO")
            ->where('drelacionado',NULL)
            ->where('codpersona',$codpersona)
            ->where('tiposolicitud',"V")
            ->whereNull('incweb.deleted_at')
            ->count();

        $total= $ingresadas + $aprobadas;

        return response()->json($total);
    }

    public function obtenerDatosDetoperaSolicitudVacacion(Request $request, $id)
    {

        $detalleoperacion = DB::table('incweb')
            ->select(
                    'creadodesde',
                    'editadodesde'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $creadodesde= $detalleoperacion[0]->creadodesde;

        $editadodesde= $detalleoperacion[0]->editadodesde;

        if($creadodesde!=""){

            $cadena = explode('/',$creadodesde);

            //dd($cadena1);
            //die();

         
            $creadodesde=$cadena[0];
            $latitudcreado=$cadena[1];
            $longitudcreado=$cadena[2];

            //dd($creadodesde.$latitud.$longitud);

        }
        else{

            $creadodesde="";
            $latitudcreado="";
            $longitudcreado="";


        }

        if($editadodesde!=""){

            $cadena = explode('/',$editadodesde);

            //dd($cadena1);
            //die();

         
            $editadodesde=$cadena[0];
            $latitudeditado=$cadena[1];
            $longitudeditado=$cadena[2];

            //dd($creadodesde.$latitud.$longitud);

        }
        else{

            $editadodesde="";
            $latitudeditado="";
            $longitudeditado="";


        }

        $respuesta = array(
                    "creadodesde"  => $creadodesde,
                    "latitudcreado" => $latitudcreado,
                    "longitudcreado" => $longitudcreado,
                    "editadodesde"  => $editadodesde,
                    "latitudeditado" => $latitudeditado,
                    "longitudeditado" => $longitudeditado,
        );

        return response()->json($respuesta);

    
    }

    public function obtenerDatosDetaprobarSolicitudVacacion(Request $request, $id)
    {

        $detalleaprobar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fechaaprob= $detalleaprobar[0]->fechaaprob;

        $horaaprob= $detalleaprobar[0]->horaaprob;


        $respuesta = array(
                    "fechaaprob"  => $fechaaprob,
                    "horaaprob" => $horaaprob,
        );

        return response()->json($respuesta);

    
    }

    public function obtenerDatosDetrechazarSolicitudVacacion(Request $request, $id)
    {

        $detallerechazar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fecharechaz= $detallerechazar[0]->fecharechaz;

        $horarechaz= $detallerechazar[0]->horarechaz;


        $respuesta = array(
                    "fecharechaz"  => $fecharechaz,
                    "horarechaz" => $horarechaz,
        );

        return response()->json($respuesta);

    
    }

    public function obtenerDatosDetrevisarSolicitudVacacion(Request $request, $id)
    {

        $detallerevisar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fecharevi , "%d/%m/%Y") as fecharevi'),
                    'horarevi'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fecharevi= $detallerevisar[0]->fecharevi;

        $horarevi= $detallerevisar[0]->horarevi;


        $respuesta = array(
                    "fecharevi"  => $fecharevi,
                    "horarevi" => $horarevi,
        );

        return response()->json($respuesta);

    
    }

    public function CargarDiasPendiSolicitudVacacion(Request $request)
    {

        $usuario_actual = \Auth::User();
        $codpersona = $usuario_actual->co_usuario;

        $cargardiaspendi = DB::table('ppvacdes')
            ->select(
                'ppvacdes.cperiodo',
                'ppvacdes.ndiaspendi')
            ->distinct()
            ->where('codperson',$codpersona)
            ->where('ndiaspendi','>',0)
            ->orderBy('ppvacdes.cperiodo','asc')
            ->get();

        //dd($cargardiaspendi);
        //die();

        return response()->json($cargardiaspendi);

    }

    public function obtenertotaldiaspendientessolicitudvacacion()
    {

        $usuario_actual = \Auth::User();
        $codpersona = $usuario_actual->co_usuario;

        $cargartotaldiaspendi = DB::table('ppvacdes')
            ->select(
                DB::raw('SUM(ndiaspendi) as ndiaspendi')
             )
            ->distinct()
            ->where('codperson',$codpersona)
            ->where('ndiaspendi','>',0)
            ->orderBy('ppvacdes.cperiodo','asc')
            ->get();

        //dd($cargardiaspendi);
        //die();

        return($cargartotaldiaspendi);
    }

    public function obtenercorrelativodocumentosolicitudvacacion()
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fecha = Carbon::now();
        $fecha = $fecha->format('Ymd');

        $hora = Carbon::now();
        $hora = $hora->format('H:i:s');

        $numerodocumento= $co_usuario."-".$fecha.$hora;
        
       

        return($numerodocumento);
    }


    public function ExcelSolicitudVacacion(Request $request)
    {
         if ($request->ajax()) {
            $lista = [];
            $columns = self::arregloColumnasSolicitudVacacion();
            $titulo = 'Solicitud Incidencia Vacacion';
            $buscar = $request['buscar'];
            $tipoBusqueda = $request['tipo'];

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            
                 $dp_fedesde_solicitudvacacion  = $request->input('dp_fedesde_solicitudvacacion');
                 $dp_fehasta_solicitudvacacion  = $request->input('dp_fehasta_solicitudvacacion');

                 $fechadesde = explode("-",$dp_fedesde_solicitudvacacion);
                 $fechahasta = explode("-",$dp_fehasta_solicitudvacacion);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

                 $estatus=$request->input('estatus');


               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::SolicitudVacacionEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$estatus);

            }
            else{
                $consulta = self::SolicitudVacacionGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){

                $lista  = self::buscarEnConsultaSolicitudVacacion($consulta, $buscar, $columns);

            }

            $lista = self::obtenerSolicitudVacacion($consulta);

           
            return response()->json(self::crearExcelSolicitudVacacion($titulo,$lista));


            //return response()->json(self::crearExcel($titulo,$lista));
        }
            

    }



    private function crearExcelSolicitudVacacion($titulo,$lista)
    {
          
       $miExcel = Excel::create('Laravel Excel', function($excel) use ($lista) {

            $excel->sheet('Shetname', function ($sheet) use ($lista){


                // PARA HORIZONTAL
                //$sheet->getPageSetup()->setOrientation("landscape");

                $sheet->getPageSetup()->setOrientation("landscape");

                $sheet->mergeCells("A1:O1");

                $sheet->cell('A1', function($cell) {
                    $cell->setValue('LISTADO SOLICITUDES VACACION');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                //$sheet->getStyle("A2".":G2")->getAlignment()->setWrapText(true); 
               
                $sheet->mergeCells("A2:O2");
                //$sheet->mergeCells("A3:O3");

                $sheet->cell('A3', function($cell) {
                    $cell->setValue('N°');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    //$cell->setFontSize(14);
                    //$cell->setFontFamily('Arial Black');
                    $cell->setAlignment('center');
                });

                $sheet->cell('B3', function($cell) {
                    $cell->setValue('FECHA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('C3', function($cell) {
                    $cell->setValue('HORA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('D3', function($cell) {
                    $cell->setValue('DESDE');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('E3', function($cell) {
                    $cell->setValue('HASTA');
                   $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('F3', function($cell) {
                    $cell->setValue('CONCEPTO');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('G3', function($cell) {
                    $cell->setValue('DESCRIPCION');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('H3', function($cell) {
                    $cell->setValue('APROBADO/RECHAZ POR');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('I3', function($cell) {
                    $cell->setValue('FECHA APROB');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('J3', function($cell) {
                    $cell->setValue('HORA APROB');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('K3', function($cell) {
                    $cell->setValue('FECHA RECHAZ');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('L3', function($cell) {
                    $cell->setValue('HORA RECHAZ');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('M3', function($cell) {
                    $cell->setValue('OBSERVACION');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('N3', function($cell) {
                    $cell->setValue('PROCESADA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('O3', function($cell) {
                    $cell->setValue('ESTATUS');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $i=4;

                foreach ($lista as $datosconsulta) {

                 //dd($d->sucursal);

                  $sheet->cell('A'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $idsolicitud = $datosconsulta->idsolicitud;
                      $cell->SetValue($idsolicitud);

                  });

                  $sheet->cell('B'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechasolicitud = $datosconsulta->fechasolicitud;
                      $cell->SetValue($fechasolicitud);

                  });

                  $sheet->cell('C'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horasolicitud = $datosconsulta->horasolicitud;
                      $cell->SetValue($horasolicitud);

                  });

                  $sheet->cell('D'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechainicio = $datosconsulta->fechainicio;
                      $cell->SetValue($fechainicio);

                  });

                  $sheet->cell('E'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechafin = $datosconsulta->fechafin;
                      $cell->SetValue($fechafin);

                  });

                  $sheet->cell('F'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $concepto = "VACACION";
                      $cell->SetValue($concepto);

                  });

                  $sheet->cell('G'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $descripcion = $datosconsulta->descripcion;
                      $cell->SetValue($descripcion);

                  });

                  $sheet->cell('H'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $aprobadarechazpor = $datosconsulta->aprobadarechazpor;
                      $cell->SetValue($aprobadarechazpor);

                  });

                  $sheet->cell('I'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechaaprob = $datosconsulta->fechaaprob;
                      $cell->SetValue($fechaaprob);

                  });

                   $sheet->cell('J'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horaaprob = $datosconsulta->horaaprob;
                      $cell->SetValue($horaaprob);

                  });

                  $sheet->cell('K'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fecharechaz = $datosconsulta->fecharechaz;
                      $cell->SetValue($fecharechaz);

                  });

                  $sheet->cell('L'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horarechaz = $datosconsulta->horarechaz;
                      $cell->SetValue($horarechaz);

                  });

                  $sheet->cell('M'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $observacion = $datosconsulta->observacion;
                      $cell->SetValue($observacion);

                  });

                   $sheet->cell('N'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $codigoprocesado = $datosconsulta->codigoprocesado;
                      $cell->SetValue($codigoprocesado);

                  });

                  $sheet->cell('O'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $estatus = $datosconsulta->estatus;
                      $cell->SetValue($estatus);

                  });
                
                  $i=$i+1;

                  $finalregistros=$i;

                }

            });
           
        });

        $miExcel = $miExcel->string('xlsx');
        $response = array(
            'name' => $titulo,
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($miExcel)
        );
        return $response;

    }


   public function pdfSolicitudVacacion(Request $request, $tipoBusqueda,$buscar,$dp_fedesde_solicitudvacacion,$dp_fehasta_solicitudvacacion,$estatus,$tipovacacion)
    {

            $lista = [];
            $columns = self::arregloColumnasSolicitudVacacion();

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

                 $fechadesde = explode("-",$dp_fedesde_solicitudvacacion);
                 $fechahasta = explode("-",$dp_fehasta_solicitudvacacion);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::SolicitudVacacionEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$estatus,$tipovacacion);

            }
            else{
                $consulta = self::SolicitudVacacionGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaSolicitudVacacion($consulta,$buscar,$columns);
            }

            $total = self::contarSolicitudVacacion($consulta);

            $registrosporpaginas= 4;
            $contador= 1;
            $contadorpaginas= 1;
            $contadorregistros= 1;
            $estatusbusqueda=$estatus;

            $lista = self::obtenerSolicitudVacacion($consulta);

            //dd($lista);
            //die();

            $pdf = app('FPDF');
          
            $pdf->AliasNbPages();
            $pdf->AddPage('L','A4');

            $fecharegistro = Carbon::now();
            $fecharegistro = $fecharegistro->format('d/m/Y');

            $horaregistro = Carbon::now();
            $horaregistro = $horaregistro->format('H:i:s:A');
            
            //$numeropag=$pdf->PageNo();

            $pdf->Image('img/logoempresa.png',10,4,25);

            $i=0;

            $pdf->SetFont('Arial', 'B', 14);

            $fechaimpresion = utf8_decode($fecharegistro);
            $pdf->SetXY(200,16);
            $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

            $horaimpresion = utf8_decode($horaregistro);
            $pdf->SetXY(200,22);
            $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');


            $pdf->SetXY(150,35);
            $titulo = utf8_decode("LISTADO SOLICITUDES INCIDENCIA VACACION");
            $pdf->Cell(5, 1, $titulo, '', 1, 'C');

            
            if($tipoBusqueda=="especifico") {

                if(($estatusbusqueda!="todos")){

                    $fechadesde = explode("-",$dp_fedesde_solicitudvacacion);
                    $fechahasta = explode("-",$dp_fehasta_solicitudvacacion);

                    $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                    $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                    $pdf->SetXY(110,43);
                    $titulo = utf8_decode("DESDE:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(135,43);
                    $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                    $pdf->SetXY(165,43);
                    $titulo = utf8_decode("HASTA:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(190,43);
                    $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                }

            }    


            $posicioninicial = 45;

            $posicionXdetalle = $posicioninicial + 15;

            $posicioninicial1 = 73;

            $posicionXdetalle1 = $posicioninicial1 + 9;

            $contador=1;

            $pdf->SetXY(15,179);
            $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C'); 

            foreach ($lista as $datosconsulta) {

                if($registrosporpaginas==$contador){

                    //$pdf = app('FPDF');
          
                    $pdf->AddPage('L','A4');

                    $fecharegistro = Carbon::now();
                    $fecharegistro = $fecharegistro->format('d/m/Y');

                    $horaregistro = Carbon::now();
                    $horaregistro = $horaregistro->format('H:i:s:A');
            
                    //$numeropag=$pdf->PageNo();

                    $pdf->Image('img/logoempresa.png',10,4,25);

                    $pdf->SetFont('Arial', 'B', 14);

                    $fechaimpresion = utf8_decode($fecharegistro);
                    $pdf->SetXY(200,16);
                    $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

                    $horaimpresion = utf8_decode($horaregistro);
                    $pdf->SetXY(200,22);
                    $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 14);

                    $pdf->SetXY(150,35);
                    $titulo = utf8_decode("LISTADO SOLICITUDES INCIDENCIA VACACION");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');

                    if($tipoBusqueda=="especifico") {

                        //dd($estatus);
                        //die();

                        if(($estatusbusqueda!="todos")){

                            $fechadesde = explode("-",$dp_fedesde_solicitudvacacion);
                            $fechahasta = explode("-",$dp_fehasta_solicitudvacacion);

                            $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                            $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                            $pdf->SetXY(110,43);
                            $titulo = utf8_decode("DESDE:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(135,43);
                            $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                            $pdf->SetXY(165,43);
                            $titulo = utf8_decode("HASTA:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(190,43);
                            $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                        }

                    }    

                    $posicioninicial = 45;

                    $posicionXdetalle = $posicioninicial + 15;

                    $posicioninicial1 = 73;

                    $posicionXdetalle1 = $posicioninicial1 + 9;

                    $contador=1;

                    $contadorpaginas=  $contadorpaginas + 1;

                    $pdf->SetXY(15,179);
                    $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C');
                }

                //TITULO SOLICITUD
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(15,$posicionXdetalle-5);
                    $titulosoli = utf8_decode("SOLICITUD");
                    $pdf->Cell(10, 1, $titulosoli, '', 1, 'C');
                //FIN TITULO SOLICTUD

                //ENCABEZADO DE LA SOLICITUD TITULO-DETALLE
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(7,$posicionXdetalle);
                    $tituloid = utf8_decode("N°");
                    $pdf->Cell(10, 1, $tituloid, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $idsolicitud = $datosconsulta->idsolicitud;
                    $idsolicitud = utf8_decode($idsolicitud);
                    $pdf->SetXY(9,$posicionXdetalle+5);
                    $pdf->Cell(25, 1, $idsolicitud, '', 1, 'L');
                   
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(25,$posicionXdetalle);
                    $titulofechasoli = utf8_decode("FECHA");
                    $pdf->Cell(10, 1, $titulofechasoli, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechasolicitud = $datosconsulta->fechasolicitud;
                    $fechasolicitud = utf8_decode($fechasolicitud);
                    $pdf->SetXY(18,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechasolicitud, '', 1, 'L');
                   
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(50,$posicionXdetalle);
                    $titulohora = utf8_decode("HORA");
                    $pdf->Cell(10, 1, $titulohora, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $horasolicitud = $datosconsulta->horasolicitud;
                    $horasolicitud = utf8_decode($horasolicitud);
                    $pdf->SetXY(45,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$horasolicitud, '', 1, 'L');
                    
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(70,$posicionXdetalle);
                    $titulodesde = utf8_decode("DESDE");
                    $pdf->Cell(20, 1, $titulodesde, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechainicio = $datosconsulta->fechainicio;
                    $fechainicio = utf8_decode($fechainicio);
                    $pdf->SetXY(70,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechainicio, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(95,$posicionXdetalle);
                    $titulohasta = utf8_decode("HASTA");
                    $pdf->Cell(20, 1, $titulohasta, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechafin = $datosconsulta->fechafin;
                    $fechafin = utf8_decode($fechafin);
                    $pdf->SetXY(95,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechafin, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(130,$posicionXdetalle);
                    $tituloconcepto = utf8_decode("CONCEPTO");
                    $pdf->Cell(20, 1, $tituloconcepto, '', 1, 'C');

                    $mediajornadavac = $datosconsulta->mediajornadavac;

                    if($mediajornadavac=="NO"){
                      $concepto = "Vacaciones";
                    }

                    if($mediajornadavac=="SI"){
                      $concepto = "MediaJornada";
                    }

                    $pdf->SetFont('Arial', '', 12);
                    
                    $concepto = utf8_decode($concepto);
                    $pdf->SetXY(123,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$concepto, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(175,$posicionXdetalle);
                    $titulodescipcion = utf8_decode("DESCRIPCION");
                    $pdf->Cell(20, 1, $titulodescipcion, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $descripcion = $datosconsulta->descripcion;
                    $descripcion =str_replace(' ', '', $descripcion);
                    $descripcion = utf8_decode($descripcion);
                    $pdf->SetXY(169,$posicionXdetalle+3);
                    $pdf->MultiCell(75,4,$descripcion);

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(264,$posicionXdetalle);
                    $tituloestatus = utf8_decode("ESTATUS");
                    $pdf->Cell(20, 1, $tituloestatus, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $estatus = $datosconsulta->estatus;
                    $estatus = utf8_decode($estatus);
                    $pdf->SetXY(262,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$estatus, '', 1, 'L');


                //FIN ENCABEZADO DE LA SOLICITUD TITULO-DETALLE
            
                //TITULO DETALLE APROBACION
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(27,$posicionXdetalle1-5);
                    $estatus = $datosconsulta->estatus;
                    $estatus = utf8_decode($estatus);
                    $titulodetsoli = utf8_decode("DETALLE"." ".$estatus);
                    $pdf->Cell(10, 1, $titulodetsoli, '', 1, 'C');
                //FIN TITULO DETALLE APROBACION

                //DETALLE DE LA APROBACION 
                    
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(21,$posicionXdetalle1);
                    $tituloelaborado = utf8_decode("SUPERVISOR");
                    $pdf->Cell(20, 1, $tituloelaborado, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $aprobadarechazpor = $datosconsulta->aprobadarechazpor;
                    $aprobadarechazpor = utf8_decode($aprobadarechazpor);
                    $pdf->SetXY(12,$posicionXdetalle1+5);
                    $pdf->Cell(25, 1,$aprobadarechazpor, '', 1, 'L');


                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(65,$posicionXdetalle1);
                    $titulofechaaprob = utf8_decode("FECHA");
                    $pdf->Cell(10, 1, $titulofechaaprob, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    if($estatus=="INGRESADO"){
                       $fecha = $datosconsulta->fechasolicitud;
                    }
                    
                    if($estatus=="APROBADO"){
                       $fecha = $datosconsulta->fechaaprob;
                    }
                    if($estatus=="RECHAZADO"){
                       $fecha = $datosconsulta->fecharechaz;
                    }
                    if($estatus=="REVISADO"){
                       $fecha = $datosconsulta->fecharevi;
                    }
                  
                    $fecha = utf8_decode($fecha);
                    $pdf->SetXY(60,$posicionXdetalle1+5);
                    $pdf->Cell(25, 1, $fecha, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(95,$posicionXdetalle1);
                    $titulohoraaprob = utf8_decode("HORA");
                    $pdf->Cell(10, 1, $titulohoraaprob, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    if($estatus=="INGRESADO"){
                       $hora = $datosconsulta->horasolicitud;
                    }
                    if($estatus=="APROBADO"){
                       $hora = $datosconsulta->horaaprob;
                    }
                    if($estatus=="RECHAZADO"){
                       $hora  = $datosconsulta->horarechaz;
                    }
                    if($estatus=="REVISADO"){
                       $hora  = $datosconsulta->horarevi;
                    }
                   
                    $hora = utf8_decode($hora);
                    $pdf->SetXY(91,$posicionXdetalle1+5);
                    $pdf->Cell(25, 1,$hora, '', 1, 'L');

                
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(130,$posicionXdetalle1);
                    $tituloobservacion = utf8_decode("OBSERVACION");
                    $pdf->Cell(10, 1, $tituloobservacion, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $observacion = $datosconsulta->observacion;
                    $observacion =str_replace(' ', '', $observacion);
                    $observacion = utf8_decode($observacion);
                    $pdf->SetXY(119,$posicionXdetalle1+3);
                    $pdf->MultiCell(100,3,$observacion);

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(220,$posicionXdetalle1);
                    $tituloprocesada = utf8_decode("PROCESADA");
                    $pdf->Cell(10, 1, $tituloprocesada, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $codigoprocesado = $datosconsulta->codigoprocesado;
                    $codigoprocesado = utf8_decode($codigoprocesado);
                    $pdf->SetXY(222,$posicionXdetalle1+5);
                    $pdf->Cell(25, 1,$codigoprocesado, '', 1, 'L');

                    //FIN DETALLE DE LA APROBACION

                    //SE INCREMENTA LA POSICION X DEL PRIMER DETALLE
                    //ES DECIR DEL ENCABEZADO
                    $posicionXdetalle = $posicionXdetalle + 42;
                    
                    //SE INCREMENTA LA POSICION X DEL SEGUNDO DETALLE
                    //ES DECIR DEL DETALLE APROBACION
                    $posicionXdetalle1 = $posicionXdetalle1 + 41;

                    $contador=  $contador + 1;

                    $contadorregistros = $contadorregistros + 1;

            }


        $pdf->output();

        //dd($pdf);
        //die();
        exit;


    }

     public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }

    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

    public function obtenerdescripcionconceptosolicitudvacacion($codigoconcepto)
    {
          $descrip = DB::table('gconceptos')
            ->select('gconceptos.desconcepto')
            ->distinct()
            ->where('codconcepto',$codigoconcepto)
            ->get();

        //dd($codigoconcepto);
        //die();

        $descripconcepto= $descrip[0]->desconcepto;

        return($descripconcepto);
    }

    public function obtenercorreosupervisorsolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;
        
        $super = DB::table('personal')
            ->select('psuperv')
            ->distinct()
            ->where('codperson',$co_usuario)
            ->get();

        $supervisor= $super[0]->psuperv;


        $correo = DB::table('personal')
            ->select(
                'personal.mail'        
            )
            ->distinct()
            ->where('codperson',$supervisor)
            ->get();

        //dd($correo);
        //die();

        $contador = $correo->count();

        if($contador>0){
          $correosupervisor= $correo[0]->mail;
        }
        else{
          $correosupervisor= "";
        }

        return($correosupervisor);
    }

    
    public function obtenerdispositivoaccesosolicitudvacacion()
    {

        $tablet_browser = 0;
        $mobile_browser = 0;
        $body_class = 'desktop';
 
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
            $body_class = "tablet";
        }
         
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');
         
        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }
         
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }
        if ($tablet_browser > 0) {
        // Si es tablet has lo que necesites
           $dispositivo= 'Tablet';
           return($dispositivo);
          
        }
        else if ($mobile_browser > 0) {
        // Si es dispositivo mobil has lo que necesites
           $dispositivo= 'Mobil';
           return($dispositivo);
          
        }
        else {
        // Si es ordenador de escritorio has lo que necesites

           $dispositivo= 'Ordenador';
           return($dispositivo);
           
        } 

    } 


}
