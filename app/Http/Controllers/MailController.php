<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Storage;




class MailController extends Controller
{

    
 

    public function EnviarCorreoPlantillaUno(Request $request)
    {
      
        // $rutapapeleta="";
         $adjunto1=public_path()."/"."papeletas"."/"."AMETODOLOGIA DE ESTUDIO AUTODIDACTA ALEMAN.pdf";
         $adjunto2=public_path()."/"."papeletas"."/"."APlanilla Danke Du.docx";
         $adjunto3=public_path()."/"."papeletas"."/"."Asesoría y Costo Danke Du.pdf";

        Mail::send('admin.correo.PlantillaUno',$request->all(), function($msj) use($request,$adjunto1, $adjunto2, $adjunto3){
            // $asunto = $request['asunto'];
            $asunto="ASESORIA DE PROYECTOS: VIVE , ESTUDIA Y TRABAJA EN ALEMANIA";
            $correosupervisor = $request['correosupervisor'];
            //  $correosupervisor = "javier.barreto@hotmail.com";
            $msj->subject($asunto);
            $msj->to($correosupervisor);
             $msj->attach($adjunto1);
            $msj->attach($adjunto2);
             $msj->attach($adjunto3);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }

    public function EnviarCorreoPlantillaDos(Request $request)
    {
      
        // $rutapapeleta="";
         $adjunto1=public_path()."/"."papeletas"."/"."AMETODOLOGIA DE ESTUDIO AUTODIDACTA ALEMAN.pdf";
         $adjunto2=public_path()."/"."papeletas"."/"."APlanilla Danke Du.docx";
         $adjunto3=public_path()."/"."papeletas"."/"."Asesoría y Costo Danke Du.pdf";

        Mail::send('admin.correo.PlantillaDos',$request->all(), function($msj) use($request,$adjunto1, $adjunto2, $adjunto3){
            // $asunto = $request['asunto'];
            $asunto="ASESORIA DE PROYECTOS: VIVE , ESTUDIA Y TRABAJA EN ALEMANIA";
            $correosupervisor = $request['correosupervisor'];
            //  $correosupervisor = "javier.barreto@hotmail.com";
            $msj->subject($asunto);
            $msj->to($correosupervisor);
             $msj->attach($adjunto1);
            $msj->attach($adjunto2);
             $msj->attach($adjunto3);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }

    public function EnviarCorreoPlantillaTres(Request $request)
    {
      
        // $rutapapeleta="";
         $adjunto1=public_path()."/"."papeletas"."/"."AMETODOLOGIA DE ESTUDIO AUTODIDACTA ALEMAN.pdf";
         $adjunto2=public_path()."/"."papeletas"."/"."APlanilla Danke Du.docx";
         $adjunto3=public_path()."/"."papeletas"."/"."Asesoría y Costo Danke Du.pdf";

        Mail::send('admin.correo.PlantillaTres',$request->all(), function($msj) use($request,$adjunto1, $adjunto2, $adjunto3){
            // $asunto = $request['asunto'];
            $asunto="ASESORIA DE PROYECTOS: VIVE , ESTUDIA Y TRABAJA EN ALEMANIA";
            $correosupervisor = $request['correosupervisor'];
            //  $correosupervisor = "javier.barreto@hotmail.com";
            $msj->subject($asunto);
            $msj->to($correosupervisor);
             $msj->attach($adjunto1);
            $msj->attach($adjunto2);
             $msj->attach($adjunto3);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }


   
    public function EnviarCorreoSolicitudCreada(Request $request)
    {
        // $rutapapeleta="";
         $rutapapeleta=public_path()."/"."papeletas"."/"."papeleta00030663-2.pdf";

        Mail::send('admin.correo.solicitudcreada',$request->all(), function($msj) use($request,$rutapapeleta){
            $asunto = $request['asunto'];
            $correosupervisor = $request['correosupervisor'];
            //  $correosupervisor = "javier.barreto@hotmail.com";
            $msj->subject($asunto);
            $msj->to($correosupervisor);
             $msj->attach($rutapapeleta);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }

    
    public function EnviarCorreoSolicitudEditada(Request $request)
    {
        
        Mail::send('admin.correo.solicitudeditada',$request->all(), function($msj) use($request){
            $asunto = $request['asunto'];
            $correosupervisor = $request['correosupervisor'];
            $msj->subject($asunto);
            $msj->to($correosupervisor);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }

    
    public function EnviarCorreoSolicitudEliminada(Request $request)
    {
        
        Mail::send('admin.correo.solicitudeliminada',$request->all(), function($msj) use($request){
            $asunto = $request['asunto'];
            $correosupervisor = $request['correosupervisor'];
            $msj->subject($asunto);
            $msj->to($correosupervisor);
        });

        return response()->json([
            "msj" => "creado"
        ]);
       
    }

    
    public function EnviarCorreoSolicitudAprobada(Request $request)
    {

        $idsolicitud= trim($request['numerosolicitud']);
        $idpapeleta= trim($request['idpapeleta']);
        $usuario_actual = \Auth::User();

        $cousuario = $usuario_actual->co_usuario;

 $rutapapeleta=public_path()."/"."papeletas"."/"."papeleta".$cousuario."-".$idsolicitud.".pdf";



        //$rutacontactenos= public_path()."/"."img"."/"."contactenos.png";
       
        Mail::send('admin.correo.solicitudaprobada',$request->all(), function($msj) use($request,$rutapapeleta){
            $asunto = $request['asunto'];
            $correosolicitante = $request['correosolicitante'];
            $correorecursoshumanos = $request['correorecursoshumanos'];
            $msj->subject($asunto);
            $msj->to($correosolicitante);
            $msj->attach($rutapapeleta);

            /*if($correorecursoshumanos!=""){
               $msj->Cc($correorecursoshumanos);
            }*/
        });

        Mail::send('admin.correo.solicitudaprobadarhumanos',$request->all(), function($msj) use($request,$rutapapeleta){
            $asunto = $request['asunto'];
            $correorecursoshumanos = $request['correorecursoshumanos'];
            $msj->subject($asunto);
            if($correorecursoshumanos!=""){
               $msj->to($correorecursoshumanos);
            }
            $msj->attach($rutapapeleta);
            
        }); 

        Storage::disk('rutapapeletas')->delete("papeleta".$cousuario."-".$idsolicitud.".pdf");

        $respuesta = array(
            "msj" => "creado",
            "idsolicitud" => $idsolicitud,
            "idpapeleta" => $idpapeleta,  
        );

        return response()->json($respuesta);

        /*return response()->json([
            "msj" => "creado"
        ]);*/
    }


   

     public function EnviarCorreoSolicitudRechazada(Request $request)
    {
        
        Mail::send('admin.correo.solicitudrechazada',$request->all(), function($msj) use($request){
            $asunto = $request['asunto'];
            $correosolicitante = $request['correosolicitante'];
            $msj->subject($asunto);
            $msj->to($correosolicitante);
        });

        return response()->json([
            "msj" => "creado"
        ]);
    }

    
    public function EnviarCorreoSolicitudRevisada(Request $request)
    {
        
        Mail::send('admin.correo.solicitudrevisada',$request->all(), function($msj) use($request){
            $asunto = $request['asunto'];
            $correosolicitante = $request['correosolicitante'];
            $msj->subject($asunto);
            $msj->to($correosolicitante);
        });

        return response()->json([
            "msj" => "creado"
        ]);
    }

    

    public function EnviarCorreoAsistencia(Request $request)
    {
        
        Mail::send('admin.correo.registroasistencia',$request->all(), function($msj) use($request){
            $asunto = $request['asunto'];
            $correoempleado = $request['correoempleado'];
            // $correoregistrador=$request['correoregistrador'];
            $msj->subject($asunto);
            $msj->to($correoempleado);
            //if($correoregistrador!=""){
               //$msj->Cc($correoregistrador);
            //}
            
        });

        $respuesta = array(
                "msj" => "creado",
                "dni" => $request['dni'],
                "nombreempleado" => $request['nombreempleado'],
                "fecha" => $request['fechaasistencia'],
                "hora" => $request['horaasistencia'],
                "registradodesde" => $request['creadodesde'],
        );

        return response()->json($respuesta);
       
    }


    

}
