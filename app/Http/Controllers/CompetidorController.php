<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Competidor;
use App\Correos;


class CompetidorController extends Controller
{

    public function competidor(Request $request)
    {

     
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.competidor.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarCompetidor(Request $request)
    {
        $competidor = Competidor::all();
        return response()->json($competidor);
    }


    public function CrearCompetidor(Request $request)
    {

        

        if ($request->ajax()) {

            $competidor = $request['competidor'];
            $peso = $request['peso'];
            $casa = trim($request['casa']);

            $correo = Competidor::create([
                    "nb_competidor"  => $competidor,
                    "peso"  => $peso,
                    "id_casa"  => $casa,
            ]);

            $respuesta = array(
                "mensaje"  => "creado"
            );

            return response()->json($respuesta);

        }

    }

    public function editarCompetidor(Request $request)
    {
        if ($request->ajax()) {

            $id = $request['id'];
            $competidor = trim($request['competidor']);
            $peso = $request['peso'];
            $casa = $request['casa'];


            $casa = Competidor::where('id_competidor', $id)->update([
                "nb_competidor"  => $competidor,
                "peso"  => $peso,
                "id_casa"  => $casa,
            ]);

            return response()->json([
                "mensaje" => "actualizado"
            ]);
        }
    }

    public function eliminarCompetidor(Request $request)
    {
        if ($request->ajax()) {

            $date = trim(Carbon::now());

            try { 
                $id = $request['id'];

                $competidor = Competidor::where('id_competidor', $id)->delete();

                return response()->json([
                    "mensaje" => "eliminado"
                ]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }
    }


    public function obtenerCompetidor(Request $request,$id)
    {
        if ($request->ajax()) {

            $competidor = DB::table('competidor')
            ->select(
                    'id_competidor',
                    'nb_competidor',
                    'peso',
                    'id_casa'
                    )
            ->where('id_competidor', $id)
            ->get();

            return response()->json($competidor);
        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
