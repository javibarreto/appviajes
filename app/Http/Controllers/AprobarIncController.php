<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PDF;
use App\IncWeb;
use App\PapeletasIncWeb;




class AprobarIncController extends Controller
{


    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";


 
    public function arregloColumnasAprobarInc()
    {
        return array(
            0 =>  'id',
            1 =>  'fechasoli',
            2 =>  'horasoli',
            3 =>  'fechadesde',
            4 =>  'fechahasta',
            5 =>  'gconceptos.desconcepto',
            6 =>  'descripcio',
            7 =>  'useraprob',
            8 =>  'observacion',
            9 =>  'drelacionado',
            10 => 'estatus',
            11 => 'documento',
            12 => 'personal.appaterno',
            13 => 'personal.apmaterno',
           
           
        );
    }

    
    public function contarAprobarInc($consulta)
    {
        return $consulta->count();
    }

    
    public function obtenerAprobarInc($consulta)
    {
        return $consulta->get();
    }

   
    private function limitarConsultaAprobarInc($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {
            $consulta->offset($inicio)
                ->limit($limite);
        }

        return $consulta;
    }

    
    private function ordenarConsultaAprobarInc($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

   
    private function buscarEnConsultaAprobarInc($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }

   
    

    public function AprobarInc(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonaaprobarinc();

        $nombrepersonaaccesa= self::obtenernombrepersonaaprobarinc();

        return view('admin.aprobarincidencia.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    
   
    private function AprobarIncEspecifica($buscar,$fe_desde,$fe_hasta,$codigoaprobarinc,$estatus)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        if ($estatus == 'todos') {
            $estatus = '%';
        }

        if ($estatus == '') {
            $estatus = 'fecha';
        }  

        if ($codigoaprobarinc == 'todos') {
            $codigoaprobarinc ='%';
        }

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta.$estatus);
        //die();

        if (($codigoaprobarinc=='%')&&($estatus=='%')){


            $consulta = DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                ->join('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'desconcepto as concepto',
                    'descripcio as descripcion',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS elaboradopor'),
                    'nomperson as elaboradopor',
                    DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                    
                )
                ->distinct()
                ->orderBy('id','asc')
                ->where('useraprob',$co_usuario)
                ->whereNull('incweb.deleted_at');
        }

        else{

            if ($estatus=='%'){

                $consulta = DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                ->join('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'desconcepto as concepto',
                    'descripcio as descripcion',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS elaboradopor'),
                    'nomperson as elaboradopor',
                    DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                    
                )
                ->distinct()
                ->orderBy('id','asc')
                ->where('useraprob',$co_usuario)
                ->whereNull('incweb.deleted_at');

                if ($codigoaprobarinc!=""){
                    //dd("pase por aqui".$codigoincdia);
                    $consulta->where('incweb.codconcepto',$codigoaprobarinc);
                }

                if (($estatus!='%')&&($estatus!='fecha')){
                    //dd("pase por aqui".$estatus);
                    $consulta->where('estatus',$estatus);
                }
            }
            else{

                $consulta = DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                ->join('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'desconcepto as concepto',
                    'descripcio as descripcion',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS elaboradopor'),
                    'nomperson as elaboradopor',
                    DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                    
                )
                ->distinct()
                ->whereBetween('fechasoli',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc')
                ->where('useraprob',$co_usuario)
                ->whereNull('incweb.deleted_at');

                if ($codigoaprobarinc!='%'){
                    //dd("pase por aqui".$codigoaprobarinc);
                    $consulta->where('incweb.codconcepto',$codigoaprobarinc);
                }

                if (($estatus!='%')&&($estatus!='fecha')){
                    //dd("pase por aqui".$estatus);
                    $consulta->where('estatus',$estatus);
                }


            }     

        }
     
        //dd($consulta->tosql());
       
        //$listasolicitudincdia = self::obtenerAprobarInc($consulta);
        //dd($listasolicitudincdia);
        //die();
        return $consulta;

    }

  

    private function AprobarIncGeneral()
    {

        //$fecha = Carbon::now();
        //$fecha = $fecha->format('Ymd');
        //dd($fecha);

        $fecha_actual = Carbon::now();

        //LA FECHA DESDE Y LA FECHA HASTA SE ARMA DE ESTA MANERA PARA QUE MUESTREN 
        //LA INFORMACION DE TODO EL MES

        $monthfinal = $fecha_actual->format('m');
        $yearfinal  = $fecha_actual->format('Y');

        if ($monthfinal == 01) {
            $monthdesde = 12;
            $yeardesde  = $yearfinal;

        } else {
            $monthdesde = $monthfinal;
            $yeardesde  = $yearfinal;
        }

        $daydesde = "01";
        $fe_desde = $yeardesde . '-' . $monthdesde . '-' . $daydesde;

        $dayfinal = "31";
        $fe_hasta = $yearfinal . '-' . $monthfinal . '-' . $dayfinal;

        $fechadesde = explode("-",$fe_desde);
        $fechahasta = explode("-",$fe_hasta);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        return DB::table('incweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                ->join('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                ->select(
                    'id as idsolicitud',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechainicio'),
                    DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechafin'),
                    'desconcepto as concepto',
                    'descripcio as descripcion',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS elaboradopor'),
                    'nomperson as elaboradopor',
                    DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob as horaaprob',
                    DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz as horarechaz',
                    'observacion as observacion',
                    'drelacionado  as codigoprocesado',
                    'estatus as estatus',
                    'documento as documento',
                     DB::raw('DATE_FORMAT(fecharevi, "%d/%m/%Y") as fecharevi'),
                    'horarevi as horarevi',
                    'mediajornadavac as mediajornadavac'
                )
                ->distinct()
                //->whereBetween('fechasoli',[$fechadesdeformateada,$fechahastaformateada])
                ->where('estatus','INGRESADO')
                ->where('useraprob',$co_usuario)
                ->whereNull('incweb.deleted_at')
                ->orderBy('id', 'asc');

    }

   
     private function BuscarEnConsultaFiltroAprobarInc($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
  
    public function ListarAprobarInc(Request $request)
    {

        $listaaprobarincidencia = [];
        $columns = self::arregloColumnasAprobarInc();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico


        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_aprobarincidencia  = $request->input('dp_fedesde_aprobarincidencia');
            $dp_fehasta_aprobarincidencia  = $request->input('dp_fehasta_aprobarincidencia');

            //dd("pase por aqui".$dp_fedesde_aprobarincidencia.$dp_fehasta_aprobarincidencia);
            //die();


            $fechadesde = explode("-",$dp_fedesde_aprobarincidencia);
            $fechahasta = explode("-",$dp_fehasta_aprobarincidencia);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

            $estatus=$request->input('estatus');

            $codigoaprobarinc=$request->input('codigoaprobarinc');

            //dd($estatus);
            //die();


            $consulta = self::AprobarIncEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$codigoaprobarinc,$estatus);

        }else{


            $consulta = self::AprobarIncGeneral();
          
        }

        $consulta  = self::buscarEnConsultaAprobarInc($consulta, $buscar, $columns);

        $total = self::contarAprobarInc($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaAprobarInc($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listaaprobarincidencia = self::obtenerAprobarInc($consulta);

        //dd($listamarcaasistencia);
        //die();

       
        //dd($totaltardanza);
        //die();

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listaaprobarincidencia,

        );

     

        return response()->json($json_data);
    
    }


    
    public function ObtenerEstatusSolicitudAprobar(Request $request, $id)
    {

         $estatus = DB::table('incweb')
            ->select(
                    'estatus',
                    'documento'
                    )
            ->where('id', $id)
                    ->whereNull('incweb.deleted_at')
                    ->get();


            return response()->json($estatus);
    }

   

    public function ObtenerDatosSolicitudAprobar(Request $request, $id)
    {

         $solicitud = DB::table('incweb')
            ->join('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
            ->select(
                    'id',
                    'gconceptos.codconcepto',
                    'gconceptos.desconcepto',
                    'descripcio',
                    'documento',
                    'estatus',
                    'documento' 
                    )
            ->where('id', $id)
                    ->whereNull('incweb.deleted_at')
                    ->get();
            return response()->json($solicitud);
    }

   

    public function AprobarSolicitud(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $observacion = trim($request['observacion']);

            $documento = trim($request['documento']);

            $correosolicitante=trim($request['correosolicitante']);

            $estatus="APROBADO";

            $fechaaprob = Carbon::now();
            $fechaaprob = $fechaaprob->format('Ymd');

            $horaaprob = Carbon::now();
            $horaaprob = $horaaprob->format('H:i:s');


            DB::beginTransaction();

            try {

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudaprobar();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;
                 
                $incweb = IncWeb::where('id', $request['id'])
                        ->update([
                            "observacion" => $observacion,
                            "estatus" => $estatus,
                            "fechaaprob" => $fechaaprob,
                            "horaaprob" => $horaaprob,
                            "aprobadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                            "documento" => $documento,

                ]);

                

                $utimopapeleta = DB::select(DB::raw('select id from papeletasincweb where id = (select max(`id`) from papeletasincweb)')); 
                
                if($utimopapeleta==null){
                   $idingresarpapeleta= 1;
                }
                else{
                   $utimopapeleta = trim($utimopapeleta[0]->id);
                   $idingresarpapeleta= $utimopapeleta + 1;
                   $idingresarpapeleta= (int)$idingresarpapeleta;
                }      

                $papeletasincweb = PapeletasIncWeb::create([
                "id"  => $idingresarpapeleta,
                "idincweb"  =>$request['id'],
                "fecha"  => $fechaaprob,
                "hora"    => $horaaprob,
                ]);

                $idsolicitud=$request['id'];
                $idpapeleta=$idingresarpapeleta;


                $papeleta= self::guardarpapeletapdf($idsolicitud,$idpapeleta); 


                DB::commit();

                //$correosolicitante= self::obtenercorreosolicitanteaprobar($request['id']);
                $descripcion=self::obtenerdescripcionsolicitudaprobar($request['id']);
                $descripconcepto=self::obtenerdescripcionconceptoaprobar($request['id']);
                
                $aprobadopor=self::obtenernombrepersonaaprobarinc();

                $solicitante=self::obtenernombresolicitanteaprobarinc($request['id']);

                $correorecursoshumanos=self::obtenercorreorecursoshumanosaprobarinc(); 

                $fechadesde="";
                $fechahasta="";

                $tiposoli = DB::table('incweb')
                    ->select('tiposolicitud')
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $tiposolicitud= $tiposoli[0]->tiposolicitud; 

                if(($tiposolicitud=="D")||($tiposolicitud=="V")){

                    $fechadesdesoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                    $fechadesde= $fechadesdesoli[0]->fechadesde; 

                    $fechahastasoli = DB::table('incweb')
                        ->select(
                          DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechahasta'))
                        ->distinct()
                        ->where('id',$request['id'])
                        ->get();

                    $fechahasta= $fechahastasoli[0]->fechahasta;

                }

                if($tiposolicitud=="H"){

                    $fechadesdesoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                    $fechadesde= $fechadesdesoli[0]->fechadesde; 

                }    

                $respuesta = array(
                    "mensaje"  => "aprobado",
                    "tipo"  => $tiposolicitud,
                    "id_solicitud" => $request['id'],
                    "fechadesde" => $fechadesde,
                    "fechahasta" => $fechahasta,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "aprobadopor" => $aprobadopor,
                    "observacion" => $observacion,
                    "correosolicitante" => $correosolicitante,
                    "correorecursoshumanos" =>$correorecursoshumanos,
                    "solicitante" => $solicitante,
                    "idpapeleta" => $idpapeleta,
                );

                //dd($respuesta);
                //die();

                return response()->json($respuesta);
              
                //return response()->json(["mensaje" => "aprobado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

   
    public function guardarpapeletapdf($idsolicitud,$idpapeleta)
    {

        //dd($idsolicitud.$idpapeleta);
        //die();

        $datossolicitud = DB::table('incweb')
                    ->select(
                        'tiposolicitud',
                        'descripcio',
                        //DB::raw('DATE_FORMAT(fechasoli,"%d/%m/%Y")'),
                        DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasoli'),
                        'horasoli',
                        'codpersona',
                        'useraprob',
                        DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                        'horaaprob',
                        'observacion',
                        'documento',
                        'codconcepto'
                    )
                    ->distinct()
                    ->where('id',$idsolicitud)
                    ->get();

        $tiposolicitud= $datossolicitud[0]->tiposolicitud;
        $codigoconcepto= $datossolicitud[0]->codconcepto;
        $descripsolicitud= $datossolicitud[0]->descripcio;
        $fechasolicitud= $datossolicitud[0]->fechasoli;
        $horasolicitud= $datossolicitud[0]->horasoli;
        $codsolicitante= $datossolicitud[0]->codpersona;
        $codaprobador= $datossolicitud[0]->useraprob;
        $fechaaprobado= $datossolicitud[0]->fechaaprob;
        $horaaprobado= $datossolicitud[0]->horaaprob;
        $observacionaprobado= $datossolicitud[0]->observacion;
        $documentoaprobado= $datossolicitud[0]->documento;

        $datossolicitante = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codsolicitante)
                    ->get();

        $datossolicitante= $datossolicitante[0]->nomperson;

        $datossupervisor = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codaprobador)
                    ->get();

        $datossupervisor= $datossupervisor[0]->nomperson;
        
        $datosconcepto = DB::table('gconceptos')
                    ->select(
                        'desconcepto'
                    )
                    ->distinct()
                    ->where('codconcepto',$codigoconcepto)
                    ->get();

        $datosconcepto= $datosconcepto[0]->desconcepto;


        $pdf = app('FPDF');
        $pdf->AliasNbPages();
        $pdf->AddPage('P','A4');

        $fecharegistro = Carbon::now();
        $fecharegistro = $fecharegistro->format('d/m/Y');

        $horaregistro = Carbon::now();
        $horaregistro = $horaregistro->format('H:i:s:A');
            
        //$numeropag=$pdf->PageNo();

        $pdf->Image('img/logoempresa.png',10,4,25);

        $pdf->SetFont('Arial', 'B', 14);

        $fechaimpresion = utf8_decode($fecharegistro);
        $pdf->SetXY(135,16);
        $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

        $horaimpresion = utf8_decode($horaregistro);
        $pdf->SetXY(135,22);
        $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

        $pdf->SetXY(100,35);
        $titulo = utf8_decode("PAPELETA"."   "."N°"." ".$idpapeleta);
        $pdf->Cell(5, 1, $titulo, '', 1, 'C');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,45);
        $titulodatosdocumento = utf8_decode("DOCUMENTO N°:"." ");
        $pdf->Cell(25, 1, $titulodatosdocumento, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,45);
        $pdf->Cell(25, 1, utf8_decode($documentoaprobado), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,55);
        $titulodatossolicitante = utf8_decode("SOLICITADA POR:"." ");
        $pdf->Cell(25, 1, $titulodatossolicitante, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,55);
        $pdf->Cell(25, 1, utf8_decode($datossolicitante), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,65);
        $titulofechasolicitud = utf8_decode("FECHA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulofechasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(57,65);
        $pdf->Cell(25, 1, $fechasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,75);
        $titulohorasolicitud = utf8_decode("HORA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulohorasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,75);
        $pdf->Cell(25, 1, $horasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,85);
        $titulotiposolicitud = utf8_decode("TIPO SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulotiposolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,85);
        //if($tiposolicitud=="V"){
          //  $tiposolicitud="VACACION";
        //}
        //if($tiposolicitud=="D"){
          //  $tiposolicitud="DIA";
        //}
        //if($tiposolicitud=="H"){
          //  $tiposolicitud="HORA";
        //}
        //$pdf->Cell(25, 1, $tiposolicitud, '', 1, 'L');
        $pdf->Cell(25, 1, $datosconcepto, '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,95);
        $titulodescripcionsolicitud = utf8_decode("DESCRIPCION SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulodescripcionsolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(75,95);
        $pdf->Cell(25, 1, utf8_decode($descripsolicitud), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,105);
        $tituloaprobadopor = utf8_decode("APROBADO POR:"." ");
        $pdf->Cell(25, 1, $tituloaprobadopor, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,105);
        $pdf->Cell(25, 1, utf8_decode($datossupervisor), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,115);
        $titulofechaaprobado = utf8_decode("FECHA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulofechaaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(58,115);
        $pdf->Cell(25, 1, $fechaaprobado, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,125);
        $titulohoraaprobado = utf8_decode("HORA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulohoraaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(56,125);
        $pdf->Cell(25, 1, $horaaprobado, '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,135);
        $tituloobservacion = utf8_decode("OBSERVACIONES APROBACION:"." ");
        $pdf->Cell(25, 1, $tituloobservacion, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(90,135);
        $pdf->Cell(25, 1, utf8_decode($observacionaprobado), '', 1, 'L');
        

        
        $usuario_actual = \Auth::User();

        $cousuario = $usuario_actual->co_usuario;

        //$ruta=public_path('/papeletas/'."papeleta".$cousuario."-".$idsolicitud);
        
        $path = base_path().'/../public_html/version3/papeletas/';
        $ruta= $path."papeleta".$cousuario."-".$idsolicitud;

        $pdf->Output($ruta.".pdf","F"); 

    }   

   
    public function ImprimirPapeleta($idsolicitud,$idpapeleta)
    {

        //dd($idsolicitud.$idpapeleta);
        //die();

        $datossolicitud = DB::table('incweb')
                    ->select(
                        'tiposolicitud',
                        'descripcio',
                        //DB::raw('DATE_FORMAT(fechasoli,"%d/%m/%Y")'),
                        DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasoli'),
                        'horasoli',
                        'codpersona',
                        'useraprob',
                        DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                        'horaaprob',
                        'observacion',
                        'documento',
                        'codconcepto'
                    )
                    ->distinct()
                    ->where('id',$idsolicitud)
                    ->get();

        $tiposolicitud= $datossolicitud[0]->tiposolicitud;
        $codigoconcepto= $datossolicitud[0]->codconcepto;
        $descripsolicitud= $datossolicitud[0]->descripcio;
        $fechasolicitud= $datossolicitud[0]->fechasoli;
        $horasolicitud= $datossolicitud[0]->horasoli;
        $codsolicitante= $datossolicitud[0]->codpersona;
        $codaprobador= $datossolicitud[0]->useraprob;
        $fechaaprobado= $datossolicitud[0]->fechaaprob;
        $horaaprobado= $datossolicitud[0]->horaaprob;
        $observacionaprobado= $datossolicitud[0]->observacion;
        $documentoaprobado= $datossolicitud[0]->documento;

        $datossolicitante = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codsolicitante)
                    ->get();

        $datossolicitante= $datossolicitante[0]->nomperson;

        $datossupervisor = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codaprobador)
                    ->get();

        $datossupervisor= $datossupervisor[0]->nomperson;
        
        $datosconcepto = DB::table('gconceptos')
                    ->select(
                        'desconcepto'
                    )
                    ->distinct()
                    ->where('codconcepto',$codigoconcepto)
                    ->get();

        $datosconcepto= $datosconcepto[0]->desconcepto;


        $pdf = app('FPDF');
        $pdf->AliasNbPages();
        $pdf->AddPage('P','A4');

        $fecharegistro = Carbon::now();
        $fecharegistro = $fecharegistro->format('d/m/Y');

        $horaregistro = Carbon::now();
        $horaregistro = $horaregistro->format('H:i:s:A');
            
        //$numeropag=$pdf->PageNo();

        $pdf->Image('img/logoempresa.png',10,4,25);

        $pdf->SetFont('Arial', 'B', 14);

        $fechaimpresion = utf8_decode($fecharegistro);
        $pdf->SetXY(135,16);
        $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

        $horaimpresion = utf8_decode($horaregistro);
        $pdf->SetXY(135,22);
        $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

        $pdf->SetXY(100,35);
        $titulo = utf8_decode("PAPELETA"."   "."N°"." ".$idpapeleta);
        $pdf->Cell(5, 1, $titulo, '', 1, 'C');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,45);
        $titulodatosdocumento = utf8_decode("DOCUMENTO N°:"." ");
        $pdf->Cell(25, 1, $titulodatosdocumento, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,45);
        $pdf->Cell(25, 1, utf8_decode($documentoaprobado), '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,55);
        $titulodatossolicitante = utf8_decode("SOLICITADA POR:"." ");
        $pdf->Cell(25, 1, $titulodatossolicitante, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,55);
        $pdf->Cell(25, 1, utf8_decode($datossolicitante), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,65);
        $titulofechasolicitud = utf8_decode("FECHA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulofechasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(57,65);
        $pdf->Cell(25, 1, $fechasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,75);
        $titulohorasolicitud = utf8_decode("HORA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulohorasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,75);
        $pdf->Cell(25, 1, $horasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,85);
        $titulotiposolicitud = utf8_decode("TIPO SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulotiposolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,85);
        //if($tiposolicitud=="V"){
        //    $tiposolicitud="VACACION";
        //}
        //if($tiposolicitud=="D"){
        //    $tiposolicitud="DIA";
        //}
        //if($tiposolicitud=="H"){
            //$tiposolicitud="HORA";
        //}
        //$pdf->Cell(25, 1, $tiposolicitud, '', 1, 'L');
        $pdf->Cell(25, 1, $datosconcepto, '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,95);
        $titulodescripcionsolicitud = utf8_decode("DESCRIPCION SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulodescripcionsolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(75,95);
        $pdf->Cell(25, 1, utf8_decode($descripsolicitud), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,105);
        $tituloaprobadopor = utf8_decode("APROBADO POR:"." ");
        $pdf->Cell(25, 1, $tituloaprobadopor, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,105);
        $pdf->Cell(25, 1, utf8_decode($datossupervisor), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,115);
        $titulofechaaprobado = utf8_decode("FECHA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulofechaaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(58,115);
        $pdf->Cell(25, 1, $fechaaprobado, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,125);
        $titulohoraaprobado = utf8_decode("HORA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulohoraaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(56,125);
        $pdf->Cell(25, 1, $horaaprobado, '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,135);
        $tituloobservacion = utf8_decode("OBSERVACIONES APROBACION:"." ");
        $pdf->Cell(25, 1, $tituloobservacion, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(90,135);
        $pdf->Cell(25, 1, utf8_decode($observacionaprobado), '', 1, 'L');
        

        
        $pdf->output();

        //dd($pdf);
        //die();
        exit;

    }    
 

    
     public function RechazarSolicitud(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $observacion = trim($request['observacion']);

            $correosolicitante=trim($request['correosolicitante']);

            $estatus="RECHAZADO";

            $fecharecha = Carbon::now();
            $fecharecha = $fecharecha->format('Ymd');

            $horarecha = Carbon::now();
            $horarecha = $horarecha->format('H:i:s');


            DB::beginTransaction();

            try {

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudaprobar();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;
                 
                $incweb = IncWeb::where('id', $request['id'])
                        ->update([
                            "observacion" => $observacion,
                            "estatus" => $estatus,
                            "fecharechaz" => $fecharecha,
                            "horarechaz" => $horarecha,
                            "rechazadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                ]);

            DB::commit();

            //$correosolicitante= self::obtenercorreosolicitanterechazar($request['id']);
            $descripcion=self::obtenerdescripcionsolicitudrechazar($request['id']);
            $descripconcepto=self::obtenerdescripcionconceptorechazar($request['id']);
                
            $rechazadopor=self::obtenernombrepersonarechazarinc();

            $fechadesde="";
            $fechahasta="";

            $tiposoli = DB::table('incweb')
                ->select('tiposolicitud')
                ->distinct()
                ->where('id',$request['id'])
                ->get();

            $tiposolicitud= $tiposoli[0]->tiposolicitud; 

            if(($tiposolicitud=="D")||($tiposolicitud=="V")){

                $fechadesdesoli = DB::table('incweb')
                ->select(
                  DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                ->distinct()
                ->where('id',$request['id'])
                ->get();

                $fechadesde= $fechadesdesoli[0]->fechadesde; 

                $fechahastasoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechahasta'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $fechahasta= $fechahastasoli[0]->fechahasta;

            }

            if($tiposolicitud=="H"){

                $fechadesdesoli = DB::table('incweb')
                ->select(
                  DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                ->distinct()
                ->where('id',$request['id'])
                ->get();

                $fechadesde= $fechadesdesoli[0]->fechadesde; 

            }    

            $respuesta = array(
                    "mensaje"  => "rechazado",
                    "tipo"  => $tiposolicitud,
                    "id_solicitud" => $request['id'],
                    "fechadesde" => $fechadesde,
                    "fechahasta" => $fechahasta,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "rechazadopor" => $rechazadopor,
                    "observacion" => $observacion,
                    "correosolicitante" => $correosolicitante,
            );

            return response()->json($respuesta);

            //return response()->json(["mensaje" => "rechazado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

  
    public function RevisarSolicitud(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $observacion = trim($request['observacion']);

            $correosolicitante=trim($request['correosolicitante']);

            $estatus="REVISADO";

            $fecharevi = Carbon::now();
            $fecharevi = $fecharevi->format('Ymd');

            $horarevi = Carbon::now();
            $horarevi = $horarevi->format('H:i:s');


            DB::beginTransaction();

            try {

                $dispositivoacceso= self::obtenerdispositivoaccesosolicitudaprobar();

                $ubicacion_usuario = \Auth::User();

                $latitudubicusua = $ubicacion_usuario->latitud;
                $longitudubiusua = $ubicacion_usuario->longitud;
                 
                $incweb = IncWeb::where('id', $request['id'])
                        ->update([
                            "observacion" => $observacion,
                            "estatus" => $estatus,
                            "fecharevi" => $fecharevi,
                            "horarevi" => $horarevi,
                            "revisadodesde"  => $dispositivoacceso."/".$latitudubicusua."/".$longitudubiusua,
                ]);

                DB::commit();

                //$correosolicitante= self::obtenercorreosolicitanterevisar($request['id']);
                $descripcion=self::obtenerdescripcionsolicitudrevisar($request['id']);
                $descripconcepto=self::obtenerdescripcionconceptorevisar($request['id']);
                
                $aprobadopor=self::obtenernombrepersonarevisarinc();

                $fechadesde="";
                $fechahasta="";

                $tiposoli = DB::table('incweb')
                    ->select('tiposolicitud')
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                $tiposolicitud= $tiposoli[0]->tiposolicitud; 

                if(($tiposolicitud=="D")||($tiposolicitud=="V")){

                    $fechadesdesoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                    $fechadesde= $fechadesdesoli[0]->fechadesde; 

                    $fechahastasoli = DB::table('incweb')
                        ->select(
                          DB::raw('DATE_FORMAT(fechahasta, "%d/%m/%Y") as fechahasta'))
                        ->distinct()
                        ->where('id',$request['id'])
                        ->get();

                    $fechahasta= $fechahastasoli[0]->fechahasta;

                }

                if($tiposolicitud=="H"){

                    $fechadesdesoli = DB::table('incweb')
                    ->select(
                      DB::raw('DATE_FORMAT(fechadesde, "%d/%m/%Y") as fechadesde'))
                    ->distinct()
                    ->where('id',$request['id'])
                    ->get();

                    $fechadesde= $fechadesdesoli[0]->fechadesde; 

                }    

                $respuesta = array(
                    "mensaje"  => "revisado",
                    "tipo"  => $tiposolicitud,
                    "id_solicitud" => $request['id'],
                    "fechadesde" => $fechadesde,
                    "fechahasta" => $fechahasta,
                    "descripsolicitud" => $descripcion,
                    "descripconcepto" => $descripconcepto,
                    "revisadopor" => $aprobadopor,
                    "observacion" => $observacion,
                    "correosolicitante" => $correosolicitante,
                );

                return response()->json($respuesta);
              
                //return response()->json(["mensaje" => "aprobado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }

    
    
    public function obtenerDatosDetoperaSolicitudAprobar(Request $request, $id)
    {

        $operacion="";
        $desde="";
        $latitud="";
        $longitud="";

        $detalleoperacion = DB::table('incweb')
            ->select(
                    'aprobadodesde',
                    'rechazadodesde'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $aprobadodesde= $detalleoperacion[0]->aprobadodesde;

        $rechazadodesde= $detalleoperacion[0]->rechazadodesde;

        if($aprobadodesde!=""){

            $cadena = explode('/',$aprobadodesde);

            //dd($cadena1);
            //die();

            $operacion="APROBADO";
            $desde=$cadena[0];
            $latitud=$cadena[1];
            $longitud=$cadena[2];

            //dd($creadodesde.$latitud.$longitud);

        }
       

        if($rechazadodesde!=""){

            $cadena = explode('/',$rechazadodesde);

            //dd($cadena1);
            //die();

            $operacion="RECHAZADO";
            $desde=$cadena[0];
            $latitud=$cadena[1];
            $longitud=$cadena[2];

            //dd($creadodesde.$latitud.$longitud);

        }
        
        $respuesta = array(
                    "operacion"  => $operacion,
                    "desde"  => $desde,
                    "latitud" => $latitud,
                    "longitud" => $longitud,
        );

        return response()->json($respuesta);

    
    }

   
    public function obtenerDatosDetaprobarInc(Request $request, $id)
    {

        $detalleaprobar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                    'horaaprob'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fechaaprob= $detalleaprobar[0]->fechaaprob;

        $horaaprob= $detalleaprobar[0]->horaaprob;


        $respuesta = array(
                    "fechaaprob"  => $fechaaprob,
                    "horaaprob" => $horaaprob,
        );

        return response()->json($respuesta);

    
    }

    
    public function obtenerDatosDetrechazarInc(Request $request, $id)
    {

        $detallerechazar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fecharechaz, "%d/%m/%Y") as fecharechaz'),
                    'horarechaz'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fecharechaz= $detallerechazar[0]->fecharechaz;

        $horarechaz= $detallerechazar[0]->horarechaz;


        $respuesta = array(
                    "fecharechaz"  => $fecharechaz,
                    "horarechaz" => $horarechaz,
        );

        return response()->json($respuesta);

    
    }

   
    public function obtenerDatosDetrevisarInc(Request $request, $id)
    {

        $detallerevisar = DB::table('incweb')
            ->select(
                     DB::raw('DATE_FORMAT(fecharevi , "%d/%m/%Y") as fecharevi'),
                    'horarevi'
                    )
            ->where('id', $id)
            ->whereNull('incweb.deleted_at')
            ->get();

        $fecharevi= $detallerevisar[0]->fecharevi;

        $horarevi= $detallerevisar[0]->horarevi;


        $respuesta = array(
                    "fecharevi"  => $fecharevi,
                    "horarevi" => $horarevi,
        );

        return response()->json($respuesta);

    
    }

   
    public function ExcelAprobarInc(Request $request)
    {
        if ($request->ajax()) {
            $lista = [];
            $columns = self::arregloColumnasAprobarInc();
            $titulo = 'Aprobar Incidencia';
            $buscar = $request['buscar'];
            $tipoBusqueda = $request['tipo'];

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            
                 $dp_fedesde_aprobarincidencia  = $request->input('dp_fedesde_aprobarincidencia');
                 $dp_fehasta_aprobarincidencia  = $request->input('dp_fehasta_aprobarincidencia');

                 $fechadesde = explode("-",$dp_fedesde_aprobarincidencia);
                 $fechahasta = explode("-",$dp_fehasta_aprobarincidencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

                 $estatus=$request->input('estatus');

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::AprobarIncEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$estatus);

            }
            else{
                $consulta = self::AprobarIncGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaAprobarInc($consulta,$buscar,$columns);
            }

            $lista = self::obtenerAprobarInc($consulta);

            return response()->json(self::crearExcelAprobarInc($titulo,$lista));


            //return response()->json(self::crearExcel($titulo,$lista));
        }
            

    }


    
    private function crearExcelAprobarInc($titulo,$lista)
    {
          
        $miExcel = Excel::create('Laravel Excel', function($excel) use ($lista) {

            $excel->sheet('Shetname', function ($sheet) use ($lista){


                // PARA HORIZONTAL
                //$sheet->getPageSetup()->setOrientation("landscape");

                $sheet->getPageSetup()->setOrientation("landscape");

                $sheet->mergeCells("A1:O1");

                $sheet->cell('A1', function($cell) {
                    $cell->setValue('LISTADO SOLICITUDES INCIDENCIA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                //$sheet->getStyle("A2".":G2")->getAlignment()->setWrapText(true); 
               
                $sheet->mergeCells("A2:O2");
                //$sheet->mergeCells("A3:O3");

                $sheet->cell('A3', function($cell) {
                    $cell->setValue('N°');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    //$cell->setFontSize(14);
                    //$cell->setFontFamily('Arial Black');
                    $cell->setAlignment('center');
                });

                $sheet->cell('B3', function($cell) {
                    $cell->setValue('FECHA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('C3', function($cell) {
                    $cell->setValue('HORA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('D3', function($cell) {
                    $cell->setValue('DESDE');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('E3', function($cell) {
                    $cell->setValue('HASTA');
                   $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('F3', function($cell) {
                    $cell->setValue('CONCEPTO');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('G3', function($cell) {
                    $cell->setValue('DESCRIPCION');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('H3', function($cell) {
                    $cell->setValue('ELABORADA POR');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('I3', function($cell) {
                    $cell->setValue('FECHA APROB');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('J3', function($cell) {
                    $cell->setValue('HORA APROB');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('K3', function($cell) {
                    $cell->setValue('FECHA RECHAZ');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('L3', function($cell) {
                    $cell->setValue('HORA RECHAZ');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('M3', function($cell) {
                    $cell->setValue('OBSERVACION');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('N3', function($cell) {
                    $cell->setValue('PROCESADA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('O3', function($cell) {
                    $cell->setValue('ESTATUS');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $i=4;

                foreach ($lista as $datosconsulta) {

                 //dd($d->sucursal);

                  $sheet->cell('A'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $idsolicitud = $datosconsulta->idsolicitud;
                      $cell->SetValue($idsolicitud);

                  });

                  $sheet->cell('B'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechasolicitud = $datosconsulta->fechasolicitud;
                      $cell->SetValue($fechasolicitud);

                  });

                  $sheet->cell('C'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horasolicitud = $datosconsulta->horasolicitud;
                      $cell->SetValue($horasolicitud);

                  });

                  $sheet->cell('D'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechainicio = $datosconsulta->fechainicio;
                      $cell->SetValue($fechainicio);

                  });

                  $sheet->cell('E'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechafin = $datosconsulta->fechafin;
                      $cell->SetValue($fechafin);

                  });

                  $sheet->cell('F'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $concepto = $datosconsulta->concepto;
                      $cell->SetValue($concepto);

                  });

                  $sheet->cell('G'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $descripcion = $datosconsulta->descripcion;
                      $cell->SetValue($descripcion);

                  });

                  $sheet->cell('H'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $elaboradopor = $datosconsulta->elaboradopor;
                      $cell->SetValue($elaboradopor);

                  });

                  $sheet->cell('I'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fechaaprob = $datosconsulta->fechaaprob;
                      $cell->SetValue($fechaaprob);

                  });

                   $sheet->cell('J'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horaaprob = $datosconsulta->horaaprob;
                      $cell->SetValue($horaaprob);

                  });

                  $sheet->cell('K'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fecharechaz = $datosconsulta->fecharechaz;
                      $cell->SetValue($fecharechaz);

                  });

                  $sheet->cell('L'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $horarechaz = $datosconsulta->horarechaz;
                      $cell->SetValue($horarechaz);

                  });

                  $sheet->cell('M'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $observacion = $datosconsulta->observacion;
                      $cell->SetValue($observacion);

                  });

                   $sheet->cell('N'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $codigoprocesado = $datosconsulta->codigoprocesado;
                      $cell->SetValue($codigoprocesado);

                  });

                  $sheet->cell('O'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $estatus = $datosconsulta->estatus;
                      $cell->SetValue($estatus);

                  });
                
                  $i=$i+1;

                  $finalregistros=$i;

                }

            });
           
        });

        $miExcel = $miExcel->string('xlsx');
        $response = array(
            'name' => $titulo,
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($miExcel)
        );
        return $response;

    }

    
    public function pdfAprobarInc(Request $request, $tipoBusqueda,$buscar,$dp_fedesde_aprobarincidencia,$dp_fehasta_aprobarincidencia,$estatus,$codigoinc)
    {

            $lista = [];
            $columns = self::arregloColumnasAprobarInc();

            //dd($tipoBusqueda,$dp_fedesde_aprobarincidencia,$dp_fehasta_aprobarincidencia);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

                 $fechadesde = explode("-",$dp_fedesde_aprobarincidencia);
                 $fechahasta = explode("-",$dp_fehasta_aprobarincidencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


               //dd($tipoBusqueda.$fechadesdeformateada .$fechahastaformateada.$estatus);

              

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::AprobarIncEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada,$codigoinc,$estatus);

            }
            else{
                $consulta = self::AprobarIncGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaAprobarInc($consulta,$buscar,$columns);
            }

            $total = self::contarAprobarInc($consulta);

            $registrosporpaginas= 4;
            $contador= 1;
            $contadorpaginas= 1;
            $contadorregistros= 1;
            $estatusbusqueda=$estatus;

            $lista = self::obtenerAprobarInc($consulta);

            //dd($lista);
            //die();

            $pdf = app('FPDF');
            $pdf->AliasNbPages();
            $pdf->AddPage('L','A4');

            $fecharegistro = Carbon::now();
            $fecharegistro = $fecharegistro->format('d/m/Y');

            $horaregistro = Carbon::now();
            $horaregistro = $horaregistro->format('H:i:s:A');
            
            //$numeropag=$pdf->PageNo();

            $pdf->Image('img/logoempresa.png',10,4,25);

            $i=0;

            $pdf->SetFont('Arial', 'B', 14);

            $fechaimpresion = utf8_decode($fecharegistro);
            $pdf->SetXY(200,16);
            $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

            $horaimpresion = utf8_decode($horaregistro);
            $pdf->SetXY(200,22);
            $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

            $pdf->SetXY(150,35);
            $titulo = utf8_decode("LISTADO SOLICITUDES INCIDENCIA");
            $pdf->Cell(5, 1, $titulo, '', 1, 'C');

            //dd($tipoBusqueda.$estatusbusqueda);
            //die();

            if($tipoBusqueda=="especifico") {

                if(($estatusbusqueda!="todos")){

                    $fechadesde = explode("-",$dp_fedesde_aprobarincidencia);
                    $fechahasta = explode("-",$dp_fehasta_aprobarincidencia);

                    $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                    $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                    $pdf->SetXY(110,43);
                    $titulo = utf8_decode("DESDE:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(135,43);
                    $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                    $pdf->SetXY(165,43);
                    $titulo = utf8_decode("HASTA:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(190,43);
                    $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                }

            }    

            $posicioninicial = 45;

            $posicionXdetalle = $posicioninicial + 15;

            $posicioninicial1 = 65;

            $posicionXdetalle1 = $posicioninicial1 + 12;

            $contador=1;

            $pdf->SetXY(15,179);
            $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C'); 

            foreach ($lista as $datosconsulta) {

                if($registrosporpaginas==$contador){

                    //$pdf = app('FPDF');
          
                    $pdf->AddPage('L','A4');

                    $pdf->SetFont('Arial', 'B', 14);

                    $fecharegistro = Carbon::now();
                    $fecharegistro = $fecharegistro->format('d/m/Y');

                    $horaregistro = Carbon::now();
                    $horaregistro = $horaregistro->format('H:i:s:A');
            
                    //$numeropag=$pdf->PageNo();

                    $pdf->Image('img/logoempresa.png',10,4,25);

                    $pdf->SetFont('Arial', 'B', 14);

                    $fechaimpresion = utf8_decode($fecharegistro);
                    $pdf->SetXY(200,16);
                    $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

                    $horaimpresion = utf8_decode($horaregistro);
                    $pdf->SetXY(200,22);
                    $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');


                    $pdf->SetXY(150,35);
                    $titulo = utf8_decode("LISTADO SOLICITUDES INCIDENCIA");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');

                    if($tipoBusqueda=="especifico") {

                        //dd($estatus);
                        //die();

                        if(($estatusbusqueda!="todos")){

                            $fechadesde = explode("-",$dp_fedesde_aprobarincidencia);
                            $fechahasta = explode("-",$dp_fehasta_aprobarincidencia);

                            $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                            $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                            $pdf->SetXY(110,43);
                            $titulo = utf8_decode("DESDE:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(135,43);
                            $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                            $pdf->SetXY(165,43);
                            $titulo = utf8_decode("HASTA:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(190,43);
                            $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                        }

                    }    

                    $posicioninicial = 45;

                    $posicionXdetalle = $posicioninicial + 15;

                    $posicioninicial1 = 65;

                    $posicionXdetalle1 = $posicioninicial1 + 12;

                    $contador=1;

                    $contadorpaginas=  $contadorpaginas + 1;

                    $pdf->SetXY(15,179);
                    $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C');
                }


                //TITULO SOLICITUD
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(15,$posicionXdetalle-5);
                    $titulosoli = utf8_decode("SOLICITUD");
                    $pdf->Cell(10, 1, $titulosoli, '', 1, 'C');
                //FIN TITULO SOLICTUD

                //ENCABEZADO DE LA SOLICITUD TITULO-DETALLE
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(7,$posicionXdetalle);
                    $tituloid = utf8_decode("N°");
                    $pdf->Cell(10, 1, $tituloid, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $idsolicitud = $datosconsulta->idsolicitud;
                    $idsolicitud = utf8_decode($idsolicitud);
                    $pdf->SetXY(9,$posicionXdetalle+5);
                    $pdf->Cell(25, 1, $idsolicitud, '', 1, 'L');
                   
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(25,$posicionXdetalle);
                    $titulofechasoli = utf8_decode("FECHA");
                    $pdf->Cell(10, 1, $titulofechasoli, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechasolicitud = $datosconsulta->fechasolicitud;
                    $fechasolicitud = utf8_decode($fechasolicitud);
                    $pdf->SetXY(18,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechasolicitud, '', 1, 'L');
                   
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(50,$posicionXdetalle);
                    $titulohora = utf8_decode("HORA");
                    $pdf->Cell(10, 1, $titulohora, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $horasolicitud = $datosconsulta->horasolicitud;
                    $horasolicitud = utf8_decode($horasolicitud);
                    $pdf->SetXY(45,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$horasolicitud, '', 1, 'L');
                    
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(70,$posicionXdetalle);
                    $titulodesde = utf8_decode("DESDE");
                    $pdf->Cell(20, 1, $titulodesde, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechainicio = $datosconsulta->fechainicio;
                    $fechainicio = utf8_decode($fechainicio);
                    $pdf->SetXY(70,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechainicio, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(95,$posicionXdetalle);
                    $titulohasta = utf8_decode("HASTA");
                    $pdf->Cell(20, 1, $titulohasta, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $fechafin = $datosconsulta->fechafin;
                    $fechafin = utf8_decode($fechafin);
                    $pdf->SetXY(95,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$fechafin, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(130,$posicionXdetalle);
                    $tituloconcepto = utf8_decode("CONCEPTO");
                    $pdf->Cell(20, 1, $tituloconcepto, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $concepto = $datosconsulta->concepto;
                    $concepto = utf8_decode($concepto);

                    if($concepto=="Vacaciones")
                    {
                        $mediajornadavac = $datosconsulta->mediajornadavac;

                        if($mediajornadavac=="NO"){
                          $concepto = "Vacaciones";
                        }

                        if($mediajornadavac=="SI"){
                          $concepto = "MediaJornada";
                        }
                    }

                    $pdf->SetXY(123,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$concepto, '', 1, 'L');

                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(175,$posicionXdetalle);
                    $titulodescipcion = utf8_decode("DESCRIPCION");
                    $pdf->Cell(20, 1, $titulodescipcion, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $descripcion = $datosconsulta->descripcion;
                    $descripcion =str_replace(' ', '', $descripcion);
                    $descripcion = utf8_decode($descripcion);
                    $pdf->SetXY(169,$posicionXdetalle+3);
                    $pdf->MultiCell(75,4,$descripcion);


                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(250,$posicionXdetalle);
                    $tituloestatus = utf8_decode("ESTATUS");
                    $pdf->Cell(20, 1, $tituloestatus, '', 1, 'C');

                    $pdf->SetFont('Arial', '', 12);
                    $estatus = $datosconsulta->estatus;
                    $estatus = utf8_decode($estatus);
                    $pdf->SetXY(247,$posicionXdetalle+5);
                    $pdf->Cell(25, 1,$estatus, '', 1, 'L');


                //FIN ENCABEZADO DE LA SOLICITUD TITULO-DETALLE
            
                //TITULO DETALLE APROBACION
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->SetXY(27,$posicionXdetalle1-5);
                    $estatus = $datosconsulta->estatus;
                    $estatus = utf8_decode($estatus);
                    $titulodetsoli = utf8_decode("DETALLE"." ".$estatus);
                    $pdf->Cell(10, 1, $titulodetsoli, '', 1, 'C');
                //FIN TITULO DETALLE APROBACION

                //DETALLE DE LA APROBACION 
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetXY(21,$posicionXdetalle1);
                $tituloelaborado = utf8_decode("ELABORADA");
                $pdf->Cell(20, 1, $tituloelaborado, '', 1, 'C');

                $pdf->SetFont('Arial', '', 12);
                $elaboradopor = $datosconsulta->elaboradopor;
                $elaboradopor = utf8_decode($elaboradopor);
                $pdf->SetXY(12,$posicionXdetalle1+5);
                $pdf->Cell(25, 1,$elaboradopor, '', 1, 'L');


                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetXY(65,$posicionXdetalle1);
                $titulofechaaprob = utf8_decode("FECHA");
                $pdf->Cell(10, 1, $titulofechaaprob, '', 1, 'C');

                $pdf->SetFont('Arial', '', 12);
                if($estatus=="INGRESADO"){
                   $fecha = $datosconsulta->fechasolicitud;
                }
                if($estatus=="APROBADO"){
                   $fecha = $datosconsulta->fechaaprob;
                }
                if($estatus=="RECHAZADO"){
                   $fecha = $datosconsulta->fecharechaz;
                }
                if($estatus=="REVISADO"){
                   $fecha = $datosconsulta->fecharevi;
                }
              
                $fecha = utf8_decode($fecha);
                $pdf->SetXY(60,$posicionXdetalle1+5);
                $pdf->Cell(25, 1, $fecha, '', 1, 'L');

                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetXY(95,$posicionXdetalle1);
                $titulohoraaprob = utf8_decode("HORA");
                $pdf->Cell(10, 1, $titulohoraaprob, '', 1, 'C');

                $pdf->SetFont('Arial', '', 12);
                if($estatus=="INGRESADO"){
                    $hora = $datosconsulta->horasolicitud;
                }
                if($estatus=="APROBADO"){
                   $hora = $datosconsulta->horaaprob;
                }
                if($estatus=="RECHAZADO"){
                   $hora  = $datosconsulta->horarechaz;
                }
                if($estatus=="REVISADO"){
                   $hora  = $datosconsulta->horarevi;
                }
               
                $hora = utf8_decode($hora);
                $pdf->SetXY(91,$posicionXdetalle1+5);
                $pdf->Cell(25, 1,$hora, '', 1, 'L');

            
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetXY(130,$posicionXdetalle1);
                $tituloobservacion = utf8_decode("OBSERVACION");
                $pdf->Cell(10, 1, $tituloobservacion, '', 1, 'C');

                $pdf->SetFont('Arial', '', 12);
                $observacion = $datosconsulta->observacion;
                $observacion =str_replace(' ', '', $observacion);
                $observacion = utf8_decode($observacion);
                $pdf->SetXY(119,$posicionXdetalle1+3);
                $pdf->MultiCell(100,3,$observacion);

                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetXY(235,$posicionXdetalle1);
                $tituloprocesada = utf8_decode("PROCESADA");
                $pdf->Cell(10, 1, $tituloprocesada, '', 1, 'C');

                $pdf->SetFont('Arial', '', 12);
                $codigoprocesado = $datosconsulta->codigoprocesado;
                $codigoprocesado = utf8_decode($codigoprocesado);
                $pdf->SetXY(230,$posicionXdetalle1+5);
                $pdf->Cell(25, 1,$codigoprocesado, '', 1, 'L');

                //FIN DETALLE DE LA APROBACION

                //SE INCREMENTA LA POSICION X DEL PRIMER DETALLE
                //ES DECIR DEL ENCABEZADO
                $posicionXdetalle = $posicionXdetalle + 42;
                
                //SE INCREMENTA LA POSICION X DEL SEGUNDO DETALLE
                //ES DECIR DEL DETALLE APROBACION
                $posicionXdetalle1 = $posicionXdetalle1 + 41;
                
                $contador=  $contador + 1;

                $contadorregistros = $contadorregistros + 1;


            }

        $pdf->output();

        //dd($pdf);
        //die();
        exit;


    }

   
     public function obtenerfotopersonaaprobarinc()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }


   

    public function obtenernombrepersonaaprobarinc()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

    
    public function CargarConceptosAprobarInc(Request $request)
    {

        $conceptosaprobarinc = DB::table('gconceptos')
            ->select(
                'gconceptos.codconcepto',
                'gconceptos.desconcepto')
            ->distinct()
            ->where('cconcepto', 'like', "0%")
            ->orwhere('cconcepto', 'like', "1%")
            ->orderBy('gconceptos.desconcepto','asc')
            ->get();

        //dd($conceptosaprobarinc);
        //die();

        return response()->json($conceptosaprobarinc);

    }

   

     public function RecargarConceptosAprobarInc(Request $request, $criterio)
    {

        $criterio = "%" . $criterio . "%";

        $conceptosaprobarinc = DB::table('gconceptos')
            ->select('gconceptos.codconcepto','gconceptos.desconcepto')
            ->distinct()
            ->where('desconcepto', 'like', $criterio)
            //->where('cconcepto', 'like', "0%")
            //->orwhere('cconcepto', 'like', "1%")
            ->orderBy('gconceptos.desconcepto','asc')
            ->get();
            
        return response()->json($conceptosaprobarinc);

    }

    
    public function obtenerdescripcionconceptoaprobar($idsolicitud)
    {
          
        $codigoconcep = DB::table('incweb')
            ->select('codconcepto')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

          $codigoconcepto= $codigoconcep[0]->codconcepto; 

        $descrip = DB::table('gconceptos')
            ->select('gconceptos.desconcepto')
            ->distinct()
            ->where('codconcepto',$codigoconcepto)
            ->get();

        //dd($codigoconcepto);
        //die();

          $descripconcepto= $descrip[0]->desconcepto;

        return($descripconcepto);
    }

    

    public function obtenerdescripcionsolicitudaprobar($idsolicitud)
    {
          $descrip = DB::table('incweb')
            ->select('descripcio')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        //dd($codigoconcepto);
        //die();

        $descripsolicitud= $descrip[0]->descripcio;

        return($descripsolicitud);
    }

    
    public function obtenercorreosolicitanteaprobar($idsolicitud)
    {
        
        $codigoper = DB::table('incweb')
            ->select('codpersona')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        $codigopersona= $codigoper[0]->codpersona;


        $correo = DB::table('personal')
            ->select(
                'personal.mail'        
            )
            ->distinct()
            ->where('codperson',$codigopersona)
            ->get();

        //dd($correo);
        //die();

        $contador = $correo->count();

        if($contador>0){
           $corresolicitante= $correo[0]->mail;
        }
        else{
          $corresolicitante= "";
        }

    
        return($corresolicitante);
    }

    
    public function obtenerdescripcionsolicitudrechazar($idsolicitud)
    {
          $descrip = DB::table('incweb')
            ->select('descripcio')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        //dd($codigoconcepto);
        //die();

        $descripsolicitud= $descrip[0]->descripcio;

        return($descripsolicitud);
    }

    
    public function obtenernombrepersonarechazarinc()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

    
    public function obtenerdescripcionconceptorechazar($idsolicitud)
    {
          
        $codigoconcep = DB::table('incweb')
            ->select('codconcepto')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

          $codigoconcepto= $codigoconcep[0]->codconcepto; 

        $descrip = DB::table('gconceptos')
            ->select('gconceptos.desconcepto')
            ->distinct()
            ->where('codconcepto',$codigoconcepto)
            ->get();

        //dd($codigoconcepto);
        //die();

          $descripconcepto= $descrip[0]->desconcepto;

        return($descripconcepto);
    }

    

    public function obtenercorreosolicitanterechazar($idsolicitud)
    {
        
        $codigoper = DB::table('incweb')
            ->select('codpersona')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        $codigopersona= $codigoper[0]->codpersona;


        $correo = DB::table('personal')
            ->select(
                'personal.mail'        
            )
            ->distinct()
            ->where('codperson',$codigopersona)
            ->get();

        //dd($correo);
        //die();

        $contador = $correo->count();

        if($contador>0){
           $corresolicitante= $correo[0]->mail;
        }
        else{
          $corresolicitante= "";
        }

        return($corresolicitante);
    }

    

    public function obtenercorreosolicitanterevisar($idsolicitud)
    {
        
        $codigoper = DB::table('incweb')
            ->select('codpersona')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        $codigopersona= $codigoper[0]->codpersona;


        $correo = DB::table('personal')
            ->select(
                'personal.mail'        
            )
            ->distinct()
            ->where('codperson',$codigopersona)
            ->get();

        //dd($correo);
        //die();

        $contador = $correo->count();

        if($contador>0){
           $corresolicitante= $correo[0]->mail;
        }
        else{
          $corresolicitante= "";
        }

        return($corresolicitante);
    }

    
    public function obtenerdescripcionsolicitudrevisar($idsolicitud)
    {
          $descrip = DB::table('incweb')
            ->select('descripcio')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        //dd($codigoconcepto);
        //die();

        $descripsolicitud= $descrip[0]->descripcio;

        return($descripsolicitud);
    }

    
    public function obtenernombrepersonarevisarinc()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

   
     public function obtenerdescripcionconceptorevisar($idsolicitud)
    {
          
        $codigoconcep = DB::table('incweb')
            ->select('codconcepto')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

          $codigoconcepto= $codigoconcep[0]->codconcepto; 

        $descrip = DB::table('gconceptos')
            ->select('gconceptos.desconcepto')
            ->distinct()
            ->where('codconcepto',$codigoconcepto)
            ->get();

        //dd($codigoconcepto);
        //die();

          $descripconcepto= $descrip[0]->desconcepto;

        return($descripconcepto);
    }


    
     public function obtenercorreorecursoshumanosaprobarinc()
    {

        //COMO ESTOS CORREOS VAN POR COPIA EN EL CONTROLLER MAIL SE DEBEN ENVIAR EN UN ARRAY
        //PORQUE EL METODO FUNCIONA DE ESA MANERA

        //$correotecnicos= "";
        $correorecursoshumanos= [];

        $estado="A";
        //$comillas= "''";

        //$correofinales= [];
        //$j=0;

        $correorhumanos = DB::table('correorhumanos')
            ->select(
                  
                'correorhumanos.correo'
            )
            ->distinct()
            ->where('correorhumanos.estado', $estado)
            ->get(); 

        $contador = $correorhumanos->count();

        //dd($correotec[1]->correo);
        //die();
        
        
        for ($posicionarreglo = 0; $posicionarreglo < $contador; $posicionarreglo++) {

            //$correo= "'".$correotec[$posicionarreglo]->correo."'";
            //$correotecnicos= $correotecnicos.",".$correo;
            $correo= $correorhumanos[$posicionarreglo]->correo;

            $correorecursoshumanos[$posicionarreglo]=$correo;

            /*if($posicionarreglo==0){
                $correotecnicos[$posicionarreglo]=$correo;               
            }
            else{
                 
                $correotecnicos[$posicionarreglo]= $correo; 
            }*/

        } 

        //dd($correotecnicos);
        //die();


        return($correorecursoshumanos);
    }

    
    public function obtenernombresolicitanteaprobarinc($idsolicitud)
    {
        
        $codigoper = DB::table('incweb')
            ->select('codpersona')
            ->distinct()
            ->where('id',$idsolicitud)
            ->get();

        $codigopersona= $codigoper[0]->codpersona;


        $nombre = DB::table('personal')
            ->select(
                'nomperson AS solicitante'       
            )
            ->distinct()
            ->where('codperson',$codigopersona)
            ->get();

        //dd($correo);
        //die();

        $contador = $nombre->count();

        if($contador>0){
           $nombresolicitante= $nombre[0]->solicitante;
        }
        else{
          $nombresolicitante= "";
        }

        return($nombresolicitante);
    }

   
    public function obtenerdispositivoaccesosolicitudaprobar()
    {

        $tablet_browser = 0;
        $mobile_browser = 0;
        $body_class = 'desktop';
 
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
            $body_class = "tablet";
        }
         
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');
         
        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }
         
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }
        if ($tablet_browser > 0) {
        // Si es tablet has lo que necesites
           $dispositivo= 'Tablet';
           return($dispositivo);
          
        }
        else if ($mobile_browser > 0) {
        // Si es dispositivo mobil has lo que necesites
           $dispositivo= 'Mobil';
           return($dispositivo);
          
        }
        else {
        // Si es ordenador de escritorio has lo que necesites

           $dispositivo= 'Ordenador';
           return($dispositivo);
           
        } 

    } 

}
