<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;
use App\Consultas;
use Illuminate\Support\Facades\Log;


class ConsultasController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";

    public function arregloColumnasConsultas()
    {
        return array(
            0 => 'id',
            1 => 'descripcion',
            2 => 'fecha',
            3 => 'hora',
            4 => 'respuesta',
            5 => 'respondido'
        );
    }

     public function contarConsultas($consulta)
    {
        return $consulta->count();
    }

    public function obtenerConsultas($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaConsultas($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {

            //dd($inicio.$limite);
            //die();
            $consulta->offset($inicio)
                ->limit($limite);

            //dd($consulta->tosql());
            //die();
        }

        return $consulta;
    }

    private function ordenarConsultaConsultas($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaConsultas($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function Consultas(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonaconsultas();

        $nombrepersonaaccesa= self::obtenernombrepersonaconsultas();

        return view('admin.consultas.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    
    private function ConsultasEspecificaEmpleado($buscar,$fe_desde,$fe_hasta)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();

        $consulta = DB::table('consultas')
                ->join('personal','personal.dniperson', '=', 'consultas.codpersona')
                ->leftjoin('personal as personaresponde','personaresponde.dniperson', '=', 'consultas.codpersonaresp')
                ->select(
                    'id',
                    'personal.nomperson',
                    'descripcion',
                    DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha'),
                    'hora',
                    'respuesta',
                    'respondido',
                    'personaresponde.nomperson as personaresp'
                    
                )
                ->distinct()
                ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->where('codpersona',$co_usuario)
                ->orderBy('id', 'asc');
                //->get();

        //dd($consulta);
        //die();
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function ConsultasEspecificaSupervisados($buscar,$fe_desde,$fe_hasta,$personalconsultas)
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        if ($personalconsultas == 'todos') {
            $personalconsultas ="";
        }

        
        $buscarsupervisor = DB::table('personal')
            ->select(
                 'personal.psuperv'
                    
            )
            ->distinct()
            ->where('personal.psuperv',$co_usuario)
            ->get();

        $contador = $buscarsupervisor->count();

        if($contador>0){

            $supervisor = trim($buscarsupervisor[0]->psuperv);

            $consultasupervisados = DB::table('consultas')
               ->join('personal','personal.dniperson', '=', 'consultas.codpersona')
                ->leftjoin('personal as personaresponde','personaresponde.dniperson', '=', 'consultas.codpersonaresp')
                ->select(
                    'id',
                    'personal.nomperson',
                    'descripcion',
                    DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha'),
                    'hora',
                    'respuesta',
                    'respondido',
                    'personaresponde.nomperson as personaresp'

                )
                ->distinct()
                ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->where('personal.psuperv',$supervisor)
                ->orderBy('id', 'asc');
                //->get();
            
            //BUSQUEDA POR PERSONA 
            if ($personalconsultas!= ""){
               //dd("pase por aqui".$personalconsultas);
               //die();
               $consultasupervisados->where('codpersona',$personalconsultas);
            }
       

        }
        else{

            $consultasupervisados="";

        }

        return $consultasupervisados;

        //dd($consultasupervisados);
        //die();

            //->get();

        //dd($consulta);
        //die();

    }

    private function ConsultasGeneralEmpleado()
    {

        //$fecha = Carbon::now();
        //$fecha = $fecha->format('Ymd');
        //dd($fecha);

        $fecha_actual = Carbon::now();

        //LA FECHA DESDE Y LA FECHA HASTA SE ARMA DE ESTA MANERA PARA QUE MUESTREN 
        //LA INFORMACION DE TODO EL MES

        $monthfinal = $fecha_actual->format('m');
        $yearfinal  = $fecha_actual->format('Y');

        if ($monthfinal == 01) {
            $monthdesde = 12;
            $yeardesde  = $yearfinal;

        } else {
            $monthdesde = $monthfinal;
            $yeardesde  = $yearfinal;
        }

        $daydesde = "01";
        $fe_desde = $yeardesde . '-' . $monthdesde . '-' . $daydesde;

        $dayfinal = "31";
        $fe_hasta = $yearfinal . '-' . $monthfinal . '-' . $dayfinal;

        $fechadesde = explode("-",$fe_desde);
        $fechahasta = explode("-",$fe_hasta);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

        //dd($fechadesdeformateada.$fechahastaformateada);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        
        return DB::table('consultas')
            ->join('personal','personal.dniperson', '=', 'consultas.codpersona')
            ->leftjoin('personal as personaresponde','personaresponde.dniperson', '=', 'consultas.codpersonaresp')
            ->select(
                'id',
                'personal.nomperson',
                'descripcion',
                DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha'),
                'hora',
                'respuesta',
                'respondido',
                'personaresponde.nomperson as personaresp'
            )
            ->distinct()
            ->whereBetween('fecha',[$fechadesdeformateada,$fechahastaformateada])
            ->where('codpersona',$co_usuario)
            ->orderBy('id', 'asc');
        
        
        //dd($consultaempleado);
        //die();

        //return $consultaempleado;
    }





    private function ConsultasGeneralSupervisados()
    {

        //$fecha = Carbon::now();
        //$fecha = $fecha->format('Ymd');
        //dd($fecha);

        $fecha_actual = Carbon::now();

        //LA FECHA DESDE Y LA FECHA HASTA SE ARMA DE ESTA MANERA PARA QUE MUESTREN 
        //LA INFORMACION DE TODO EL MES

        $monthfinal = $fecha_actual->format('m');
        $yearfinal  = $fecha_actual->format('Y');

        if ($monthfinal == 01) {
            $monthdesde = 12;
            $yeardesde  = $yearfinal;

        } else {
            $monthdesde = $monthfinal;
            $yeardesde  = $yearfinal;
        }

        $daydesde = "01";
        $fe_desde = $yeardesde . '-' . $monthdesde . '-' . $daydesde;

        $dayfinal = "31";
        $fe_hasta = $yearfinal . '-' . $monthfinal . '-' . $dayfinal;

        $fechadesde = explode("-",$fe_desde);
        $fechahasta = explode("-",$fe_hasta);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

        //dd($fechadesdeformateada.$fechahastaformateada);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        
        $buscarsupervisor = DB::table('personal')
            ->select(
                 'personal.psuperv'
                    
            )
            ->distinct()
            ->where('personal.psuperv',$co_usuario)
            ->get();

        $contador = $buscarsupervisor->count();

        if($contador>0){

            $supervisor = trim($buscarsupervisor[0]->psuperv);

               return DB::table('consultas')
                ->join('personal','personal.dniperson', '=', 'consultas.codpersona')
                ->leftjoin('personal as personaresponde','personaresponde.dniperson', '=', 'consultas.codpersonaresp')
                ->select(
                    'id',
                    'personal.nomperson',
                    'descripcion',
                    DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha'),
                    'hora',
                    'respuesta',
                    'respondido',
                    'personaresponde.nomperson as personaresp'
                )
                ->distinct()
                ->whereBetween('fecha',[$fechadesdeformateada,$fechahastaformateada])
                ->where('personal.psuperv',$supervisor)
                ->orderBy('id', 'asc');
        }
        else{

            $consultasupervisados="NO EXISTEN DATOS";
            return $consultasupervisados;

        }
           
       
        //return $consultasupervisados;
        
       

    }

     private function BuscarEnConsultaFiltroConsultas($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

    public function CargarPersonalSupervisorConsultas(Request $request)
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;
       
        $personalsupervisor = DB::table('personal')
            ->select(
                 'CODPERSON as dni',
                 'nomperson as nombre'
                 //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) nombre')
            )
            ->distinct()
            ->where('psuperv',$co_usuario)
            ->orderBy('dniperson','asc')
            ->get();

        //dd($conceptosaprobarinc);
        //die();

        return response()->json($personalsupervisor);

    }

    public function RecargarPersonalSupervisorConsultas(Request $request, $criterio)
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $criterio = "%" . $criterio . "%";

        $personalsupervisor = DB::table('personal')
            ->select(
                 'CODPERSON as dni',
                 'nomperson as nombre'
                 //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) nombre')
            )
            ->distinct()
            ->where('psuperv',$co_usuario)
            ->where('personal.appaterno', 'like', $criterio)
            //->where('personal.apmaterno', 'like', $criterio)
            ->orderBy('CODPERSON','asc')
            ->get();
            
        return response()->json($personalsupervisor);

    }

   
    public function ListarConsultas(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasConsultas();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico
        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_consultas  = $request->input('dp_fedesde_consultas');
            $dp_fehasta_consultas  = $request->input('dp_fehasta_consultas');

            $fechadesde = explode("-",$dp_fedesde_consultas);
            $fechahasta = explode("-",$dp_fehasta_consultas);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

            $personalconsultas=$request->input('personalconsultas');


            $usuario_actual = \Auth::User();

            $supervisor = $usuario_actual->supervisor;


            if($personalconsultas=="todos"){


                $consulta1 = self::ConsultasEspecificaEmpleado($buscar,$fechadesdeformateada,$fechahastaformateada);

                if($buscar!=""){

                    $consulta1 = self::buscarEnConsultaConsultas($consulta1, $buscar, $columns); 

                }

                $total1 = self::contarConsultas($consulta1);

                $consulta1 = self::limitarConsultaConsultas($consulta1, $inicio, $limite);

            }


            if($supervisor=="SI"){


                $consulta2 = self::ConsultasEspecificaSupervisados($buscar,$fechadesdeformateada,$fechahastaformateada,$personalconsultas);


                if($buscar!=""){
                    $consulta2 = self::buscarEnConsultaConsultas($consulta2, $buscar, $columns);
                } 

                $total2 = self::contarConsultas($consulta2);

                $consulta2 = self::limitarConsultaConsultas($consulta2, $inicio, $limite);


                if($personalconsultas=="todos"){

                    $listaconsultas = self::obtenerConsultas($consulta1)->merge(self::obtenerConsultas($consulta2)); 

                }
                else{

                    $listaconsultas = self::obtenerConsultas($consulta2);
                    $total1=0;

                }

            }
            else{

                $listaconsultas = self::obtenerConsultas($consulta1);
                $total2=0; 

            }

            $total= $total1 + $total2;



        }else{

            $consulta1 = self::ConsultasGeneralEmpleado(); 

            if($buscar!=""){

                $consulta1 = self::buscarEnConsultaConsultas($consulta1, $buscar, $columns); 
            }

            $total1 = self::contarConsultas($consulta1);


            //dd($total1);
            //die();

            $consulta1 = self::limitarConsultaConsultas($consulta1, $inicio, $limite);


            $usuario_actual = \Auth::User();

            $supervisor = $usuario_actual->supervisor;

            if($supervisor=="SI"){

                $consulta2 = self::ConsultasGeneralSupervisados();

                if($buscar!=""){

                    $consulta2 = self::buscarEnConsultaConsultas($consulta2, $buscar, $columns); 
                }

                $total2 = self::contarConsultas($consulta2);
           

                $consulta2 = self::limitarConsultaConsultas($consulta2, $inicio, $limite);


                $listaconsultas = self::obtenerConsultas($consulta1)->merge(self::obtenerConsultas($consulta2));

            }

            else{

                $listaconsultas = self::obtenerConsultas($consulta1);
                $total2=0;  
            }

            //$totales = self::contarConsultas($listaconsultas);

            $total= $total1 + $total2;
          
        }


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listaconsultas,
            //"totaltardanza" => $totaltardanza,
            //"totalantessalida" => $totalantessalida,

        );

     

        return response()->json($json_data);
    
    }

    public function IngresarConsultas(Request $request)
    {

        if ($request->ajax()) {

            $descripcion = trim($request['descripcion']);

            DB::beginTransaction();

            try {
                 
                $utimoid = DB::select(DB::raw('select id from consultas where id = (select max(`id`) from consultas )'));
                
                if($utimoid==null){
                   $idingresar= 1;
                }
                else{

                    $ultimo = trim($utimoid[0]->id);
                    $idingresar= $ultimo + 1;
                    $idingresar= (int)$idingresar;
                }

                $fecha = Carbon::now();
                $fecha = $fecha->format('Ymd');

                $hora = Carbon::now();
                $hora = $hora->format('H:i:s');

                $usuario_actual = \Auth::User();
                $codpersona = $usuario_actual->co_usuario;

                //dd($idingresar."  ".$fechasoli."  ".$horasoli."  ".$codpersona."  ".$supervisor);
                //die();

                $respondido="N";

                $incweb = Consultas::create([
                "id"  => $idingresar,
                "codpersona"    => $codpersona,
                "fecha"  => $fecha,
                "hora"    => $hora,
                "descripcion"    => $descripcion,
                "respondido"    => $respondido,
                ]);


            DB::commit();

                $respuesta = array(
                    "mensaje"  => "creado",
                );

                

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "creado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

     }

     public function ObtenerDatosConsulta(Request $request,$id)
    {
        //
       
        if($request->ajax()){

            $usuario_actual = \Auth::User();
            $usuarioactual = $usuario_actual->co_usuario;

            $consulta = DB::table('consultas')
             ->join('users','users.co_usuario', '=', 'consultas.codpersona')
                ->select(
                        'id',
                        'codpersona',
                        'respondido'
                        )
                ->where('id',$id)
                ->whereNull('consultas.deleted_at')
                ->get();

            $id= $consulta[0]->id;
            $respondido= $consulta[0]->respondido; 
            $usuarioconsulta= $consulta[0]->codpersona;

            $respuesta = array(
                "id"  => $id,
                "respondido"  => $respondido,
                "usuarioconsulta" => $usuarioconsulta,
                "usuarioactual" => $usuarioactual,
            ); 

            return response()->json($respuesta); 
        }
    }


    public function ActualizarRespuesta(Request $request)
    {
            
            if($request->ajax()){

                DB::beginTransaction();

                try {

                    $respondido="S";

                    $usuario_actual = \Auth::User();
                    $respondidopor = $usuario_actual->co_usuario;

                    $respuesta=Consultas::where('id',$request['codigo'])->update([
                          "respuesta"=>$request['respuesta'],
                          "respondido"=>$respondido,
                          "codpersonaresp"=>$respondidopor,


                    ]);

                    DB::commit();

                    return response()->json([
                    "mensaje" => "actualizado"
                    ]);

                } 
                catch (\Throwable $e) {
                
                    DB::rollback();
                    Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                    return response()->json(["error" => $e->getMessage()]);
                }
            }
    }

    

    public function ExcelMarcaAsistenciaRepo(Request $request)
    {
        if ($request->ajax()) {
            $lista = [];
            $columns = self::arregloColumnasMarcaAsistenciaRepo();
            $titulo = 'Marca Asistencia';
            $buscar = $request['buscar'];
            $tipoBusqueda = $request['tipo'];

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            
                 $dp_fedesde_marcaasistencia  = $request->input('dp_fedesde_marcaasistencia');
                 $dp_fehasta_marcaasistencia  = $request->input('dp_fehasta_marcaasistencia');

                 $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                 $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

            }
            else{
                $consulta = self::MarcaAsistenciaGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

             if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaMarcaAsistenciaRepo($consulta,$buscar,$columns);
            }

            $lista = self::obtener($consulta);

            $totaltardanza = self::totalizartardanza($lista);
            $totalantessalida = self::totalizarantes($lista);

            $titulo= "Marca Asistencia";

            return response()->json(self::crearExcel($titulo,$lista,$totaltardanza,$totalantessalida));


            //return response()->json(self::crearExcel($titulo,$lista));
        }
            

    }



    private function crearExcel($titulo,$lista,$totaltardanza,$totalantessalida)
    {
          
        $miExcel = Excel::create('Laravel Excel', function($excel) use ($lista,$totaltardanza,$totalantessalida) {

            $excel->sheet('Shetname', function ($sheet) use ($lista,$totaltardanza,$totalantessalida){


                // PARA HORIZONTAL
                //$sheet->getPageSetup()->setOrientation("landscape");

                $sheet->getPageSetup()->setOrientation("portrait");

                $sheet->mergeCells("A1:G1");

                $sheet->cell('A1', function($cell) {
                    $cell->setValue('REGISTRO MARCA ASISTENCIA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                //$sheet->getStyle("A2".":G2")->getAlignment()->setWrapText(true); 
               
                $sheet->mergeCells("A2:G2");
                $sheet->mergeCells("A3:G3");

                $sheet->cell('A4', function($cell) {
                    $cell->setValue('DIA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    //$cell->setFontSize(14);
                    //$cell->setFontFamily('Arial Black');
                    $cell->setAlignment('center');
                });

                $sheet->cell('B4', function($cell) {
                    $cell->setValue('FECHA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('C4', function($cell) {
                    $cell->setValue('ENTRADA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('D4', function($cell) {
                    $cell->setValue('SALIDA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('E4', function($cell) {
                    $cell->setValue('TARDANZA');
                   $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('F4', function($cell) {
                    $cell->setValue('ANTES DE SALIDA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('G4', function($cell) {
                    $cell->setValue('OBSERVACIONES');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });


                $i=5;

                foreach ($lista as $datosconsulta) {

                    if($i==5){

                       $sheet->mergeCells("A2:G2");

                       $sheet->cell('A2',function($cell) use($datosconsulta){
                         $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                         )); 
                        //$cell->setFontSize(12);
                        $cell->setAlignment('left');
                        $dni = $datosconsulta->dni;
                        $nombre = $datosconsulta->nombre;
                        $cell->SetValue($dni."-".$nombre );
                       });

                    }


                 //dd($d->sucursal);

                  $sheet->cell('A'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $dia = $datosconsulta->dia;
                      $cell->SetValue($dia);

                  });

                  $sheet->cell('B'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fecha = $datosconsulta->cfecha;
                      $cell->SetValue($fecha);

                  });

                  $sheet->cell('C'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $entrada = $datosconsulta->entrada;
                      $cell->SetValue($entrada);

                  });

                  $sheet->cell('D'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $salida = $datosconsulta->salida;
                      $cell->SetValue($salida);

                  });

                  $sheet->cell('E'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $tardanza = $datosconsulta->tardanza;
                      $cell->SetValue($tardanza);

                  });

                  $sheet->cell('F'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $antesllegada = $datosconsulta->antes;
                      $cell->SetValue($antesllegada);

                  });

                   $sheet->cell('G'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $observacion = $datosconsulta->observaci;
                      $cell->SetValue($observacion);

                  });
                
                  $i=$i+1;

                  $finalregistros=$i;

                }

                $sheet->mergeCells("A".$finalregistros.":"."B".$finalregistros);

                $sheet->cell('A'.$finalregistros, function($cell) {
                    $cell->setValue('TOTAL TARDANZA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '12',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('left');
                });

                $sheet->cell('E'.$finalregistros,function($cell) use($totaltardanza){ 

                      $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                       )); 
                      $cell->setAlignment('center');
                      $totaltardanza = $totaltardanza;
                      $cell->SetValue($totaltardanza);

                });

                $finalregistros= $finalregistros +1;
                $sheet->mergeCells("A".$finalregistros.":"."B".$finalregistros);

                $sheet->cell('A'.$finalregistros, function($cell) {
                    $cell->setValue('TOTAL ANTES SALIDA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '12',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('left');
                });

                $sheet->cell('F'.$finalregistros,function($cell) use($totalantessalida){ 

                      $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                       )); 
                      $cell->setAlignment('center');
                      $totalantessalida = $totalantessalida;
                      $cell->SetValue($totalantessalida);

                });



            });
           
        });

        $miExcel = $miExcel->string('xlsx');
        $response = array(
            'name' => $titulo,
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($miExcel)
        );
        return $response;

    }


    public function PdfMarcaAsistenciaRepo(Request $request, $tipoBusqueda,$buscar,$dp_fedesde_marcaasistencia,$dp_fehasta_marcaasistencia,$estatus)
    {

            $lista = [];
            $columns = self::arregloColumnasMarcaAsistenciaRepo();

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

                 $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                 $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

            }
            else{
                $consulta = self::MarcaAsistenciaGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaMarcaAsistenciaRepo($consulta,$buscar,$columns);
            }

            $lista = self::obtener($consulta);

            $registrosporpaginas= 32;
            $contador= 1;
            $contadorpaginas= 1;
            $contadorregistros= 1;
            $estatusbusqueda=$estatus;

            //dd($lista);
            //die();

            $pdf = app('FPDF');
            $pdf->AliasNbPages();
            $pdf->AddPage('P');

            $fecharegistro = Carbon::now();
            $fecharegistro = $fecharegistro->format('d/m/Y');

            $horaregistro = Carbon::now();
            $horaregistro = $horaregistro->format('H:i:s:A');

            $i=0;

            $pdf->SetFont('Arial', 'B', 14);

            $fechaimpresion = utf8_decode($fecharegistro);
            $pdf->SetXY(130,10);
            $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

            $horaimpresion = utf8_decode($horaregistro);
            $pdf->SetXY(130,16);
            $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

            $pdf->SetXY(105,30);
            $titulo = utf8_decode("REGISTRO MARCA ASISTENCIA");
            $pdf->Cell(5, 1, $titulo, '', 1, 'C');

            $pdf->Ln(8);

            if($tipoBusqueda=="especifico") {

                if(($estatusbusqueda!="todos")){

                    $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                    $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                    $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                    $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                    $pdf->SetXY(60,35);
                    $titulo = utf8_decode("DESDE:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(85,35);
                    $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                    $pdf->SetXY(115,35);
                    $titulo = utf8_decode("HASTA:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(140,35);
                    $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                }

            }    

            $pdf->SetFont('Arial', 'B', 12);

            $pdf->SetXY(9,55);
            $titulodia = utf8_decode("DIA");
            $pdf->Cell(10, 1, $titulodia, '', 1, 'C');

            $pdf->SetXY(32,55);
            $titulofecha = utf8_decode("FECHA");
            $pdf->Cell(10, 1, $titulofecha, '', 1, 'C');

            $pdf->SetXY(58,55);
            $tituloentrada = utf8_decode("ENTRADA");
            $pdf->Cell(10, 1, $tituloentrada, '', 1, 'C');

            $pdf->SetXY(77,55);
            $titulosalida = utf8_decode("SALIDA");
            $pdf->Cell(20, 1, $titulosalida, '', 1, 'C');

            $pdf->SetXY(102,55);
            $titulotardanza = utf8_decode("TARDANZA");
            $pdf->Cell(20, 1, $titulotardanza, '', 1, 'C');

            $pdf->SetXY(135,55);
            $tituloantessalida = utf8_decode("ANTES SALIDA");
            $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

            $pdf->SetXY(172,55);
            $tituloantessalida = utf8_decode("OBSERVACION");
            $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

            $posicioninicial = 49;

            $posicionXdetalle = $posicioninicial + 15;

            $contador=1;

            $pdf->SetXY(15,265);
            $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C'); 


            foreach ($lista as $datosconsulta) {


                if($registrosporpaginas==$contador){

                    //$pdf = app('FPDF');
          
                    $pdf->AddPage('P');

                    $pdf->SetFont('Arial', 'B', 14);

                    $fechaimpresion = utf8_decode($fecharegistro);
                    $pdf->SetXY(130,10);
                    $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

                    $horaimpresion = utf8_decode($horaregistro);
                    $pdf->SetXY(130,16);
                    $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

                    $pdf->SetXY(105,30);
                    $titulo = utf8_decode("REGISTRO MARCA ASISTENCIA");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');

                    if($tipoBusqueda=="especifico") {

                        //dd($estatus);
                        //die();

                        if(($estatusbusqueda!="todos")){

                            $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                            $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                            $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                            $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                            $pdf->SetXY(60,35);
                            $titulo = utf8_decode("DESDE:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(85,35);
                            $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                            $pdf->SetXY(115,35);
                            $titulo = utf8_decode("HASTA:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(140,35);
                            $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                        }

                    }

                    $contador=1;

                    $contadorpaginas=  $contadorpaginas + 1; 

                    

                    $pdf->SetXY(15,265);
                    $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C');

                    $posicioninicial = 55;

                    $posicionXdetalle = $posicioninicial + 10; 

                    $pdf->SetFont('Arial', 'B', 12);

                      $pdf->SetXY(10,47);
                        $dni = $datosconsulta->dni;
                        $nombre = $datosconsulta->nombre;
                        $pdf->Cell(5, 1, $dni."-".$nombre, '', 1, 'L');

                    $pdf->SetXY(9,$posicioninicial);
                    $titulodia = utf8_decode("DIA");
                    $pdf->Cell(10, 1, $titulodia, '', 1, 'C');

                    $pdf->SetXY(32,$posicioninicial);
                    $titulofecha = utf8_decode("FECHA");
                    $pdf->Cell(10, 1, $titulofecha, '', 1, 'C');

                    $pdf->SetXY(58,$posicioninicial);
                    $tituloentrada = utf8_decode("ENTRADA");
                    $pdf->Cell(10, 1, $tituloentrada, '', 1, 'C');

                    $pdf->SetXY(77,$posicioninicial);
                    $titulosalida = utf8_decode("SALIDA");
                    $pdf->Cell(20, 1, $titulosalida, '', 1, 'C');

                    $pdf->SetXY(102,$posicioninicial);
                    $titulotardanza = utf8_decode("TARDANZA");
                    $pdf->Cell(20, 1, $titulotardanza, '', 1, 'C');

                    $pdf->SetXY(135,$posicioninicial);
                    $tituloantessalida = utf8_decode("ANTES SALIDA");
                    $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

                    $pdf->SetXY(172,$posicioninicial);
                    $tituloantessalida = utf8_decode("OBSERVACION");
                    $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');


                }  

                if($i==0){

                        $pdf->SetXY(10,47);
                        $dni = $datosconsulta->dni;
                        $nombre = $datosconsulta->nombre;
                        $pdf->Cell(5, 1, $dni."-".$nombre, '', 1, 'L');
                }
                $i=$i+1;

                $pdf->SetFont('Arial', '', 12);

                $dia = $datosconsulta->dia;
                $dia = utf8_decode($dia);
                $pdf->SetXY(10,$posicionXdetalle);
                $pdf->Cell(25, 1, $dia, '', 1, 'L');

                $fecha = $datosconsulta->cfecha;
                $fecha = utf8_decode($fecha);
                $pdf->SetXY(25,$posicionXdetalle);
                $pdf->Cell(25, 1,$fecha, '', 1, 'L');

                $entrada = $datosconsulta->entrada;
                $entrada = utf8_decode($entrada);
                $pdf->SetXY(56,$posicionXdetalle);
                $pdf->Cell(25, 1,$entrada, '', 1, 'L');

                $salida = $datosconsulta->salida;
                $salida = utf8_decode($salida);
                $pdf->SetXY(80,$posicionXdetalle);
                $pdf->Cell(25, 1,$salida, '', 1, 'L');

                $tardanza = $datosconsulta->tardanza;
                $tardanza = utf8_decode($tardanza);
                $pdf->SetXY(105,$posicionXdetalle);
                $pdf->Cell(25, 1,$tardanza, '', 1, 'L');

                $antessalida = $datosconsulta->antes;
                $antessalida = utf8_decode($antessalida);
                $pdf->SetXY(138,$posicionXdetalle);
                $pdf->Cell(25, 1,$antessalida, '', 1, 'L');

                $observacion = $datosconsulta->observaci;
                $observacion = utf8_decode($observacion);
                $pdf->SetXY(166,$posicionXdetalle);


                if(($observacion=="")){

                    $observacion="OK";
                       
                }
                
                $pdf->Cell(25, 1,$observacion, '', 1, 'L');

                $posicionXdetalle = $posicionXdetalle + 6;

                //$contador=1;

                //$contadorpaginas=  $contadorpaginas + 1;

                $contador=  $contador + 1;

                $contadorregistros = $contadorregistros + 1;

               


            }

        $pdf->SetFont('Arial', 'B', 12); 

       
        $totaltardanza = self::totalizartardanza($lista);
        $totalantessalida = self::totalizarantes($lista);

        $posicionXdetalle = $posicionXdetalle + 6;

        $pdf->SetXY(20,$posicionXdetalle);
        $titulototaltardanza = utf8_decode("TOTAL TARDANZA");
        $pdf->Cell(20, 1, $titulototaltardanza, '', 1, 'C'); 
        
        $pdf->SetXY(106,$posicionXdetalle);
        $pdf->Cell(25, 1,$totaltardanza, '', 1, 'L');

        $posicionXdetalle = $posicionXdetalle + 6;

        $pdf->SetXY(24,$posicionXdetalle);
        $titulototalantes = utf8_decode("TOTAL ANTES SALIDA");
        $pdf->Cell(20, 1, $titulototalantes, '', 1, 'C'); 
        
        $pdf->SetXY(139,$posicionXdetalle);
        $pdf->Cell(25, 1,$totalantessalida, '', 1, 'L');
        

        $pdf->output();

        //dd($pdf);
        //die();
        exit;


    }

     public function obtenerfotopersonaconsultas()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }

    public function obtenernombrepersonaconsultas()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }


}
