<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;



class RepoMarcaAsistenciaController extends Controller
{
    
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";


   
    public function arregloColumnasMarcaAsistenciaRepo()
    {
        return array(
            0 => 'dia',
            1 => 'cfecha',
            2 => 'entrada',
            3 => 'salida',
            4 => 'tardanza',
            5 => 'antes',
            6 => 'observaci'
        );
    }


   

     public function contar($consulta)
    {
        return $consulta->count();
    }

    
    public function obtener($consulta)
    {
        return $consulta->get();
    }

    
    private function limitarConsulta($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {
            $consulta->offset($inicio)
                ->limit($limite);
        }

        return $consulta;
    }

    
    private function ordenarConsulta($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    
    private function buscarEnConsultaMarcaAsistenciaRepo($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


   
    public function MarcaAsistenciaRepo(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonareportemarcaasistencia();

        $nombrepersonaaccesa= self::obtenernombrepersonareportemarcaasistencia();

        return view('admin.repomarcaasistencia.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function MarcaAsistenciaEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();


        $consulta = DB::table('bpdata')
                ->select(
                    'codigo',
                    'dni',
                    'nombre',
                    DB::raw('DATE_FORMAT(cfecha, "%d/%m/%Y") as cfecha'),
                    'cfecha as fecha',
                    'entrada',
                    'salida',
                    'marca1',
                    'marca2',
                    'marca3',
                    'marca4',
                    'sal1',
                    'sal2',
                    'observaci',
                    'dia',
                    'tardanza',
                    'antes',
                    'feriado',
                    'descanso',
                    'marcae'
                )
                ->distinct()
                ->whereBetween('cfecha',[$fe_desde,$fe_hasta])
                ->where('codigo',$co_usuario)
                ->orderBy('fecha', 'asc');
                //->orderBy('cfecha', 'desc');

       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    

    private function MarcaAsistenciaGeneral()
    {

        //$fecha = Carbon::now();
        //$fecha = $fecha->format('Ymd');
        //dd($fecha);

        $fecha_actual = Carbon::now();

        //LA FECHA DESDE Y LA FECHA HASTA SE ARMA DE ESTA MANERA PARA QUE MUESTREN 
        //LA INFORMACION DE TODO EL MES

        $monthfinal = $fecha_actual->format('m');
        $yearfinal  = $fecha_actual->format('Y');

        if ($monthfinal == 01) {
            $monthdesde = 12;
            $yeardesde  = $yearfinal;

        } else {
            $monthdesde = $monthfinal;
            $yeardesde  = $yearfinal;
        }

        $daydesde = "01";
        $fe_desde = $yeardesde . '-' . $monthdesde . '-' . $daydesde;

        $dayfinal = "31";
        $fe_hasta = $yearfinal . '-' . $monthfinal . '-' . $dayfinal;

        $fechadesde = explode("-",$fe_desde);
        $fechahasta = explode("-",$fe_hasta);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        return DB::table('bpdata')
                ->select(
                    'codigo',
                    'dni',
                    'nombre',
                    DB::raw('DATE_FORMAT(cfecha, "%d/%m/%Y") as cfecha'),
                    'cfecha as fecha',
                    'entrada',
                    'salida',
                    'marca1',
                    'marca2',
                    'marca3',
                    'marca4',
                    'sal1',
                    'sal2',
                    'observaci',
                    'dia',
                    'tardanza',
                    'antes',
                    'feriado',
                    'descanso',
                    'marcae'
                )
                ->distinct()
                ->whereBetween('cfecha',[$fechadesdeformateada,$fechahastaformateada])
                ->where('codigo',$co_usuario)
                ->orderBy('fecha', 'asc');

    }


   
     private function BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    
    public function ListarMarcaAsistenciaRepo(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasMarcaAsistenciaRepo();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico
        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_marcaasistencia  = $request->input('dp_fedesde_marcaasistencia');
            $dp_fehasta_marcaasistencia  = $request->input('dp_fehasta_marcaasistencia');

            $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
            $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


            $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::MarcaAsistenciaGeneral();
          
        }

        $consulta  = self::buscarEnConsultaMarcaAsistenciaRepo($consulta, $buscar, $columns);

        $total = self::contar($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsulta($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listamarcaasistencia = self::obtener($consulta);

        //dd($listamarcaasistencia);
        //die();

       
        //dd($totaltardanza);
        //die();

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listamarcaasistencia,

        );

     

        return response()->json($json_data);
    
    }


    

    public function TotalizarCantidadRegistros(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasMarcaAsistenciaRepo();

        $limite = $request->input('length');
        $inicio = $request->input('start');
       // $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');
        
        $total = 0;
        $tipoBusqueda =  $request['tipo'];


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico
        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            $dp_fedesde_marcaasistencia  = $request['dp_fedesde_marcaasistencia'];
            $dp_fehasta_marcaasistencia  = $request['dp_fehasta_marcaasistencia'];

            $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
            $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


            $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{

            $consulta = self::MarcaAsistenciaGeneral();
        }

        $total = self::contar($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsulta($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listamarcaasistencia = self::obtener($consulta);

        //dd($listamarcaasistencia);
        //die();

        $json_data = array (
                "mensaje" => "creado",
                "cantidadregistros" => $total,
        );


        return response()->json($json_data);
    
    }

   

    public function totalizartardanza($listamarcaasistencia)
    {

        $totalminutostardanza=0;
        $totalhorastardanza=0;
        $totaltardanza=0;
        $totalminutosredonde=0;
        //$totalminutosredonde1=0;
        $i=0;
        $arrayminutos= [];

        //dd($listamarcaasistencia);
        //die();

        foreach ($listamarcaasistencia as $datosconsulta) {

            $tardanza = $datosconsulta->tardanza;

            if ($tardanza!=""){

                 //dd($tardanza);
                 //die();
                $tardanzasplit= explode(":",$tardanza);

                //dd($tardanzasplit[0].$tardanzasplit[1]);
                //die();

                if(($tardanzasplit[1] !="00")){

                    $conversionminutos= ($tardanzasplit[1])/60;

                    $conversionminutos = floatval($conversionminutos);

                    $conversionminutos = round($conversionminutos,2);

                    //$arrayminutos[$i]= $tardanzasplit[1]."-".$conversionminutos;                 

                    $totalminutostardanza= floatval($totalminutostardanza)+floatval($conversionminutos);

                    $totalminutosredonde= floatval($totalminutostardanza);

                    $totalhorastardanza= floatval($totalhorastardanza)+floatval($tardanzasplit[0]);

                    $totalhorastardanza= round($totalhorastardanza,2);

                    //$arrayminutos[$i]= $tardanzasplit[0].$tardanzasplit[1]."-".$conversionminutos."-".$totalminutosredonde;

                    $totaltardanza= floatval($totalhorastardanza)+floatval($totalminutosredonde);

                    $i=$i+1;

                }
                else{

                    if(($tardanzasplit[0]!="00")&&($tardanzasplit[1]=="00")){

                        //dd("pase por aqui");
                        //die();

                        $totalhorastardanza= floatval($totalhorastardanza)+floatval($tardanzasplit[0]);

                        $totalhorastardanza= round($totalhorastardanza,2);

                        $totaltardanza= floatval($totalhorastardanza);

                        // $arrayminutos[$i]= $tardanzasplit[0]."-".$totaltardanza;


                        $i=$i+1;

                    }
                }

            }
        }

        //dd($arrayminutos);
        //die();

        return($totaltardanza);


    }

   

    public function totalizarantes($listamarcaasistencia)
    {

        $totalminutosantessalida=0;
        $totalhorasantessalida=0;
        $totalantessalida=0;
        $totalgeneral=0;
        $arrayminutos= [];
        $i=0;

        //dd($listamarcaasistencia);
        //die();

        foreach ($listamarcaasistencia as $datosconsulta) {

            $antessalida = $datosconsulta->antes;

            if ($antessalida!=""){

                 //dd($tardanza);
                 //die();

                $antessalidasplit= explode(":",$antessalida);


                if(($antessalidasplit[1] !="00")){
                    
                    //dd("pase por aqui".$antessalidasplit[0].$antessalidasplit[1]);
                    //die();
                    

                    $conversionminutos= ($antessalidasplit[1])/60;

                    $conversionminutos = floatval($conversionminutos);

                    $conversionminutos = round($conversionminutos,2);

                    $totalminutosantessalida= floatval($totalminutosantessalida)+floatval($conversionminutos);

                    //dd($conversionminutos.$totalminutosantessalida);
                    //die();

                    $totalminutosredonde= floatval($totalminutosantessalida);

                    //dd($conversionminutos.$totalminutosantessalida.$totalminutosredonde);
                    //die();


                    $totalhorasantessalida= floatval($totalhorasantessalida)+floatval($antessalidasplit[0]);

                    $totalhorasantessalida= round($totalhorasantessalida,2);

                    $totalantessalida= floatval($totalhorasantessalida)+floatval($totalminutosredonde);
                    
                    $totalgeneral= $totalgeneral + $totalantessalida;
                    
                    //dd($totalgeneral);
                    //die();

                    //$arrayminutos[$i]= $tardanzasplit[1]."-".$conversionminutos."-".$totalminutosredonde;

                    $i=$i+1;

                }

                else{

                    if(($antessalidasplit[0]!="00")&&($antessalidasplit[1]=="00")){

                        //dd("pase por aqui".$antessalidasplit[0].$antessalidasplit[1]);
                        //die();

                        $totalhorasantessalida= floatval($totalhorasantessalida)+floatval($antessalidasplit[0]);
                        
                        //dd("pase por aqui". $totalhorasantessalida);
                        //die();

                        $totalantessalidahoras= round($totalhorasantessalida,2);
                        
                        //dd("pase por aqui". $totalantessalidahoras);
                        //die();

                        //$totalantessalidahoras= floatval($totalantessalida);
                        
                        //dd("pase por aqui". $totalantessalidahoras);
                        //die();
                        
                        //dd($totalgeneral.$totalantessalidahoras.$totalantessalidahoras);
                        //die();
                        
                        $totalgeneral= $totalgeneral + $totalantessalidahoras;

                        // $arrayminutos[$i]= $tardanzasplit[0]."-".$totaltardanza;


                        $i=$i+1;

                    }
                }

            }

            //dd($totalminutosantessalida);
            //die();


        }

        //dd($arrayminutos);
        //die();

        return($totalgeneral);
    }


   
    public function ExcelMarcaAsistenciaRepo(Request $request)
    {
        if ($request->ajax()) {
            $lista = [];
            $columns = self::arregloColumnasMarcaAsistenciaRepo();
            $titulo = 'Marca Asistencia';
            $buscar = $request['buscar'];
            $tipoBusqueda = $request['tipo'];

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            
                 $dp_fedesde_marcaasistencia  = $request->input('dp_fedesde_marcaasistencia');
                 $dp_fehasta_marcaasistencia  = $request->input('dp_fehasta_marcaasistencia');

                 $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                 $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

            }
            else{
                $consulta = self::MarcaAsistenciaGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

             if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaMarcaAsistenciaRepo($consulta,$buscar,$columns);
            }

            $lista = self::obtener($consulta);

            $totaltardanza = self::totalizartardanza($lista);
            $totalantessalida = self::totalizarantes($lista);

            $titulo= "Marca Asistencia";

            return response()->json(self::crearExcel($titulo,$lista,$totaltardanza,$totalantessalida));


            //return response()->json(self::crearExcel($titulo,$lista));
        }
            

    }


   
    private function crearExcel($titulo,$lista,$totaltardanza,$totalantessalida)
    {
          
        $miExcel = Excel::create('Laravel Excel', function($excel) use ($lista,$totaltardanza,$totalantessalida) {

            $excel->sheet('Shetname', function ($sheet) use ($lista,$totaltardanza,$totalantessalida){


                // PARA HORIZONTAL
                //$sheet->getPageSetup()->setOrientation("landscape");

                $sheet->getPageSetup()->setOrientation("portrait");

                $sheet->mergeCells("A1:G1");

                $sheet->cell('A1', function($cell) {
                    $cell->setValue('REGISTRO MARCA ASISTENCIA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                //$sheet->getStyle("A2".":G2")->getAlignment()->setWrapText(true); 
               
                $sheet->mergeCells("A2:G2");
                $sheet->mergeCells("A3:G3");

                $sheet->cell('A4', function($cell) {
                    $cell->setValue('DIA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    //$cell->setFontSize(14);
                    //$cell->setFontFamily('Arial Black');
                    $cell->setAlignment('center');
                });

                $sheet->cell('B4', function($cell) {
                    $cell->setValue('FECHA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('C4', function($cell) {
                    $cell->setValue('ENTRADA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('D4', function($cell) {
                    $cell->setValue('SALIDA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('E4', function($cell) {
                    $cell->setValue('TARDANZA');
                   $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('F4', function($cell) {
                    $cell->setValue('ANTES DE SALIDA');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });

                $sheet->cell('G4', function($cell) {
                    $cell->setValue('OBSERVACIONES');
                    $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '14',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('center');
                });


                $i=5;

                foreach ($lista as $datosconsulta) {

                    if($i==5){

                       $sheet->mergeCells("A2:G2");

                       $sheet->cell('A2',function($cell) use($datosconsulta){
                         $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                         )); 
                        //$cell->setFontSize(12);
                        $cell->setAlignment('left');
                        $dni = $datosconsulta->dni;
                        $nombre = $datosconsulta->nombre;
                        $cell->SetValue($dni."-".$nombre );
                       });

                    }


                 //dd($d->sucursal);

                  $sheet->cell('A'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $dia = $datosconsulta->dia;
                      $cell->SetValue($dia);

                  });

                  $sheet->cell('B'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $fecha = $datosconsulta->cfecha;
                      $cell->SetValue($fecha);

                  });

                  $sheet->cell('C'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $entrada = $datosconsulta->entrada;
                      $cell->SetValue($entrada);

                  });

                  $sheet->cell('D'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $salida = $datosconsulta->salida;
                      $cell->SetValue($salida);

                  });

                  $sheet->cell('E'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $tardanza = $datosconsulta->tardanza;
                      $cell->SetValue($tardanza);

                  });

                  $sheet->cell('F'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('center');
                      $antesllegada = $datosconsulta->antes;
                      $cell->SetValue($antesllegada);

                  });

                   $sheet->cell('G'.$i,function($cell) use($datosconsulta){ 

                      $cell->setFontSize(12);
                      $cell->setAlignment('left');
                      $observacion = $datosconsulta->observaci;
                      $cell->SetValue($observacion);

                  });
                
                  $i=$i+1;

                  $finalregistros=$i;

                }

                $sheet->mergeCells("A".$finalregistros.":"."B".$finalregistros);

                $sheet->cell('A'.$finalregistros, function($cell) {
                    $cell->setValue('TOTAL TARDANZA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '12',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('left');
                });

                $sheet->cell('E'.$finalregistros,function($cell) use($totaltardanza){ 

                      $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                       )); 
                      $cell->setAlignment('center');
                      $totaltardanza = $totaltardanza;
                      $cell->SetValue($totaltardanza);

                });

                $finalregistros= $finalregistros +1;
                $sheet->mergeCells("A".$finalregistros.":"."B".$finalregistros);

                $sheet->cell('A'.$finalregistros, function($cell) {
                    $cell->setValue('TOTAL ANTES SALIDA');
                     $cell->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '12',
                    'bold'       =>  true
                    ));
                    $cell->setAlignment('left');
                });

                $sheet->cell('F'.$finalregistros,function($cell) use($totalantessalida){ 

                      $cell->setFont(array(
                            'family'     => 'Calibri',
                            'size'       => '12',
                            'bold'       =>  true
                       )); 
                      $cell->setAlignment('center');
                      $totalantessalida = $totalantessalida;
                      $cell->SetValue($totalantessalida);

                });



            });
           
        });

        $miExcel = $miExcel->string('xlsx');
        $response = array(
            'name' => $titulo,
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($miExcel)
        );
        return $response;

    }


    
    public function PdfMarcaAsistenciaRepo(Request $request, $tipoBusqueda,$buscar,$dp_fedesde_marcaasistencia,$dp_fehasta_marcaasistencia,$estatus)
    {

            $lista = [];
            $columns = self::arregloColumnasMarcaAsistenciaRepo();

            //dd($tipoBusqueda);
            //die();


            if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

                 $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                 $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                 $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
                 $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

               //dd($tipoBusqueda.$dp_fedesde_marcaasistencia.$dp_fedesde_marcaasistencia);

                $consulta = self::MarcaAsistenciaEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

            }
            else{
                $consulta = self::MarcaAsistenciaGeneral();
                //dd($consulta);
                //dd($consulta->tosql());
                //$consulta = self::BuscarEnConsultaFiltroMarcaAsistencia($consulta, $buscar);

           
            }

            if($buscar!="todos"){
                $consulta  = self::buscarEnConsultaMarcaAsistenciaRepo($consulta,$buscar,$columns);
            }

            $registrosporpaginas= 20;
            $contador= 1;
            $contadorpaginas= 1;
            $contadorregistros= 1;
            $estatusbusqueda=$estatus;

            $lista = self::obtener($consulta);

            //dd($lista);
            //die();

            $pdf = app('FPDF');
            //$pdf->AddPage('P');
            $pdf->AliasNbPages();
            $pdf->AddPage('L','A4');

            $fecharegistro = Carbon::now();
            $fecharegistro = $fecharegistro->format('d/m/Y');

            $horaregistro = Carbon::now();
            $horaregistro = $horaregistro->format('H:i:s:A');
            
            //$numeropag=$pdf->PageNo();

            $pdf->Image('img/logoempresa.png',10,4,25);

            $i=0;

            $pdf->SetFont('Arial', 'B', 14);

            $fechaimpresion = utf8_decode($fecharegistro);
            $pdf->SetXY(200,16);
            $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

            $horaimpresion = utf8_decode($horaregistro);
            $pdf->SetXY(200,22);
            $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

            $pdf->SetXY(150,35);
            $titulo = utf8_decode("REGISTRO MARCA ASISTENCIA");
            $pdf->Cell(5, 1, $titulo, '', 1, 'C');

           if($tipoBusqueda=="especifico") {

                if(($estatusbusqueda!="todos")){

                    $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                    $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                    $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                    $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                    $pdf->SetXY(110,43);
                    $titulo = utf8_decode("DESDE:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(135,43);
                    $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                    $pdf->SetXY(165,43);
                    $titulo = utf8_decode("HASTA:");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                    $pdf->SetXY(190,43);
                    $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');
                }

            }    


            $pdf->SetFont('Arial', 'B', 12);

            $pdf->SetXY(9,55);
            $titulodia = utf8_decode("DIA");
            $pdf->Cell(10, 1, $titulodia, '', 1, 'C');

            $pdf->SetXY(32,55);
            $titulofecha = utf8_decode("FECHA");
            $pdf->Cell(10, 1, $titulofecha, '', 1, 'C');

            $pdf->SetXY(58,55);
            $tituloentrada = utf8_decode("ENTRADA");
            $pdf->Cell(10, 1, $tituloentrada, '', 1, 'C');

            $pdf->SetXY(77,55);
            $titulosalida = utf8_decode("SALIDA");
            $pdf->Cell(20, 1, $titulosalida, '', 1, 'C');

            $pdf->SetXY(102,55);
            $titulotardanza = utf8_decode("TARDANZA");
            $pdf->Cell(20, 1, $titulotardanza, '', 1, 'C');

            $pdf->SetXY(135,55);
            $tituloantessalida = utf8_decode("ANTES SALIDA");
            $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

            $pdf->SetXY(172,55);
            $tituloantessalida = utf8_decode("OBSERVACION");
            $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

            $posicioninicial = 49;

            $posicionXdetalle = $posicioninicial + 15;

            $contador=1;

            $pdf->SetXY(15,179);
            $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C'); 


            foreach ($lista as $datosconsulta) {

                if($registrosporpaginas==$contador){

                    //$pdf = app('FPDF');
          
                    $pdf->AddPage('L','A4');

                    $fecharegistro = Carbon::now();
                    $fecharegistro = $fecharegistro->format('d/m/Y');

                    $horaregistro = Carbon::now();
                    $horaregistro = $horaregistro->format('H:i:s:A');
            
                    //$numeropag=$pdf->PageNo();

                    $pdf->Image('img/logoempresa.png',10,4,25);

                    $pdf->SetFont('Arial', 'B', 14);

                    $fechaimpresion = utf8_decode($fecharegistro);
                    $pdf->SetXY(200,16);
                    $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

                    $horaimpresion = utf8_decode($horaregistro);
                    $pdf->SetXY(200,22);
                    $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

                    $pdf->SetXY(150,35);
                    $titulo = utf8_decode("REGISTRO MARCA ASISTENCIA");
                    $pdf->Cell(5, 1, $titulo, '', 1, 'C');

                    if($tipoBusqueda=="especifico") {

                        //dd($estatus);
                        //die();

                        if(($estatusbusqueda!="todos")){

                            $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
                            $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

                            $fechadesdeformateadatitulo =  $fechadesde[2]."/".$fechadesde[1]."/".$fechadesde[0];
                            $fechahastaformateadatitulo =  $fechahasta[2]."/".$fechahasta[1]."/".$fechahasta[0];

                            $pdf->SetXY(110,43);
                            $titulo = utf8_decode("DESDE:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(135,43);
                            $pdf->Cell(5, 1, $fechadesdeformateadatitulo, '', 1, 'C');

                            $pdf->SetXY(165,43);
                            $titulo = utf8_decode("HASTA:");
                            $pdf->Cell(5, 1, $titulo, '', 1, 'C');
                            $pdf->SetXY(190,43);
                            $pdf->Cell(5, 1, $fechahastaformateadatitulo, '', 1, 'C');


                            $pdf->SetFont('Arial', 'B', 12);
                            $pdf->SetXY(9,50);
                            $dni = $datosconsulta->dni;
                            $nombre = $datosconsulta->nombre;
                            $pdf->Cell(5, 1, $dni."-".$nombre, '', 1, 'L');

                           
                        }

                    }

                    $contador=1;

                    $contadorpaginas=  $contadorpaginas + 1; 

                    

                    $pdf->SetXY(15,179);
                    $pdf->Cell(0,10,'Pagina '.$contadorpaginas.'/{nb}',0,0,'C');

                    $posicioninicial = 55;

                    $posicionXdetalle = $posicioninicial + 10; 

                    $pdf->SetFont('Arial', 'B', 12);

                    $pdf->SetXY(9,$posicioninicial);
                    $titulodia = utf8_decode("DIA");
                    $pdf->Cell(10, 1, $titulodia, '', 1, 'C');

                    $pdf->SetXY(32,$posicioninicial);
                    $titulofecha = utf8_decode("FECHA");
                    $pdf->Cell(10, 1, $titulofecha, '', 1, 'C');

                    $pdf->SetXY(58,$posicioninicial);
                    $tituloentrada = utf8_decode("ENTRADA");
                    $pdf->Cell(10, 1, $tituloentrada, '', 1, 'C');

                    $pdf->SetXY(77,$posicioninicial);
                    $titulosalida = utf8_decode("SALIDA");
                    $pdf->Cell(20, 1, $titulosalida, '', 1, 'C');

                    $pdf->SetXY(102,$posicioninicial);
                    $titulotardanza = utf8_decode("TARDANZA");
                    $pdf->Cell(20, 1, $titulotardanza, '', 1, 'C');

                    $pdf->SetXY(135,$posicioninicial);
                    $tituloantessalida = utf8_decode("ANTES SALIDA");
                    $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');

                    $pdf->SetXY(172,$posicioninicial);
                    $tituloantessalida = utf8_decode("OBSERVACION");
                    $pdf->Cell(20, 1, $tituloantessalida, '', 1, 'C');


                }  


                if($i==0){

                        $pdf->SetXY(9,50);
                        $dni = $datosconsulta->dni;
                        $nombre = $datosconsulta->nombre;
                        $pdf->Cell(5, 1, $dni."-".$nombre, '', 1, 'L');
                }
                $i=$i+1;

                $pdf->SetFont('Arial', '', 12);

                $dia = $datosconsulta->dia;
                $dia = utf8_decode($dia);
                $pdf->SetXY(10,$posicionXdetalle);
                $pdf->Cell(25, 1, $dia, '', 1, 'L');

                $fecha = $datosconsulta->cfecha;
                $fecha = utf8_decode($fecha);
                $pdf->SetXY(25,$posicionXdetalle);
                $pdf->Cell(25, 1,$fecha, '', 1, 'L');

                $entrada = $datosconsulta->entrada;
                $marca1 = $datosconsulta->marca1;
                
                $entradasplit= explode(":",$entrada);
                $marca1split= explode(":",$marca1);
                
                $conversionmarca1=0;
                $conversionentrada=0;
                
                if(($entradasplit[0])!="00" && ($entradasplit[0])!=null){
                    
                     $conversionminutos= ($entradasplit[1])/60;

                     $conversionminutos = floatval($conversionminutos);

                     $conversionminutos = round($conversionminutos,2);
                     
                     $conversionentrada=  floatval($entradasplit[0])+floatval($conversionminutos);
                }
                
                if(($marca1split[0])!="00" && ($marca1split[0])!=null){
                
                     $conversionminutosmarca1= ($marca1split[1])/60;

                     $conversionminutosmarca1 = floatval($conversionminutosmarca1);

                     $conversionminutosmarca1 = round($conversionminutosmarca1,2);
                     
                     $conversionmarca1=  floatval($marca1split[0])+floatval($conversionminutosmarca1);
                }
                    
                if($conversionmarca1>$conversionentrada){
                    
                       $marca1 = utf8_decode($marca1);
                       $pdf->SetXY(56,$posicionXdetalle);
                       $pdf->Cell(25, 1,$marca1, '', 1, 'L');
                        
                }
                else{
                        
                       $entrada = utf8_decode($entrada);
                       $pdf->SetXY(56,$posicionXdetalle);
                       $pdf->Cell(25, 1,$entrada, '', 1, 'L');
                }
                
                
                $salida = $datosconsulta->salida;
                $marcae = $datosconsulta->marcae;
                
                $salidasplit= explode(":",$salida);
                $marcaesplit= explode(":",$marcae);
                
                
                $conversionmarcae=0;
                $conversionsalida=0;
                
                if(($salidasplit[0])!="00" && ($salidasplit[0])!=null){
                    
                     $conversionminutos= ($salidasplit[1])/60;

                     $conversionminutos = floatval($conversionminutos);

                     $conversionminutos = round($conversionminutos,2);
                     
                     $conversionsalida=  floatval($salidasplit[0])+floatval($conversionminutos);
                }
                
                //dd($marca1split);
                //die();
                
                if(($marcaesplit[0])!="00" && ($marcaesplit[0])!=null){
                
                     $conversionminutosmarcae= ($marcaesplit[1])/60;

                     $conversionminutosmarcae = floatval($conversionminutosmarcae);

                     $conversionminutosmarcae = round($conversionminutosmarcae,2);
                     
                     $conversionmarcae=  floatval($marcaesplit[0])+floatval($conversionminutosmarcae);
                    
                }
                
                if($conversionmarcae==0){
                    
                    $salida = $datosconsulta->salida;
                    $salida = utf8_decode($salida);
                    $pdf->SetXY(80,$posicionXdetalle);
                    $pdf->Cell(25, 1,$salida, '', 1, 'L');
                    
                }
                else{
                    
                    if($conversionmarcae<$conversionsalida){
                    
                        $marcae = utf8_decode($marcae);
                        $pdf->SetXY(80,$posicionXdetalle);
                        $pdf->Cell(25, 1,$marcae, '', 1, 'L');
                        
                    }
                    else{
                    
                        $salida = $datosconsulta->salida;
                        $salida = utf8_decode($salida);
                        $pdf->SetXY(80,$posicionXdetalle);
                        $pdf->Cell(25, 1,$salida, '', 1, 'L');
                       
                    }
                    
                }
                
                
                $tardanza = $datosconsulta->tardanza;
                $tardanza = utf8_decode($tardanza);
                $pdf->SetXY(105,$posicionXdetalle);
                $pdf->Cell(25, 1,$tardanza, '', 1, 'L');

                $antessalida = $datosconsulta->antes;
                $antessalida = utf8_decode($antessalida);
                $pdf->SetXY(138,$posicionXdetalle);
                $pdf->Cell(25, 1,$antessalida, '', 1, 'L');

                $observacion = $datosconsulta->observaci;
                $observacion = utf8_decode($observacion);
                
                $tardanza= $datosconsulta->tardanza;
                $antes= $datosconsulta->antes;
                    
                if(($tardanza!="")|| ($antes!="")){
                    
                     $descripcionobservacion="ASISTENCIA CON OBSERVACIONES";
                     $pdf->SetXY(166,$posicionXdetalle);
                     $pdf->Cell(25, 1,$descripcionobservacion, '', 1, 'L');
                    
                }
                else{
                    
                     $descripcionobservacion="ASISTENCIA OK";
                     $pdf->SetXY(166,$posicionXdetalle);
                     $pdf->Cell(25, 1,$descripcionobservacion, '', 1, 'L');  
                    
                }
            
                
                //if(($observacion=="")){

                //    $observacion="OK";
                       
                //}

                //$pdf->SetXY(166,$posicionXdetalle);
                //$pdf->Cell(25, 1,$observacion, '', 1, 'L');

                $posicionXdetalle = $posicionXdetalle + 6;

                //$contador=1;

                //$contadorpaginas=  $contadorpaginas + 1;

                $contador=  $contador + 1;

                $contadorregistros = $contadorregistros + 1;

               


            }


        $pdf->SetFont('Arial', 'B', 12); 

       
        $totaltardanza = self::totalizartardanza($lista);
        $totalantessalida = self::totalizarantes($lista);

        $posicionXdetalle = $posicionXdetalle + 6;

        $pdf->SetXY(20,$posicionXdetalle);
        $titulototaltardanza = utf8_decode("TOTAL TARDANZA");
        $pdf->Cell(20, 1, $titulototaltardanza, '', 1, 'C'); 
        
        $pdf->SetXY(106,$posicionXdetalle);
        $pdf->Cell(25, 1,$totaltardanza, '', 1, 'L');

        $posicionXdetalle = $posicionXdetalle + 6;

        $pdf->SetXY(24,$posicionXdetalle);
        $titulototalantes = utf8_decode("TOTAL ANTES SALIDA");
        $pdf->Cell(20, 1, $titulototalantes, '', 1, 'C'); 
        
        $pdf->SetXY(139,$posicionXdetalle);
        $pdf->Cell(25, 1,$totalantessalida, '', 1, 'L');
        

        $pdf->output();

        //dd($pdf);
        //die();
        exit;


    }

   

     public function obtenerfotopersonareportemarcaasistencia()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }

   
    public function obtenernombrepersonareportemarcaasistencia()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }


   
    public function totaltardanzatotalantes($fedesde_marcaasistencia,$fehasta_marcaasistencia)
    {

        $dp_fedesde_marcaasistencia  = $fedesde_marcaasistencia;
        $dp_fehasta_marcaasistencia  = $fehasta_marcaasistencia;

        $fechadesde = explode("-",$dp_fedesde_marcaasistencia);
        $fechahasta = explode("-",$dp_fehasta_marcaasistencia);

        $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
        $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

        $buscar="";

        $consulta = self::MarcaAsistenciaEspecificareporte($buscar,$fechadesdeformateada,$fechahastaformateada);

        $total = self::contar($consulta);
        
        $listamarcaasistencia = self::obtener($consulta);

        //dd($listamarcaasistencia);
        //die();

        $totaltardanza = self::totalizartardanza($listamarcaasistencia);
        $totalantessalida = self::totalizarantes($listamarcaasistencia);

        $respuesta = array (
           "totaltardanza" => $totaltardanza,
           "totalantessalida" => $totalantessalida,
        );

        return response()->json($respuesta);

    }
    
    public function totaltardanzatotalanteslimpiar()
    {

        $consulta = self::MarcaAsistenciaGeneral();

        $total = self::contar($consulta);
        
        $listamarcaasistencia = self::obtener($consulta);

        //dd($listamarcaasistencia);
        //die();

        $totaltardanza = self::totalizartardanza($listamarcaasistencia);
        $totalantessalida = self::totalizarantes($listamarcaasistencia);

        $respuesta = array (
           "totaltardanza" => $totaltardanza,
           "totalantessalida" => $totalantessalida,
        );

        return response()->json($respuesta);

    }
    


   
    private function MarcaAsistenciaEspecificareporte($buscar,$fe_desde,$fe_hasta)
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $consulta = DB::table('bpdata')
                ->select(
                   'codigo',
                    'dni',
                    'nombre',
                    DB::raw('DATE_FORMAT(cfecha, "%d/%m/%Y") as cfecha'),
                    'entrada',
                    'salida',
                    'marca1',
                    'marca2',
                    'marca3',
                    'marca4',
                    'sal1',
                    'sal2',
                    'observaci',
                    'dia',
                    'tardanza',
                    'antes',
                    'feriado',
                    'descanso',
                    'marcae'
                )
                ->distinct()
                ->whereBetween('cfecha',[$fe_desde,$fe_hasta])
                ->orderBy('cfecha','asc')
                ->where('codigo',$co_usuario)
                ->offset(0)
                ->limit(31);
                //->get();
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }


}
