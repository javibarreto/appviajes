<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Viaje;
use App\Correos;


class ViajeController extends Controller
{

    public function viajes(Request $request)
    {

     
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.viaje.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarViajes(Request $request)
    {
        $viaje = Viaje::all();
        return response()->json($viaje);
    }


    public function CrearViaje(Request $request)
    {

        if ($request->ajax()) {

            $nombre = trim($request['nombre']);
            $plazas = trim($request['plazas']);
            $origen = trim($request['origen']);
            $destino = trim($request['destino']);
            $precio = trim($request['precio']);

            $viaje = Viaje::create([
                "co_viaje"  => $nombre,
                "nu_plazas"  => $plazas,
                "nb_origen"  => $origen,
                "nb_destino"  => $destino,
                "nu_precio"  => $precio,
            ]);


            $respuesta = array(
                "mensaje"  => "creado"
            );

            return response()->json($respuesta);

        }

    }

    public function editarViaje(Request $request)
    {
        if ($request->ajax()) {

            $id = $request['id'];
            $nombre = trim($request['nombre']);
            $plazas = trim($request['plazas']);
            $origen = trim($request['origen']);
            $destino = trim($request['destino']);
            $precio = trim($request['precio']);



            $casa = Viaje::where('id_viaje', $id)->update([
                "co_viaje"  => $nombre,
                "nu_plazas"  => $plazas,
                "nb_origen"  => $origen,
                "nb_destino"  => $destino,
                "nu_precio"  => $precio,
            ]);

            return response()->json([
                "mensaje" => "actualizado"
            ]);
        }
    }

    public function eliminarViaje(Request $request)
    {
        if ($request->ajax()) {

            $date = trim(Carbon::now());

            try { 
                $id = $request['id'];

                $viaje = Viaje::where('id_viaje', $id)->delete();

                return response()->json([
                    "mensaje" => "eliminado"
                ]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }
    }


    public function obtenerViaje(Request $request,$id)
    {
        if ($request->ajax()) {

            $viaje = DB::table('viaje')
            ->select(
                    'co_viaje',
                    'nu_plazas',
                    'nb_origen',
                    'nb_destino',
                    'nu_precio'
                    )
            ->where('id_viaje', $id)
            ->get();

            return response()->json($viaje);
        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
