<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PDF;
use App\IncWeb;
use App\PapeletasIncWeb;




class PapeletasController extends Controller
{

    
    private $tipoBusqueda = "especifico";


    

    //private $tipoBusqueda = "especifico";
    public function arregloColumnasPapeletas()
    {
        return array(
            0 =>  'papeletasincweb.id',
            1 =>  'fechasoli',
            2 =>  'horasoli',
            3 =>  'tiposolicitud',
            4 =>  'descripcio',
            5 =>  'fechaaprob',
            6 =>  'horaaprob',
            7 =>  'useraprob',
            8 =>  'observacion',
            9 =>  'personal.appaterno',
            10 => 'personal.apmaterno',
           
        );
    }


    

     public function contarPapeletas($consulta)
    {
        return $consulta->count();
    }

   
    public function obtenerPapeletas($consulta)
    {
        return $consulta->get();
    }

    
    private function limitarConsultaPapeletas($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {
            $consulta->offset($inicio)
                ->limit($limite);
        }

        return $consulta;
    }

   
    private function ordenarConsultaPapeletas($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    
    private function buscarEnConsultaPapeletas($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


   
    public function Papeletas(Request $request)
    {
        //dd("paso por aqui");
        //die();
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonapapeletas();

        $nombrepersonaaccesa= self::obtenernombrepersonapapeletas();

        return view('admin.papeletas.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    
     
    
    private function PapeletasEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $consulta = DB::table('papeletasincweb')
                    ->join('incweb','incweb.id', '=', 'papeletasincweb.idincweb')
                    ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                    ->leftjoin('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                    ->select(
                        'papeletasincweb.id as idpapeleta',
                        'nomperson as solicitadapor',
                        //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS solicitadapor'),
                        DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                        'horasoli as horasolicitud',
                        //'tiposolicitud  as tiposolicitud',
                        'descripcio as descripcionsolicitud',
                        DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprobado'),
                        'horaaprob as horaaprobado',
                        'observacion as observacionaprobado',
                        'desconcepto as tiposolicitud'
                         
                    )
                    ->distinct()
                    //->whereBetween('fechasoli',[$fechadesdeformateada,$fechahastaformateada])
                    ->where('useraprob',$co_usuario)
                    ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                    ->whereNull('papeletasincweb.deleted_at')
                    ->orderBy('papeletasincweb.id', 'asc');

        return $consulta;

    }

    
    private function PapeletasGeneral()
    {

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        return DB::table('papeletasincweb')
                ->join('incweb','incweb.id', '=', 'papeletasincweb.idincweb')
                ->leftjoin('personal','personal.codperson', '=', 'incweb.codpersona')
                ->leftjoin('gconceptos','gconceptos.codconcepto', '=', 'incweb.codconcepto')
                ->select(
                    'papeletasincweb.id as idpapeleta',
                    //DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS solicitadapor'),
                    'nomperson as solicitadapor',
                    DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasolicitud'),
                    'horasoli as horasolicitud',
                    //'tiposolicitud  as tiposolicitud',
                    'descripcio as descripcionsolicitud',
                    DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprobado'),
                    'horaaprob as horaaprobado',
                    'observacion as observacionaprobado',
                    'desconcepto as tiposolicitud'
                )
                ->distinct()
                //->whereBetween('fechasoli',[$fechadesdeformateada,$fechahastaformateada])
                ->where('useraprob',$co_usuario)
                ->whereNull('papeletasincweb.deleted_at')
                ->orderBy('papeletasincweb.id', 'asc');

    }

    
    
    private function BuscarEnConsultaFiltroPapeletas($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    
    public function ListarPapeletas(Request $request)
    {

        $listapapeletas = [];
        $columns = self::arregloColumnasPapeletas();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        //LA VARIABLE $tipoBusqueda SE COMPARA CON $this->tipoBusqueda QUE SE DEFINE AL PRINCIPIO COMO especifico


        
        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_papeletas  = $request->input('dp_fedesde_papeletas');
            $dp_fehasta_papeletas  = $request->input('dp_fehasta_papeletas');

            //dd("pase por aqui".$dp_fedesde_aprobarincidencia.$dp_fehasta_aprobarincidencia);
            //die();


            $fechadesde = explode("-",$dp_fedesde_papeletas);
            $fechahasta = explode("-",$dp_fehasta_papeletas);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

            //$estatus=$request->input('estatus');

            //$codigoaprobarinc=$request->input('codigoaprobarinc');

            //dd($estatus);
            //die();


            $consulta = self::PapeletasEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::PapeletasGeneral();
          
        }

        $consulta  = self::buscarEnConsultaPapeletas($consulta, $buscar, $columns);

        $total = self::contarPapeletas($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaPapeletas($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listapapeletas = self::obtenerPapeletas($consulta);

        //dd($listamarcaasistencia);
        //die();

       
        //dd($totaltardanza);
        //die();

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listapapeletas,

        );

     

        return response()->json($json_data);
    
    }

    
    public function PdfImprimirPapeleta($idpapeleta)
    {

        //dd($idsolicitud.$idpapeleta);
        //die();

        $idsolicitud = DB::table('papeletasincweb')
                       ->select(
                          'idincweb'
                       )
                       ->distinct()
                       ->where('id',$idpapeleta)
                       ->get();
        
        $idsolicitud= $idsolicitud[0]->idincweb;


        $datossolicitud = DB::table('incweb')
                    ->select(
                        'tiposolicitud',
                        'descripcio',
                        //DB::raw('DATE_FORMAT(fechasoli,"%d/%m/%Y")'),
                        DB::raw('DATE_FORMAT(fechasoli, "%d/%m/%Y") as fechasoli'),
                        'horasoli',
                        'codpersona',
                        'useraprob',
                        DB::raw('DATE_FORMAT(fechaaprob, "%d/%m/%Y") as fechaaprob'),
                        'horaaprob',
                        'observacion',
                        'documento',
                        'codconcepto'
                    )
                    ->distinct()
                    ->where('id',$idsolicitud)
                    ->get();

        $tiposolicitud= $datossolicitud[0]->tiposolicitud;
        $codigoconcepto= $datossolicitud[0]->codconcepto;
        $descripsolicitud= $datossolicitud[0]->descripcio;
        $fechasolicitud= $datossolicitud[0]->fechasoli;
        $horasolicitud= $datossolicitud[0]->horasoli;
        $codsolicitante= $datossolicitud[0]->codpersona;
        $codaprobador= $datossolicitud[0]->useraprob;
        $fechaaprobado= $datossolicitud[0]->fechaaprob;
        $horaaprobado= $datossolicitud[0]->horaaprob;
        $observacionaprobado= $datossolicitud[0]->observacion;
        $documentoaprobado= $datossolicitud[0]->documento;

        $datossolicitante = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codsolicitante)
                    ->get();

        $datossolicitante= $datossolicitante[0]->nomperson;

        $datossupervisor = DB::table('personal')
                    ->select(
                        'nomperson'
                    )
                    ->distinct()
                    ->where('codperson',$codaprobador)
                    ->get();

        $datossupervisor= $datossupervisor[0]->nomperson;
        
        $datosconcepto = DB::table('gconceptos')
                    ->select(
                        'desconcepto'
                    )
                    ->distinct()
                    ->where('codconcepto',$codigoconcepto)
                    ->get();

        $datosconcepto= $datosconcepto[0]->desconcepto;


        $pdf = app('FPDF');
        $pdf->AliasNbPages();
        $pdf->AddPage('P','A4');

        $fecharegistro = Carbon::now();
        $fecharegistro = $fecharegistro->format('d/m/Y');

        $horaregistro = Carbon::now();
        $horaregistro = $horaregistro->format('H:i:s:A');
            
        //$numeropag=$pdf->PageNo();

        $pdf->Image('img/logoempresa.png',10,4,25);

        $pdf->SetFont('Arial', 'B', 14);

        $fechaimpresion = utf8_decode($fecharegistro);
        $pdf->SetXY(135,16);
        $pdf->Cell(25, 1, 'Fecha Impresion: '.$fechaimpresion, '', 1, 'L');

        $horaimpresion = utf8_decode($horaregistro);
        $pdf->SetXY(135,22);
        $pdf->Cell(25, 1, 'Hora Impresion:   '.$horaimpresion, '', 1, 'L');

        $pdf->SetXY(100,35);
        $titulo = utf8_decode("PAPELETA"."   "."N°"." ".$idpapeleta);
        $pdf->Cell(5, 1, $titulo, '', 1, 'C');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,45);
        $titulodatosdocumento = utf8_decode("DOCUMENTO N°:"." ");
        $pdf->Cell(25, 1, $titulodatosdocumento, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,45);
        $pdf->Cell(25, 1, utf8_decode($documentoaprobado), '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,55);
        $titulodatossolicitante = utf8_decode("SOLICITADA POR:"." ");
        $pdf->Cell(25, 1, $titulodatossolicitante, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,55);
        $pdf->Cell(25, 1, $datossolicitante, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,65);
        $titulofechasolicitud = utf8_decode("FECHA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulofechasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(57,65);
        $pdf->Cell(25, 1, $fechasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,75);
        $titulohorasolicitud = utf8_decode("HORA SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulohorasolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(55,75);
        $pdf->Cell(25, 1, $horasolicitud, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,85);
        $titulotiposolicitud = utf8_decode("TIPO SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulotiposolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,85);
        //if($tiposolicitud=="V"){
        //    $tiposolicitud="VACACION";
        //}
        //if($tiposolicitud=="D"){
        //    $tiposolicitud="DIA";
        //}
        //if($tiposolicitud=="H"){
            //$tiposolicitud="HORA";
        //}
        //$pdf->Cell(25, 1, $tiposolicitud, '', 1, 'L');
        $pdf->Cell(25, 1, $datosconcepto, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,95);
        $titulodescripcionsolicitud = utf8_decode("DESCRIPCION SOLICITUD:"." ");
        $pdf->Cell(25, 1, $titulodescripcionsolicitud, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(75,95);
        $pdf->Cell(25, 1, utf8_decode($descripsolicitud), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,105);
        $tituloaprobadopor = utf8_decode("APROBADO POR:"." ");
        $pdf->Cell(25, 1, $tituloaprobadopor, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(53,105);
        $pdf->Cell(25, 1, utf8_decode($datossupervisor), '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,115);
        $titulofechaaprobado = utf8_decode("FECHA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulofechaaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(58,115);
        $pdf->Cell(25, 1, $fechaaprobado, '', 1, 'L');

        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,125);
        $titulohoraaprobado = utf8_decode("HORA APROBADO:"." ");
        $pdf->Cell(25, 1, $titulohoraaprobado, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(56,125);
        $pdf->Cell(25, 1, $horaaprobado, '', 1, 'L');


        $pdf->SetFont('Arial', '', 14);
        $pdf->SetXY(10,135);
        $tituloobservacion = utf8_decode("OBSERVACIONES APROBACION:"." ");
        $pdf->Cell(25, 1, $tituloobservacion, '', 1, 'L');
        
        $pdf->SetFont('Arial', '', 12);
        $pdf->SetXY(90,135);
        $pdf->Cell(25, 1, utf8_decode($observacionaprobado), '', 1, 'L');
        

        $pdf->output();

        //dd($pdf);
        //die();
        exit;

    }    


    

     public function obtenerfotopersonapapeletas()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }

    

    public function obtenernombrepersonapapeletas()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }


}
