<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Redirect;
use DB;

use App\Fmarcasw;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;



class LogController extends Controller
{
    
    
    public function logout(){
        
        Auth::logout();
       
        return Redirect::to('/');
        
        //return redirect('/');
    }

    

    public function buscar_usuario_tablapersonal(Request $request, $co_usuario)
    {


        if ($request->ajax()) {

            //dd($co_usuario);
            //die();


            $usuariopersonal = DB::table('personal')
                    ->select(
                        'personal.codperson'
                    )    
            ->where('personal.codperson',$co_usuario)
            ->count();

            //dd($usuariopersonal);
            //die();


            if($usuariopersonal >0){


                $usuarioclavereloj = DB::table('personal')
                    ->select(
                         'personal.clavereloj'  
                    )
                    
                     ->where('personal.codperson',$co_usuario)
                     ->get();

                $clavereloj = trim($usuarioclavereloj[0]->clavereloj);

                //dd("clave reloj".$clavereloj);
                //die();

                if ($clavereloj==""){

                     $usuarioestatus="CAMPO CLAVE VACIO";

                }
                else{

                    $usuarioestatus = DB::table('personal')
                        ->select(
                             'personal.activo'
                             
                        )
                        
                         ->where('personal.codperson',$co_usuario)
                         ->get();

                }
                    //dd($usuarioestatus);
                    //die();
             }
             else{

                 $usuarioestatus="NO EXISTE USUARIO";

             }


            return response()->json($usuarioestatus);


        }

    }

    
   
    public function estatus_usuario(Request $request, $co_usuario)
    {


        if ($request->ajax()) {

            $usuario = DB::table('users')
                ->select(
                  
                    'users.st_usuario'
                )
                ->where('users.co_usuario', $co_usuario)
                ->get();


            return response()->json($usuario);


        }

    }


    
    public function buscar_usuario(Request $request, $co_usuario)
    {

        $buscaruser="USUARIO NO ENCONTRADO";

        $buscar = DB::table('personal')
        ->select(
          
          'personal.dniperson'

        )
           
        ->where('personal.dniperson',$co_usuario)
        ->count();

        if($buscar >0){

            $buscaruser="USUARIO ENCONTRADO";
        }

        $respuesta = array(
            "buscaruser"  => $buscaruser,
        );

        return response()->json($respuesta);

    }


    
    
    public function buscar_estatus_personal(Request $request, $co_usuario)
    {


        if ($request->ajax()) {

            $estatuspersonal = DB::table('personal')
                ->select(
                  
                    'personal.activo'
                )
                ->where('personal.dniperson', $co_usuario)
                ->get();


            return response()->json($estatuspersonal);


        }

    }


    

     public function buscar_datosusuario(Request $request, $dni)
    {

      
        if ($request->ajax()) {

            $datosusuario = DB::table('personal')
                ->select(
                  
                    'nomperson',
                    'telefono1',
                    'mail',
                    'dniperson'
                )
                ->where('dniperson',$dni)
                ->get();

            $contador = $datosusuario->count();

            if($contador>0){

                $respuesta = array(
                    //"buscarusuario"  => "USUARIO ENCONTRADO",
                    "datosusuario" => $datosusuario,
                );    
            }
            else{
                
                $datosusuario="";
                $respuesta = array(
                    //"buscarusuario"  => "USUARIO NO ENCONTRADO",
                    "datosusuario" => $datosusuario,
                );
            }

        }

         return response()->json($respuesta);

    }

    
    
    public function cerrar_sesion_usuario(){
        
        Auth::logout();

        $respuesta = array (
            "mensaje" => "sesioncerrada"
        );
            

        return response()->json($respuesta);
       
        
        //return redirect('/');
    }

   

    public function buscar_marcacion_dniip(Request $request, $co_usuario,$ip)
    {

        $buscarmarcacion="MARCACIONDNIIP NO ENCONTRADA";

        $buscar = DB::table('fmarcas')
        ->select(
          
          'fmarcas.dniperson'

        )
           
        ->where('fmarcas.ipmarcacion',$ip)
        ->where('fmarcas.dniperson',"!=",$co_usuario)
        ->get();

        $contador = $buscar->count();

        //dd("el contador es".$contador);
        //die();

        if($contador>0){

            $buscarmarcacion="MARCACIONDNIIP ENCONTRADA";
        }

        $respuesta = array(
            "buscarmarcacion"  => $buscarmarcacion,
        );

        return response()->json($respuesta);

    }


    

    public function registrar_asistencia(Request $request)
    {

        //dd($co_usuario);
        //die();
        $co_usuario= trim($request['usuario']);


        $latitud= trim($request['latitud']);
        $longitud= trim($request['longitud']);
        $ipcliente= trim($request['ipcliente']);
        //$perfil= trim($request['perfil']);

        $correoregistrador="";


        DB::beginTransaction();

        try {

            //dd($latitud.$longitud);
            //die();

            $utimoid = DB::select(DB::raw('select idasis from fmarcas where idasis = (select max(`idasis`) from fmarcas )'));

            //dd($utimoid);
            //die();
 
            if($utimoid==null){
               $idingresar= 1;
            }
            else{
               $ultimo = trim($utimoid[0]->idasis);
               $idingresar= $ultimo + 1;
               $idingresar= (int)$idingresar;
            }

            //dd($idingresar);
            //die();

            
            //FUNCIONA EN UNA RED LOCAL
            //$nombreequipo= gethostbyaddr($_SERVER['REMOTE_ADDR']);

        
            //dd($nombreequipo);
            //die();

            $dispositivoacceso= self::obtenerdispositivoaccesologin();

            if ($dispositivoacceso=="Ordenador"){

                $device="999";

            }

            if ($dispositivoacceso=="Mobil"){

                 $device="998";
                
            }

            if ($dispositivoacceso=="Tablet"){

                 $device="998";
                
            }


            $fecharegistro = Carbon::now();
            $fecharegistro = $fecharegistro->format('Ymd');

            $horaregistro = Carbon::now();
            $horaregistro = $horaregistro->format('H:i:s');

            $fechahora= $fecharegistro." ".$horaregistro;

            $asistencia = Fmarcasw::create([
                "idasis"  => $idingresar,
                "codperson"    => $co_usuario,
                "dniperson"    => $co_usuario,
                "cfecha"  => $fecharegistro,
                "creadodesde"  => $dispositivoacceso,
                "latitud"  => $latitud,
                "longitud"  => $longitud,
                "creadodesde"  => $dispositivoacceso,
                "hora"  => $horaregistro,
                "device"  => $device,
                "ipmarcacion"  => $ipcliente,
            ]);


            $correoempleado= self::obtenercorreoasistencia($co_usuario);

            $datosempleados= self::obtenerdatosempleadoasistencia($co_usuario);

            $datosempleado= $datosempleados[0]->dniperson."  ".$datosempleados[0]->NOMPERSON;
            

            $dni= $datosempleados[0]->dniperson;
            //dd($dni);
            $nombreempleado= $datosempleados[0]->NOMPERSON;
           
            DB::commit();

            $fecharegistro = Carbon::now();
            $fechaasistencia = $fecharegistro->format('d-m-Y');


            $respuesta = array(
                "mensaje" => "asistenciaregistrada",
                "correoempleado" => $correoempleado,
                //"datosempresa" => $datosempresa,
                "datosempleado" => $datosempleado,
                "fechaasistencia" => $fechaasistencia,
                "horaasistencia" => $horaregistro,
                "registradodesde" => $dispositivoacceso,
                "dni" => $dni,
                "nombreempleado" => $nombreempleado,
            );

            return response()->json($respuesta);

        } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
        }

    }


     

    public function obtenercorreoasistencia($co_usuario)
    {


        $correo = DB::table('personal')
        ->select(
            'personal.mail'        
        )
        ->distinct()
        ->where('dniperson',$co_usuario)
        ->get();

        $correoasistencia= $correo[0]->mail;

        return($correoasistencia);
    }


    
    public function obtenerdatosempleadoasistencia($co_usuario)
    {

        $datos = DB::table('personal')
            ->select(
              
                'personal.dniperson',
                'personal.NOMPERSON'
            )
            ->where('personal.dniperson', $co_usuario)
            ->get(); 

        //$dniperson= $datos[0]->dniperson;
        //$apmaterno= $datos[0]->apmaterno;
        //$appaterno= $datos[0]->appaterno;

        //$datosempleado= $dniperson."  ".$apmaterno."  ".$appaterno;

        return($datos);
    }


   
    public function obtenerdispositivoaccesologin()
    {

        $tablet_browser = 0;
        $mobile_browser = 0;
        $body_class = 'desktop';
 
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
            $body_class = "tablet";
        }
         
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
         
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');
         
        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }
         
        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }
        if ($tablet_browser > 0) {
        // Si es tablet has lo que necesites
           $dispositivo= 'Tablet';
           return($dispositivo);
          
        }
        else if ($mobile_browser > 0) {
        // Si es dispositivo mobil has lo que necesites
           $dispositivo= 'Mobil';
           return($dispositivo);
          
        }
        else {
        // Si es ordenador de escritorio has lo que necesites

           $dispositivo= 'Ordenador';
           return($dispositivo);
           
        } 

    } 



}



