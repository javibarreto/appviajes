<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Vuelo;
use App\Viaje;
use App\Viajero;
use App\Correos;


class VueloController extends Controller
{

    public function vuelos(Request $request)
    {

     
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        
        $viajes = Viaje::all(); 
       
        $viajeros = Viajero::all();

        return view('admin.vuelo.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("viajeros",$viajeros)
            ->with("viajes",$viajes)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarVuelos(Request $request)
    {
        $vuelo = Vuelo::all();
        return response()->json($vuelo);
    }


    public function crearVuelo(Request $request)
    {

        if ($request->ajax()) {

            $nombre =  trim($request['nombre']);
            $cedula =  trim($request['cedula']);
            $feNacimiento = trim($request['feNacimiento']);
            $telefono = trim($request['telefono']);
            $codigo = trim($request['codigo']);
            $origen = trim($request['origen']);
            $destino = trim($request['destino']);
            $precio = trim($request['precio']);

            $vuelo = Vuelo::create([
                "nb_viajero"  => $nombre,
                "nu_ci"  => $cedula,
                "fe_nacimiento"  => $feNacimiento,
                "nu_telefono"  => $telefono,
                "co_viaje"  => $codigo,
                "nb_origen"  => $origen,
                "nb_destino"  => $destino,
                "nu_precio"  => $precio,
            ]);


            $respuesta = array(
                "mensaje"  => "creado"
            );

            return response()->json($respuesta);

        }

    }


    public function obtenerViaje(Request $request,$id)
    {
        if ($request->ajax()) {

            $viaje = DB::table('viaje')
            ->select(
                    'co_viaje',
                    'nu_plazas',
                    'nb_origen',
                    'nb_destino',
                    'nu_precio'
                    )
            ->where('id_viaje', $id)
            ->get();

            return response()->json($viaje);
        }
    }

    public function obtenerViajeroInfo(Request $request,$id)
    {
        if ($request->ajax()) {

            $viajero = DB::table('viajero')
            ->select(
                    'nb_viajero',
                    'fe_nacimiento',
                    'nu_telefono'
                    )
            ->where('nu_ci', $id)
            ->get();

            return response()->json($viajero);
        }
    }

    public function obtenerViajeInfo(Request $request,$id)
    {
        if ($request->ajax()) {

            $viaje = DB::table('viaje')
            ->select(
                    'nu_plazas',
                    'nb_origen',
                    'nb_destino',
                    'nu_precio'
                    )
            ->where('co_viaje', $id)
            ->get();

            return response()->json($viaje);
        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
