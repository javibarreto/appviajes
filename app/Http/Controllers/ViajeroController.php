<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use PDF;
use App\IncWeb;
use App\Viajero;
use App\Correos;


class ViajeroController extends Controller
{

    public function viajero(Request $request)
    {

        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersonasolicitudvacacion();

        $nombrepersonaaccesa= self::obtenernombrepersonasolicitudvacacion();

        return view('admin.viajero.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function listarViajero(Request $request)
    {
        $viajero = Viajero::all();
        return response()->json($viajero);
    }


    public function crearViajero(Request $request)
    {

        if ($request->ajax()) {

            $nombre =  trim($request['nombre']);
            $cedula =  trim($request['cedula']);
            $feNacimiento = trim($request['feNacimiento']);
            $telefono = trim($request['telefono']);
            $correo = trim($request['correo']);


            $viajero = Viajero::create([
                    "nb_viajero"  => $nombre,
                    "nu_ci"  => $cedula,
                    "fe_nacimiento"  => $feNacimiento,
                    "nu_telefono"  => $telefono,
                    "tx_correo"  => $correo,
            ]);

            $respuesta = array(
                "mensaje"  => "creado"
            );

            return response()->json($respuesta);

        }

    }

    public function editarViajero(Request $request)
    {
        if ($request->ajax()) {

            $id = $request['id'];
            $nombre =  trim($request['nombre']);
            $cedula =  trim($request['cedula']);
            $feNacimiento = trim($request['feNacimiento']);
            $telefono = trim($request['telefono']);
            $correo = trim($request['correo']);


            $casa = Viajero::where('id_viajero', $id)->update([
                "nb_viajero"  => $nombre,
                "nu_ci"  => $cedula,
                "fe_nacimiento"  => $feNacimiento,
                "nu_telefono"  => $telefono,
                "tx_correo"  => $correo,
            ]);

            return response()->json([
                "mensaje" => "actualizado"
            ]);
        }
    }

    public function eliminarViajero(Request $request)
    {
        if ($request->ajax()) {

            $date = trim(Carbon::now());

            try { 
                $id = $request['id'];

                $viajero = Viajero::where('id_viajero', $id)->delete();

                return response()->json([
                    "mensaje" => "eliminado"
                ]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }
    }


    public function obtenerViajero(Request $request,$id)
    {
        if ($request->ajax()) {

            $viajero = DB::table('viajero')
            ->select(
                    'nu_ci',
                    'id_viajero',
                    'nb_viajero',
                    'fe_nacimiento',
                    'nu_telefono',
                    'tx_correo'
                    )
            ->where('id_viajero', $id)
            ->get();

            return response()->json($viajero);
        }
    }


    public function obtenerfotopersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('fpersonal')
                ->select(
                     'fpersonal.fimagen'  
                )
                ->distinct()
                ->where('fpersonal.codperson',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

        return($foto);
    }
    public function obtenernombrepersonasolicitudvacacion()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('personal')
                ->select(
                    DB::raw('CONCAT(personal.appaterno, " ", personal.apmaterno) AS personaaccesa')
                )
                ->distinct()
                ->where('personal.codperson',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }

}
