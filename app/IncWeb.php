<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncWeb extends Model
{

	use SoftDeletes;

    protected $table = 'incweb';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'id',
    'fechasoli',
    'horasoli',
    'codpersona',
    'codconcepto',
    'fechadesde',
    'fechahasta',
    'horadesde',
    'horahasta',
    'descripcio',
    'observacion',
    'documento',
    'estatus',
    'drelacionado',
    'useraprob',
    'fechaaprob',
    'horaaprob',
    'tiposolicitud',
    'mediajornadavac',
    'creadodesde',
    'editadodesde',
    'eliminadodesde',
    'aprobadodesde',
    'rechazadodesde'

	];

    //protected $dates = ['deleted_at'];
    //
}
   