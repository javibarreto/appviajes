<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Torneo extends Model
{
//	use SoftDeletes;

    protected $table = 'torneo';
    protected $fillable = [
        'id_torneo',
        'fe_torneo',
        'nu_competidores',
        'peso_dif',
        'created_at',
        'updated_at',
        'deleted_at'
	];
}


