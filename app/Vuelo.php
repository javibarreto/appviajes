<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Vuelo extends Model
{
//	use SoftDeletes;

    protected $table = 'vuelo';
    protected $fillable = [
        'id_viaje',
        'nu_ci',
        'id_viajero',
        'nb_viajero',
        'fe_nacimiento',
        'nu_telefono',
        'tx_correo',
        'co_viaje',
        'nb_origen',
        'nb_destino',
        'nu_precio',
        'created_at',
        'updated_at',
        'deleted_at'
	];
}
