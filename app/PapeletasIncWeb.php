<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PapeletasIncWeb extends Model
{

	use SoftDeletes;

    protected $table = 'papeletasincweb';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'id',
    'idincweb',
    'fecha',
    'hora'
	];

    //protected $dates = ['deleted_at'];
    //
}
   