<?php

/*
|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'FrontController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', 'FrontController@admin');

//RUTAS LOGIN
    Route::post('iniciar_sesion/', 'Auth\AuthController@iniciar_sesion');
    Route::get('buscar_usuario_tablapersonal/{co_usuario}', 'LogController@buscar_usuario_tablapersonal');

    Route::get('buscar_supervisor/{co_usuario}','LogController@buscar_supervisor');

    Route::get('estatus_usuario/{co_usuario}', 'LogController@estatus_usuario');
    Route::get('logout', 'LogController@logout');

    Route::get('buscar_usuario/{co_usuario}','LogController@buscar_usuario');

    Route::get('buscar_estatus_personal/{co_usuario}', 'LogController@buscar_estatus_personal');
    
    Route::post('registrar_asistencia/','LogController@registrar_asistencia');

    Route::get('buscar_datosusuario/{dni}','LogController@buscar_datosusuario');

    Route::get('cerrar_sesion_usuario', 'LogController@cerrar_sesion_usuario');
    
    Route::get('buscar_usuario_tablapersonal/{co_usuario}', 'LogController@buscar_usuario_tablapersonal');

    Route::get('buscar_marcacion_dniip/{co_usuario}/{ip}', 'LogController@buscar_marcacion_dniip');

    
//FIN RUTAS LOGIN


//RUTAS CAPTCHA
    Route::get('refrescarcaptcha', 'CaptchaController@RefrescarCaptcha');
    Route::post('validarcaptcha', 'CaptchaController@ValidarCaptcha');
//

//ENVIO DE CORREO


// Route::post('enviarcorreoplantillauno', 'MailController@EnviarCorreoSolicitudCreada');
Route::post('EnviarCorreoPlantillaUno', 'MailController@EnviarCorreoPlantillaUno');
    Route::post('enviarcorreosolicitudcreada', 'MailController@EnviarCorreoSolicitudCreada');
    Route::post('enviarcorreosolicitudaprobada', 'MailController@EnviarCorreoSolicitudAprobada');
    Route::post('enviarcorreosolicitudrechazada', 'MailController@EnviarCorreoSolicitudRechazada');
    Route::post('enviarcorreosolicitudrevisada', 'MailController@EnviarCorreoSolicitudRevisada');
    Route::post('enviarcorreosolicitudeditada', 'MailController@EnviarCorreoSolicitudEditada');
    Route::post('enviarcorreosolicitudeliminada', 'MailController@EnviarCorreoSolicitudEliminada');
    Route::post('enviarcorreoasistencia', 'MailController@EnviarCorreoAsistencia');
//FIN ENVIO DE CORREO

    

//PROCESOS

    //SOLICITUD VACACION

        Route::get('solicitudvacacion/', 'SolicitudVacacionController@SolicitudVacacion');

        Route::post('listarsolicitudvacacion/', 'SolicitudVacacionController@ListarSolicitudVacacion');

        Route::post('ingresarsolicitudvacacion/', 'SolicitudVacacionController@IngresarSolicitudVacacion');

        Route::get('obtenerdatossolicitudvacacion/{id}','SolicitudVacacionController@ObtenerDatosSolicitudVacacion');

        Route::post('actalizarsolicitudvacacion/', 'SolicitudVacacionController@ActualizarSolicitudVacacion');

        Route::post('eliminarsolicitudvacacion/', 'SolicitudVacacionController@EliminarSolicitudVacacion');

        Route::get('obtenerestatussolicitudvacacion/{id}','SolicitudVacacionController@ObtenerEstatusSolicitudVacacion');

        Route::get('obtenerestatusingresadosolicitudvacacion','SolicitudVacacionController@ObtenerEstatusIngresadoSolicitudVacacion');

        Route::post('excel/solicitudvacacion/', 'SolicitudVacacionController@ExcelSolicitudVacacion');

        Route::get('pdfsolicitudvacacion/{tipobusqueda}/{buscar}/{fdesde}/{fhasta}/{estatus}/{tipovacacion}', 'SolicitudVacacionController@pdfSolicitudVacacion');

        Route::get('obtenercorreosupervisorsolicitudvacacion', 'SolicitudVacacionController@obtenercorreosupervisorsolicitudvacacion');

        Route::get('obtenerdatosdetoperasolicitudvacacion/{id}', 'SolicitudVacacionController@obtenerDatosDetoperaSolicitudVacacion');

        Route::get('obtenerdatosdetaprobarsolicitudvacacion/{id}', 'SolicitudVacacionController@obtenerDatosDetaprobarSolicitudVacacion');

        Route::get('obtenerdatosdetrechazarsolicitudvacacion/{id}', 'SolicitudVacacionController@obtenerDatosDetrechazarSolicitudVacacion');

        Route::get('obtenerdatosdetrevisarsolicitudvacacion/{id}', 'SolicitudVacacionController@obtenerDatosDetrevisarSolicitudVacacion');

        Route::get('cargardiaspendisolicitudvacacion/', 'SolicitudVacacionController@CargarDiasPendiSolicitudVacacion');

        Route::get('obtenertotaldiaspendientessolicitudvacacion', 'SolicitudVacacionController@obtenertotaldiaspendientessolicitudvacacion');

        Route::get('obtenersolicitudvacacionpendientes','SolicitudVacacionController@ObtenerSolicitudVacacionPendientes');

        Route::get('obtenercorrelativodocumentosolicitudvacacion', 'SolicitudVacacionController@obtenercorrelativodocumentosolicitudvacacion');
    

    //FIN SOLICITUD VACACION

    //INCIDENCIA DIA

        Route::get('solicitudincdia/', 'SolicitudIncDiaController@SolicitudIncDia');

        Route::post('listarsolicitudincdia/', 'SolicitudIncDiaController@ListarSolicitudIncDia');

        Route::get('cargarconceptosincdia/', 'SolicitudIncDiaController@CargarConceptosIncDia');

        Route::get('recargarconceptosincdia/{criterio}', 'SolicitudIncDiaController@RecargarConceptosIncDia');

        Route::post('ingresarsolicitudincdia/', 'SolicitudIncDiaController@IngresarSolicitudIncDia');

        Route::get('obtenersolicitudingresadaincdia/{id}','SolicitudIncDiaController@ObtenerSolicitudIngresadaIncDia');

        Route::get('obtenerestatussolicitudincdia/{id}','SolicitudIncDiaController@ObtenerEstatusSolicitudIncDia');

        Route::get('obtenerdatossolicitudincdia/{id}','SolicitudIncDiaController@ObtenerDatosSolicitudIncDia');

        Route::post('actalizarsolicitudincdia/', 'SolicitudIncDiaController@ActualizarSolicitudIncDia');

        Route::post('eliminarsolicitudincdia/', 'SolicitudIncDiaController@EliminarSolicitudIncDia');

        Route::post('excel/solicitudincdia/', 'SolicitudIncDiaController@ExcelSolicitudIncDia');

        Route::get('pdfsolicitudincdia/{tipobusqueda}/{buscar}/{fdesde}/{fhasta}/{estatus}/{codigoincdia}', 'SolicitudIncDiaController@pdfSolicitudIncDia');

        Route::get('obtenercorreosupervisorsolicitudincdia', 'SolicitudIncDiaController@obtenercorreosupervisorsolicitudincdia');

        Route::get('obtenerdatosdetoperasolicitudincdia/{id}', 'SolicitudIncDiaController@obtenerDatosDetoperaSolicitudIncDia');

        Route::get('obtenerdatosdetaprobarsolicitudincdia/{id}', 'SolicitudIncDiaController@obtenerDatosDetaprobarSolicitudIncDia');

        Route::get('obtenerdatosdetrechazarsolicitudincdia/{id}', 'SolicitudIncDiaController@obtenerDatosDetrechazarSolicitudIncDia');

        Route::get('obtenerdatosdetrevisarsolicitudincdia/{id}', 'SolicitudIncDiaController@obtenerDatosDetrevisarSolicitudIncDia');

        Route::get('obtenercorrelativodocumentosolicitudincdia', 'SolicitudIncDiaController@obtenercorrelativodocumentosolicitudincdia');
        
        
    //FIN INCIDENCIA DIA

    //INCIDENCIA HORA

        Route::get('solicitudinchora/', 'SolicitudIncHoraController@SolicitudIncHora');

        Route::post('listarsolicitudinchora/', 'SolicitudIncHoraController@ListarSolicitudIncHora');

        Route::get('cargarconceptosinchora/', 'SolicitudIncHoraController@CargarConceptosIncHora');

        Route::get('recargarconceptosinchora/{criterio}', 'SolicitudIncHoraController@RecargarConceptosIncHora');

        Route::post('ingresarsolicitudinchora/', 'SolicitudIncHoraController@IngresarSolicitudIncHora');

        Route::get('obtenersolicitudingresadainchora/{id}','SolicitudIncHoraController@ObtenerSolicitudIngresadaIncHora');

        Route::get('obtenerestatussolicitudinchora/{id}','SolicitudIncHoraController@ObtenerEstatusSolicitudIncHora');

        Route::get('obtenerdatossolicitudinchora/{id}','SolicitudIncHoraController@ObtenerDatosSolicitudIncHora');

        Route::post('actalizarsolicitudinchora/', 'SolicitudIncHoraController@ActualizarSolicitudIncHora');

        Route::post('eliminarsolicitudinchora/', 'SolicitudIncHoraController@EliminarSolicitudIncHora');

        Route::post('excel/solicitudinchora/', 'SolicitudIncHoraController@ExcelSolicitudIncHora');

        Route::get('pdfsolicitudinchora/{tipobusqueda}/{buscar}/{fdesde}/{fhasta}/{estatus}/{codigoincdia}', 'SolicitudIncHoraController@pdfSolicitudIncHora');

        Route::get('obtenercorreosupervisorsolicitudinchora', 'SolicitudIncHoraController@obtenercorreosupervisorsolicitudinchora');

         Route::get('obtenerdatosdetoperasolicitudinchora/{id}', 'SolicitudIncHoraController@obtenerDatosDetoperaSolicitudIncHora');

         Route::get('obtenerdatosdetaprobarsolicitudinchora/{id}', 'SolicitudIncHoraController@obtenerDatosDetaprobarSolicitudIncHora');

        Route::get('obtenerdatosdetrechazarsolicitudinchora/{id}', 'SolicitudIncHoraController@obtenerDatosDetrechazarSolicitudIncHora');

        Route::get('obtenerdatosdetrevisarsolicitudinchora/{id}', 'SolicitudIncHoraController@obtenerDatosDetrevisarSolicitudIncHora');


        Route::get('obtenercorrelativodocumentosolicitudinchora', 'SolicitudIncHoraController@obtenercorrelativodocumentosolicitudinchora');



    //FIN INCIDENCIA HORA

    //APROBAR INCIDENCIA

        Route::get('aprobarincidencia/', 'AprobarIncController@AprobarInc');

        Route::post('listaraprobarincidencia/', 'AprobarIncController@ListarAprobarInc');

        Route::get('obtenerestatussolicitudaprobar/{id}','AprobarIncController@ObtenerEstatusSolicitudAprobar');

        Route::get('obtenerdatossolicitudaprobar/{id}','AprobarIncController@ObtenerDatosSolicitudAprobar');

        Route::post('aprobarsolicitud/', 'AprobarIncController@AprobarSolicitud');

        Route::post('rechazarsolicitud/', 'AprobarIncController@RechazarSolicitud');

        Route::post('revisarsolicitud/', 'AprobarIncController@RevisarSolicitud');

        Route::post('excel/aprobarincidencia/', 'AprobarIncController@ExcelAprobarInc');

        Route::get('pdfaprobarincidencia/{tipobusqueda}/{buscar}/{fdesde}/{fhasta}/{estatus}/{codigoinc}','AprobarIncController@pdfAprobarInc');

        Route::get('cargarconceptosaprobarinc/', 'AprobarIncController@CargarConceptosAprobarInc');

        Route::get('recargarconceptosaprobarinc/{criterio}', 'AprobarIncController@RecargarConceptosAprobarInc');

        Route::get('obtenercorreosolicitanteaprobar/{id}','AprobarIncController@obtenercorreosolicitanteaprobar');

        Route::get('obtenercorreosolicitanterechazar/{id}','AprobarIncController@obtenercorreosolicitanterechazar');

         Route::get('obtenercorreosolicitanterevisar/{id}','AprobarIncController@obtenercorreosolicitanterevisar');

         Route::get('obtenerdatosdetoperaaprobarincidencia/{id}', 'AprobarIncController@obtenerDatosDetoperaSolicitudAprobar');

          Route::get('obtenerdatosdetaprobarincidencia/{id}', 'AprobarIncController@obtenerDatosDetaprobarInc');

        Route::get('obtenerdatosdetrechazarincidencia/{id}', 'AprobarIncController@obtenerDatosDetrechazarInc');

        Route::get('obtenerdatosdetrevisarincidencia/{id}', 'AprobarIncController@obtenerDatosDetrevisarInc');

        Route::get('imprimirpapeleta/{idsolicitud}/{idpapeleta}','AprobarIncController@ImprimirPapeleta');
         

    //FIN APROBAR INCIDENCIA

    //PAPELETAS

        Route::get('mostrarpapeletas/', 'PapeletasController@Papeletas');

        Route::post('listarpapeletas/', 'PapeletasController@ListarPapeletas');

        Route::get('pdfimprimirboletas/{idpapeleta}','PapeletasController@PdfImprimirPapeleta');

    //FIN PAPELETAS

    //CONSULTAS

        Route::get('consultas/', 'ConsultasController@Consultas');
    
        Route::post('listarconsultas/', 'ConsultasController@ListarConsultas');

        Route::get('obtenerdatosconsulta/{id}', 'ConsultasController@ObtenerDatosConsulta');

        Route::post('actualizarrespuesta/', 'ConsultasController@ActualizarRespuesta');

        Route::post('ingresarconsultas/', 'ConsultasController@IngresarConsultas');

        Route::get('cargarpersonalsupervisorconsultas/', 'ConsultasController@CargarPersonalSupervisorConsultas');

        Route::get('recargarpersonalsupervisorconsultas/{criterio}', 'ConsultasController@RecargarPersonalSupervisorConsultas');
        
    //FIN CONSULTAS 

    //MARCACION ASISTENCIA

        Route::post('registrar_asistenciamarcacion/','MarcacionAsistenciaController@registrar_asistencia');
        
        Route::get('buscar_marcacion_dniipmarcacion/{ip}', 'MarcacionAsistenciaController@buscar_marcacion_dniip');

    //FIN MARCACION ASISTENCIA   
        

//FIN PROCESOS  

//REPORTES

    //MARCA ASISTENCIA

        Route::get('marcaasistencia/', 'RepoMarcaAsistenciaController@MarcaAsistenciaRepo');
    
        Route::post('listarmarcaasistenciarepo/', 'RepoMarcaAsistenciaController@ListarMarcaAsistenciaRepo');

        Route::post('excel/marcaasistenciarepo/', 'RepoMarcaAsistenciaController@ExcelMarcaAsistenciaRepo');

        Route::get('pdfmarcaasistenciarepo/{tipobusqueda}/{buscar}/{fdesde}/{fhasta}/{estatus}', 'RepoMarcaAsistenciaController@PdfMarcaAsistenciaRepo');

        Route::post('totalizarcantidadregistros/', 'RepoMarcaAsistenciaController@TotalizarCantidadRegistros');

        Route::get('totaltardanzatotalantes/{fdesde}/{fhasta}','RepoMarcaAsistenciaController@totaltardanzatotalantes');
         
        Route::get('totaltardanzatotalanteslimpiar/','RepoMarcaAsistenciaController@totaltardanzatotalanteslimpiar');

    //FIN MARCA ASISTENCIA

//FIN REPORTES




//RUTAS GESTION DE CORREOS
Route::get('VerPlantillaUno/', 'GestionDeCorreosController@VerPlantillaUno');
Route::get('VerPlantillaDos/', 'GestionDeCorreosController@VerPlantillaDos');
Route::get('VerPlantillaTres/', 'GestionDeCorreosController@VerPlantillaTres');

Route::post('EnviarPlantillaUno/', 'GestionDeCorreosController@EnviarPlantillaUno');

Route::get('gestioncorreos/', 'GestionDeCorreosController@GestionCorreos');
Route::post('crearcorreo/', 'GestionDeCorreosController@CrearCorreo');
Route::post('editarcorreo/', 'GestionDeCorreosController@editarcorreo');
Route::post('eliminarcorreos/', 'GestionDeCorreosController@eliminarcorreo');
Route::get('listarcorreos/', 'GestionDeCorreosController@listarcorreos');
Route::get('obtenercorreo/{id}', 'GestionDeCorreosController@obtenercorreo');

//FIN GESTION DE CORREOS

//ENVIO DE CORREOS
Route::get('enviocorreo/', 'SolicitudVacacionController@SolicitudVacacion');
Route::get('enviocorreos/', 'EnvioDeCorreosController@enviocorreos');
//FIN ENVIO DE CORREOS


//INICIO RUTA CASA
Route::get('casa/', 'CasaController@casa');
Route::post('crearCasa/', 'CasaController@crearCasa');
Route::post('editarCasa/', 'CasaController@editarCasa');
Route::post('eliminarCasa/', 'CasaController@eliminarCasa');
Route::get('listarCasa/', 'CasaController@listarCasa');
Route::get('obtenerCasa/{id}', 'CasaController@obtenerCasa');
//FIN RUTA CASA

//INICIO RUTA COMPETIDOR
Route::get('competidor/', 'CompetidorController@competidor');
Route::post('crearCompetidor/', 'CompetidorController@crearCompetidor');
Route::post('editarCompetidor/', 'CompetidorController@editarCompetidor');
Route::post('eliminarCompetidor/', 'CompetidorController@eliminarCompetidor');
Route::get('listarCompetidor/', 'CompetidorController@listarCompetidor');
Route::get('obtenerCompetidor/{id}', 'CompetidorController@obtenerCompetidor');


//FIN RUTA COMPETIDOR



//INICIO RUTA TORNEO
Route::get('torneo/', 'TorneoController@torneo');
Route::get('listarTorneo/', 'TorneoController@listarTorneo');
Route::get('cargarCompetidores/{id}', 'TorneoController@cargarCompetidores');
Route::post('crearTorneo/', 'TorneoController@crearTorneo');
//FIN RUTA TORNEO



//INICIO RUTA VIAJERO
Route::get('viajero/', 'ViajeroController@Viajero');
Route::post('crearViajero/', 'ViajeroController@crearViajero');
Route::post('editarViajero/', 'ViajeroController@editarViajero');
Route::post('eliminarViajero/', 'ViajeroController@eliminarViajero');
Route::get('listarViajero/', 'ViajeroController@listarViajero');
Route::get('obtenerViajero/{id}', 'ViajeroController@obtenerViajero');
//FIN RUTA VIAJERO

//INICIO RUTA VIAJES
Route::get('viajes/', 'ViajeController@viajes');
Route::post('crearViaje/', 'ViajeController@crearViaje');
Route::post('editarViaje/', 'ViajeController@editarViaje');
Route::post('eliminarViaje/', 'ViajeController@eliminarViaje');
Route::get('listarViajes/', 'ViajeController@listarViajes');
Route::get('obtenerViaje/{id}', 'ViajeController@obtenerViaje');
//FIN RUTA VIAJES

//INICIO RUTA VUELOS
Route::get('vuelos/', 'VueloController@vuelos');
Route::post('crearVuelo/', 'VueloController@crearVuelo');
Route::post('editarVuelo/', 'VueloController@editarVuelo');
Route::post('eliminarVuelo/', 'VueloController@eliminarVuelo');
Route::get('listarVuelos/', 'VueloController@listarVuelos');
Route::get('obtenerVuelo/{id}', 'VueloController@obtenerVuelo');
Route::get('obtenerViajeroInfo/{id}', 'VueloController@obtenerViajeroInfo');
Route::get('obtenerViajeInfo/{id}', 'VueloController@obtenerViajeInfo');

//FIN RUTA VUELOS


});
