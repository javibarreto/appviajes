
var busquedaGeneral = {
    dp_fedesde_marcaasistencia: 'todos',
    dp_fehasta_marcaasistencia: 'todos',
    tipo: "general",
    buscar: ""
};

var subtotalminutostardanza=0;
var subtotalhorastardanza=0;
var subtotaltardanza=0;
var subtotalminutosantes= 0;
var subtotalhorasantes=0;
var subtotalantes=0;

var totalregistros=0;


var contadorregistrostardanza=1;
var contadorregistrosantes=1;





function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_marcaasistencia = 'todos';
    busquedaGeneral.dp_fehasta_marcaasistencia = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});

function totaltardanzatotalantes(dp_fedesde_marcaasistencia,dp_fehasta_marcaasistencia) {

    var route = "/totaltardanzatotalantes/"+dp_fedesde_marcaasistencia+"/"+dp_fehasta_marcaasistencia;

    $.get(route , function(data) {
      
    }) 

    .fail(function() {
            alert( "error" );
    })

    .done(function(msj) {

        //console.log(msj);

        var totalsumatoriatardanza= msj.totaltardanza;
        var totalsumatoriaantessalida= msj.totalantessalida;

        //alert(totalsumatoriatardanza);

        totalsumatoriatardanza=totalsumatoriatardanza.toFixed(2);
        totalsumatoriaantessalida= totalsumatoriaantessalida.toFixed(2);


        $("#totalsumatoriatardanza").text(totalsumatoriatardanza);
        $("#totalsumatoriaantessalida").text(totalsumatoriaantessalida);

    });

}


function totaltardanzatotalanteslimpiar() {

    var route = "/totaltardanzatotalanteslimpiar";

    $.get(route , function(data) {
      
    }) 

    .fail(function() {
            alert( "error" );
    })

    .done(function(msj) {

        //console.log(msj);

        var totalsumatoriatardanza= msj.totaltardanza;
        var totalsumatoriaantessalida= msj.totalantessalida;

        //alert(totalsumatoriatardanza);

        totalsumatoriatardanza=totalsumatoriatardanza.toFixed(2);
        totalsumatoriaantessalida= totalsumatoriaantessalida.toFixed(2);


        $("#totalsumatoriatardanza").text(totalsumatoriatardanza);
        $("#totalsumatoriaantessalida").text(totalsumatoriaantessalida);

    });

}


function listarmarcaasistenciarepo() {

    //alert("pase");

    var route = "/listarmarcaasistenciarepo";

    //SE INICIALIZAN LAS VARIABLES AL ENTRAR A LA FUNCION
    totalminutostardanza=0;
    totalhorastardanza=0;
    totaltardanza=0;
    totalminutosantes= 0;
    totalhorasantes=0;
    totalantes=0;
    contadorregistrostardanza=1;
    contadorregistrosantes=1;
    totalhorasantes=0;
    totalanteshorasminutos=0;
    totalanteshoras=0;
    totalgeneral=0;
    totaltardanzashoras=0;
    totalgeneraltardanza=0;
    totaltardanzahorasminutos=0;
    totalhorastardanza1=0;

   
    var tipo="general"
    totalizarcantidadregistros(tipo);

    var blanco= "";
    $("#totalsumatoriatardanza").text(blanco);
    $("#totalsumatoriaantessalida").text(blanco);

    var recargartablatotal = jQuery('#listar_marcaasistencia_repo').dataTable({
        
        "lengthMenu": [31],
        "pageLength": 31,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                totalminutostardanza=0;
                totalhorastardanza=0;
                totaltardanza=0;
                totalminutosantes= 0;
                totalhorasantes=0;
                totalantes=0;
                totalhorasantes=0;
                totalanteshorasminutos=0;
                totalanteshoras=0;
                totalgeneral=0;
                totaltardanzashoras=0;
                totalgeneraltardanza=0;
                totaltardanzahorasminutos=0;
                totalhorastardanza1=0;

                var blanco= "";
                
                $("#totalsumatoriatardanza").text(blanco);
                $("#totalsumatoriaantessalida").text(blanco);
                
                contadorregistrostardanza=1;
                contadorregistrosantes=1;

                data.dp_fedesde_marcaasistencia = busquedaGeneral.dp_fedesde_marcaasistencia;
                data.dp_fehasta_marcaasistencia = busquedaGeneral.dp_fehasta_marcaasistencia;
                data.tipo = busquedaGeneral.tipo;

            }

        },

        columns: [
            {
               className: 'text-center',
                data: 'dia'
            },
            {
                className: 'text-center',
                name: 'cfecha',
                data: 'cfecha'
                
            },
            {
                className: 'text-center',
                name: 'entrada',
                data: 'entrada',
                
                render: function(data, type, row) {
                    
                    //alert("pase por aqui");
                    var marca1= row.marca1;
                    var entrada= row.entrada;
                    
                    var entradasplit= entrada.split(":");
                    var marca1split= marca1.split(":");
                       
                    if((entradasplit[0])!="00" && (entradasplit[0])!=null){
                        
                        var conversionminutos= (parseInt(entradasplit[1]) * 1)/60;
                            
                        conversionminutos= parseFloat(conversionminutos);
                        
                        var conversionentrada= parseInt(entradasplit[0])+ parseFloat(conversionminutos);
                        
                       
                        
                        //alert("la conversion entrada es"+conversionentrada);
                       
                    }
                
                    if((marca1split[0])!="00" && (marca1split[0])!=null){
                        
                        var conversionminutosmarca1= (parseInt(marca1split[1]) * 1)/60;
                            
                        conversionminutosmarca1= parseFloat(conversionminutosmarca1);
                        
                        var conversionmarca1= parseInt(marca1split[0])+ parseFloat(conversionminutosmarca1);
                        
                        //alert("la conversion marca es"+conversionmarca1);
                       
                    }
                    
                    
                
                    if(conversionmarca1>conversionentrada){
                    
                        var marca1= row.marca1;
                        return marca1; 
                        
                    }
                    else{

                        if((marca1=="")){

                           //alert("la marca es"+conversionmarca1);
                    
                            var marca1= row.marca1;
                            return marca1; 
                        
                        }
                        else{

                            var entrada= row.entrada;
                            return entrada;  

                        }
                        
                        
                    }
                
                } 
                
            },
            {
                className: 'text-center',
                name: 'salida',
                data: 'salida',
                
                render: function(data, type, row) {
                    
                    //alert("pase por aqui");
                    
                    var salida= row.salida;
                    //alert("la conversion salida es"+salida);
                    var salidasplit= salida.split(":");
                    //alert("la conversion salida2 es"+salidasplit);
                    
                    var marcae= row.marcae;
                    var marcaesplit= marcae.split(":");
                    
                    if((salidasplit[0])!="00" && (salidasplit[0])!=null){
                        
                        //alert("pase por aqui");
                        
                        var conversionminutos= (parseInt(salidasplit[1]) * 1)/60;
                            
                        conversionminutos= parseFloat(conversionminutos);
                        
                        var conversionsalida= parseInt(salidasplit[0])+ parseFloat(conversionminutos);
                    
                    }
                
                    if((marcaesplit[0])!="00" && (marcaesplit[0])!=null){
                        
                        var conversionminutosmarcae= (parseInt(marcaesplit[1]) * 1)/60;
                            
                        conversionminutosmarcae= parseFloat(conversionminutosmarcae);
                        
                        var conversionmarcae= parseInt(marcaesplit[0])+ parseFloat(conversionminutosmarcae);
                        
                        //alert("la conversion marca es"+conversionmarca1);
                       
                    }
                    
                    
                    //alert("marcae"+conversionmarcae+"salida"+conversionsalida);
                
                    if(conversionmarcae<conversionsalida){
                    
                        var marcae= row.marcae;
                        return marcae; 
                    }
                    
                    else{

                        //alert(marcae);

                        if((marcae=="")){

                           //alert("la marcae es"+conversionmarcae);
                    
                            var marcae= row.marcae;
                            return marcae; 
                        
                        }
                        else{

                            if((marcae=="xx")){

                                var salidanomarcada=" ";
                                return salidanomarcada;   

                            }
                            else{

                               var salida= row.salida;
                               return salida; 

                            }
                        }
                          
                    }
                    
                } 
                
            },
            {

                className: 'text-center',
                name: 'tardanza',
                data: 'tardanza',

                render: function(data, type, row) {

                //alert(totalregistros);

                    if(contadorregistrostardanza<=totalregistros){

                        var tardanza= row.tardanza;

                        //alert(tardanza);
                        var tardanzasplit= tardanza.split(":");

                        var tardanzamostrar= tardanzasplit[0]+":"+tardanzasplit[1];


                        //alert(contador+"  "+tardanzasplit[1]+"   "+totalminutostardanza);

                        //alert(tardanzasplit[1]);
                        if((tardanzasplit[1])!="00" && (tardanzasplit[1])!=null && (tardanzasplit[1])!=""){

                            //alert("pase por aqui"+tardanzasplit[1]);

                            //alert(contadorregistrostardanza+tardanza)

                            //alert(contadorregistrostardanza+tardanza)

                            var conversionminutos= (parseInt(tardanzasplit[1]) * 1)/60;
                            
                            conversionminutos= parseFloat(conversionminutos);

                            totalminutostardanza= parseFloat(totalminutostardanza)+parseFloat(conversionminutos);

                            totalminutostardanza= totalminutostardanza.toFixed(2);

                            //alert("minutos convertidos  "+conversionminutos+"total de minutos  "+totalminutostardanza+"  "+tardanzamostrar);

                            totalhorastardanza= parseFloat(totalhorastardanza)+parseInt(tardanzasplit[0]);

                            totalhorastardanza= totalhorastardanza.toFixed(2);

                            //alert("horas convertidas  "+totalhorastardanza+"  "+tardanzamostrar);
                            
                            totaltardanzahorasminutos= parseFloat(totalminutostardanza) + parseInt(totalhorastardanza);

                            //alert("horas-minutos convertidos  "+totaltardanzahorasminutos+"  "+tardanzamostrar);

                            //totaltardanza= parseFloat(totalminutostardanza) + parseInt(totalhorastardanza);

                            //alert("totalgeneral antes  "+totalgeneraltardanza+"  "+tardanzamostrar);
                            
                            totalgeneraltardanza=parseFloat(totalminutostardanza)+parseInt(totalhorastardanza);
                               
                            totalgeneraltardanza=totalgeneraltardanza.toFixed(2);

                            //alert("totalgeneral despues  "+totalgeneraltardanza+"  "+tardanzamostrar);

                            //totaltardanza= totaltardanza.toFixed(2);

                            //$("#totalsumatoriatardanza").text(totaltardanza);

                            //alert("horas-minutos"+totalgeneraltardanza+"  "+tardanzamostrar);

                        }
                        else{
                            
                            if((tardanzasplit[0])!="00" && (tardanzasplit[0])!=null && (tardanzasplit[0])!=""){
                                
                                //alert("pase por aqui1");
                                
                                totalhorastardanza1= parseFloat(totalhorastardanza1)+parseInt(tardanzasplit[0]);
                                
                                totalhorastardanza1= totalhorastardanza1.toFixed(2);
                                
                                //alert(totalhorasantes);
    
                                totaltardanzashoras= parseFloat(totalhorastardanza1);
                                
                                totalgeneraltardanza=parseFloat(totalgeneraltardanza)+parseFloat(totaltardanzashoras);
                               
                                totalgeneraltardanza=totalgeneraltardanza.toFixed(2);

                                //alert("horasssss"+totalgeneraltardanza+""+tardanzamostrar);
                            }
                            
                            
                        }
                        
                       
                   }

                    $("#totalsumatoriatardanza").text(totalgeneraltardanza);

                   contadorregistrostardanza++;

                   var datostardanza= row.tardanza;

                   if(datostardanza=="00:00"){

                       var tardanzadatos="";
                       return tardanzadatos;

                   }
                   else{

                      return data +' ';
                   }

                }
               
            },
            {
                className: 'text-center',
                name: 'antes',
                data: 'antes',

                //render json: {all_data: {data: @data, data1: @data1, data2: @data2}}} 

                render: function(data,type, row)  {

                    //alert(meta);
                   
                    if(contadorregistrosantes<=totalregistros){

                        var antes= row.antes;
                   
                        //alert(tardanza);
                        var antessplit= antes.split(":");

                        var antesmostrar= antessplit[0]+":"+antessplit[1];
                        
                        
                        //alert(contador+"  "+tardanzasplit[1]+"   "+totalminutostardanza);
                        if((antessplit[1])!="00" && (antessplit[1])!=null && (antessplit[1])!=""){
                            
                            var conversionminutos= (parseInt(antessplit[1]) * 1)/60;
                            
                            conversionminutos= parseFloat(conversionminutos);

                            totalminutosantes= parseFloat(totalminutosantes)+parseFloat(conversionminutos);

                            totalminutosantes= totalminutosantes.toFixed(2);

                            totalhorasantes= parseFloat(totalhorasantes)+parseInt(antessplit[0]);

                            totalhorasantes= totalhorasantes.toFixed(2);

                            totalanteshorasminutos= parseFloat(totalminutosantes) + parseInt(totalhorasantes);
                            
                            totalgeneral=parseFloat(totalgeneral)+parseFloat(totalanteshorasminutos);
                               
                            totalgeneral=totalgeneral.toFixed(2);

                        }
                        else{
                            
                           
                            //alert(antessplit[0]);
                            
                            if((antessplit[0])!="00" && (antessplit[0])!=null && (antessplit[0])!=""){
                                
                                //alert("pase por aqui1");
                                
                                totalhorasantes= parseFloat(totalhorasantes)+parseInt(antessplit[0]);
                                
                                totalhorasantes= totalhorasantes.toFixed(2);
                                
                                //alert(totalhorasantes);
    
                                totalanteshoras= parseFloat(totalhorasantes);
    
                                //totalantes= totalanteshoras.toFixed(2);
                                
                                totalgeneral=parseFloat(totalgeneral)+parseFloat(totalanteshoras);
                               
                                totalgeneral=totalgeneral.toFixed(2);
                            }
                            
                            
                            
                        }
                        
                         $("#totalsumatoriaantessalida").text(totalgeneral);
                    }
                    
                    contadorregistrosantes++;

                    var datosantessalida= row.antes;

                    if(datosantessalida=="00:00"){

                       var antessalidadatos="";
                       return antessalidadatos;

                    }
                    else{

                      return data +' ';
                    }
                    
                }

            },
            {
                className: 'text-center',
                name: 'observaci',
                //data: 'observaci'

                render: function(data, type, row) {

                    var observacion= row.observaci;
                   
                    var tardanza= row.tardanza;
                    var antes= row.antes;

                    var marca1=row.marca1;
                    var marcae=row.marcae;
                    
                    if((tardanza!="")&&(tardanza!="00:00")){
                        
                        var observacionasistencia="ASISTENCIA CON OBSERVACIONES";
                        return observacionasistencia;   
                        
                    }
                    else{

                        if((antes!="")&&(antes!="00:00")){

                            var observacionasistencia="ASISTENCIA CON OBSERVACIONES";
                            return observacionasistencia; 

                        }

                        else {

                            if((observacion=="Descanso")||(observacion==" Descanso")){
                            
                                var observacionasistencia="DESCANSO";
                                return observacionasistencia;   
                            
                            }
                            else{

                                if(observacion=="Falta"){
                            
                                  var observacionasistencia="FALTA";
                                  return observacionasistencia;   
                            
                                }
                                else{
                                   
                                   if((marca1=="")|| (marcae=="")){

                                      var observacionasistencia="ASISTENCIA CON OBSERVACIONES";
                                      return observacionasistencia;  

                                   }

                                    else{

                                      var observacionasistencia="ASISTENCIA OK";
                                      return observacionasistencia; 


                                    }
                                
                                   

                                }
                            }

                        }

                        
                       
                        
                    }
                        
                        
                   // alert(estatus);

                   //if((observacion!="")){
                        //alert(row.tecnicoasignado)  
                     // var observacionasistencia=row.observaci; 
                     // return observacionasistencia;
                   //}
                   //else{
                     // var observacionasistencia="OK";
                     // return observacionasistencia;   
                   //}

                } 

            }

        ],

        language: lenguaje

    });

    

}

function totalizarcantidadregistros(tipo) {

    var dp_fedesde_marcaasistencia = $("#dp_fedesde_marcaasistencia_repo").val();
    var dp_fehasta_marcaasistencia = $("#dp_fehasta_marcaasistencia_repo").val();


    busquedaGeneral.buscar = "";

    var route = "/totalizarcantidadregistros";


    $.post(route, {
            tipo:tipo,
            dp_fedesde_marcaasistencia : dp_fedesde_marcaasistencia,
            dp_fehasta_marcaasistencia : dp_fehasta_marcaasistencia,
            "_token": $("#token").val()
    },

    function() {
    })

    .fail(function(msj) {


    })

    .done(function(msj) {

        if (msj.mensaje == 'creado') {

            var cantidadregistros=msj.cantidadregistros;

            //SE LE ASIGNA LA CANTIDAD DE REGISTROS A LA VARIABLE GLOBAL
            totalregistros=cantidadregistros;
           
        }


    });


}


$("#btn_marcaasistencia_repo_buscar").on('click', function() {


    $("#msj-fechamayor-marcaasistenciarepo").fadeOut();
    $("#msj-noexistenregistros-marcaasistenciarepo").fadeOut();
    $("#msj-fechadesdemesactual-marcaasistenciarepo").fadeOut();
    $("#msj-fechahastamesactual-marcaasistenciarepo").fadeOut();
    $("#msj-fechahastadiaactual-marcaasistenciarepo").fadeOut();
    
    var dp_fedesde_marcaasistencia = $("#dp_fedesde_marcaasistencia_repo").val();
    var dp_fehasta_marcaasistencia = $("#dp_fehasta_marcaasistencia_repo").val();


    var validarfechavalor = validar_fechas(dp_fedesde_marcaasistencia , dp_fehasta_marcaasistencia);

    var fullDate = new Date()

    var dia = ((fullDate.getDate().length) === 1) ? (fullDate.getDate()) : '0' + (fullDate.getDate());
    var mes = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);



    var fechaactual = fullDate.getFullYear() + "-" + mes + "-" + dia;

    var validarfechadesdemarcaasistencia = validar_fechasmesactual(dp_fedesde_marcaasistencia, fechaactual);

    var validarfechahastamarcaasistencia = validar_fechasmesactual(dp_fehasta_marcaasistencia, fechaactual);


    if (validarfechavalor == 0) {


        $("#msj-fechamayor-marcaasistenciarepo").fadeIn();

        return false;

    }
    
    //SE INHABILITO LA VALIDACION QUE TIENE LA BUSQUEDA
    //SE AGREGO ESTE ELSE
    else{

        busquedaGeneral.dp_fedesde_marcaasistencia = dp_fedesde_marcaasistencia;
        busquedaGeneral.dp_fehasta_marcaasistencia = dp_fehasta_marcaasistencia;
        busquedaGeneral.tipo = "especifico";
        busquedaGeneral.buscar = "";

        subtotalminutostardanza=0;
        subtotalhorastardanza=0;
        subtotaltardanza=0;
        subtotalminutosantes= 0;
        subtotalhorasantes=0;
        subtotalantes=0;
        totalregistros=0;
        contadorregistrostardanza=1;
        contadorregistrosantes=1;

        var tipo="especifico"
        totalizarcantidadregistros(tipo);

        var blanco= "";
        $("#totalsumatoriatardanza").text(blanco);
        $("#totalsumatoriaantessalida").text(blanco);

        setTimeout(function() { 
                     //alert("pase1");
            totaltardanzatotalantes(dp_fedesde_marcaasistencia,dp_fehasta_marcaasistencia);
                     
                     //return false; 
        }, 2000);  

       


        jQuery('#listar_marcaasistencia_repo').DataTable().search('');
        jQuery('#listar_marcaasistencia_repo').DataTable().ajax.reload();

       

    } 


    
    /*if (validarfechadesdemarcaasistencia == 0) {


        $("#msj-fechadesdemesactual-marcaasistenciarepo").fadeIn();

        return false;

    } else {

        if (validarfechahastamarcaasistencia == 0) {


            $("#msj-fechahastamesactual-marcaasistenciarepo").fadeIn();

            return false;
               

        }
        else{

            var validarfechahastamarcaasistencia = validar_fechasdiaactual(dp_fehasta_marcaasistencia, fechaactual);

            if (validarfechahastamarcaasistencia == 0) {

                $("#msj-fechahastadiaactual-marcaasistenciarepo").fadeIn();
                
                return false;
            
            }
            else
            {

                busquedaGeneral.dp_fedesde_marcaasistencia = dp_fedesde_marcaasistencia;
                busquedaGeneral.dp_fehasta_marcaasistencia = dp_fehasta_marcaasistencia;
                busquedaGeneral.tipo = "especifico";
                busquedaGeneral.buscar = "";

                subtotalminutostardanza=0;
                subtotalhorastardanza=0;
                subtotaltardanza=0;
                subtotalminutosantes= 0;
                subtotalhorasantes=0;
                subtotalantes=0;
                totalregistros=0;
                contadorregistrostardanza=1;
                contadorregistrosantes=1;

                var tipo="especifico"
                totalizarcantidadregistros(tipo);

                var blanco= "";
                $("#totalsumatoriatardanza").text(blanco);
                $("#totalsumatoriaantessalida").text(blanco);


                jQuery('#listar_marcaasistencia_repo').DataTable().search('');
                jQuery('#listar_marcaasistencia_repo').DataTable().ajax.reload();
            }
        
        }

        //cargar_grafica_barras_total_ventas();

    }*/


});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_marcaasistencia_repo_limpiar").on('click', function() {


    var limpiar = 'todos';

    $("#msj-fechamayor-marcaasistenciarepo").fadeOut();
    $("#msj-noexistenregistros-marcaasistenciarepo").fadeOut();
    $("#msj-fechadesdemesactual-marcaasistenciarepo").fadeOut();
    $("#msj-fechahastamesactual-marcaasistenciarepo").fadeOut();
    $("#msj-fechahastadiaactual-marcaasistenciarepo").fadeOut();

    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_marcaasistencia_repo").val(fechahoy);
    $("#dp_fehasta_marcaasistencia_repo").val(fechahoy);
    
    var dp_fedesde_marcaasistencia = $("#dp_fedesde_marcaasistencia_repo").val();
    var dp_fehasta_marcaasistencia = $("#dp_fehasta_marcaasistencia_repo").val();

    reestablecerBusquedaGeneral();

    var tipo="general"
    totalizarcantidadregistros(tipo);
    
    totalminutostardanza=0;
    totalhorastardanza=0;
    totaltardanza =0;
    totalminutosantes=0;
    totalhorasantes=0;
    totalantes =0;
    contadorregistros=1;
    totalhorasantes=0;
    totalanteshorasminutos=0;
    totalanteshoras=0;
    totalgeneral=0;
    totaltardanzashoras=0;
    totalgeneraltardanza=0;
    totaltardanzahorasminutos=0;
    totalhorastardanza1=0;

    var blanco= "";
    $("#totalsumatoriatardanza").text(blanco);
    $("#totalsumatoriaantessalida").text(blanco);


    jQuery('#listar_marcaasistencia_repo').DataTable().search('');
    jQuery('#listar_marcaasistencia_repo').DataTable().ajax.reload();

    
    setTimeout(function() { 
                     //alert("pase1");
           totaltardanzatotalanteslimpiar();
                     
                     //return false; 
    }, 2000);

    

});

////////////////////////////FUNCIONES/////////////////////////////


jQuery("#excelmarcaasistenciarepo").on('click', function() {
   
    var route = "/excel/marcaasistenciarepo";

   
    var query = jQuery('#listar_marcaasistencia_repo').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});

jQuery("#pdfmarcaasistenciarepo").on('click', function() {

    var estatus="fecha";
   
    var tipobusqueda= busquedaGeneral.tipo;
    //var buscar = busquedaGeneral.buscar;
    var dp_fedesde_marcaasistencia=  busquedaGeneral.dp_fedesde_marcaasistencia;
    var dp_fehasta_marcaasistencia=  busquedaGeneral.dp_fehasta_marcaasistencia;

    var buscar = jQuery('#listar_marcaasistencia_repo').DataTable().search();
    busquedaGeneral.buscar = buscar;

    if(buscar==""){
       buscar="todos"; 
    }

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfmarcaasistenciarepo/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_marcaasistencia+"/"+dp_fehasta_marcaasistencia+"/"+estatus;

    window.open(url,'_blank');

    desactivarCargando();

   
});





