registros_conceptosincdia = [];
registros_filtrados_conceptosincdia = [];

var busquedaGeneral = {
    dp_fedesde_solicitudincdia: 'todos',
    dp_fehasta_solicitudincdia: 'todos',
    tipo: "general",
    codigoincdia: "",
    estatus:"",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_solicitudincdia = 'todos';
    busquedaGeneral.dp_fehasta_solicitudincdia = 'todos';
    busquedaGeneral.codigoincdia = "";
    busquedaGeneral.estatus = "";
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});

function filtrarDataincdia(data, term) {
    listaconceptosincdia = [];
    var contiene;
    if ($(data).filter(function(index) {
            contiene = this.text.toLowerCase().indexOf(term.toLowerCase()) != -1;
            if (contiene) {
                listaconceptosincdia.push(data[index]);
            }
            return contiene;
        }).length > 0) {
        return listaconceptosincdia;
    } else {
        return listaconceptosincdia;
    }
}

function select2incdia(idconceptosincdia,datos,select2conceptosincdia,mensajeNoEncontradoIncDia) {

    //alert("paso"+idconceptosincdia);
    //select2producto= 1;

    //select2conceptosincdia=2;

    jQuery(idconceptosincdia).select2({

        closeOnSelect: false,
        maximumSelectionSize: 1,
        multiple: true,
        initSelection: function(element, callback) {

            if (select2conceptosincdia!= null) {
                 
                $.each(datos, function(key, registro) {
                    
                    if (registro.id == select2conceptosincdia) {
                        callback(datos[key]);
                        return false;
                    }
                });
            } else {
                //alert("pase por aqui son iguales");
                //callback(datos[0]);
            }
        },

        formatSelectionTooBig: function(limit) {
            return 'S\u00F3lo puede seleccionar un Concepto';
        },
        createSearchChoice: function(term, data) {
            filtrarDataincdia(data, term);
        },
        formatNoMatches: function() {
            return mensajeNoEncontradoIncDia;
        },
        query: function(options) {

            if (options.term.length >= 2) {

                var criterio = "";

                criterio = options.term;

                var ruta = "/recargarconceptosincdia/" + criterio;

                listarincdia(idconceptosincdia, ruta, null, options.term);

                setTimeout(function() {
                    options.callback({
                        results: listaconceptosincdia
                    });
                }, 1000);
            } else {
                if (options.term != "") {
                    listaconceptosincdia = filtrarDataincdia(listaconceptosincdia, options.term);
                    options.callback({
                        results: listaconceptosincdia
                    });
                } else {
                    options.callback({
                        results: listaconceptosincdia
                    });
                }

            }
        },
        placeholder: "Por Favor Seleccione el Concepto",
        data: datos
    });

    select2Defecto(idconceptosincdia);

}

function select2Defecto(identificador) {
    jQuery(identificador).select2('val', []);
}

var listaconceptosincdia = [];

function listarincdia(idconceptosincdia, ruta, select2conceptosincdia = null, buscar = null) {
    //var route = buscar != null ? ruta + buscar : ruta;
    var mensajeNoEncontradoIncDia = 'No se encontr\u00F3 Conceptos'
    var mensajeFalloCarga = 'Fallo el cargado de Conceptos';
    listaconceptosincdia = [];
    $.get(ruta, function(data) {
            listaconceptosincdia = [];
            $.each(data, function(key, registro) {
                //alert(registro.codconcepto);
                listaconceptosincdia.push({
                    id: registro.codconcepto,
                    text: registro.desconcepto
                });
            });

            if (buscar == null) {

                //console.log(listaconceptosincdia);
                //console.log(idconceptosincdia);

                select2incdia(idconceptosincdia,listaconceptosincdia,select2conceptosincdia,mensajeNoEncontradoIncDia);
            }
        })
        .fail(function() {
            select2incdia(idconceptosincdia,listaconceptosincdia,select2conceptosincdia,mensajeFalloCarga);
        });
}


function ListarSolicitudIncDia() {

    //alert("pase");

    var route = "/listarsolicitudincdia";

    var recargartablatotal = jQuery('#listar_solicitudincdia').dataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                data.dp_fedesde_solicitudincdia = busquedaGeneral.dp_fedesde_solicitudincdia;
                data.dp_fehasta_solicitudincdia = busquedaGeneral.dp_fehasta_solicitudincdia;
                data.tipo = busquedaGeneral.tipo;
                data.codigoincdia = busquedaGeneral.codigoincdia;
                data.estatus = busquedaGeneral.estatus;
                
            }

        },

        columns: [

            {
                data: 'idsolicitud',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idsolicituddia" value="' + data + '" class="flat-blue">' + data + '';
                }
            },

            {
                className: 'text-center',
                data: 'fechasolicitud'
            },
            {
                className: 'text-center',
                name: 'horasolicitud',
                data: 'horasolicitud'
                
            },
            {
                className: 'text-center',
                name: 'fechainicio',
                data: 'fechainicio'
                
            },
            {
                className: 'text-center',
                name: 'fechafin',
                data: 'fechafin'
                
            },
            {
                className: 'text-center',
                name: 'concepto',
                data: 'concepto'
                
            },
            {
                className: 'text-center',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'documento',
                data: 'documento'
                
            },
            {
                className: 'text-center',
                name: 'aprobadarechazpor',
                data: 'aprobadarechazpor'
                
            },
            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "APROBADO") ?
                        "<center> <a href=''  id='detalleaprobarsolicitudincdia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "RECHAZADO") ?
                        "<center> <a href=''  id='detallerechazarsolicitudincdia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "REVISADO") ?
                        "<center> <a href=''  id='detallerevisarsolicitudincdia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                name: 'observacion',
                data: 'observacion'
                
            },
            {


                'defaultContent': "<center> <a href=''  id='detalleoperasolictudincdia' class='' data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>"

            },
            {
                
                className: 'text-center',
                data: 'codigoprocesado',
                defaultContent: ""

                
            },
            {
                className: 'text-center',
                name: 'estatus',
                data: 'estatus'
            }

        ],

        language: lenguaje

    });


}


$("#btn_solicitudincdia_buscar").on('click', function() {


    ocultarmensajesmodalesindexincdia();

    var estatus='';
   
    var dp_fedesde_solicitudincdia = $("#dp_fedesde_solicitudincdia").val();
    var dp_fehasta_solicitudincdia = $("#dp_fehasta_solicitudincdia").val();

    var codigoincdia = $("#s2_incidencia_solicitudincdia").val();

    if(codigoincdia==""){
        codigoincdia="todos"; 
    }

    var todas = $('input:radio[id=ra_todas_solicitudincdia]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_solicitudincdia]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_solicitudincdia]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_solicitudincdia]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_solicitudincdia]:checked').val();
    //alert(todas,ingresadas,aprobadas,rechazadas);


    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);

    } 
    
    var validarfechavalor = validar_fechas(dp_fedesde_solicitudincdia , dp_fehasta_solicitudincdia);

    if (validarfechavalor == 0) {


        $("#msj-fechamayor-solicitudincdia").fadeIn();

        return false;

    } 
    else 
    {

            //alert(codigoincdia);

            busquedaGeneral.dp_fedesde_solicitudincdia = dp_fedesde_solicitudincdia;
            busquedaGeneral.dp_fehasta_solicitudincdia = dp_fehasta_solicitudincdia;
            busquedaGeneral.tipo = "especifico";
            busquedaGeneral.codigoincdia = codigoincdia;
            busquedaGeneral.estatus = estatus;
            busquedaGeneral.buscar = "";


            jQuery('#listar_solicitudincdia').DataTable().search('');
            jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
 
    }



});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_solicitudincdia_limpiar").on('click', function() {

    ocultarmensajesmodalesindexincdia();
    var limpiar = 'todos';

    $("#s2id_s2_incidencia_solicitudincdia li.select2-search-choice").remove();
    $("#s2_incidencia_solicitudincdia").val("");

    fechahoy=obtener_fechaactual();

    //$("#ra_todas_solicitudincdia").iCheck('check');

    //alert(fechahoy);

    $("#dp_fedesde_solicitudincdia").val(fechahoy);
    $("#dp_fehasta_solicitudincdia").val(fechahoy);

    reestablecerBusquedaGeneral();

    jQuery('#listar_solicitudincdia').DataTable().search('');
    jQuery('#listar_solicitudincdia').DataTable().ajax.reload();

    $("#ra_todas_solicitudincdia").iCheck('uncheck');
    $("#ra_ingresadas_solicitudincdia").iCheck('uncheck');
    $("#ra_aprobadas_solicitudincdia").iCheck('uncheck');
    $("#ra_rechazadas_solicitudincdia").iCheck('uncheck');
    $("#ra_revisadas_solicitudincdia").iCheck('uncheck');


});

$("#btn_solicitudincdia_crear").on('click', function() {

    ocultarmensajesmodalesindexincdia();
    ocultarmensajesmodalescrearincdia();

    $("#s2id_sp_conceptos_crsolicitudincdia li.select2-search-choice").remove();
    $("#sp_conceptos_crsolicitudincdia").val("");

    $("#check_enviarcorreo_solicitudincdia").iCheck('check');

    var blanco = "";
    $("#tx_numerodocumento_crsolicitudincdia").val(blanco);
    $("#textarea_descripcion_crsolicitudincdia").val(blanco);

    $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
    $("#btnmodalcancelar_crsolicitudincdia").removeAttr('disabled');


    $('#solicitudincdia_crear').modal({
        backdrop: 'static',
        keyboard: false
    });


    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_crsolicitudincdia").val(fechahoy);
    $("#dp_fehasta_crsolicitudincdia").val(fechahoy);
   
    var idconceptosincdia = "#sp_conceptos_crsolicitudincdia";
    var route = "/cargarconceptosincdia";
    //alert(route);
    listarincdia(idconceptosincdia,route)

    var route = "/obtenercorreosupervisorsolicitudincdia";
    var token = $("#token").val();

    $.get(route, function (data) {
    })
    .done(function (data) {
      //alert(data);
      $("#txtoculto_correosupervisor_solicitudincdia").val(data);    
    })
    .fail(function () {
        alert("error");
    });


    var route = "/obtenercorrelativodocumentosolicitudincdia";
    var token = $("#token").val();

    $.get(route, function (data) {
    })
    .done(function (data) {
      //alert(data);
      $("#tx_numerodocumento_crsolicitudincdia").val(data);    
    })
    .fail(function () {
        alert("error");
    });



});

$("#btnmodalaceptar_crsolicitudincdia").on('click', function() {

    ocultarmensajesmodalesvalidacioncrincdia();

    $("#btnmodalaceptar_crsolicitudincdia").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_crsolicitudincdia").val();

    var fechahasta = $("#dp_fehasta_crsolicitudincdia").val();

    var concepto= $("#sp_conceptos_crsolicitudincdia").val();

    var descripcion = $("#textarea_descripcion_crsolicitudincdia").val();

    var documento= $("#tx_numerodocumento_crsolicitudincdia").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_solicitudincdia]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if (fechadesde == "") {

        $("#msj-fechadesdevalidacion-crsolicitudincdia").fadeIn();
        $('#solicitudincdia_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
        return false;
    }

    if (fechahasta == "") {

        $("#msj-fechahastavalidacion-crsolicitudincdia").fadeIn();
        $('#solicitudincdia_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
        return false;
    }

    var validarfechavalor = validar_fechas(fechadesde,fechahasta);

    //alert(validarfechavalor);

    if (validarfechavalor == 0) {

        $("#msj-fechadesdemayor-crsolicitudincdia").fadeIn();
        $('#solicitudincdia_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
        return false;

    } 


    if(concepto==""){
        
        $("#msj-seleccionarconcepto-crsolicitudincdia").fadeIn();
        $('#solicitudincdia_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
        return false;
    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_solicitudincdia").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-crsolicitudincdia").fadeIn();
            $('#solicitudincdia_crvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-crsolicitudincdia").fadeIn();
                $('#solicitudincdia_crvalidacion').modal('show');
                $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
                return false;
            }

            activarCargando();
            $("#btnmodalcancelar_crsolicitudincdia").attr("disabled", "disabled");

            var route = "/ingresarsolicitudincdia";
            var token = $("#token").val();

            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                dataType: 'json',

                data: {
                    fechadesde: fechadesde,
                    fechahasta: fechahasta,
                    concepto:concepto,
                    descripcion: descripcion,
                    documento: documento,
                    correosupervisor:correosupervisor
                },
                success: function(msj) {

                    if (msj.mensaje == 'creado') {

                        //setTimeout(function() {
                        
                            var route = "/enviarcorreosolicitudcreada";
                            var token = $("#token").val();

                            var tiposolicitud = msj.tipo;
                            var idsolicitud = msj.id_solicitud;
                            var fechadesdesolicitud= msj.fechadesde;
                            var fechahastasolicitud= msj.fechahasta;
                            var descripsolicitud= msj.descripsolicitud;
                            var descripconcepto= msj.descripconcepto;
                            var generadapor= msj.soligeneradapor;

                            var asunto= asuntocorreo(descripconcepto);
                            var textnumerosolicitud= textonumerosolicitud();
                            var textfechadesdesolicitud= textofechadesde();
                            var textfechahastasolicitud= textofechahasta();

                            var textconcepto= textoconcepto();
                            var textdescripcion= textodescripcion();
                            var textelaboradapor = textoelaboradapor();
                            var textaprobadarechazada= textoaprobadarechazada();

                            var correosupervisor= msj.correosupervisor;

                            //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                            $.ajax({
                                url: route,
                                headers: {
                                    'X-CSRF-TOKEN': token
                                },
                                type: 'POST',
                                dataType: 'json',

                                data: {
                                    asunto: asunto,
                                    tiposolicitud:tiposolicitud,
                                    textonumerosolicitud: textnumerosolicitud,
                                    numerosolicitud:idsolicitud,
                                    textofechadesdesolicitud:textfechadesdesolicitud,
                                    fechadesdesolicitud:fechadesdesolicitud,
                                    textofechahastasolicitud:textfechahastasolicitud,
                                    fechahastasolicitud:fechahastasolicitud,
                                    textoconcepto:textconcepto,
                                    descripcionconcepto:descripconcepto,
                                    textodescripcion:textdescripcion,
                                    descripcion:descripsolicitud,
                                    textoelaboradapor:textelaboradapor,
                                    generadapor:generadapor,
                                    textoaprobadarechazada:textaprobadarechazada,
                                    correosupervisor:correosupervisor
                                },
                                success: function(msj) {

                                    $("#msj-success-crsolicitudincdia").fadeIn();
                                     desactivarCargando();
                                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                                    setTimeout(function() {
                                        $('#solicitudincdia_crear').modal('hide')
                                    }, 1500);

                                    reestablecerBusquedaGeneral();
                                    jQuery('#listar_solicitudincdia').DataTable().search('');
                                    jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
                                },
                                error: function(msj) {
                                }
                            });
                        //}, 1500);
                    } else {
                        $('#msj-error-crsolicitudincdia').text(msj.error);
                        $("#msj-error-crsolicitudincdia").removeClass('hidden');
                        $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
                    }

                },
                error: function(msj) {
                }
            });
        }     
    }
    else{

        var route = "/ingresarsolicitudincdia";
        var token = $("#token").val();

        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',

            data: {
                fechadesde: fechadesde,
                fechahasta: fechahasta,
                concepto:concepto,
                documento: documento,
                descripcion: descripcion
            },
            success: function(msj) {

                if (msj.mensaje == 'creado') {

                    //setTimeout(function() {
                    
                    $("#msj-success-crsolicitudincdia").fadeIn();
                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudincdia_crear').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudincdia').DataTable().search('');
                    jQuery('#listar_solicitudincdia').DataTable().ajax.reload();

                   
                    //}, 1500);
                } else {
                    $('#msj-error-crsolicitudincdia').text(msj.error);
                    $("#msj-error-crsolicitudincdia").removeClass('hidden');
                    $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
                }

            },
            error: function(msj) {
            }
        });

    }
});


$("#btnmodalaceptar_crsolicitudincdiacorreo").on('click', function() {

    $("#btnmodalcancelar_crsolicitudincdia").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_crsolicitudincdia").val();

    var fechahasta = $("#dp_fehasta_crsolicitudincdia").val();

    var concepto= $("#sp_conceptos_crsolicitudincdia").val();

    var descripcion = $("#textarea_descripcion_crsolicitudincdia").val();

    var documento= $("#tx_numerodocumento_crsolicitudincdia").val();


    var route = "/ingresarsolicitudincdia";
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        type: 'POST',
        dataType: 'json',

        data: {
            fechadesde: fechadesde,
            fechahasta: fechahasta,
            concepto:concepto,
            documento: documento,
            descripcion: descripcion
        },
        success: function(msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crsolicitudincdia").fadeIn();
                $('#solicitudincdia_crvalidacioncorreo').modal('hide')
                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudincdia_crear').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudincdia').DataTable().search('');
                jQuery('#listar_solicitudincdia').DataTable().ajax.reload();

            } else {
                $('#msj-error-crsolicitudincdia').text(msj.error);
                $("#msj-error-crsolicitudincdia").removeClass('hidden');
                $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
                $("#btnmodalcancelar_crsolicitudincdia").removeAttr('disabled');
            }

        },
        error: function(msj) {
        }
    });

});


$("#btnmodalcancelar_crsolicitudincdiacorreo").on('click', function() {
    //alert("pase por aqui");
    $('#solicitudincdia_crvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');
    return false;

});


$(document.body).on('change', '#sp_conceptos_crsolicitudincdia', function() {

    ocultarmensajesmodalesvalidacioncrincdia();
    
    $("#btnmodalaceptar_crsolicitudincdia").removeAttr('disabled');

    var concepto= $("#sp_conceptos_crsolicitudincdia").val();

    var route = "/obtenersolicitudingresadaincdia/"+concepto;

    //alert(route);

    if(concepto!=""){

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var cantidadsoliingresadasincdia = data;

            if(cantidadsoliingresadasincdia>0){
                //alert(cantidadsoliingresadasincdia);

                $("#msj-solicitudconceptoingresada-crsolicitudincdia").fadeIn();
                $('#solicitudincdia_crvalidacion').modal('show');
                $("#s2id_sp_conceptos_crsolicitudincdia li.select2-search-choice").remove();
                $("#sp_conceptos_crsolicitudincdia").val("");
                $("#btnmodalaceptar_crsolicitudincdia").attr("disabled", "disabled");
                return false;
            }
        }); 
    }

});

$("#btn_solicitudincdia_editar").on('click', function() {

    ocultarmensajesmodalesindexincdia();
    ocultarmensajesmodaleseditarincdia();
   
    var valorregistroeditar= $('input:radio[name=idsolicituddia]:checked').val();

    $("#check_enviarcorreo_edsolicitudincdia").iCheck('check');

    if (valorregistroeditar==undefined){

        $("#msj-seleccionarregistroeditar-solicitudincdia").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudincdia/" + $('input:radio[name=idsolicituddia]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if(estatus!="INGRESADO"){

              $("#msj-soloeditarsolicitud-solicitudincdia").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
                $("#btnmodalcancelar_edsolicitudincdia").removeAttr('disabled');

                $('#solicitudincdia_editar').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudincdia/" + $('input:radio[name=idsolicituddia]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    var fechadesdeformateada= darformatofecha(data[0].fechadesde);

                    var fechahastaformateada= darformatofecha(data[0].fechahasta);

                    $("#dp_fedesde_edsolicitudincdia").val(fechadesdeformateada);
                    $("#dp_fehasta_edsolicitudincdia").val(fechahastaformateada);
                    $("#textarea_descripcion_edsolicitudincdia").val(data[0].descripcio);
                    $("#tx_numerodocumento_edsolicitudincdia").val(data[0].documento);
                    
                    var idconceptosincdia = "#sp_conceptos_edsolicitudincdia";
                    var route = "/cargarconceptosincdia";
                    listarincdia(idconceptosincdia,route,data[0].codconcepto);

                    var route = "/obtenercorreosupervisorsolicitudincdia";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      
                      $("#txtoculto_correosupervisor_edsolicitudincdia").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });
                    
                    $("#sp_conceptos_edsolicitudincdia").attr('disabled', true);
                    
                    $("#btnmodalaceptar_edsolicitudincdia").attr("data-editarsolicitudincdia", data[0].id);

                });

            }
       
        });  
    } 
});

$("#btnmodalaceptar_edsolicitudincdia").on('click', function() {

    $("#btnmodalaceptar_edsolicitudincdia").attr("disabled", "disabled");

    ocultarmensajesmodaleseditarincdia();
    ocultarmensajesmodalesvalidacionedincdia();

    var fechadesde = $("#dp_fedesde_edsolicitudincdia").val();

    var fechahasta = $("#dp_fehasta_edsolicitudincdia").val();

    var descripcion = $("#textarea_descripcion_edsolicitudincdia").val();

    var concepto= $("#sp_conceptos_edsolicitudincdia").val();

    var documento= $("#tx_numerodocumento_edsolicitudincdia").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_edsolicitudincdia]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if (fechadesde == "") {

            $("#msj-fechadesdevalidacion-edsolicitudincdia").fadeIn();
            $('#solicitudincdia_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
            return false;
    }

    if (fechahasta == "") {

            $("#msj-fechahastavalidacion-edsolicitudincdia").fadeIn();
            $('#solicitudincdia_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
            return false;
    }

    var validarfechavalor = validar_fechas(fechadesde,fechahasta);

    //alert(validarfechavalor);

    if (validarfechavalor == 0) {

        $("#msj-fechadesdemayor-edsolicitudincdia").fadeIn();
        $('#solicitudincdia_edvalidacion').modal('show');
        $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
        return false;

    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_edsolicitudincdia").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-edsolicitudincdia").fadeIn();
            $('#solicitudincdia_edvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-edsolicitudincdia").fadeIn();
                $('#solicitudincdia_edvalidacion').modal('show');
                $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
                return false;
            }

            activarCargandoed();
            $("#btnmodalcancelar_edsolicitudincdia").attr("disabled", "disabled");

            var route = "/actalizarsolicitudincdia"

            $.post(route, {
                id: $("#btnmodalaceptar_edsolicitudincdia").attr("data-editarsolicitudincdia"),
                fechadesde: fechadesde,
                fechahasta: fechahasta,
                descripcion: descripcion,
                concepto:concepto,
                documento: documento,
                correosupervisor:correosupervisor,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {

                if (msj.mensaje == 'actualizado') {

                    var route = "/enviarcorreosolicitudeditada";
                    var token = $("#token").val();

                    var idsolicitud = msj.id_solicitud;
                    var tiposolicitud = msj.tipo;
                    var fechadesdesolicitud= msj.fechadesde;
                    var fechahastasolicitud= msj.fechahasta;
                    var descripsolicitud= msj.descripsolicitud;
                    var descripconcepto= msj.descripconcepto;
                    var generadapor= msj.soligeneradapor;

                    var asunto= asuntocorreoeditada(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudeditada();
                    var textfechadesdesolicitud= textofechadesde();
                    var textfechahastasolicitud= textofechahasta();
                    var textconcepto= textoconcepto();
                    var textdescripcion= textodescripcion();
                    var textelaboradapor = textoelaboradapor();
                    var textaprobadarechazada= textoaprobadarechazada();

                    var correosupervisor= msj.correosupervisor;

                    //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textofechahastasolicitud:textfechahastasolicitud,
                            fechahastasolicitud:fechahastasolicitud,
                            numerosolicitud:idsolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textoelaboradapor: textelaboradapor,
                            generadapor:generadapor,
                            textoaprobadarechazada:textaprobadarechazada,
                            correosupervisor:correosupervisor
                        },
                        success: function(msj) {

                          $("#msj-success-edsolicitudincdia").fadeIn();
                          desactivarCargandoed();

                          //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                          setTimeout(function() {
                            $('#solicitudincdia_editar').modal('hide')
                          }, 1500);

                          reestablecerBusquedaGeneral();
                          jQuery('#listar_solicitudincdia').DataTable().search('');
                          jQuery('#listar_solicitudincdia').DataTable().ajax.reload();

                        },
                        
                        error: function(msj) {
                        }
                    });   
                }
                else{

                    $('#msj-error-edsolicitudincdia').text(msj.error);
                    $("#msj-error-edsolicitudincdia").removeClass('hidden');
                    $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
                }

            });

        }

    }
    else{

        var route = "/actalizarsolicitudincdia"

        $.post(route, {
            id: $("#btnmodalaceptar_edsolicitudincdia").attr("data-editarsolicitudincdia"),
            fechadesde: fechadesde,
            fechahasta: fechahasta,
            descripcion: descripcion,
            concepto:concepto,
            documento: documento,
            "_token": $("#token").val()
        }, function() {})
        .fail(function(msj) {

        })

        .done(function(msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edsolicitudincdia").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudincdia_editar').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudincdia').DataTable().search('');
                jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
            }
            else{

                $('#msj-error-edsolicitudincdia').text(msj.error);
                $("#msj-error-edsolicitudincdia").removeClass('hidden');
                $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
            }

        });

    }

});

$("#btnmodalaceptar_edsolicitudincdiacorreo").on('click', function() {

    $("#btnmodalcancelar_edsolicitudincdia").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_edsolicitudincdia").val();

    var fechahasta = $("#dp_fehasta_edsolicitudincdia").val();

    var descripcion = $("#textarea_descripcion_edsolicitudincdia").val();

    var documento= $("#tx_numerodocumento_edsolicitudincdia").val();

    var concepto= $("#sp_conceptos_edsolicitudincdia").val();

    var route = "/actalizarsolicitudincdia"

    $.post(route, {
            id: $("#btnmodalaceptar_edsolicitudincdia").attr("data-editarsolicitudincdia"),
            fechadesde: fechadesde,
            fechahasta: fechahasta,
            descripcion: descripcion,
            concepto:concepto,
            documento: documento,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
        if (msj.mensaje == 'actualizado') {

            $("#msj-success-edsolicitudincdia").fadeIn();
            $('#solicitudincdia_edvalidacioncorreo').modal('hide')
            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
               
                $('#solicitudincdia_editar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_solicitudincdia').DataTable().search('');
            jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-edsolicitudincdia').text(msj.error);
            $("#msj-error-edsolicitudincdia").removeClass('hidden');
            $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
            $("#btnmodalcancelar_edsolicitudincdia").removeAttr('disabled');
        }

    });

});

$("#btnmodalcancelar_edsolicitudincdiacorreo").on('click', function() {

    $('#solicitudincdia_edvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_edsolicitudincdia").removeAttr('disabled');
    return false;

});

$("#btn_solicitudincdia_eliminar").on('click', function() {

    ocultarmensajesmodalesindexincdia();
    ocultarmensajesmodaleseliminarincdia();
   
    var valorregistroeliminar= $('input:radio[name=idsolicituddia]:checked').val();

    $("#check_enviarcorreo_elsolicitudincdia").iCheck('check');

    if (valorregistroeliminar==undefined){

        $("#msj-seleccionarregistroeliminar-solicitudincdia").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudincdia/" + $('input:radio[name=idsolicituddia]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if(estatus!="INGRESADO"){

              $("#msj-soloeliminarsolicitud-solicitudincdia").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');
                $("#btnmodalcancelar_elsolicitudincdia").removeAttr('disabled');


                $('#solicitudincdia_eliminar').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudincdia/" + $('input:radio[name=idsolicituddia]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {
                    
                    $("#btnmodalaceptar_elsolicitudincdia").attr("data-eliminarsolicitudincdia", data[0].id);

                    var route = "/obtenercorreosupervisorsolicitudincdia";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      $("#txtoculto_correosupervisor_elsolicitudincdia").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                });

            }
       
        });  
    } 
});

$("#btnmodalaceptar_elsolicitudincdia").on('click', function() {

    $("#btnmodalaceptar_elsolicitudincdia").attr("disabled", "disabled");

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_elsolicitudincdia]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_elsolicitudincdia").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-elsolicitudincdia").fadeIn();
            $('#solicitudincdia_elvalidacioncorreo').modal('show');
            return false;
        }
        else{

           var validarcorreo= validar_correo(correosupervisor);

           if (validarcorreo == false) {

             $("#msj-correosupervisornovalido-elsolicitudincdia").fadeIn();
             $('#solicitudincdia_elvalidacion').modal('show');
             $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');
             return false;
           }

           activarCargandoel();
           $("#btnmodalcancelar_elsolicitudincdia").attr("disabled", "disabled");

           var route = "/eliminarsolicitudincdia"

            $.post(route, {
                id: $("#btnmodalaceptar_elsolicitudincdia").attr("data-eliminarsolicitudincdia"),
                correosupervisor:correosupervisor,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {

                if (msj.mensaje == 'eliminado') {

                    var route = "/enviarcorreosolicitudeliminada";
                    var token = $("#token").val();

                    var idsolicitud = msj.id_solicitud;
                    var tiposolicitud = msj.tipo;
                    var fechadesdesolicitud= msj.fechadesde;
                    var fechahastasolicitud= msj.fechahasta;
                    var descripsolicitud= msj.descripsolicitud;
                    var descripconcepto= msj.descripconcepto;
                    var generadapor= msj.soligeneradapor;

                    var asunto= asuntocorreoeliminada(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudeliminada();
                    var textfechadesdesolicitud= textofechadesde();
                    var textfechahastasolicitud= textofechahasta();
                    var textconcepto= textoconcepto();
                    var textdescripcion= textodescripcion();
                    var textelaboradapor = textoelaboradapor();

                    var correosupervisor= msj.correosupervisor;

                    //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textofechahastasolicitud:textfechahastasolicitud,
                            fechahastasolicitud:fechahastasolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textoelaboradapor: textelaboradapor,
                            generadapor:generadapor,
                            correosupervisor:correosupervisor
                        },
                        success: function(msj) {

                          $("#msj-success-elsolicitudincdia").fadeIn();
                          desactivarCargandoel();

                          //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                          setTimeout(function() {
                            $('#solicitudincdia_eliminar').modal('hide')
                          }, 1500);

                          reestablecerBusquedaGeneral();
                          jQuery('#listar_solicitudincdia').DataTable().search('');
                          jQuery('#listar_solicitudincdia').DataTable().ajax.reload();

                        },
                        
                        error: function(msj) {
                        }
                    });   
                }
                else{

                    $('#msj-error-elsolicitudincdia').text(msj.error);
                    $("#msj-error-elsolicitudincdia").removeClass('hidden');
                    $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');
                }

            });
        }
    }
    else{

        var route = "/eliminarsolicitudincdia"

        $.post(route, {
                id: $("#btnmodalaceptar_elsolicitudincdia").attr("data-eliminarsolicitudincdia"),
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(data) {
            if (data.mensaje == 'eliminado') {

                $("#msj-success-elsolicitudincdia").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudincdia_eliminar').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudincdia').DataTable().search('');
                jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
            }
            else{

                $('#msj-error-elsolicitudincdia').text(msj.error);
                $("#msj-error-elsolicitudincdia").removeClass('hidden');
                $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');

            }

        });

    }
});

$("#btnmodalaceptar_elsolicitudincdiacorreo").on('click', function() {

    $("#btnmodalcancelar_elsolicitudincdia").attr("disabled", "disabled");

    var route = "/eliminarsolicitudincdia"

    $.post(route, {
            id: $("#btnmodalaceptar_elsolicitudincdia").attr("data-eliminarsolicitudincdia"),
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
        if (msj.mensaje == 'eliminado') {

            $("#msj-success-elsolicitudincdia").fadeIn();
            $('#solicitudincdia_elvalidacioncorreo').modal('hide')

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#solicitudincdia_eliminar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_solicitudincdia').DataTable().search('');
            jQuery('#listar_solicitudincdia').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-elsolicitudincdia').text(msj.error);
            $("#msj-error-elsolicitudincdia").removeClass('hidden');
            $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');
            $("#btnmodalcancelar_elsolicitudincdia").removeAttr('disabled');
        }

    });

});

$("#btnmodalcancelar_elsolicitudincdiacorreo").on('click', function() {

    $('#solicitudincdia_elvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_elsolicitudincdia").removeAttr('disabled');
    return false;

});

$(document.body).on('click', '#detalleoperasolictudincdia', function() {


    var radioTabla = $($('#listar_solicitudincdia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodaloperasolicitudincdia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicituddia]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetoperasolicitudincdia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosoperasolicitudincdia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        

        var creadodesde = data.creadodesde;
        var latitudcreado = data.latitudcreado;
        var longitudcreado = data.longitudcreado;
        var editadodesde = data.editadodesde;
        var latitudeditado = data.latitudeditado;
        var longitudeditado = data.longitudeditado;
        
        $('#datosoperasolicitudincdia').append(
            '<tr><td><center>' + creadodesde + 
            '<td><center>' + latitudcreado + 
            '<td><center>' + longitudcreado + 
            '<td><center>' + editadodesde +
            '<td><center>' + latitudeditado +
            '<td><center>' + longitudeditado + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detalleaprobarsolicitudincdia', function() {


    var radioTabla = $($('#listar_solicitudincdia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalaprobarsolicitudincdia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicituddia]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetaprobarsolicitudincdia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosaprobarsolicitudincdia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fechaaprob = data.fechaaprob;
        var horaaprob = data.horaaprob;
        
        $('#datosaprobarsolicitudincdia').append(
            '<tr><td><center>' + fechaaprob + 
            '<td><center>' + horaaprob + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detallerechazarsolicitudincdia', function() {


    var radioTabla = $($('#listar_solicitudincdia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrechazarsolicitudincdia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicituddia]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrechazarsolicitudincdia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrechazarsolicitudincdia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharechaz = data.fecharechaz;
        var horarechaz = data.horarechaz;
        
        $('#datosrechazarsolicitudincdia').append(
            '<tr><td><center>' + fecharechaz + 
            '<td><center>' + horarechaz + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


$(document.body).on('click', '#detallerevisarsolicitudincdia', function() {


    var radioTabla = $($('#listar_solicitudincdia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrevisarsolicitudincdia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicituddia]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrevisarsolicitudincdia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrevisarsolicitudincdia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharevi = data.fecharevi;
        var horarevi = data.horarevi;
        
        $('#datosrevisarsolicitudincdia').append(
            '<tr><td><center>' + fecharevi + 
            '<td><center>' + horarevi + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


function ocultarmensajesmodalesindexincdia() {

    $("#msj-seleccionarregistroeditar-solicitudincdia").fadeOut();
    $("#msj-seleccionarregistroeliminar-solicitudincdia").fadeOut();
    $("#msj-soloeditarsolicitud-solicitudincdia").fadeOut();
    $("#msj-soloeliminarsolicitud-solicitudincdia").fadeOut();
    $("#msj-fechamayor-solicitudincdia").fadeOut();
 
}

function ocultarmensajesmodalescrearincdia(){

    $("#msj-success-crsolicitudincdia").fadeOut();
    $("#msj-error-crsolicitudincdia").fadeOut();
 
}

function ocultarmensajesmodalesvalidacioncrincdia(){

    $("#msj-fechadesdevalidacion-crsolicitudincdia").fadeOut();
    $("#msj-fechahastavalidacion-crsolicitudincdia").fadeOut();
    $("#msj-seleccionarconcepto-crsolicitudincdia").fadeOut();
    $("#msj-solicitudconceptoingresada-crsolicitudincdia").fadeOut();
    $("#msj-seleccionarconcepto-crsolicitudincdia").fadeOut();
    $("#msj-fechadesdemayor-crsolicitudincdia").fadeOut();
    $("#msj-correosupervisornovalido-crsolicitudincdia").fadeOut();
    
}

function ocultarmensajesmodaleseditarincdia(){

    $("#msj-success-edsolicitudincdia").fadeOut();
    $("#msj-error-edsolicitudincdia").fadeOut();

}

function ocultarmensajesmodalesvalidacionedincdia(){

    $("#msj-fechadesdevalidacion-edsolicitudincdia").fadeOut();
    $("#msj-fechahastavalidacion-edsolicitudincdia").fadeOut();
    $("#msj-fechadesdemayor-edsolicitudincdia").fadeOut();
    $("#msj-correosupervisornovalido-edsolicitudincdia").fadeOut();
    

}

function ocultarmensajesmodaleseliminarincdia(){

    $("#msj-success-elsolicitudincdia").fadeOut();
    $("#msj-error-elsolicitudincdia").fadeOut();
}

function cargarconceptosincdia(){

    registros_conceptosincdia = [];

    //console.log(registros);

    var route = "/cargarconceptosincdia";

    $.get(route, function(data) {

    })

    .done(function(data) {


        $.each(data, function(key, registro) {

            registros_conceptosincdia.push({
                    id: data[key].codconcepto,
                    text: data[key].desconcepto
            });

        });

        return registros_conceptosincdia;

    })

    .fail(function() {
        alert("error");

    });

}

function recargarconceptosincdia(criterio){


    var route = "/recargarconceptosincdia/" + criterio;

    $.get(route, function(data) {

    })

    .done(function(data) {

        //cambio
        registros_conceptosincdia = [];

        $.each(data, function(key, registro) {

            if (data == "") {

                registros[0] = "";

            } else {

                registros_conceptosincdia.push({
                        id: data[key].codconcepto,
                        text: data[key].desconcepto
                });

            }

        });

    })

    .fail(function() {
        alert("error");

    });

}


////////////////////////////FUNCIONES/////////////////////////////


jQuery("#excelsolicitudincdia").on('click', function() {
   
    var route = "/excel/solicitudincdia";

   
    var query = jQuery('#listar_solicitudincdia').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});

jQuery("#pdfsolicitudincdia").on('click', function() {

    var estatus="fecha";
   
    var tipobusqueda= busquedaGeneral.tipo;
    //var buscar = busquedaGeneral.buscar;
    var dp_fedesde_solicitudincdia=  busquedaGeneral.dp_fedesde_solicitudincdia;
    var dp_fehasta_solicitudincdia=  busquedaGeneral.dp_fehasta_solicitudincdia;

    var codigoincdia = $("#s2_incidencia_solicitudincdia").val();

    if(codigoincdia==""){
        codigoincdia="todos"; 
    }


    var buscar = jQuery('#listar_solicitudincdia').DataTable().search();
    busquedaGeneral.buscar = buscar;

    if(buscar==""){
       buscar="todos"; 
    }

    var todas = $('input:radio[id=ra_todas_solicitudincdia]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_solicitudincdia]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_solicitudincdia]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_solicitudincdia]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_solicitudincdia]:checked').val();

    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);
    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);
    } 

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfsolicitudincdia/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_solicitudincdia+"/"+dp_fehasta_solicitudincdia+"/"+estatus+"/"+codigoincdia;   

    //alert(url);
    window.open(url,'_blank');

    desactivarCargando();

   
});



jQuery(function($) {

    setTimeout(function() {

        $('#s2_incidencia_solicitudincdia').select2({

            closeOnSelect: true,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione la Incidencia",


            formatSelectionTooBig: function (limit) {
                return 'S\u00F3lo puede seleccionar un Registro';
            },

            formatNoMatches: function(options) {

                return 'No existen registros Asociados';

            },
            query: function(options) {

                if (options.term.length >= 2) {

                    //CAMBIO
                    registros_conceptosincdia.length= 0;
                    registros_filtrados_conceptosincdia.length= 0;

                    recargarconceptosincdia(options.term);

                    setTimeout(function() {
                        options.callback({
                            results: registros_conceptosincdia
                        })
                    }, 2000); 

                } else {

                    if (options.term != "") {

                        filtrar_data(registros_conceptosincdia, options.term,registros_filtrados_conceptosincdia);

                        options.callback({
                            results: data_registrosfiltrados 
                        })

                    } else {

                        options.callback({
                            results: registros_conceptosincdia
                        })
                    }
                }

            }
        });

     }, 500);

});


