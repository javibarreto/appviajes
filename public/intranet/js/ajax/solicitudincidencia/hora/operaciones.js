registros_conceptosinchora = [];
registros_filtrados_conceptosinchora = [];

var busquedaGeneral = {
    dp_fedesde_solicitudinchora: 'todos',
    dp_fehasta_solicitudinchora: 'todos',
    tipo: "general",
    codigoinchora: "",
    estatus:"",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_solicitudinchora = 'todos';
    busquedaGeneral.dp_fehasta_solicitudinchora = 'todos';
    busquedaGeneral.codigoinchora = "";
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});

function filtrarDatainchora(data, term) {
    listaconceptosinchora = [];
    var contiene;
    if ($(data).filter(function(index) {
            contiene = this.text.toLowerCase().indexOf(term.toLowerCase()) != -1;
            if (contiene) {
                listaconceptosinchora.push(data[index]);
            }
            return contiene;
        }).length > 0) {
        return listaconceptosinchora;
    } else {
        return listaconceptosinchora;
    }
}

function select2inchora(idconceptosinchora, datos, select2conceptosinchora, mensajeNoEncontradoIncHora) {

    //alert(select2producto);

    //select2producto= 1;

    jQuery(idconceptosinchora).select2({

        closeOnSelect: false,
        maximumSelectionSize: 1,
        multiple: true,
        initSelection: function(element, callback) {
            if (select2conceptosinchora != null) {
                //alert("pase por aqui");
                $.each(datos, function(key, registro) {
                    if (registro.id == select2conceptosinchora) {
                        callback(datos[key]);
                        return false;
                    }
                });
            } else {
                //alert("pase por aqui son iguales");
                //callback(datos[0]);
            }
        },
        formatSelectionTooBig: function(limit) {
            return 'S\u00F3lo puede seleccionar un Concepto';
        },
        createSearchChoice: function(term, data) {
            filtrarDatainchora(data, term);
        },
        formatNoMatches: function() {
            return mensajeNoEncontradoIncHora;
        },
        query: function(options) {

            if (options.term.length >= 2) {

                var criterio = "";

                criterio = options.term;

                var ruta = "/recargarconceptosinchora/" + criterio;

                listarinchora(idconceptosinchora, ruta, null, options.term);

                setTimeout(function() {
                    options.callback({
                        results: listaconceptosinchora
                    });
                }, 1000);
            } else {
                if (options.term != "") {
                    listaconceptosinchora = filtrarDatainchora(listaconceptosinchora, options.term);
                    options.callback({
                        results: listaconceptosinchora
                    });
                } else {
                    options.callback({
                        results: listaconceptosinchora
                    });
                }

            }
        },
        placeholder: "Por Favor Seleccione el Concepto",
        data: datos
    });

     select2Defecto(idconceptosinchora);

}

function select2Defecto(identificador) {
    jQuery(identificador).select2('val', []);
}



var listaconceptosinchora = [];

function listarinchora(idconceptosinchora, ruta, select2conceptosinchora = null, buscar = null) {
    //var route = buscar != null ? ruta + buscar : ruta;
    var mensajeNoEncontradoIncHora = 'No se encontr\u00F3 Conceptos'
    var mensajeFalloCarga = 'Fallo el cargado de Conceptos';
    listaconceptosinchora = [];
    $.get(ruta, function(data) {
            listaconceptosinchora = [];
            $.each(data, function(key, registro) {
                //alert(registro.id_almacen);
                listaconceptosinchora.push({
                    id: registro.codconcepto,
                    text: registro.desconcepto
                });

            });

            if (buscar == null) {
                select2inchora(idconceptosinchora, listaconceptosinchora, select2conceptosinchora, mensajeNoEncontradoIncHora);
            }
        })
        .fail(function() {
            select2inchora(idconceptosinchora, listaconceptosinchora, select2conceptosinchora, mensajeFalloCarga);
        });
}


function ListarSolicitudIncHora() {

    //alert("pase");

    var route = "/listarsolicitudinchora";

    var recargartablatotal = jQuery('#listar_solicitudinchora').dataTable({
        
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                data.dp_fedesde_solicitudinchora = busquedaGeneral.dp_fedesde_solicitudinchora;
                data.dp_fehasta_solicitudinchora = busquedaGeneral.dp_fehasta_solicitudinchora;
                data.tipo = busquedaGeneral.tipo;
                data.codigoinchora = busquedaGeneral.codigoinchora;
                data.estatus = busquedaGeneral.estatus;
                
            }

        },

        columns: [

            {
                data: 'idsolicitud',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idsolicitudhora" value="' + data + '" class="flat-blue">' + data + '';
                }
            },

            {
                className: 'text-center',
                data: 'fechasolicitud'
            },
            {
                className: 'text-center',
                name: 'horasolicitud',
                data: 'horasolicitud'
                
            },
            {
                className: 'text-center',
                name: 'fechainicio',
                data: 'fechainicio'
                
            },
            {
                className: 'text-center',
                name: 'concepto',
                data: 'concepto'
                
            },
            {
                className: 'text-center',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'documento',
                data: 'documento'
                
            },
            {
                className: 'text-center',
                name: 'aprobadarechazpor',
                data: 'aprobadarechazpor'
                
            },
            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "APROBADO") ?
                        "<center> <a href=''  id='detalleaprobarsolicitudinchora'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "RECHAZADO") ?
                        "<center> <a href=''  id='detallerechazarsolicitudinchora'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "REVISADO") ?
                        "<center> <a href=''  id='detallerevisarsolicitudinchora'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },
            {
                className: 'text-center',
                name: 'observacion',
                data: 'observacion'
                
            },

            {


                'defaultContent': "<center> <a href=''  id='detalleoperasolictudinchora' class='' data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>"

            },
            
            {
                
                className: 'text-center',
                data: 'codigoprocesado',
                defaultContent: ""

                
            },

            {
                className: 'text-center',
                name: 'estatus',
                data: 'estatus'
            }

        ],

        language: lenguaje

    });


}


$("#btn_solicitudinchora_buscar").on('click', function() {

    ocultarmensajesmodalesindexinchora();

    var estatus='';
  
    var dp_fedesde_solicitudinchora = $("#dp_fedesde_solicitudinchora").val();
    var dp_fehasta_solicitudinchora = $("#dp_fehasta_solicitudinchora").val();

    var codigoinchora = $("#s2_incidencia_solicitudinchora").val();

    if(codigoinchora==""){
        codigoinchora="todos"; 
    }

    
    var todas = $('input:radio[id=ra_todas_solicitudinchora]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_solicitudinchora]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_solicitudinchora]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_solicitudinchora]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_solicitudinchora]:checked').val();
    //alert(todas,ingresadas,aprobadas,rechazadas);


    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   
    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);

    } 
    
    var validarfechavalor = validar_fechas(dp_fedesde_solicitudinchora , dp_fehasta_solicitudinchora);

    if (validarfechavalor == 0) {


        $("#msj-fechamayor-solicitudinchora").fadeIn();

        return false;

    } 
    else 
    {

            busquedaGeneral.dp_fedesde_solicitudinchora = dp_fedesde_solicitudinchora;
            busquedaGeneral.dp_fehasta_solicitudinchora = dp_fehasta_solicitudinchora;
            busquedaGeneral.tipo = "especifico";
            busquedaGeneral.codigoinchora = codigoinchora;
            busquedaGeneral.estatus = estatus;
            busquedaGeneral.buscar = "";


            jQuery('#listar_solicitudinchora').DataTable().search('');
            jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
 
    }



});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_solicitudinchora_limpiar").on('click', function() {

    ocultarmensajesmodalesindexinchora();

    var limpiar = 'todos';

    $("#s2id_s2_incidencia_solicitudinchora li.select2-search-choice").remove();
    $("#s2_incidencia_solicitudinchora").val("");


    fechahoy=obtener_fechaactual();

     $("#ra_todas_solicitudinchora").iCheck('check');

    //alert(fechahoy);


    $("#dp_fedesde_solicitudinchora").val(fechahoy);
    $("#dp_fehasta_solicitudinchora").val(fechahoy);

    reestablecerBusquedaGeneral();

   
    jQuery('#listar_solicitudinchora').DataTable().search('');
    jQuery('#listar_solicitudinchora').DataTable().ajax.reload();

    $("#ra_todas_solicitudinchora").iCheck('uncheck');
    $("#ra_ingresadas_solicitudinchora").iCheck('uncheck');
    $("#ra_aprobadas_solicitudinchora").iCheck('uncheck');
    $("#ra_rechazadas_solicitudinchora").iCheck('uncheck');
    $("#ra_revisadas_solicitudinchora").iCheck('uncheck');


});

$("#btn_solicitudinchora_crear").on('click', function() {

    ocultarmensajesmodalesindexinchora();
    ocultarmensajesmodalescrearinchora();

    $("#s2id_sp_conceptos_crsolicitudinchora li.select2-search-choice").remove();
    $("#sp_conceptos_crsolicitudinchora").val("");

    $("#check_enviarcorreo_solicitudinchora").iCheck('check');

    var blanco = "";
    $("#tx_numerodocumento_crsolicitudinchora").val(blanco);
    $("#textarea_descripcion_crsolicitudinchora").val(blanco);


    $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
    $("#btnmodalcancelar_crsolicitudinchora").removeAttr('disabled');

    $('#solicitudinchora_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_crsolicitudinchora").val(fechahoy);

    var idconceptosinchora = "#sp_conceptos_crsolicitudinchora";
    var route = "/cargarconceptosinchora";
    //alert(route);
    listarinchora(idconceptosinchora, route)

    var route = "/obtenercorreosupervisorsolicitudinchora";
    var token = $("#token").val();

    $.get(route, function (data) {
    })
    .done(function (data) {
      //alert(data);
      $("#txtoculto_correosupervisor_solicitudinchora").val(data);    
    })
    .fail(function () {
        alert("error");
    });

    var route = "/obtenercorrelativodocumentosolicitudinchora";
    var token = $("#token").val();

    $.get(route, function (data) {
    })
    .done(function (data) {
      //alert(data);
      $("#tx_numerodocumento_crsolicitudinchora").val(data);    
    })
    .fail(function () {
        alert("error");
    });

});

$("#btnmodalaceptar_crsolicitudinchora").on('click', function() {

    ocultarmensajesmodalesvalidacioncrinchora();

    $("#btnmodalaceptar_crsolicitudinchora").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_crsolicitudinchora").val();

    var concepto= $("#sp_conceptos_crsolicitudinchora").val();

    var descripcion = $("#textarea_descripcion_crsolicitudinchora").val();

    var documento= $("#tx_numerodocumento_crsolicitudinchora").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_solicitudinchora]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 


    if (fechadesde == "") {

        $("#msj-fechadesdevalidacion-crsolicitudinchora").fadeIn();
        $('#solicitudinchora_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
        return false;
    }


    if(concepto==""){
        
        $("#msj-seleccionarconcepto-crsolicitudinchora").fadeIn();
        $('#solicitudinchora_crvalidacion').modal('show');
        $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
        return false;
    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_solicitudinchora").val();

        if (correosupervisor=="") {

            $("#msj-correosupervisorvacio-crsolicitudinchora").fadeIn();
            $('#solicitudinchora_crvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-crsolicitudinchora").fadeIn();
                $('#solicitudinchora_crvalidacion').modal('show');
                $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
                return false;
            }

            activarCargando();
            $("#btnmodalcancelar_crsolicitudinchora").attr("disabled", "disabled");

            var route = "/ingresarsolicitudinchora";
            var token = $("#token").val();

            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                dataType: 'json',

                data: {
                    fechadesde: fechadesde,
                    concepto:concepto,
                    descripcion: descripcion,
                    documento: documento,
                    correosupervisor:correosupervisor
                },
                success: function(msj) {

                    if (msj.mensaje == 'creado') {

                        var route = "/enviarcorreosolicitudcreada";
                        var token = $("#token").val();

                        var idsolicitud = msj.id_solicitud;
                        var tiposolicitud = msj.tipo;
                        var fechadesdesolicitud= msj.fechadesde;
                        var descripsolicitud= msj.descripsolicitud;
                        var descripconcepto= msj.descripconcepto;
                        var generadapor= msj.soligeneradapor;

                        var asunto= asuntocorreo(descripconcepto);
                        var textnumerosolicitud= textonumerosolicitud();
                        var textfechadesdesolicitud= textofecha();
                        var textconcepto= textoconcepto();
                        var textdescripcion= textodescripcion();
                        var textelaboradapor = textoelaboradapor();
                        var textaprobadarechazada= textoaprobadarechazada();

                        var correosupervisor= msj.correosupervisor;

                        //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                        $.ajax({
                            url: route,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            type: 'POST',
                            dataType: 'json',

                            data: {
                                asunto: asunto,
                                tiposolicitud:tiposolicitud,
                                textonumerosolicitud: textnumerosolicitud,
                                numerosolicitud:idsolicitud,
                                textofechadesdesolicitud:textfechadesdesolicitud,
                                fechadesdesolicitud:fechadesdesolicitud,
                                textoconcepto: textconcepto,
                                descripcionconcepto:descripconcepto,
                                textodescripcion: textdescripcion,
                                descripcion:descripsolicitud,
                                textoelaboradapor: textelaboradapor,
                                generadapor:generadapor,
                                textoaprobadarechazada:textaprobadarechazada,
                                correosupervisor:correosupervisor
                            },
                            success: function(msj) {

                                $("#msj-success-crsolicitudinchora").fadeIn();
                                 desactivarCargando();

                                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                                setTimeout(function() {
                                    $('#solicitudinchora_crear').modal('hide')
                                }, 1500);

                                reestablecerBusquedaGeneral();
                                jQuery('#listar_solicitudinchora').DataTable().search('');
                                jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
                            },
                            error: function(msj) {
                            }
                        });
                    } else {
                        $('#msj-error-crsolicitudinchora').text(msj.error);
                        $("#msj-error-crsolicitudinchora").removeClass('hidden');
                        $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
                    }
                },
                   error: function(msj) {
                }
            });
        }
            
    }
    else{

        var route = "/ingresarsolicitudinchora";
        var token = $("#token").val();

        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',

            data: {
                fechadesde: fechadesde,
                concepto:concepto,
                descripcion: descripcion,
                documento: documento
            },
            success: function(msj) {

                if (msj.mensaje == 'creado') {

                    $("#msj-success-crsolicitudinchora").fadeIn();

                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudinchora_crear').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudinchora').DataTable().search('');
                    jQuery('#listar_solicitudinchora').DataTable().ajax.reload();

                    
                } else {
                    $('#msj-error-crsolicitudinchora').text(msj.error);
                    $("#msj-error-crsolicitudinchora").removeClass('hidden');
                    $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
                }

            },
            error: function(msj) {
            }
        });

    }

});

$("#btnmodalaceptar_crsolicitudinchoracorreo").on('click', function() {

    $("#btnmodalcancelar_crsolicitudinchora").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_crsolicitudinchora").val();

    var concepto= $("#sp_conceptos_crsolicitudinchora").val();

    var descripcion = $("#textarea_descripcion_crsolicitudinchora").val();

    var documento= $("#tx_numerodocumento_crsolicitudinchora").val();


    var route = "/ingresarsolicitudinchora";
    var token = $("#token").val();

    $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',

            data: {
                fechadesde: fechadesde,
                concepto:concepto,
                documento: documento,
                descripcion: descripcion
            },
            success: function(msj) {

                if (msj.mensaje == 'creado') {

                    $("#msj-success-crsolicitudinchora").fadeIn();
                    $('#solicitudinchora_crvalidacioncorreo').modal('hide')

                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudinchora_crear').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudinchora').DataTable().search('');
                    jQuery('#listar_solicitudinchora').DataTable().ajax.reload();

                    
                } else {
                    $('#msj-error-crsolicitudinchora').text(msj.error);
                    $("#msj-error-crsolicitudinchora").removeClass('hidden');
                    $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
                    $("#btnmodalcancelar_crsolicitudinchora").removeAttr('disabled');
                }

            },
            error: function(msj) {
            }
    });


});

$("#btnmodalcancelar_crsolicitudinchoracorreo").on('click', function() {

    //alert("pase por aqui");
    $('#solicitudinchora_crvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');
    return false;

});

$(document.body).on('change', '#sp_conceptos_crsolicitudinchora', function() {

    ocultarmensajesmodalesvalidacioncrinchora();
    
    $("#btnmodalaceptar_crsolicitudinchora").removeAttr('disabled');

    var concepto= $("#sp_conceptos_crsolicitudinchora").val();

    var route = "/obtenersolicitudingresadainchora/"+concepto;

    //alert(route);

    if(concepto!=""){

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var cantidadsoliingresadasinchora = data;

            if(cantidadsoliingresadasinchora>0){
                //alert(cantidadsoliingresadasincdia);

                $("#msj-solicitudconceptoingresada-crsolicitudinchora").fadeIn();
                $('#solicitudinchora_crvalidacion').modal('show');
                $("#s2id_sp_conceptos_crsolicitudinchora li.select2-search-choice").remove();
                $("#sp_conceptos_crsolicitudinchora").val("");
                $("#btnmodalaceptar_crsolicitudinchora").attr("disabled", "disabled");
                return false;
            }
        }); 
    }

});

$("#btn_solicitudinchora_editar").on('click', function() {

    ocultarmensajesmodalesindexinchora();
    ocultarmensajesmodaleseditarinchora();
   
    var valorregistroeditar= $('input:radio[name=idsolicitudhora]:checked').val();

    $("#check_enviarcorreo_edsolicitudinchora").iCheck('check');

    if (valorregistroeditar==undefined){

        $("#msj-seleccionarregistroeditar-solicitudinchora").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudinchora/" + $('input:radio[name=idsolicitudhora]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if(estatus!="INGRESADO"){

              $("#msj-soloeditarsolicitud-solicitudinchora").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
                $("#btnmodalcancelar_edsolicitudinchora").removeAttr('disabled');


                $('#solicitudinchora_editar').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudinchora/" + $('input:radio[name=idsolicitudhora]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    var fechadesdeformateada= darformatofecha(data[0].fechadesde);


                    $("#dp_fedesde_edsolicitudinchora").val(fechadesdeformateada);
                    $("#textarea_descripcion_edsolicitudinchora").val(data[0].descripcio);
                    $("#tx_numerodocumento_edsolicitudinchora").val(data[0].documento);
                    
                    var idconceptosinchora = "#sp_conceptos_edsolicitudinchora";
                    var route = "/cargarconceptosinchora";
                    listarinchora(idconceptosinchora,route,data[0].codconcepto);

                    var route = "/obtenercorreosupervisorsolicitudinchora";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      $("#txtoculto_correosupervisor_edsolicitudinchora").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                    $("#sp_conceptos_edsolicitudinchora").attr('disabled', true);
                    
                    $("#btnmodalaceptar_edsolicitudinchora").attr("data-editarsolicitudinchora", data[0].id);

                });

            }
       
        });  
    } 
});

$("#btnmodalaceptar_edsolicitudinchora").on('click', function() {

    $("#btnmodalaceptar_edsolicitudinchora").attr("disabled", "disabled");

    ocultarmensajesmodaleseditarinchora();
    ocultarmensajesmodalesvalidacionedinchora();


    var fechadesde = $("#dp_fedesde_edsolicitudinchora").val();

    var descripcion = $("#textarea_descripcion_edsolicitudinchora").val();

    var concepto= $("#sp_conceptos_edsolicitudinchora").val();

    var documento= $("#tx_numerodocumento_edsolicitudinchora").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_edsolicitudinchora]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 


    if (fechadesde == "") {

        $("#msj-fechadesdevalidacion-edsolicitudinchora").fadeIn();
        $('#solicitudinchora_edvalidacion').modal('show');
        $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
        return false;
    }

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_edsolicitudinchora").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-edsolicitudinchora").fadeIn();
            $('#solicitudinchora_edvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-edsolicitudinchora").fadeIn();
                $('#solicitudinchora_edvalidacion').modal('show');
                $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
                return false;
            }


            activarCargandoed();
            $("#btnmodalcancelar_edsolicitudinchora").attr("disabled", "disabled");

            var route = "/actalizarsolicitudinchora"

            $.post(route, {
                id: $("#btnmodalaceptar_edsolicitudinchora").attr("data-editarsolicitudinchora"),
                fechadesde: fechadesde,
                descripcion: descripcion,
                concepto:concepto,
                documento: documento,
                correosupervisor:correosupervisor,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {

                if (msj.mensaje == 'actualizado') {

                    var route = "/enviarcorreosolicitudeditada";
                    var token = $("#token").val();

                    var idsolicitud = msj.id_solicitud;
                    var tiposolicitud = msj.tipo;
                    var fechadesdesolicitud= msj.fechadesde;
                    var descripsolicitud= msj.descripsolicitud;
                    var descripconcepto= msj.descripconcepto;
                    var generadapor= msj.soligeneradapor;

                    var asunto= asuntocorreoeditada(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudeditada();
                    var textfechadesdesolicitud= textofecha();
                    var textconcepto= textoconcepto();
                    var textdescripcion= textodescripcion();
                    var textelaboradapor = textoelaboradapor();
                    var textaprobadarechazada= textoaprobadarechazada();

                    var correosupervisor= msj.correosupervisor;

                    //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textoelaboradapor: textelaboradapor,
                            generadapor:generadapor,
                            textoaprobadarechazada:textaprobadarechazada,
                            correosupervisor:correosupervisor
                        },
                        success: function(msj) {

                          $("#msj-success-edsolicitudinchora").fadeIn();
                          desactivarCargandoed();

                          //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                          setTimeout(function() {
                            $('#solicitudinchora_editar').modal('hide')
                          }, 1500);

                          reestablecerBusquedaGeneral();
                          jQuery('#listar_solicitudinchora').DataTable().search('');
                          jQuery('#listar_solicitudinchora').DataTable().ajax.reload();

                        },
                        
                        error: function(msj) {
                        }
                    });   
                }
                else{

                    $('#msj-error-edsolicitudinchora').text(msj.error);
                    $("#msj-error-edsolicitudinchora").removeClass('hidden');
                    $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
                }

            });

        }

    }
    else{

        var route = "/actalizarsolicitudinchora"

        $.post(route, {
            id: $("#btnmodalaceptar_edsolicitudinchora").attr("data-editarsolicitudinchora"),
            fechadesde: fechadesde,
            descripcion: descripcion,
            documento: documento,
            concepto:concepto,
            "_token": $("#token").val()
        }, function() {})
        .fail(function(msj) {

        })

        .done(function(data) {
            if (data.mensaje == 'actualizado') {

                $("#msj-success-edsolicitudinchora").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudinchora_editar').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudinchora').DataTable().search('');
                jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
            }
            else{

                $('#msj-error-edsolicitudinchora').text(msj.error);
                $("#msj-error-edsolicitudinchora").removeClass('hidden');
                $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
            }

        });


    }


});

$("#btnmodalaceptar_edsolicitudinchoracorreo").on('click', function() {

    $("#btnmodalcancelar_edsolicitudinchora").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_edsolicitudinchora").val();

    var descripcion = $("#textarea_descripcion_edsolicitudinchora").val();

    var documento= $("#tx_numerodocumento_edsolicitudinchora").val();

    var concepto= $("#sp_conceptos_edsolicitudinchora").val();

    var route = "/actalizarsolicitudinchora"

    $.post(route, {
            id: $("#btnmodalaceptar_edsolicitudinchora").attr("data-editarsolicitudinchora"),
            fechadesde: fechadesde,
            descripcion: descripcion,
            concepto:concepto,
            documento: documento,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
        if (msj.mensaje == 'actualizado') {

            $("#msj-success-edsolicitudinchora").fadeIn();
            $('#solicitudinchora_edvalidacioncorreo').modal('hide')
            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
               
                $('#solicitudinchora_editar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_solicitudinchora').DataTable().search('');
            jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-edsolicitudinchora').text(msj.error);
            $("#msj-error-edsolicitudinchora").removeClass('hidden');
            $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
            $("#btnmodalcancelar_edsolicitudinchora").removeAttr('disabled');
        }

    });

});

$("#btnmodalcancelar_edsolicitudinchoracorreo").on('click', function() {

    $('#solicitudinchora_edvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_edsolicitudinchora").removeAttr('disabled');
    return false;

});



$("#btn_solicitudinchora_eliminar").on('click', function() {

    ocultarmensajesmodalesindexinchora();
    ocultarmensajesmodaleseliminarinchora();
   
    var valorregistroeliminar= $('input:radio[name=idsolicitudhora]:checked').val();

    $("#check_enviarcorreo_elsolicitudinchora").iCheck('check');

    if (valorregistroeliminar==undefined){

        $("#msj-seleccionarregistroeliminar-solicitudinchora").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudinchora/" + $('input:radio[name=idsolicitudhora]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if(estatus!="INGRESADO"){

              $("#msj-soloeliminarsolicitud-solicitudinchora").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');
                $("#btnmodalcancelar_elsolicitudinchora").removeAttr('disabled');


                $('#solicitudinchora_eliminar').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudinchora/" + $('input:radio[name=idsolicitudhora]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {
                    
                    $("#btnmodalaceptar_elsolicitudinchora").attr("data-eliminarsolicitudinchora", data[0].id);

                    var route = "/obtenercorreosupervisorsolicitudinchora";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      $("#txtoculto_correosupervisor_elsolicitudinchora").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                });

            }
       
        });  
    } 
});

$("#btnmodalaceptar_elsolicitudinchora").on('click', function() {

    $("#btnmodalaceptar_elsolicitudinchora").attr("disabled", "disabled");

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_elsolicitudinchora]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_elsolicitudinchora").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-elsolicitudinchora").fadeIn();
            $('#solicitudinchora_elvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-elsolicitudinchora").fadeIn();
                $('#solicitudinchora_elvalidacion').modal('show');
                $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');
                return false;
            }

            activarCargandoel();
            $("#btnmodalcancelar_elsolicitudinchora").attr("disabled", "disabled");

            var route = "/eliminarsolicitudinchora"

            $.post(route, {
                id: $("#btnmodalaceptar_elsolicitudinchora").attr("data-eliminarsolicitudinchora"),
                correosupervisor:correosupervisor,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {

                if (msj.mensaje == 'eliminado') {

                    var route = "/enviarcorreosolicitudeliminada";
                    var token = $("#token").val();

                    var idsolicitud = msj.id_solicitud;
                    var tiposolicitud = msj.tipo;
                    var fechadesdesolicitud= msj.fechadesde;
                    var descripsolicitud= msj.descripsolicitud;
                    var descripconcepto= msj.descripconcepto;
                    var generadapor= msj.soligeneradapor;

                    var asunto= asuntocorreoeliminada(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudeliminada();
                    var textfechadesdesolicitud= textofecha();
                    var textconcepto= textoconcepto();
                    var textdescripcion= textodescripcion();
                    var textelaboradapor = textoelaboradapor();

                    var correosupervisor= msj.correosupervisor;

                    //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textoelaboradapor: textelaboradapor,
                            generadapor:generadapor,
                            correosupervisor:correosupervisor
                        },
                        success: function(msj) {

                          $("#msj-success-elsolicitudinchora").fadeIn();
                          desactivarCargandoel();

                          //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                          setTimeout(function() {
                            $('#solicitudinchora_eliminar').modal('hide')
                          }, 1500);

                          reestablecerBusquedaGeneral();
                          jQuery('#listar_solicitudinchora').DataTable().search('');
                          jQuery('#listar_solicitudinchora').DataTable().ajax.reload();

                        },
                        
                        error: function(msj) {
                        }
                    });   
                }
                else{

                    $('#msj-error-elsolicitudinchora').text(msj.error);
                    $("#msj-error-elsolicitudinchora").removeClass('hidden');
                    $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');
                }

            });

        }


    }
    else{

        var route = "/eliminarsolicitudinchora"

        $.post(route, {
                id: $("#btnmodalaceptar_elsolicitudinchora").attr("data-eliminarsolicitudinchora"),
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(msj) {
            if (msj.mensaje == 'eliminado') {

                $("#msj-success-elsolicitudinchora").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudinchora_eliminar').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudinchora').DataTable().search('');
                jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
            }
            else{

                $('#msj-error-elsolicitudinchora').text(msj.error);
                $("#msj-error-elsolicitudinchora").removeClass('hidden');
                $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');

            }

        });

    }

});

$("#btnmodalaceptar_elsolicitudinchoracorreo").on('click', function() {

    $("#btnmodalcancelar_elsolicitudinchora").attr("disabled", "disabled");

    var route = "/eliminarsolicitudinchora"

    $.post(route, {
            id: $("#btnmodalaceptar_elsolicitudinchora").attr("data-eliminarsolicitudinchora"),
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
        if (msj.mensaje == 'eliminado') {

            $("#msj-success-elsolicitudinchora").fadeIn();
            $('#solicitudinchora_elvalidacioncorreo').modal('hide')

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#solicitudinchora_eliminar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_solicitudinchora').DataTable().search('');
            jQuery('#listar_solicitudinchora').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-elsolicitudinchora').text(data.error);
            $("#msj-error-elsolicitudinchora").removeClass('hidden');
            $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');
            $("#btnmodalcancelar_elsolicitudinchora").removeAttr('disabled');
        }

    });

});

$("#btnmodalcancelar_elsolicitudinchoracorreo").on('click', function() {

    $('#solicitudinchora_elvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_elsolicitudinchora").removeAttr('disabled');
    return false;

});

$(document.body).on('click', '#detalleoperasolictudinchora', function() {


    var radioTabla = $($('#listar_solicitudinchora tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodaloperasolicitudinchora").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudhora]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetoperasolicitudinchora/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosoperasolicitudinchora > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var creadodesde = data.creadodesde;
        var latitudcreado = data.latitudcreado;
        var longitudcreado = data.longitudcreado;
        var editadodesde = data.editadodesde;
        var latitudeditado = data.latitudeditado;
        var longitudeditado = data.longitudeditado;
        
        $('#datosoperasolicitudinchora').append(
            '<tr><td><center>' + creadodesde + 
            '<td><center>' + latitudcreado + 
            '<td><center>' + longitudcreado + 
            '<td><center>' + editadodesde +
            '<td><center>' + latitudeditado +
            '<td><center>' + longitudeditado + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detalleaprobarsolicitudinchora', function() {


    var radioTabla = $($('#listar_solicitudinchora tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalaprobarsolicitudinchora").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudhora]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetaprobarsolicitudinchora/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosaprobarsolicitudinchora > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fechaaprob = data.fechaaprob;
        var horaaprob = data.horaaprob;
        
        $('#datosaprobarsolicitudinchora').append(
            '<tr><td><center>' + fechaaprob + 
            '<td><center>' + horaaprob + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detallerechazarsolicitudinchora', function() {


    var radioTabla = $($('#listar_solicitudinchora tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrechazarsolicitudinchora").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudhora]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrechazarsolicitudinchora/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrechazarsolicitudinchora > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharechaz = data.fecharechaz;
        var horarechaz = data.horarechaz;
        
        $('#datosrechazarsolicitudinchora').append(
            '<tr><td><center>' + fecharechaz + 
            '<td><center>' + horarechaz + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


$(document.body).on('click', '#detallerevisarsolicitudinchora', function() {


    var radioTabla = $($('#listar_solicitudinchora tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrevisarsolicitudinchora").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudhora]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrevisarsolicitudinchora/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrevisarsolicitudinchora > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharevi = data.fecharevi;
        var horarevi = data.horarevi;
        
        $('#datosrevisarsolicitudinchora').append(
            '<tr><td><center>' + fecharevi + 
            '<td><center>' + horarevi + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});



function ocultarmensajesmodalesindexinchora() {

    $("#msj-seleccionarregistroeditar-solicitudinchora").fadeOut();
    $("#msj-seleccionarregistroeliminar-solicitudinchora").fadeOut();
    $("#msj-soloeditarsolicitud-solicitudinchora").fadeOut();
    $("#msj-soloeliminarsolicitud-solicitudinchora").fadeOut();
    $("#msj-fechamayor-solicitudinchora").fadeOut();
}

function ocultarmensajesmodalescrearinchora(){

    $("#msj-success-crsolicitudinchora").fadeOut();
    $("#msj-error-crsolicitudinchora").fadeOut();
}

function ocultarmensajesmodalesvalidacioncrinchora(){

    $("#msj-fechadesdevalidacion-crsolicitudinchora").fadeOut();
    $("#msj-seleccionarconcepto-crsolicitudinchora").fadeOut();
    $("#msj-solicitudconceptoingresada-crsolicitudinchora").fadeOut();
    $("#msj-seleccionarconcepto-crsolicitudinchora").fadeOut();
    $("#msj-correosupervisornovalido-crsolicitudinchora").fadeOut();
}

function ocultarmensajesmodaleseditarinchora(){

    $("#msj-success-edsolicitudinchora").fadeOut();
    $("#msj-error-edsolicitudinchora").fadeOut();
    $("#msj-fechadesdevalidacion-edsolicitudinchora").fadeOut();
   
}

function ocultarmensajesmodalesvalidacionedinchora(){

    $("#msj-fechadesdevalidacion-edsolicitudinchora").fadeOut();
    $("#msj-fechahastavalidacion-edsolicitudinchora").fadeOut();
    $("#msj-correosupervisornovalido-edsolicitudinchora").fadeOut();
    
}

function ocultarmensajesmodaleseliminarinchora(){

    $("#msj-success-elsolicitudinchora").fadeOut();
    $("#msj-error-elsolicitudinchora").fadeOut();
}

function cargarconceptosinchora(){

    registros_conceptosinchora = [];

    //console.log(registros);

    var route = "/cargarconceptosinchora";

    $.get(route, function(data) {

    })

    .done(function(data) {


        $.each(data, function(key, registro) {

            registros_conceptosinchora.push({
                    id: data[key].codconcepto,
                    text: data[key].desconcepto
            });

        });

        return registros_conceptosinchora;

    })

    .fail(function() {
        alert("error");

    });

}

function recargarconceptosinchora(criterio){


    var route = "/recargarconceptosinchora/" + criterio;

    $.get(route, function(data) {

    })

    .done(function(data) {

        //cambio
        registros_conceptosinchora = [];

        $.each(data, function(key, registro) {

            if (data == "") {

                registros[0] = "";

            } else {

                registros_conceptosinchora.push({
                        id: data[key].codconcepto,
                        text: data[key].desconcepto
                });

            }

        });

    })

    .fail(function() {
        alert("error");

    });

}

////////////////////////////FUNCIONES/////////////////////////////


jQuery("#excelsolicitudinchora").on('click', function() {
   
    var route = "/excel/solicitudinchora";

   
    var query = jQuery('#listar_solicitudinchora').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});

jQuery("#pdfsolicitudinchora").on('click', function() {

    var estatus="fecha";

    var tipobusqueda= busquedaGeneral.tipo;
    //var buscar = busquedaGeneral.buscar;
    var dp_fedesde_solicitudinchora=  busquedaGeneral.dp_fedesde_solicitudinchora;
    var dp_fehasta_solicitudinchora=  busquedaGeneral.dp_fehasta_solicitudinchora;

    var codigoinchora = $("#s2_incidencia_solicitudinchora").val();

    if(codigoinchora==""){
        codigoinchora="todos"; 
    }

    var buscar = jQuery('#listar_solicitudinchora').DataTable().search();
    busquedaGeneral.buscar = buscar;

    if(buscar==""){
       buscar="todos"; 
    }

    var todas = $('input:radio[id=ra_todas_solicitudinchora]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_solicitudinchora]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_solicitudinchora]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_solicitudinchora]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_solicitudinchora]:checked').val();

    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);
    } 

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfsolicitudinchora/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_solicitudinchora+"/"+dp_fehasta_solicitudinchora+"/"+estatus+"/"+codigoinchora;

    //alert(url);
    window.open(url,'_blank');

    desactivarCargando();

   
});



jQuery(function($) {

    setTimeout(function() {

        $('#s2_incidencia_solicitudinchora').select2({

            closeOnSelect: true,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione la Incidencia",


            formatSelectionTooBig: function (limit) {
                return 'S\u00F3lo puede seleccionar un Registro';
            },

            formatNoMatches: function(options) {

                return 'No existen registros Asociados';

            },
            query: function(options) {

                if (options.term.length >= 2) {

                    //CAMBIO
                    registros_conceptosinchora.length= 0;
                    registros_filtrados_conceptosinchora.length= 0;

                    recargarconceptosinchora(options.term);

                    setTimeout(function() {
                        options.callback({
                            results: registros_conceptosinchora
                        })
                    }, 2000); 

                } else {

                    if (options.term != "") {

                        filtrar_data(registros_conceptosinchora, options.term,registros_filtrados_conceptosinchora);

                        options.callback({
                            results: data_registrosfiltrados 
                        })

                    } else {

                        options.callback({
                            results: registros_conceptosinchora
                        })
                    }
                }

            }
        });

     }, 500);

});


