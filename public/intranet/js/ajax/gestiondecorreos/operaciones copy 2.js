

$("#btn_marcascrear").on('click', function () {

    //$("#tipoproducto_crear").modal("show");

    $('#marca_crear').modal({backdrop: 'static', keyboard: false});

    $("#registrar").removeAttr('disabled');


    var blanco = "";
    $('.error_nombre_crmarca').addClass('hidden');
    $('.error_nombre_crmarca').text(blanco);


    $(".modal-body input").val("");
    $("#msj-success-marca").addClass('hidden');

});
///////////////////////////////////////////
$("#btn_gestioncorreo_crear").on('click', function() {

    ocultarmensajesmodalesindex();

    $("#tx_correo").focus();
    $("#tx_correo").val('');

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#solicitudvacacion_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});
//////////////////////////////


$("#btnmodalaceptar_crgestioncorreo").on('click', function() {
    var correo = $("#tx_correo").val();
    var route = "/crearcorreo";
    var token = $("#token").val();

    $("#btnmodalaceptar_crgestioncorreo").attr("disabled", "disabled");

    // $('.error_nombre_crmarca').addClass('hidden');


    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {correo: correo},

        success: function (data) {
            $("#msj-success-correo").removeClass('hidden');
         
            setTimeout(function () {
                $('#marca_crear').modal('hide')
            }, 1500);

            // listar_marcas();
        },
        error: function (msj) {
            console.log('error')
            var errornombre = msj.responseJSON.correo;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});





$("#registrar").click(function () {
    var nombre = $("#nombre").val();
    var route = "/marcas";
    var token = $("#token").val();

    $("#registrar").attr("disabled", "disabled");

    var id_empleado = $("#tx_idempleado_index").val();


    $('.error_nombre_crmarca').addClass('hidden');


    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {nombre: nombre,id_empleado:id_empleado},

        success: function (data) {
            $("#msj-success-marca").removeClass('hidden');
         
            setTimeout(function () {
                $('#marca_crear').modal('hide')
            }, 1500);

             listar_marcas();
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.error_nombre_crmarca').text(msj.responseJSON.nombre);
                $('.error_nombre_crmarca').removeClass('hidden');
                $("#registrar").removeAttr('disabled');
            }
        }
    });
});


//Esto hace referencia al boton eliminar del encabezado
//al hacer click llama la ruta "obtener marca" se envia al controlador en el metodo show
// el registro seleccionado en el radio buton para abrir el modal con la informacion
$(".marcaEditar").on('click', function () {

    $('#editarMarca').modal({backdrop: 'static', keyboard: false});

    $("#eMarcaAceptar").removeAttr('disabled');
    //alert("La edad seleccionada es: " + $('input:radio[name=edad]:checked').val());
    var route = "/obtenermarca/" + $('input:radio[name=edad]:checked').val();

    $("#editarMarca").modal("show");


    var blanco = "";
    $('.error_nombre_edmarca').addClass('hidden');
    $('.error_nombre_edmarca').text(blanco);


    $(".modal-body input").val("");
    $("#msj-successeditar-marca").addClass('hidden');


    //obtener data
    $.get(route, function (data) {
        $("#txtEditarMarca").val(data.nb_marca);
        $("#eMarcaAceptar").attr("data-editar", data.id_marca);

    })
        .fail(function () {
            alert("error");
        });
});

//Esto hace referencia al boton aceptar del modal en donde se arma la ruta "actualizarmarca" que llama a la funcion update
//donde por "POST" uno de los parametros es tipo_actualizacion con el valor 1 que indica que solo se relizara el actualizar del campo nb_marca
$("#eMarcaAceptar").on('click', function () {

    $("#eMarcaAceptar").attr("disabled", "disabled");
    var route = "/actualizarmarca"
    var tipo_actualizacion = 1;

    var id_empleado = $("#tx_idempleado_index").val();

    $('.error_nombre_edmarca').addClass('hidden');

    $.post(route, {
        codigo: $("#eMarcaAceptar").attr("data-editar"),
        nombre: $("#txtEditarMarca").val(),
        id_empleado:id_empleado,
        "_token": $("#token").val(),
        tipo_actualizacion
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.error_nombre_edmarca').text(msj.responseJSON.nombre);
                $('.error_nombre_edmarca').removeClass('hidden');
                //alert(text(msj.responseJSON.nombre));
                $("#eMarcaAceptar").removeAttr('disabled');
            }
        })

        .done(function () {
            //listmarcas();
            $("#msj-successeditar-marca").removeClass('hidden');
            setTimeout(function () {
                $('#editarMarca').modal('hide')
            }, 1500);

             listar_marcas();

        });
})

//Esto hace referencia al boton eliminar del encabezado
//al hacer click llama la ruta "obtener marca" se envia al controlador en el metodo show
// el registro seleccionado en el radio buton para abrir el modal con la informacion
$("#btn_marcaeliminar").click(function () {

    $('#marca_eliminar').modal({backdrop: 'static', keyboard: false});

    $("#btn_marcaeliminar").removeAttr('disabled');

    $("#msj-successeliminar-marca").addClass('hidden');
    $("#msj-asociadoeliminar-marca").addClass('hidden');
    //$(#btn_marcaeliminar").on('click',function(){
    //alert("La edad seleccionada es: " + $('input:radio[name=edad]:checked').val());
    var route = "/obtenermarca/" + $('input:radio[name=edad]:checked').val();
    $("#marca_eliminar").modal("show");
    //obtener data

    $.get(route, function (data) {
        $("#txt_nombreelmarca").val(data.nb_marca);
        $("#btnmodalaceptar_elmarca").attr("data-eliminar", data.id_marca);

    })
        .fail(function () {
            alert("error");
        });
});


//Esto hace referencia al boton aceptar del modal en donde se arma la ruta "actualizarmarca" que llama al metodo update  
//donde por "POST" uno de los parametros es tipo_actualizacion con el valor 2 que indica que se relizara solo el actualizar del campo deleted_at 
//desde el controlador con la fecha actual debido a que no existe eliminacion fisica
$("#btnmodalaceptar_elmarca").on('click', function () {


     $("#btnmodalaceptar_elmarca").attr("disabled", "disabled");

     var id_empleado = $("#tx_idempleado_index").val();


    var route = "/obtenerasociacionmarca/" + $('input:radio[name=edad]:checked').val()
    var token = $("#token").val();
 

    $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType: 'json',
    
    data:{
    },
    success:function(msj){


          var route = "/eliminarmarca/" + $('input:radio[name=edad]:checked').val()+"/"+id_empleado;

            $.get(route, function(data){

            })

            .done(function(data) {
             
                $("#msj-successeliminar-marca").removeClass('hidden');
                setTimeout(function () {
                        $('#marca_eliminar').modal('hide')
                }, 1500);

                listar_marcas();


            })

            .fail(function() {
                    alert( "error" );
          
            });
           
    },
    error:function(msj){
       
        $("#msj-asociadoeliminar-marca").removeClass('hidden');
        $("#btnmodalaceptar_elmarca").removeAttr('disabled');

    }
  });

})



//FUNCION PARA REFRESCAR LA TABLA SUCURSAL CADA VEZ QUE SE REALIZAN LAS OPERACIONES
var listar_marcas = function(){

  var eliminartabla_marcas = jQuery('#listar_marcas').dataTable().fnDestroy();
  var recargartabla_marcas = jQuery('#listar_marcas').dataTable( {

        ajax: {
            url:'/listarmarcas',
            dataSrc: ""
        },
        columns: [
             {   
                data:'id_marca',
                render: function (data,type,row) {
                    return '<input type="radio" checked="" name="edad" value="'+data+'" class="flat-blue">'+data+'';    
                }
            },
            {
                name: 'nb_marca',
                data: 'nb_marca'
            }
        ],
        language : lenguaje
  }); 
  
}
//*****************FIN DE OPERACION REFRESCAR*************************************


$("#excelMarcas").on('click', function () {
    //var query = jQuery('#listar_ventas').DataTable().search();

    //busquedaGeneral.buscar = query;
    activarCargando();

    var route = "/excel/marcas/";
    jQuery.get(route, function (response, status, xhr) {
        try {
            var a = document.createElement("a");
            a.href = response.file;
            a.download = response.name;
            document.body.appendChild(a);
            a.click();
            a.remove();

        } catch (ex) {
            console.log(ex);
        }


    }).fail(function () {
        console.log("error");
    }).always(function() {
        desactivarCargando();
        });;

});