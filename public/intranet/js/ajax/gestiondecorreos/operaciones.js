
$("#btn_gestioncorreo_crear").on('click', function() {

    ocultarmensajesmodalescrear();

    $("#tx_correo").val('');

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#solicitudvacacion_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crgestioncorreo").on('click', function() {
    var correo = $("#tx_correo").val();
    var route = "/crearcorreo";
    var token = $("#token").val();

    $("#msj-success-crcorreo").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcorreo").fadeOut();

    $("#btnmodalaceptar_crgestioncorreo").attr("disabled", "disabled");


    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {correo: correo},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crcorreo").fadeIn();
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
                listarcorreos();
                setTimeout(function () {
                    $('#solicitudvacacion_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});



$("#btn_gestioncorreo_editar").on('click', function() {

     ocultarmensajesmodaleseditar();
    

    $("#tx_correo").val('');

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#solicitudvacacion_editar').modal({
        backdrop: 'static',
        keyboard: false
    });

    
    var route = "/obtenercorreo/" + $('input:radio[name=id_correo]:checked').val();

    $(".modal-body input").val("");

    $.get(route, function (data) {
        $("#tx_edcorreo").val(data[0].nb_correo);
    })
        .fail(function () {
            alert("error");
        });

});

$("#btnmodalaceptar_edgestioncorreo").on('click', function () {


    ocultarmensajesmodaleseditar();
    

    $("#btnmodalaceptar_edgestioncorreo").attr("disabled", "disabled");
    var route = "/editarcorreo"

   
    $.post(route, {
        id: $('input:radio[name=id_correo]:checked').val(),
        correo: $("#tx_edcorreo").val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.msj-error-edcorreo').text(msj.responseJSON.nombre);
                $('.msj-error-edcorreo').fadeIn();
                $("#btnmodalaceptar_edgestioncorreo").removeAttr('disabled');
            }
        })

        .done(function (msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-ssuccess-co").fadeIn();
                listarcorreos();
                setTimeout(function () {
                    $('#solicitudvacacion_editar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_edgestioncorreo").removeAttr('disabled');
            } else {
                $("#msj-success-edexistente").fadeIn();
                $("#btnmodalaceptar_edgestioncorreo").removeAttr('disabled');
            }
        });
})


$("#btn_solicitudvacacion_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminar();
   

   $("#tx_correo").val('');

   $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

   $('#solicitudvacacion_eliminar').modal({
       backdrop: 'static',
       keyboard: false
   });

   
   var route = "/obtenercorreo/" + $('input:radio[name=id_correo]:checked').val();

   $(".modal-body input").val("");

   $.get(route, function (data) {
       $("#tx_elcorreo").val(data[0].nb_correo);
   })
       .fail(function () {
           alert("error");
       });

});


$("#btnmodalaceptar_elgestioncorreo").on('click', function () {


    ocultarmensajesmodaleseditar();
    

    $("#btnmodalaceptar_elgestioncorreo").attr("disabled", "disabled");

    var route = "/eliminarcorreos"

    $.post(route, {
        id: $('input:radio[name=id_correo]:checked').val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            
            $("#btnmodalaceptar_elgestioncorreo").removeAttr('disabled');
            // var errornombre = msj.responseJSON.nombre;

            // if (errornombre != undefined) {
            //     $('.msj-error-elcorreo').text(msj.responseJSON.nombre);
            //     $('.msj-error-elcorreo').fadeIn();
            //     $("#btnmodalaceptar_elgestioncorreo").removeAttr('disabled');
            // }
        })

        .done(function (msj) {
             

                $("#msj-success-elcorreo").fadeIn();
                listarcorreos();
                setTimeout(function () {
                    $('#solicitudvacacion_eliminar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_elgestioncorreo").removeAttr('disabled');
            
        });
    
})



var listarcorreos = function(){

    var eliminartabla_marcas = jQuery('#listar_correos').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_correos').dataTable( {
  
          ajax: {
              url:'/listarcorreos',
              dataSrc: ""
          },
          columns: [
               {   
                  data:'id_correo',
                  render: function (data,type,row) {
                      return '<input type="radio" checked="" name="id_correo" value="'+data+'" class="flat-blue">'+data+'';    
                  }
              },
              {
                  name: 'nb_correo',
                  data: 'nb_correo'
              }
          ],
          language : lenguaje
    }); 
    
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crcorreo").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcorreo").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-ssuccess-co").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edcorreo").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elcorreo").fadeOut();
    $("#msj-error-elcorreo").fadeOut();
 }