registros_diaspendisolicitudvacacion = [];
registros_filtrados_diaspendisolicitudvacacion = [];

var busquedaGeneral = {
    dp_fedesde_solicitudvacacion: 'todos',
    dp_fehasta_solicitudvacacion: 'todos',
    tipovacacion: "",
    tipo: "general",
    estatus:"",
    buscar: ""
};

$(function() {



$("#btn_gestioncorreo_crear").on('click', function() {

    ocultarmensajesmodalesindex();
    ocultarmensajesmodalescrear();

    $("#tx_correo").focus();
    $("#tx_correo").val('');

    $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
    
    $("#dp_fedesde_crsolicitudvacacion").removeAttr('disabled');
    $("#dp_fehasta_crsolicitudvacacion").removeAttr('disabled');


    $("#check_mediajornada_crsolicitudvacacion").iCheck('uncheck');

    $("#check_enviarcorreo_solicitudvacacion").iCheck('check');

    $('#solicitudvacacion_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});
$("#btnmodalaceptar_crgestioncorreo").on('click', function() {
 alert("aceptar")
});

$("#btnmodalaceptar_crsolicitudvacacion").on('click', function() {

    $("#btnmodalaceptar_crsolicitudvacacion").attr("disabled", "disabled");

    ocultarmensajesmodalesvalidacioncrvacacion();

    var fechadesde = $("#dp_fedesde_crsolicitudvacacion").val();

    var fechahasta = $("#dp_fehasta_crsolicitudvacacion").val();

    var descripcion = $("#textarea_descripcion_crsolicitudvacacion").val();

    var documento= $("#tx_numerodocumento_crsolicitudvacacion").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_solicitudvacacion]:checked').val();

    var mediajornada = $('input:checkbox[id=check_mediajornada_crsolicitudvacacion]:checked').val();

    var totaldiaspendientes = $("#tx_correo").val();

    var cantidaddiasfechas= diastrancurridosentrefechas(fechadesde,fechahasta); 
    
   
    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 

    //alert(mediajornada);

    if (mediajornada=="NO") {

        if (fechadesde == fechahasta) {

            $("#msj-fechasigualesvalidacion-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;

        }

        if (fechadesde == "") {

            $("#msj-fechadesdevalidacion-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;

        }

        if (fechahasta == "") {

            $("#msj-fechahastavalidacion-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;

        }

        var validarfechavalor = validar_fechas(fechadesde,fechahasta);

        //alert(validarfechavalor);

        if (validarfechavalor == 0) {
            $("#msj-fechadesdemayor-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;
        } 


        if (parseInt(cantidaddiasfechas) > parseFloat(totaldiaspendientes)) {
            $("#msj-diassolicitados-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;
        }
        
    }
    else{

        var cantidaddias=0.50;

        if (parseFloat(cantidaddias) > parseFloat(totaldiaspendientes)) {
            $("#msj-diassolicitados-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacion').modal('show');
            $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
            return false;
        }

    }


    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_solicitudvacacion").val();

        if (correosupervisor=="") {

            $("#msj-correosupervisorvacio-crsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_crvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-crsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_crvalidacion').modal('show');
                $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
                return false;
            }

            activarCargando();
            $("#btnmodalcancelar_crsolicitudvacacion").attr("disabled", "disabled");

            var route = "/ingresarsolicitudvacacion";
            var token = $("#token").val();

           

                //alert(dato);
            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                dataType: 'json',

                data: {
                    fechadesde: fechadesde,
                    fechahasta: fechahasta,
                    descripcion: descripcion,
                    documento: documento,
                    correosupervisor:correosupervisor,
                    mediajornada: mediajornada
                },
                success: function(msj) {

                    if (msj.mensaje == 'creado') {

                        var route = "/enviarcorreosolicitudcreada";
                        var token = $("#token").val();

                        var idsolicitud = msj.id_solicitud;
                        var tiposolicitud = msj.tipo;
                        var fechadesdesolicitud= msj.fechadesde;
                        var fechahastasolicitud= msj.fechahasta;
                        var descripsolicitud= msj.descripsolicitud;
                        var descripconcepto= msj.descripconcepto;
                        var generadapor= msj.soligeneradapor;

                        if (mediajornada=="SI") {
                           descripconcepto= descripconcepto+" "+"Media Jornada";
                        }

                        var asunto= asuntocorreo(descripconcepto);
                        var textnumerosolicitud= textonumerosolicitud();
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                        var textconcepto= textoconcepto();
                        var textdescripcion= textodescripcion();
                        var textelaboradapor = textoelaboradapor();
                        var textaprobadarechazada= textoaprobadarechazada();
                       

                        var correosupervisor= msj.correosupervisor;

                        //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                        $.ajax({
                            url: route,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            type: 'POST',
                            dataType: 'json',

                            data: {
                                asunto: asunto,
                                tiposolicitud:tiposolicitud,
                                textonumerosolicitud: textnumerosolicitud,
                                numerosolicitud:idsolicitud,
                                textofechadesdesolicitud:textfechadesdesolicitud,
                                fechadesdesolicitud:fechadesdesolicitud,
                                textofechahastasolicitud:textfechahastasolicitud,
                                fechahastasolicitud:fechahastasolicitud,
                                textoconcepto:textconcepto,
                                descripcionconcepto:descripconcepto,
                                textodescripcion: textdescripcion,
                                descripcion:descripsolicitud,
                                textoelaboradapor: textelaboradapor,
                                generadapor:generadapor,
                                textoaprobadarechazada:textaprobadarechazada,
                                correosupervisor:correosupervisor
                            },
                            success: function(msj) {

                                $("#msj-success-crsolicitudvacacion").fadeIn();
                                desactivarCargando();
                                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                                setTimeout(function() {
                                    $('#solicitudvacacion_crear').modal('hide')

                                }, 1500);

                                reestablecerBusquedaGeneral();
                                jQuery('#listar_solicitudvacacion').DataTable().search('');
                                jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
                            },
                            error: function(msj) {
                            }
                        });

                    } else {
                        $('#msj-error-crsolicitudvacacion').text(msj.error);
                        $("#msj-error-crsolicitudvacacion").removeClass('hidden');
                        $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
                    }

                },
                    error: function(msj) {
                }
            });

        }
            
    }
    else{

        var route = "/ingresarsolicitudvacacion";
        var token = $("#token").val();

        //alert(mediajornada);

        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',

            data: {
                fechadesde: fechadesde,
                fechahasta: fechahasta,
                descripcion: descripcion,
                documento: documento,
                mediajornada: mediajornada
            },
            success: function(msj) {

                if (msj.mensaje == 'creado') {

                    $("#msj-success-crsolicitudvacacion").fadeIn();

                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudvacacion_crear').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudvacacion').DataTable().search('');
                    jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
                           
                } else {
                    $('#msj-error-crsolicitudvacacion').text(msj.error);
                    $("#msj-error-crsolicitudvacacion").removeClass('hidden');
                    $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
                }

            },
            error: function(msj) {

            }
        });
    }

});

$("#btnmodalaceptar_crsolicitudvacacioncorreo").on('click', function() {

    $("#btnmodalcancelar_crsolicitudvacacion").attr("disabled", "disabled");


    var fechadesde = $("#dp_fedesde_crsolicitudvacacion").val();

    var fechahasta = $("#dp_fehasta_crsolicitudvacacion").val();

    var descripcion = $("#textarea_descripcion_crsolicitudvacacion").val();

    var documento= $("#tx_numerodocumento_crsolicitudvacacion").val();

    var mediajornada = $('input:checkbox[id=check_mediajornada_crsolicitudvacacion]:checked').val();

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 

    var route = "/ingresarsolicitudvacacion";
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        type: 'POST',
        dataType: 'json',

        data: {
            fechadesde: fechadesde,
            fechahasta: fechahasta,
            descripcion: descripcion,
            mediajornada:mediajornada,
            documento: documento
        },
        success: function(msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_crvalidacioncorreo').modal('hide')

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudvacacion_crear').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudvacacion').DataTable().search('');
                jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
                       
            } else {
                $('#msj-error-crsolicitudvacacion').text(msj.error);
                $("#msj-error-crsolicitudvacacion").removeClass('hidden');
                $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');
                $("#btnmodalcancelar_crsolicitudvacacion").removeAttr('disabled');
            }

        },
        error: function(msj) {


        }
    });
});


$("#btnmodalcancelar_crsolicitudvacacioncorreo").on('click', function() {

    //alert("pase por aqui");
    $('#solicitudvacacion_crvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_crsolicitudvacacion").removeAttr('disabled');

    return false;

});


$("#btn_solicitudvacacion_editar").on('click', function() {

    ocultarmensajesmodalesindex();
    ocultarmensajesmodaleseditar();

    var valorregistroeditar= $('input:radio[name=idsolicitud]:checked').val();

    $("#check_enviarcorreo_edsolicitudvacacion").iCheck('check');

    if (valorregistroeditar==undefined){

        $("#msj-seleccionarregistroeditar-solicitudvacacion").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudvacacion/" + $('input:radio[name=idsolicitud]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if(estatus!="INGRESADO"){

              $("#msj-soloeditarsolicitud-solicitudvacacion").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                $("#btnmodalcancelar_edsolicitudvacacion").removeAttr('disabled');


                $('#solicitudvacacion_editar').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudvacacion/" + $('input:radio[name=idsolicitud]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    $("#check_enviarcorreo_edsolicitudvacacion").iCheck('check');
                    
                    $("#textarea_descripcion_edsolicitudvacacion").val(data[0].descripcio);
                    $("#tx_numerodocumento_edsolicitudvacacion").val(data[0].documento);
                    $("#btnmodalaceptar_edsolicitudvacacion").attr("data-editarsolicitudvacion", data[0].id);

                    var mediajornada= data[0].mediajornadavac;

                    if (mediajornada=="SI") {

                      $("#check_mediajornada_edsolicitudvacacion").iCheck('check');
                      
                      $("#dp_fedesde_edsolicitudvacacion").attr("disabled", "disabled");
                      $("#dp_fehasta_edsolicitudvacacion").attr("disabled", "disabled");
                      
                      var blanco="";
                      
                      $("#dp_fedesde_edsolicitudvacacion").val(blanco);
                      $("#dp_fehasta_edsolicitudvacacion").val(blanco);

                    }
                    else{

                      $("#dp_fedesde_edsolicitudvacacion").removeAttr('disabled');
                      $("#dp_fehasta_edsolicitudvacacion").removeAttr('disabled');

                      var fechadesdeformateada= darformatofecha(data[0].fechadesde);
                      var fechahastaformateada= darformatofecha(data[0].fechahasta);

                      $("#dp_fedesde_edsolicitudvacacion").val(fechadesdeformateada);
                      $("#dp_fehasta_edsolicitudvacacion").val(fechahastaformateada);

                      $("#check_mediajornada_edsolicitudvacacion").iCheck('uncheck');

                    }

                    var route = "/obtenercorreosupervisorsolicitudvacacion";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      $("#txtoculto_correosupervisor_edsolicitudvacacion").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                    var route = "/obtenertotaldiaspendientessolicitudvacacion";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                        $("#txtoculto_diaspendientes_edsolicitudvacacion").val(data[0].ndiaspendi);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                    });

            }
       
        });  
    } 
});

$("#btnmodalaceptar_edsolicitudvacacion").on('click', function() {

    $("#btnmodalaceptar_edsolicitudvacacion").attr("disabled", "disabled");

    ocultarmensajesmodalesvalidacionedvacacion();

    var fechadesde = $("#dp_fedesde_edsolicitudvacacion").val();

    var fechahasta = $("#dp_fehasta_edsolicitudvacacion").val();

    var descripcion = $("#textarea_descripcion_edsolicitudvacacion").val();

    var documento= $("#tx_numerodocumento_edsolicitudvacacion").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_edsolicitudvacacion]:checked').val();

    var mediajornada = $('input:checkbox[id=check_mediajornada_edsolicitudvacacion]:checked').val();

    var totaldiaspendientes = $("#txtoculto_diaspendientes_edsolicitudvacacion").val();

    var cantidaddiasfechas= diastrancurridosentrefechas(fechadesde,fechahasta);

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 


    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 


    if (mediajornada=="NO") {

         if (fechadesde == fechahasta) {

            $("#msj-fechasigualesvalidacion-edsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
            return false;

        }
    
        if (fechadesde == "") {

                $("#msj-fechadesdevalidacion-edsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_edvalidacion').modal('show');
                $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                return false;
        }

        if (fechahasta == "") {

                $("#msj-fechahastavalidacion-edsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_edvalidacion').modal('show');
                $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                return false;
        }

        var validarfechavalor = validar_fechas(fechadesde,fechahasta);

        //alert(validarfechavalor);

        if (validarfechavalor == 0) {

            $("#msj-fechadesdemayor-edsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
            return false;

        } 

        if (parseInt(cantidaddiasfechas) > parseFloat(totaldiaspendientes)) {
            $("#msj-diassolicitados-edsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
            return false;
        }
       
    }
    else{

        var cantidaddias=0.50;

        if (parseFloat(cantidaddias) > parseFloat(totaldiaspendientes)) {
            $("#msj-diassolicitados-edsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_edvalidacion').modal('show');
            $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
            return false;
        }

    }


    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_edsolicitudvacacion").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-edsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_edvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-edsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_edvalidacion').modal('show');
                $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                return false;
            }

            activarCargandoed();
            $("#btnmodalcancelar_edsolicitudvacacion").attr("disabled", "disabled");

            var route = "/actalizarsolicitudvacacion"

            $.post(route, {
                    id: $("#btnmodalaceptar_edsolicitudvacacion").attr("data-editarsolicitudvacion"),
                    fechadesde: fechadesde,
                    fechahasta: fechahasta,
                    descripcion: descripcion,
                    mediajornada: mediajornada,
                    documento: documento,
                    correosupervisor:correosupervisor,
                    "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {
                    
                    if (msj.mensaje == 'actualizado') {

                        var route = "/enviarcorreosolicitudeditada";
                        var token = $("#token").val();

                        var idsolicitud = msj.id_solicitud;
                        var tiposolicitud = msj.tipo;
                        var fechadesdesolicitud= msj.fechadesde;
                        var fechahastasolicitud= msj.fechahasta;
                        var descripsolicitud= msj.descripsolicitud;
                        var descripconcepto= msj.descripconcepto;
                        var generadapor= msj.soligeneradapor;

                        if (mediajornada=="SI") {
                           descripconcepto= descripconcepto+" "+"Media Jornada";
                        }

                        var asunto= asuntocorreoeditada(descripconcepto);
                        var textnumerosolicitud= textonumerosolicitudeditada();
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                        var textconcepto= textoconcepto();
                        var textdescripcion= textodescripcion();
                        var textelaboradapor = textoelaboradapor();
                        var textaprobadarechazada= textoaprobadarechazada();

                        var correosupervisor= msj.correosupervisor;

                        //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                        $.ajax({
                            url: route,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            type: 'POST',
                            dataType: 'json',

                            data: {
                                asunto: asunto,
                                tiposolicitud:tiposolicitud,
                                textonumerosolicitud: textnumerosolicitud,
                                numerosolicitud:idsolicitud,
                                textofechadesdesolicitud:textfechadesdesolicitud,
                                fechadesdesolicitud:fechadesdesolicitud,
                                textofechahastasolicitud:textfechahastasolicitud,
                                fechahastasolicitud:fechahastasolicitud,
                                textoconcepto: textconcepto,
                                descripcionconcepto:descripconcepto,
                                textodescripcion: textdescripcion,
                                descripcion:descripsolicitud,
                                textoelaboradapor: textelaboradapor,
                                generadapor:generadapor,
                                textoaprobadarechazada:textaprobadarechazada,
                                correosupervisor:correosupervisor
                            },
                            success: function(msj) {

                              $("#msj-success-edsolicitudvacacion").fadeIn();
                               desactivarCargandoed();

                              //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                              setTimeout(function() {
                                $('#solicitudvacacion_editar').modal('hide')
                              }, 1500);

                              reestablecerBusquedaGeneral();
                              jQuery('#listar_solicitudvacacion').DataTable().search('');
                              jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();

                            },
                            
                            error: function(msj) {
                            }
                        });   
                    }
                    else{

                        $('#msj-error-edsolicitudvacacion').text(msj.error);
                        $("#msj-error-edsolicitudvacacion").removeClass('hidden');
                        $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                    }

            });
        }

    }
    else{

        var route = "/actalizarsolicitudvacacion"

        $.post(route, {
                id: $("#btnmodalaceptar_edsolicitudvacacion").attr("data-editarsolicitudvacion"),
                fechadesde: fechadesde,
                fechahasta: fechahasta,
                descripcion: descripcion,
                mediajornada: mediajornada,
                documento: documento,
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(msj) {
                if (msj.mensaje == 'actualizado') {

                    $("#msj-success-edsolicitudvacacion").fadeIn();

                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudvacacion_editar').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudvacacion').DataTable().search('');
                    jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
                }
                else{

                    $('#msj-error-edsolicitudvacacion').text(msj.error);
                    $("#msj-error-edsolicitudvacacion").removeClass('hidden');
                    $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
                }

        });

    }

});

$("#btnmodalaceptar_edsolicitudvacacioncorreo").on('click', function() {

    $("#btnmodalcancelar_edsolicitudvacacion").attr("disabled", "disabled");

    var fechadesde = $("#dp_fedesde_edsolicitudvacacion").val();

    var fechahasta = $("#dp_fehasta_edsolicitudvacacion").val();

    var descripcion = $("#textarea_descripcion_edsolicitudvacacion").val();

    var documento= $("#tx_numerodocumento_edsolicitudvacacion").val();

    var mediajornada = $('input:checkbox[id=check_mediajornada_edsolicitudvacacion]:checked').val();

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 

    var route = "/actalizarsolicitudvacacion"

    $.post(route, {
            id: $("#btnmodalaceptar_edsolicitudvacacion").attr("data-editarsolicitudvacion"),
            fechadesde: fechadesde,
            fechahasta: fechahasta,
            descripcion: descripcion,
            mediajornada: mediajornada,
            documento: documento,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_edvalidacioncorreo').modal('hide')
                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#solicitudvacacion_editar').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_solicitudvacacion').DataTable().search('');
                jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
            }
            else{

                $('#msj-error-edsolicitudvacacion').text(msj.error);
                $("#msj-error-edsolicitudvacacion").removeClass('hidden');
                $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
            }

    });

});

$("#btnmodalcancelar_edsolicitudvacacioncorreo").on('click', function() {

    $('#solicitudvacacion_edvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_edsolicitudvacacion").removeAttr('disabled');
    return false;

});



$("#btn_solicitudvacacion_eliminar").on('click', function() {

    ocultarmensajesmodalesindex();
    ocultarmensajesmodaleseliminar();
   
    var valorregistroeliminar= $('input:radio[name=idsolicitud]:checked').val();

    if (valorregistroeliminar==undefined){

        $("#msj-seleccionarregistroeliminar-solicitudvacacion").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudvacacion/" + $('input:radio[name=idsolicitud]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

             var estatus = data[0].estatus;

             if(estatus!="INGRESADO"){

              $("#msj-soloeliminarsolicitud-solicitudvacacion").fadeIn();
              return false;

            }
            else{

               $("#btnmodalaceptar_elsolicitudvacacion").removeAttr('disabled');
               $("#btnmodalcancelar_elsolicitudvacacion").removeAttr('disabled');


               $('#solicitudvacacion_eliminar').modal({
                    backdrop: 'static',
                    keyboard: false
               });

   
                var route = "/obtenerdatossolicitudvacacion/" + $('input:radio[name=idsolicitud]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    $("#check_enviarcorreo_elsolicitudvacacion").iCheck('check');
                    $("#btnmodalaceptar_elsolicitudvacacion").attr("data-eliminarsolicitudvacacion", data[0].id);

                     var mediajornada= data[0].mediajornadavac;

                    if (mediajornada=="SI") {

                      $("#check_mediajornada_elsolicitudvacacion").iCheck('check');

                    }
                    else{

                      $("#check_mediajornada_elsolicitudvacacion").iCheck('uncheck');

                    }


                    var route = "/obtenercorreosupervisorsolicitudvacacion";
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                      //alert(data);
                      $("#txtoculto_correosupervisor_elsolicitudvacacion").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    });

                });
            }
        }); 
    }
 
});


$("#btnmodalaceptar_elsolicitudvacacion").on('click', function() {

    $("#btnmodalaceptar_elsolicitudvacacion").attr("disabled", "disabled");

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_elsolicitudvacacion]:checked').val();

    
    var mediajornada = $('input:checkbox[id=check_mediajornada_elsolicitudvacacion]:checked').val();

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 


    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if(enviarcorreo=="SI"){

        var correosupervisor = $("#txtoculto_correosupervisor_elsolicitudvacacion").val();

        if (correosupervisor=="") {
            $("#msj-correosupervisorvacio-elsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_elvalidacioncorreo').modal('show');
            return false;
        }
        else{

            var validarcorreo= validar_correo(correosupervisor);

            if (validarcorreo == false) {

                $("#msj-correosupervisornovalido-elsolicitudvacacion").fadeIn();
                $('#solicitudvacacion_elvalidacion').modal('show');
                $("#btnmodalaceptar_elsolicitudvacacion").removeAttr('disabled');
                return false;
            }

            activarCargandoel();
            $("#btnmodalcancelar_elsolicitudvacacion").attr("disabled", "disabled");

            var route = "/eliminarsolicitudvacacion"

            $.post(route, {
                    id: $("#btnmodalaceptar_elsolicitudvacacion").attr("data-eliminarsolicitudvacacion"),
                    correosupervisor:correosupervisor,
                    "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(msj) {
                    if (msj.mensaje == 'eliminado') {

                        var route = "/enviarcorreosolicitudeliminada";
                        var token = $("#token").val();

                        var idsolicitud = msj.id_solicitud;
                        var tiposolicitud = msj.tipo;
                        var fechadesdesolicitud= msj.fechadesde;
                        var fechahastasolicitud= msj.fechahasta;
                        var descripsolicitud= msj.descripsolicitud;
                        var descripconcepto= msj.descripconcepto;
                        var generadapor= msj.soligeneradapor;

                        if (mediajornada=="SI") {
                           descripconcepto= descripconcepto+" "+"Media Jornada";
                        }

                        var asunto= asuntocorreoeliminada(descripconcepto);
                        var textnumerosolicitud= textonumerosolicitudeliminada();
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                        var textconcepto= textoconcepto();
                        var textdescripcion= textodescripcion();
                        var textelaboradapor = textoelaboradapor();

                        var correosupervisor= msj.correosupervisor;

                        //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)

                        $.ajax({
                            url: route,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            type: 'POST',
                            dataType: 'json',

                            data: {
                                asunto: asunto,
                                tiposolicitud:tiposolicitud,
                                textonumerosolicitud: textnumerosolicitud,
                                numerosolicitud:idsolicitud,
                                textofechadesdesolicitud:textfechadesdesolicitud,
                                fechadesdesolicitud:fechadesdesolicitud,
                                textofechahastasolicitud:textfechahastasolicitud,
                                fechahastasolicitud:fechahastasolicitud,
                                textoconcepto: textconcepto,
                                descripcionconcepto:descripconcepto,
                                textodescripcion: textdescripcion,
                                descripcion:descripsolicitud,
                                textoelaboradapor: textelaboradapor,
                                generadapor:generadapor,
                                correosupervisor:correosupervisor
                            },
                            success: function(msj) {

                              $("#msj-success-elsolicitudvacacion").fadeIn();
                              desactivarCargandoel();
                              //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                              setTimeout(function() {
                                $('#solicitudvacacion_eliminar').modal('hide')
                              }, 1500);

                              reestablecerBusquedaGeneral();
                              jQuery('#listar_solicitudvacacion').DataTable().search('');
                              jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();

                            },
                            
                            error: function(msj) {
                            }
                        });   

                    }
                    else{

                        $('#msj-error-elsolicitudvacacion').text(msj.error);
                        $("#msj-error-elsolicitudvacacion").removeClass('hidden');
                        $("#btnmodalaceptar_elsolicitudvacacion").removeAttr('disabled');
                    }
            });
        }
    }
    else{

        var route = "/eliminarsolicitudvacacion"

        $.post(route, {
                id: $("#btnmodalaceptar_elsolicitudvacacion").attr("data-eliminarsolicitudvacacion"),
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(msj) {
                if (msj.mensaje == 'eliminado') {

                    $("#msj-success-elsolicitudvacacion").fadeIn();

                    //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                    setTimeout(function() {
                        $('#solicitudvacacion_eliminar').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_solicitudvacacion').DataTable().search('');
                    jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
                }

        });

    }

});

$("#btnmodalaceptar_elsolicitudvacacioncorreo").on('click', function() {

    $("#btnmodalcancelar_elsolicitudvacacion").attr("disabled", "disabled");

    var route = "/eliminarsolicitudvacacion"

    $.post(route, {
            id: $("#btnmodalaceptar_elsolicitudvacacion").attr("data-eliminarsolicitudvacacion"),
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(msj) {
        if (msj.mensaje == 'eliminado') {

            $("#msj-success-elsolicitudvacacion").fadeIn();
            $('#solicitudvacacion_elvalidacioncorreo').modal('hide')

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#solicitudvacacion_eliminar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_solicitudvacacion').DataTable().search('');
            jQuery('#listar_solicitudvacacion').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-elsolicitudvacacion').text(msj.error);
            $("#msj-error-elsolicitudvacacion").removeClass('hidden');
            $("#btnmodalaceptar_elsolicitudvacacion").removeAttr('disabled');
            $("#btnmodalcancelar_elsolicitudvacacion").removeAttr('disabled');
        }

    });

});

$("#btnmodalcancelar_elsolicitudvacacioncorreo").on('click', function() {

    $('#solicitudvacacion_elvalidacioncorreo').modal('hide')
    $("#btnmodalaceptar_elsolicitudvacacion").removeAttr('disabled');
    return false;

});

$("#check_mediajornada_crsolicitudvacacion").on('ifChanged', function(event){

    var mediajornada = $('input:checkbox[id=check_mediajornada_crsolicitudvacacion]:checked').val();

    if (mediajornada=="") {
        mediajornada = "SI";
        //alert("todas"+estatus);
    }
    else{
        mediajornada = "NO"; 
    } 

    if(mediajornada=="SI"){
        $("#dp_fedesde_crsolicitudvacacion").attr("disabled", "disabled");
        $("#dp_fehasta_crsolicitudvacacion").attr("disabled", "disabled");

        var blanco="";
        $("#dp_fedesde_crsolicitudvacacion").val(blanco);
        $("#dp_fehasta_crsolicitudvacacion").val(blanco);
    }

    if(mediajornada=="NO"){

        fechahoy=obtener_fechaactual();

        $("#dp_fedesde_crsolicitudvacacion").val(fechahoy);
        $("#dp_fehasta_crsolicitudvacacion").val(fechahoy);

        $("#dp_fedesde_crsolicitudvacacion").removeAttr('disabled');
        $("#dp_fehasta_crsolicitudvacacion").removeAttr('disabled');
    }


    //alert(mediajornada);

});

$(document.body).on('click', '#detalleoperasolictudvacacion', function() {


    var radioTabla = $($('#listar_solicitudvacacion tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodaloperasolicitudvacacion").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitud]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetoperasolicitudvacacion/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosoperasolicitudvacacion > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var creadodesde = data.creadodesde;
        var latitudcreado = data.latitudcreado;
        var longitudcreado = data.longitudcreado;
        var editadodesde = data.editadodesde;
        var latitudeditado = data.latitudeditado;
        var longitudeditado = data.longitudeditado;
        
        $('#datosoperasolicitudvacacion').append(
            '<tr><td><center>' + creadodesde + 
            '<td><center>' + latitudcreado + 
            '<td><center>' + longitudcreado + 
            '<td><center>' + editadodesde +
            '<td><center>' + latitudeditado +
            '<td><center>' + longitudeditado + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detalleaprobarsolicitudvacacion', function() {


    var radioTabla = $($('#listar_solicitudvacacion tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalaprobarsolicitudvacacion").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitud]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetaprobarsolicitudvacacion/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosaprobarsolicitudvacacion > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fechaaprob = data.fechaaprob;
        var horaaprob = data.horaaprob;
        
        $('#datosaprobarsolicitudvacacion').append(
            '<tr><td><center>' + fechaaprob + 
            '<td><center>' + horaaprob + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detallerechazarsolicitudvacacion', function() {


    var radioTabla = $($('#listar_solicitudvacacion tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrechazarsolicitudvacacion").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitud]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrechazarsolicitudvacacion/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrechazarsolicitudvacacion > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharechaz = data.fecharechaz;
        var horarechaz = data.horarechaz;
        
        $('#datosrechazarsolicitudvacacion').append(
            '<tr><td><center>' + fecharechaz + 
            '<td><center>' + horarechaz + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


$(document.body).on('click', '#detallerevisarsolicitudvacacion', function() {


    var radioTabla = $($('#listar_solicitudvacacion tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrevisarsolicitudvacacion").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitud]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrevisarsolicitudvacacion/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrevisarsolicitudvacacion > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharevi = data.fecharevi;
        var horarevi = data.horarevi;
        
        $('#datosrevisarsolicitudvacacion').append(
            '<tr><td><center>' + fecharevi + 
            '<td><center>' + horarevi + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});




});// FIN FUNCTION QUE ESTA AL PRINCIPIO


function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_solicitudvacacion = 'todos';
    busquedaGeneral.dp_fehasta_solicitudvacacion = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function ListarSolicitudVacacion() {

    //alert("pase");
    var selected = [];
    var route = "/listarsolicitudvacacion";

    var recargartablatotal = jQuery('#listar_solicitudvacacion').dataTable({

        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                data.dp_fedesde_solicitudvacacion = busquedaGeneral.dp_fedesde_solicitudvacacion;
                data.dp_fehasta_solicitudvacacion = busquedaGeneral.dp_fehasta_solicitudvacacion;
                data.tipo = busquedaGeneral.tipo;
                data.estatus = busquedaGeneral.estatus;
                data.tipovacacion = busquedaGeneral.tipovacacion;
                
            }

        },

        columns: [

            {
                data: 'idsolicitud',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idsolicitud" value="' + data + '" class="flat-blue">' + data + '';
                }
            },

            
            {
                className: 'text-center',
                name: 'mediajornadavac',


                render: function(data, type, row) {

                    var mediajornada = row.mediajornadavac;

                    if(mediajornada=="SI"){
                      //alert(mediajornada);
                      var descripcion="MediaJornada";

                      return descripcion;
                    
                    }
                    if(mediajornada=="NO"){
                      //alert(mediajornada);
                      var descripcion="Vacaciones";
                      return descripcion;
                    }
                }


                
            },

            {
                className: 'text-center',
                name: 'fechasolicitud',
                data: 'fechasolicitud'
            },
            {
                className: 'text-center',
                name: 'horasolicitud',
                data: 'horasolicitud'
                
            },
            {
                className: 'text-center',
                name: 'fechainicio',
                data: 'fechainicio'
                
            },
            {
                className: 'text-center',
                name: 'fechafin',
                data: 'fechafin'
                
            },
            {
                className: 'text-center',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'documento',
                data: 'documento'
                
            },
            {
                className: 'text-center',
                name: 'aprobadarechazpor',
                data: 'aprobadarechazpor'
                
            },
            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "APROBADO") ?
                        "<center> <a href=''  id='detalleaprobarsolicitudvacacion'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "RECHAZADO") ?
                        "<center> <a href=''  id='detallerechazarsolicitudvacacion'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "REVISADO") ?
                        "<center> <a href=''  id='detallerevisarsolicitudvacacion'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },
            {
                className: 'text-center',
                name: 'observacion',
                data: 'observacion'
                
            },

            {


                'defaultContent': "<center> <a href=''  id='detalleoperasolictudvacacion' class='' data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>"

            },

            
            {
                
                className: 'text-center',
                data: 'codigoprocesado',
                defaultContent: ""

                
            },

            {
                className: 'text-center',
                name: 'estatus',
                data: 'estatus'
            }

        ],

        language: lenguaje

    });


}
  
function ocultarmensajesmodalesindex() {

    $("#msj-noexistenregistros-solicitudvacacion").fadeOut();
    $("#msj-fechamayor-solicitudvacacion").fadeOut();
    $("#msj-soloeditarsolicitud-solicitudvacacion").fadeOut();
    $("#msj-soloeliminarsolicitud-solicitudvacacion").fadeOut();
    $("#msj-seleccionarregistroeditar-solicitudvacacion").fadeOut();
    $("#msj-seleccionarregistroeliminar-solicitudvacacion").fadeOut();
    $("#msj-solocitudesingresadas-solicitudvacacion").fadeOut();
    $("#msj-solocitudnoprocesadazincron-solicitudvacacion").fadeOut();  
   
}

function ocultarmensajesmodalescrear() {

    $("#msj-success-crsolicitudvacacion").fadeOut();
    $("#msj-error-crsolicitudvacacion").fadeOut();
}

function ocultarmensajesmodaleseditar() {

    $("#msj-success-edsolicitudvacacion").fadeOut();
    $("#msj-error-edsolicitudvacacion").fadeOut();
}

function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elsolicitudvacacion").fadeOut();
    $("#msj-error-elsolicitudvacacion").fadeOut();
}

function ocultarmensajesmodalesvalidacioncrvacacion() {

    $("#msj-fechadesdevalidacion-crsolicitudvacacion").fadeOut();
    $("#msj-fechahastavalidacion-crsolicitudvacacion").fadeOut();
    $("#msj-fechadesdemayor-crsolicitudvacacion").fadeOut();
    $("#msj-fechasigualesvalidacion-crsolicitudvacacion").fadeOut();
    $("#msj-diassolicitados-crsolicitudvacacion").fadeOut();
    $("#msj-correosupervisornovalido-crsolicitudvacacion").fadeOut();
   
    
}

function ocultarmensajesmodalesvalidacionedvacacion() {

    $("#msj-fechadesdevalidacion-edsolicitudvacacion").fadeOut();
    $("#msj-fechahastavalidacion-edsolicitudvacacion").fadeOut();
    $("#msj-fechadesdemayor-edsolicitudvacacion").fadeOut();
    $("#msj-fechasigualesvalidacion-edsolicitudvacacion").fadeOut();
    $("#msj-diassolicitados-edsolicitudvacacion").fadeOut();
    $("#msj-correosupervisornovalido-edsolicitudvacacion").fadeOut();

}


function cargardiaspendisolicitudvacacion(){

    registros_diaspendisolicitudvacacion = [];

    //console.log(registros);

    var route = "/cargardiaspendisolicitudvacacion";

    $.get(route, function(data) {

    })

    .done(function(data) {


        $.each(data, function(key, registro) {

            registros_diaspendisolicitudvacacion.push({
                    //id: registro.id_producto + '-' + registro.nb_referencia + '/' + 'CANTIDAD:' + registro.cantidad,
                    id: data[key].cperiodo,
                    text:'PERIODO'+ ':' + registro.cperiodo + '  ' + 'DIAS:' + registro.ndiaspendi
                   // text: data[key].ndiaspendi
            });

        });

        return registros_diaspendisolicitudvacacion;

    })

    .fail(function() {
        alert("error");

    });

}

function recargardiaspendisolicitudvacacion(criterio){


    var route = "/recargardiaspendisolicitudvacacion/" + criterio;

    $.get(route, function(data) {

    })

    .done(function(data) {

        //cambio
        registros_diaspendisolicitudvacacion = [];

        $.each(data, function(key, registro) {

            if (data == "") {

                registros[0] = "";

            } else {

                registros_diaspendisolicitudvacacion.push({
                        id: data[key].codconcepto,
                        text: data[key].desconcepto
                });

            }

        });

    })

    .fail(function() {
        alert("error");

    });

}


////////////////////////////FUNCIONES/////////////////////////////


jQuery("#excelsolicitudvacacion").on('click', function() {
   
    var route = "/excel/solicitudvacacion";

   
    var query = jQuery('#listar_solicitudvacacion').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});



jQuery("#pdfsolicitudvacacion").on('click', function() {

    var estatus="fecha";
   
    var tipobusqueda= busquedaGeneral.tipo;
    var buscar = busquedaGeneral.buscar;
    var dp_fedesde_solicitudvacacion=  busquedaGeneral.dp_fedesde_solicitudvacacion;
    var dp_fehasta_solicitudvacacion=  busquedaGeneral.dp_fehasta_solicitudvacacion;
    
    var tipovacacion = $("#s2_tipovacacion_solicitudvacacion").val();

    if(tipovacacion==""){
       tipovacacion="todos"; 
    }

    if(buscar==""){
       buscar="todos"; 
    }

    var todas = $('input:radio[id=ra_todas_solicitudvacacion]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_solicitudvacacion]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_solicitudvacacion]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_solicitudvacacion]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_solicitudvacacion]:checked').val();

    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);
    } 

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfsolicitudvacacion/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_solicitudvacacion+"/"+dp_fehasta_solicitudvacacion+"/"+estatus+"/"+tipovacacion;

    //alert(url);
    window.open(url,'_blank');

    desactivarCargando();

   
});


jQuery(function($) {

    setTimeout(function() {

        var listartipovacacion = [{
            id: "Vacaciones",
            text: "Vacaciones"
        }, {
            id: "Media Jornada",
            text: "Media Jornada"
        }];

        $('#s2_tipovacacion_solicitudvacacion').select2({


            closeOnSelect: false,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione el registro",

            formatSelectionTooBig: function(limit) {
                return 'S\u00F3lo puede seleccionar  un Tipo';
            },
            formatNoMatches: function() {
                return 'No se encontr\u00F3 Estatus';
            },
            data: listartipovacacion

        });

        $('#s2_diaspendientes_solicitudvacacion').select2({

            closeOnSelect: true,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione el Periodo",


            formatSelectionTooBig: function (limit) {
                return 'S\u00F3lo puede seleccionar un Registro';
            },

            formatNoMatches: function(options) {

                return 'No existen registros Asociados';

            },
            query: function(options) {

                    if (options.term != "") {

                        filtrar_data(registros_diaspendisolicitudvacacion, options.term,registros_filtrados_diaspendisolicitudvacacion);

                        options.callback({
                            results: data_registrosfiltrados 
                        })

                    } else {

                        options.callback({
                            results: registros_diaspendisolicitudvacacion
                        })
                    }
              

            }
        });



     }, 500);

});


