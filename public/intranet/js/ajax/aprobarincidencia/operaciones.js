registros_conceptosaprobarinc = [];
registros_filtrados_conceptosaprobarinc = [];

var busquedaGeneral = {
    dp_fedesde_aprobarincidencia: 'todos',
    dp_fehasta_aprobarincidencia: 'todos',
    tipo: "general",
    codigoaprobarinc: "",
    estatus:"",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_aprobarincidencia = 'todos';
    busquedaGeneral.dp_fehasta_aprobarincidencia = 'todos';
    busquedaGeneral.codigoaprobarinc = "";
    busquedaGeneral.estatus = "";
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function ListarAprobarIncidencia() {

    //alert("pase");

    var route = "/listaraprobarincidencia";

    var recargartablatotal = jQuery('#listar_aprobarincidencia').dataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                data.dp_fedesde_aprobarincidencia = busquedaGeneral.dp_fedesde_aprobarincidencia;
                data.dp_fehasta_aprobarincidencia = busquedaGeneral.dp_fehasta_aprobarincidencia;
                data.tipo = busquedaGeneral.tipo;
                data.estatus = busquedaGeneral.estatus;
                data.codigoaprobarinc = busquedaGeneral.codigoaprobarinc;
                
            }

        },

        columns: [

            {
                data: 'idsolicitud',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idsolicitudaprob" value="' + data + '" class="flat-blue">' + data + '';
                }
            },

            {
                className: 'text-center',
                data: 'fechasolicitud'
            },
            {
                className: 'text-center',
                name: 'horasolicitud',
                data: 'horasolicitud'
                
            },
            {
                className: 'text-center',
                name: 'fechainicio',
                data: 'fechainicio'
                
            },
            {
                className: 'text-center',
                name: 'fechafin',
                data: 'fechafin'
                
            },
            {
                className: 'text-center',
                name: 'concepto',
                render: function(data, type, row) {

                    var codigoconcepto= row.concepto;
                    //alert(codigoconcepto);

                    if(codigoconcepto=="Vacaciones"){


                        var mediajornada = row.mediajornadavac;

                        if(mediajornada=="SI"){
                          //alert(mediajornada);
                          var concepto="MediaJornada";
                          return concepto;
                        
                        }
                        if(mediajornada=="NO"){
                          //alert(mediajornada);
                          var concepto="Vacaciones";
                          return concepto;
                        }

                    }
                    else{

                        var concepto= row.concepto;
                        return concepto;

                    }

                    
                }
                
            },

            {
                className: 'text-center',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'documento',
                data: 'documento'
                
            },
            {
                className: 'text-center',
                name: 'elaboradopor',
                data: 'elaboradopor'
                
            },
            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "APROBADO") ?
                        "<center> <a href=''  id='detalleaprobarincidencia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "RECHAZADO") ?
                        "<center> <a href=''  id='detallerechazarincidencia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {
                className: 'text-center',
                render: function(data, type, row) {

                    return (row.estatus == "REVISADO") ?
                        "<center> <a href=''  id='detallerevisarincidencia'  data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>" :
                        "<center> <a href=''  data-toggle='modal' data-target='#detalle-modal' style='display:none'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>";

                },

            },

            {

                className: 'text-center',
                name: 'observacion',
                data: 'observacion'
                
            },
            {


                'defaultContent': "<center> <a href=''  id='detalleoperaaprobarincidencia' class='' data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>"

            },
            {
                
                className: 'text-center',
                data: 'codigoprocesado',
                defaultContent: ""

                
            },

            {
                className: 'text-center',
                name: 'estatus',
                data: 'estatus'
            }

        ],

        language: lenguaje

    });


}


$("#btn_aprobarincidencia_buscar").on('click', function() {


    ocultarmensajesmodalesindexaprobar();
   
    
    var dp_fedesde_aprobarincidencia = $("#dp_fedesde_aprobarincidencia").val();
    var dp_fehasta_aprobarincidencia = $("#dp_fehasta_aprobarincidencia").val();

    var codigoaprobarinc = $("#s2_incidencia_aprobarincidencia").val();

    if(codigoaprobarinc==""){
        codigoaprobarinc="todos"; 
    }

    
    var todas = $('input:radio[id=ra_todas_aprobarincidencia]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_aprobarincidencia]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_aprobarincidencia]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_aprobarincidencia]:checked').val();
    var revisadas = $('input:radio[id=ra_revisadas_aprobarincidencia]:checked').val();

    //alert(todas,ingresadas,aprobadas,rechazadas);


    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    if (revisadas!= undefined) {
        estatus = "REVISADO";
        //alert("rechazadas"+estatus);

    } 
    
    var validarfechavalor = validar_fechas(dp_fedesde_aprobarincidencia , dp_fehasta_aprobarincidencia);

    if (validarfechavalor == 0) {


        $("#msj-fechamayor-aprobarincidencia").fadeIn();

        return false;

    } 
    else 
    {

            busquedaGeneral.dp_fedesde_aprobarincidencia = dp_fedesde_aprobarincidencia;
            busquedaGeneral.dp_fehasta_aprobarincidencia = dp_fehasta_aprobarincidencia;
            busquedaGeneral.tipo = "especifico";
            busquedaGeneral.codigoaprobarinc = codigoaprobarinc;
            busquedaGeneral.estatus = estatus;
            busquedaGeneral.buscar = "";


            jQuery('#listar_aprobarincidencia').DataTable().search('');
            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
 
    }



});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_aprobarincidencia_limpiar").on('click', function() {

    var limpiar = 'todos';

    ocultarmensajesmodalesindexaprobar();

    fechahoy=obtener_fechaactual();

    $("#s2id_s2_incidencia_aprobarincidencia li.select2-search-choice").remove();
    $("#s2_incidencia_aprobarincidencia").val("");

    $("#ra_ingresadas_aprobarincidencia").iCheck('check');

    //alert(fechahoy);

    $("#dp_fedesde_aprobarincidencia").val(fechahoy);
    $("#dp_fehasta_aprobarincidencia").val(fechahoy);

    reestablecerBusquedaGeneral();

    jQuery('#listar_aprobarincidencia').DataTable().search('');
    jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();


});


$("#btn_aprobar_incidencia").on('click', function() {

    ocultarmensajesmodalesindexaprobar();
    ocultarmensajesmodalesaprobar();
    ocultarmensajesmodalesvalidacionaprobar();


    var blanco = "";
    $("#textarea_observacion_aprobarincidencia").val(blanco);

    $("#check_enviarcorreo_aprobarincidencia").iCheck('check');

    $("#tx_numerodocumento_aprobarincidencia").removeAttr('disabled');

    var valorregistroaprob= $('input:radio[name=idsolicitudaprob]:checked').val();


    if (valorregistroaprob==undefined){

        $("#msj-seleccionarregistro-aprobarincidencia").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            //var documento= data[0].documento;

            //if((estatus=="INGRESADO")&&(documento=="")){
            if((estatus=="APROBADO")||(estatus=="RECHAZADO")){

              $("#msj-soloaprobarsolicitud-aprobarincidencia").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');

                $("#btnmodalcancelar_aprobarincidencia").removeAttr('disabled');


                $('#aprobar_incidencia').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    $("#codigoconcepto_aprobarincidencia").val(data[0].codconcepto);
                    $("#descripconcepto_aprobarincidencia").val(data[0].desconcepto);
                    $("#textarea_descripcion_aprobarincidencia").val(data[0].descripcio);
                    
                    var documento= data[0].documento;
                    if(documento!=""){

                        $("#tx_numerodocumento_aprobarincidencia").attr("disabled", "disabled");

                        $("#tx_numerodocumento_aprobarincidencia").val(data[0].documento);

                    }
                    else{

                        $("#tx_numerodocumento_aprobarincidencia").val(data[0].documento);

                    }


                    

                    $("#btnmodalaceptar_aprobarincidencia").attr("data-aprobarincidencia", data[0].id);
                
                    var route = "/obtenercorreosolicitanteaprobar/"+ data[0].id;
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                        //alert(data);
                        $("#txtoculto_correosolicitante_aprobar").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    }); 

                });
            }
       
        }); 

    }


});


$("#btnmodalaceptar_aprobarincidencia").on('click', function() {

    ocultarmensajesmodalesaprobar();
    ocultarmensajesmodalesvalidacionaprobar();

    $("#btnmodalaceptar_aprobarincidencia").attr("disabled", "disabled");

    var observacion = $("#textarea_observacion_aprobarincidencia").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_aprobarincidencia]:checked').val();

    var documento= $("#tx_numerodocumento_aprobarincidencia").val();


    if (documento == "") {

        $("#msj-numerodocumentovalidacion-aprobarincidencia").fadeIn();
        $('#aprobar_validacion').modal('show');
        $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
        return false;
    }


    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    }


    if(enviarcorreo=="SI"){

        var correosolicitante = $("#txtoculto_correosolicitante_aprobar").val();

        if (correosolicitante=="") {

            $("#msj-solicitantevacio-aprobarincidencia").fadeIn();
            $('#aprobarincidencia_validacioncorreo').modal('show');
            return false;

        }
        else{

            var validarcorreo= validar_correo(correosolicitante);

            if (validarcorreo == false) {

                $("#msj-correosolicitantenovalido-aprobarincidencia").fadeIn();
                $('#aprobar_validacion').modal('show');
                $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
                return false;
            }

            activarCargando();
            $("#btnmodalcancelar_aprobarincidencia").attr("disabled", "disabled");

            var route = "/aprobarsolicitud"

            $.post(route, {
                id: $("#btnmodalaceptar_aprobarincidencia").attr("data-aprobarincidencia"),
                observacion: observacion,
                documento: documento,
                correosolicitante:correosolicitante,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(data) {
                
                if (data.mensaje == 'aprobado') {

                    var route = "/enviarcorreosolicitudaprobada";
                    var token = $("#token").val();

                    var fechadesdesolicitud= "";
                    var fechahastasolicitud= "";

                    var idsolicitud = data.id_solicitud;
                    var tiposolicitud = data.tipo;
                    if((tiposolicitud=="D")||(tiposolicitud=="V")){
                        fechadesdesolicitud= data.fechadesde;
                        fechahastasolicitud= data.fechahasta;
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                    }
                    if(tiposolicitud=="H"){
                        fechadesdesolicitud= data.fechadesde;
                        var textfechadesdesolicitud= textofecha();
                    }
                    var descripsolicitud= data.descripsolicitud;
                    var descripconcepto= data.descripconcepto;
                    var aprobadopor= data.aprobadopor;
                    var observacion= data.observacion;
                    
                    var asunto= asuntocorreoaprobar(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudaprobar();
                    var textconcepto= textoconcepto();
                    var textdescripcion= textodescripcion();
                    var textaprobadopor = textoaprobadopor();
                    var textobservacion= textoobservacion();
                    var correosolicitante= data.correosolicitante; 
                    var correorecursoshumanos= data.correorecursoshumanos; 
                    
                    var solicitante= data.solicitante; 
                    var textelaboradapor = textoelaboradapor();

                    var idpapeleta=data.idpapeleta;

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textofechahastasolicitud:textfechahastasolicitud,
                            fechahastasolicitud:fechahastasolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textoaprobadopor: textaprobadopor,
                            aprobadopor:aprobadopor,
                            textoobservacion:textobservacion,
                            observacion:observacion,
                            correosolicitante:correosolicitante,
                            correorecursoshumanos:correorecursoshumanos,
                            solicitante:solicitante,
                            textoelaboradapor:textelaboradapor,
                            idpapeleta:idpapeleta,
                        },
                        success: function(data) {

                            var idsolicitud = data.idsolicitud;
                            var idpapeleta=data.idpapeleta; 

                            // alert(idsolicitud+idpapeleta);

                            $("#msj-success-aprobarincidencia").fadeIn();
                            desactivarCargando();
                            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                            setTimeout(function() {
                                $('#aprobar_incidencia').modal('hide')
                            }, 1500);

                            reestablecerBusquedaGeneral();
                            jQuery('#listar_aprobarincidencia').DataTable().search('');
                            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
                            imprimirpapeleta(idsolicitud,idpapeleta);
                        },
                        error: function(msj) {
                        }
                    }); 
                }
                else{
                    $('#msj-error-aprobarincidencia').text(data.error);
                    $("#msj-error-aprobarincidencia").removeClass('hidden');
                    $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
                }
            });
        }     
    } 

    else{

        var route = "/aprobarsolicitud"

        $.post(route, {
                id: $("#btnmodalaceptar_aprobarincidencia").attr("data-aprobarincidencia"),
                observacion: observacion,
                documento: documento,
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(data) {
            if (data.mensaje == 'aprobado') {

                $("#msj-success-aprobarincidencia").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#aprobar_incidencia').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_aprobarincidencia').DataTable().search('');
                jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
            }
            else{
                $('#msj-error-aprobarincidencia').text(data.error);
                $("#msj-error-aprobarincidencia").removeClass('hidden');
                $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
            }
        });
    }

});

var imprimirpapeleta = function(idsolicitud,idpapeleta){
   
    var url = "/imprimirpapeleta/"+idsolicitud+"/"+idpapeleta;   

    //alert(url);
    window.open(url,'_blank');

    //desactivarCargando();

}

$("#btnmodalaceptar_aprobarincidenciacorreo").on('click', function() {

    $("#btnmodalcancelar_aprobarincidencia").attr("disabled", "disabled");

    var observacion = $("#textarea_observacion_aprobarincidencia").val();

    var route = "/aprobarsolicitud"

    $.post(route, {
            id: $("#btnmodalaceptar_aprobarincidencia").attr("data-aprobarincidencia"),
            observacion: observacion,
            documento: documento,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(data) {
        if (data.mensaje == 'aprobado') {

            $("#msj-success-aprobarincidencia").fadeIn();
            $('#aprobarincidencia_validacioncorreo').modal('hide')

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#aprobar_incidencia').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_aprobarincidencia').DataTable().search('');
            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-aprobarincidencia').text(data.error);
            $("#msj-error-aprobarincidencia").removeClass('hidden');
            $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
            $("#btnmodalcancelar_aprobarincidencia").removeAttr('disabled');
        }
    });

});

$("#btnmodalcancelar_aprobarincidenciacorreo").on('click', function() {

    //alert("pase por aqui");
    $('#aprobarincidencia_validacioncorreo').modal('hide')
    $("#btnmodalaceptar_aprobarincidencia").removeAttr('disabled');
    return false;

});



$("#btn_rechazar_incidencia").on('click', function() {

    ocultarmensajesmodalesindexaprobar();
    ocultarmensajesmodalesrechazar();
    ocultarmensajesmodalesvalidacionrechazar();

    var blanco = "";
    $("#textarea_observacion_rechazarincidencia").val(blanco);

     $("#check_enviarcorreo_rechazarincidencia").iCheck('check');

    var valorregistroaprob= $('input:radio[name=idsolicitudaprob]:checked').val();

    if (valorregistroaprob==undefined){

        $("#msj-seleccionarregistro-rechazarincidencia").fadeIn();
        return false;
    }
    else{

        var route = "/obtenerestatussolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            if((estatus=="APROBADO")||(estatus=="RECHAZADO")){

              $("#msj-solorechazarsolicitud-aprobarincidencia").fadeIn();
              return false;

            }
            else{

                $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
                $("#btnmodalcancelar_rechazarincidencia").removeAttr('disabled');

                $('#rechazar_incidencia').modal({
                    backdrop: 'static',
                    keyboard: false
                });

   
                var route = "/obtenerdatossolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

                $.get(route, function (data) {

                   
                })
                .fail(function () {
                        alert("error");
                })

                .done(function (data) {

                    $("#codigoconcepto_rechazarincidencia").val(data[0].codconcepto);
                    $("#descripconcepto_rechazarincidencia").val(data[0].desconcepto);
                    $("#textarea_descripcion_rechazarincidencia").val(data[0].descripcio);
                    $("#btnmodalaceptar_rechazarincidencia").attr("data-rechazarincidencia", data[0].id);

                    var documento= data[0].documento;
                    if(documento!=""){

                        $("#tx_numerodocumento_rechazarincidencia").attr("disabled", "disabled");

                        $("#tx_numerodocumento_rechazarincidencia").val(data[0].documento);

                    }
                    else{

                        $("#tx_numerodocumento_rechazarincidencia").val(data[0].documento);

                    }


                    var route = "/obtenercorreosolicitanterechazar/"+ data[0].id;
                    var token = $("#token").val();

                    $.get(route, function (data) {
                    })
                    .done(function (data) {
                        //alert(data);
                        $("#txtoculto_correosolicitante_rechazar").val(data);    
                    })
                    .fail(function () {
                        alert("error");
                    }); 

                });

            }
       
        });  

    }

});


$("#btnmodalaceptar_rechazarincidencia").on('click', function() {

    $("#btnmodalaceptar_rechazarincidencia").attr("disabled", "disabled");

    ocultarmensajesmodalesrechazar();
    ocultarmensajesmodalesvalidacionrechazar();

    var observacion = $("#textarea_observacion_rechazarincidencia").val();


    if (observacion == "") {

            $("#msj-observacionvalidacion-rechazarincidencia").fadeIn();
            $('#rechazar_validacion').modal('show');
            $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
            return false;
    }

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_rechazarincidencia]:checked').val();

    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    } 

    if(enviarcorreo=="SI"){

        var correosolicitante = $("#txtoculto_correosolicitante_rechazar").val();

        if (correosolicitante=="") {

            $("#msj-solicitantevacio-rechazarincidencia").fadeIn();
            $('#rechazarincidencia_validacioncorreo').modal('show');
            return false;
        }

        else{

            var validarcorreo= validar_correo(correosolicitante);

            if (validarcorreo == false) {

                $("#msj-correosolicitantenovalido-rechazarincidencia").fadeIn();
                $('#rechazar_validacion').modal('show');
                $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
                return false;
            }

            activarCargandoed();
            $("#btnmodalcancelar_rechazarincidencia").attr("disabled", "disabled");

            var route = "/rechazarsolicitud"

            $.post(route, {
                    id: $("#btnmodalaceptar_rechazarincidencia").attr("data-rechazarincidencia"),
                    observacion: observacion,
                    correosolicitante:correosolicitante,
                    "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(data) {
                   
                if (data.mensaje == 'rechazado') {

                    var route = "/enviarcorreosolicitudrechazada";
                    var token = $("#token").val();

                    var fechadesdesolicitud= "";
                    var fechahastasolicitud= "";

                    var idsolicitud = data.id_solicitud;
                    var tiposolicitud = data.tipo;
                    if((tiposolicitud=="D")||(tiposolicitud=="V")){
                        fechadesdesolicitud= data.fechadesde;
                        fechahastasolicitud= data.fechahasta;
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                    }
                    if(tiposolicitud=="H"){
                        fechadesdesolicitud= data.fechadesde;
                        var textfechadesdesolicitud= textofecha();
                    }
                    var descripsolicitud= data.descripsolicitud;
                    var descripconcepto= data.descripconcepto;
                    var rechazadopor= data.rechazadopor;
                    var observacion= data.observacion;
                    
                    var asunto= asuntocorreorechazar(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudrechazar();
                    var textdescripcion= textodescripcion();
                    var textrechazadopor = textorechazadopor();
                    var textobservacion= textoobservacion();
                    var correosolicitante= data.correosolicitante; 
                    var textconcepto= textoconcepto();

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textofechahastasolicitud:textfechahastasolicitud,
                            fechahastasolicitud:fechahastasolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textorechazadopor: textrechazadopor,
                            rechazadopor:rechazadopor,
                            textoobservacion:textobservacion,
                            observacion:observacion,
                            correosolicitante:correosolicitante
                        },
                        success: function(msj) {

                            $("#msj-success-rechazarincidencia").fadeIn();
                            desactivarCargandoed();
                            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                            setTimeout(function() {
                                $('#rechazar_incidencia').modal('hide')
                            }, 1500);

                            reestablecerBusquedaGeneral();
                            jQuery('#listar_aprobarincidencia').DataTable().search('');
                            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
                        },
                        error: function(msj) {
                        }
                    });        
                }
                else{
                    $('#msj-error-rechazarincidencia').text(data.error);
                    $("#msj-error-rechazarincidencia").removeClass('hidden');
                    $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
                }

            });
        }
            
    }
    else{

        var route = "/rechazarsolicitud"

        $.post(route, {
            id: $("#btnmodalaceptar_rechazarincidencia").attr("data-rechazarincidencia"),
            observacion: observacion,
            "_token": $("#token").val()
        }, function() {})
        .fail(function(msj) {

        })

        .done(function(data) {
            
            if (data.mensaje == 'rechazado') {

                $("#msj-success-rechazarincidencia").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#rechazar_incidencia').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_aprobarincidencia').DataTable().search('');
                jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();

            }
            else{
                $('#msj-error-rechazarincidencia').text(data.error);
                $("#msj-error-rechazarincidencia").removeClass('hidden');
                $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
            }

        });
    }

});

$("#btnmodalaceptar_rechazarincidenciacorreo").on('click', function() {

    $("#btnmodalcancelar_rechazarincidencia").attr("disabled", "disabled");

    var observacion = $("#textarea_observacion_rechazarincidencia").val();

    var route = "/rechazarsolicitud"

    $.post(route, {
            id: $("#btnmodalaceptar_rechazarincidencia").attr("data-rechazarincidencia"),
            observacion: observacion,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(data) {
        if (data.mensaje == 'rechazado') {

            $("#msj-success-rechazarincidencia").fadeIn();
            $('#rechazarincidencia_validacioncorreo').modal('hide')
            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
               
                $('#rechazar_incidencia').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_aprobarincidencia').DataTable().search('');
            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-rechazarincidencia').text(data.error);
            $("#msj-error-rechazarincidencia").removeClass('hidden');
            $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
            $("#btnmodalcancelar_rechazarincidencia").removeAttr('disabled');
        }
    });

});

$("#btnmodalcancelar_rechazarincidenciacorreo").on('click', function() {

    //alert("pase por aqui");
    $('#rechazarincidencia_validacioncorreo').modal('hide')
    $("#btnmodalaceptar_rechazarincidencia").removeAttr('disabled');
    return false;

});

$("#btn_revisar_incidencia").on('click', function() {

   ocultarmensajesmodalesindexaprobar();
   ocultarmensajesmodalesrevisar();

   var blanco = "";
   $("#textarea_observacion_revisarincidencia").val(blanco);

   $("#check_enviarcorreo_revisarincidencia").iCheck('check');

   //$("#tx_numerodocumento_revisarincidencia").removeAttr('disabled');

   var valorregistrorevi= $('input:radio[name=idsolicitudaprob]:checked').val();

   if (valorregistrorevi==undefined){

        $("#msj-seleccionarregistro-revisarincidencia").fadeIn();
        return false;
   }
   else{

        var route = "/obtenerestatussolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

        $.get(route, function (data) {

       
        })
        .fail(function () {
            alert("error");
        })

        .done(function (data) {

            var estatus = data[0].estatus;

            var documento= data[0].documento;

            //if((estatus=="INGRESADO")&&(documento=="")){
            if((estatus=="APROBADO")||(estatus=="RECHAZADO") ||(estatus=="REVISADO")){

              $("#msj-solorevisarsolicitud-aprobarincidencia").fadeIn();
              return false;

            }
            else{

                //alert(estatus+" "+documento)

                /*if((estatus=="INGRESADO")&&(documento!="")){

                    $("#msj-solorevisardocumentosolicitud-aprobarincidencia").fadeIn();
                    return false;

                }*/

                //else{

                    $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');

                    $("#btnmodalcancelar_revisarincidencia").removeAttr('disabled');


                    $('#revisar_incidencia').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

       
                    var route = "/obtenerdatossolicitudaprobar/" + $('input:radio[name=idsolicitudaprob]:checked').val();

                    $.get(route, function (data) {

                       
                    })
                    .fail(function () {
                            alert("error");
                    })

                    .done(function (data) {

                        $("#codigoconcepto_revisarincidencia").val(data[0].codconcepto);
                        $("#descripconcepto_revisarincidencia").val(data[0].desconcepto);
                        $("#textarea_descripcion_revisarincidencia").val(data[0].descripcio);
                        
                        var documento= data[0].documento;
                        if(documento!=""){

                            $("#tx_numerodocumento_revisarincidencia").attr("disabled", "disabled");

                            $("#tx_numerodocumento_revisarincidencia").val(data[0].documento);

                        }
                        else{

                            $("#tx_numerodocumento_revisarincidencia").val(data[0].documento);

                        }


                        $("#btnmodalaceptar_revisarincidencia").attr("data-revisarincidencia", data[0].id);
                    
                        var route = "/obtenercorreosolicitanterevisar/"+ data[0].id;
                        var token = $("#token").val();

                        $.get(route, function (data) {
                        })
                        .done(function (data) {
                            //alert(data);
                            $("#txtoculto_correosolicitante_revisar").val(data);    
                        })
                        .fail(function () {
                            alert("error");
                        }); 

                    });
                //}  
            }
       
        }); 

    }


});


$("#btnmodalaceptar_revisarincidencia").on('click', function() {

    $("#btnmodalaceptar_revisarincidencia").attr("disabled", "disabled");

    var observacion = $("#textarea_observacion_revisarincidencia").val();

    var enviarcorreo = $('input:checkbox[id=check_enviarcorreo_revisarincidencia]:checked').val();

    
    if (enviarcorreo=="") {
        enviarcorreo = "SI";
        //alert("todas"+estatus);
    }
    else{
        enviarcorreo = "NO"; 
    }


    if(enviarcorreo=="SI"){

        var correosolicitante = $("#txtoculto_correosolicitante_revisar").val();

        if (correosolicitante=="") {

            $("#msj-solicitantevacio-revisarincidencia").fadeIn();
            $('#revisarincidencia_validacioncorreo').modal('show');
            return false;

        }
        else{

            var validarcorreo= validar_correo(correosolicitante);

            //alert(validarcorreo);

            if (validarcorreo == false) {

                $("#msj-correosolicitantenovalido-revisarincidencia").fadeIn();
                $('#revisar_validacion').modal('show');
                $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');
                return false;
            }

            activarCargandorevi();
            $("#btnmodalcancelar_revisarincidencia").attr("disabled", "disabled");

            var route = "/revisarsolicitud"

            $.post(route, {
                id: $("#btnmodalaceptar_revisarincidencia").attr("data-revisarincidencia"),
                observacion: observacion,
                correosolicitante:correosolicitante,
                "_token": $("#token").val()
            }, function() {})
                .fail(function(msj) {

            })

            .done(function(data) {
                
                if (data.mensaje == 'revisado') {

                    var route = "/enviarcorreosolicitudrevisada";
                    var token = $("#token").val();

                    var fechadesdesolicitud= "";
                    var fechahastasolicitud= "";

                    var idsolicitud = data.id_solicitud;
                    var tiposolicitud = data.tipo;
                    var descripsolicitud= data.descripsolicitud;
                    var descripconcepto= data.descripconcepto;
                    var revisadopor= data.revisadopor;
                    var observacion= data.observacion;

                    if((tiposolicitud=="D")||(tiposolicitud=="V")){
                        fechadesdesolicitud= data.fechadesde;
                        fechahastasolicitud= data.fechahasta;
                        var textfechadesdesolicitud= textofechadesde();
                        var textfechahastasolicitud= textofechahasta();
                    }
                    if(tiposolicitud=="H"){
                        fechadesdesolicitud= data.fechadesde;
                        var textfechadesdesolicitud= textofecha();
                    }

                    var textconcepto= textoconcepto();
                    
                    var asunto= asuntocorreorevisar(descripconcepto);
                    var textnumerosolicitud= textonumerosolicitudrevisar();
                    var textdescripcion= textodescripcion();
                    var textrevisadopor = textorevisadopor();
                    var textobservacion= textoobservacion();
                    var textnotarevisado = notarevisado();
                    var correosolicitante= data.correosolicitante; 

                    $.ajax({
                        url: route,
                        headers: {
                            'X-CSRF-TOKEN': token
                        },
                        type: 'POST',
                        dataType: 'json',

                        data: {
                            asunto: asunto,
                            tiposolicitud:tiposolicitud,
                            textonumerosolicitud: textnumerosolicitud,
                            numerosolicitud:idsolicitud,
                            textofechadesdesolicitud:textfechadesdesolicitud,
                            fechadesdesolicitud:fechadesdesolicitud,
                            textofechahastasolicitud:textfechahastasolicitud,
                            fechahastasolicitud:fechahastasolicitud,
                            textoconcepto: textconcepto,
                            descripcionconcepto:descripconcepto,
                            textodescripcion: textdescripcion,
                            descripcion:descripsolicitud,
                            textorevisadopor: textrevisadopor,
                            revisadopor:revisadopor,
                            textoobservacion:textobservacion,
                            observacion:observacion,
                            notarevisado:textnotarevisado,
                            correosolicitante:correosolicitante
                        },
                        success: function(msj) {

                            $("#msj-success-revisarincidencia").fadeIn();
                            desactivarCargandorevi();
                            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                            setTimeout(function() {
                                $('#revisar_incidencia').modal('hide')
                            }, 1500);

                            reestablecerBusquedaGeneral();
                            jQuery('#listar_aprobarincidencia').DataTable().search('');
                            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
                        },
                        error: function(msj) {
                        }
                    }); 
                }
                else{
                    $('#msj-error-revisarincidencia').text(data.error);
                    $("#msj-error-revisarincidencia").removeClass('hidden');
                    $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');
                }
            });
        }     
    } 

    else{

        var route = "/revisarsolicitud"

        $.post(route, {
                id: $("#btnmodalaceptar_revisarincidencia").attr("data-revisarincidencia"),
                observacion: observacion,
                "_token": $("#token").val()
        }, function() {})
            .fail(function(msj) {

        })

        .done(function(data) {
            if (data.mensaje == 'revisado') {

                $("#msj-success-revisarincidencia").fadeIn();

                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#revisar_incidencia').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_aprobarincidencia').DataTable().search('');
                jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
            }
            else{
                $('#msj-error-revisarincidencia').text(data.error);
                $("#msj-error-revisarincidencia").removeClass('hidden');
                $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');
            }
        });
    }

});

$("#btnmodalaceptar_revisarincidenciacorreo").on('click', function() {

    $("#btnmodalcancelar_revisarincidencia").attr("disabled", "disabled");

    var observacion = $("#textarea_observacion_revisarincidencia").val();

    var route = "/revisarsolicitud"

    $.post(route, {
            id: $("#btnmodalaceptar_revisarincidencia").attr("data-revisarincidencia"),
            observacion: observacion,
            "_token": $("#token").val()
    }, function() {})
        .fail(function(msj) {

    })

    .done(function(data) {
        if (data.mensaje == 'revisado') {

            $("#msj-success-revisarincidencia").fadeIn();
            $('#revisarincidencia_validacioncorreo').modal('hide')

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#revisar_incidencia').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_aprobarincidencia').DataTable().search('');
            jQuery('#listar_aprobarincidencia').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-revisarincidencia').text(data.error);
            $("#msj-error-revisarincidencia").removeClass('hidden');
            $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');
            $("#btnmodalcancelar_revisarincidencia").removeAttr('disabled');
        }
    });

});

$("#btnmodalcancelar_revisarincidenciacorreo").on('click', function() {

    //alert("pase por aqui");
    $('#revisarincidencia_validacioncorreo').modal('hide')
    $("#btnmodalaceptar_revisarincidencia").removeAttr('disabled');
    return false;

});


$(document.body).on('click', '#detalleoperaaprobarincidencia', function() {


    var radioTabla = $($('#listar_aprobarincidencia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodaloperaaprobarincidencia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudaprob]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetoperaaprobarincidencia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosoperaaprobarincidencia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var operacion = data.operacion;
        var desde = data.desde;
        var latitud = data.latitud;
        var longitud = data.longitud;
        
        $('#datosoperaaprobarincidencia').append(
            '<tr><td><center>' + operacion +
            '<td><center>' + desde +  
            '<td><center>' + latitud + 
            '<td><center>' + longitud + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detalleaprobarincidencia', function() {


    var radioTabla = $($('#listar_aprobarincidencia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalaprobarincidencia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudaprob]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetaprobarincidencia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosaprobarincidencia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fechaaprob = data.fechaaprob;
        var horaaprob = data.horaaprob;
        
        $('#datosaprobarincidencia').append(
            '<tr><td><center>' + fechaaprob + 
            '<td><center>' + horaaprob + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});

$(document.body).on('click', '#detallerechazarincidencia', function() {


    var radioTabla = $($('#listar_aprobarincidencia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrechazarincidencia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudaprob]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrechazarincidencia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrechazarincidencia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharechaz = data.fecharechaz;
        var horarechaz = data.horarechaz;
        
        $('#datosrechazarincidencia').append(
            '<tr><td><center>' + fecharechaz + 
            '<td><center>' + horarechaz + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


$(document.body).on('click', '#detallerevisarincidencia', function() {


    var radioTabla = $($('#listar_aprobarincidencia tbody tr')[$(this).closest('tr').index()]);
    radioTabla.find('td input[type=radio]').prop('checked', true);

    //alert("pas  e por aqui");

    $("#detmodalrevisarincidencia").modal("show");

    var valorradiobutton = $('input:radio[name=idsolicitudaprob]:checked').val();

    //alert(valorradiobutton);

    var route = "/obtenerdatosdetrevisarincidencia/"+ valorradiobutton;
     
    //SE LIMPIA LA TABLA
    $("#datosrevisarincidencia > tbody").html("");


    $.get(route, function(data) {

    })
    .done(function(data) {

        var fecharevi = data.fecharevi;
        var horarevi = data.horarevi;
        
        $('#datosrevisarincidencia').append(
            '<tr><td><center>' + fecharevi + 
            '<td><center>' + horarevi + 
            '</td></tr>');

    })

    .fail(function() {
        alert("error");

    });


});


function ocultarmensajesmodalesindexaprobar() {

    $("#msj-seleccionarregistro-aprobarincidencia").fadeOut();
    $("#msj-seleccionarregistro-rechazarincidencia").fadeOut();
    $("#msj-seleccionarregistro-revisarincidencia").fadeOut();
    $("#msj-solorechazarsolicitud-aprobarincidencia").fadeOut();
    $("#msj-soloaprobarsolicitud-aprobarincidencia").fadeOut();
    $("#msj-solorevisarsolicitud-aprobarincidencia").fadeOut();
    $("#msj-solorevisardocumentosolicitud-aprobarincidencia").fadeOut();
    $("#msj-fechamayor-aprobarincidencia").fadeOut();
}

function ocultarmensajesmodalesaprobar() {

    $("#msj-success-aprobarincidencia").fadeOut();
    $("#msj-error-aprobarincidencia").fadeOut();
}

function ocultarmensajesmodalesvalidacionaprobar(){

    $("#msj-numerodocumentovalidacion-aprobarincidencia").fadeOut();
    $("#msj-correosolicitantenovalido-aprobarincidencia").fadeOut();
}

function ocultarmensajesmodalesrechazar() {

    $("#msj-success-rechazarincidencia").fadeOut();
    $("#msj-error-rechazarincidencia").fadeOut();
}

function ocultarmensajesmodalesrevisar() {

    $("#msj-success-revisarincidencia").fadeOut();
    $("#msj-error-revisarincidencia").fadeOut();
}

function ocultarmensajesmodalesvalidacionrechazar(){

    $("#msj-observacionvalidacion-rechazarincidencia").fadeOut();
    $("#msj-correosolicitantevacio-rechazarincidencia").fadeOut();
    $("#msj-correosolicitantenovalido-rechazarincidencia").fadeOut();

}




function cargarconceptosaprobarinc(){

    registros_conceptosaprobarinc = [];

    //console.log(registros);

    var route = "/cargarconceptosaprobarinc";

    $.get(route, function(data) {

    })

    .done(function(data) {


        $.each(data, function(key, registro) {

            registros_conceptosaprobarinc.push({
                    id: data[key].codconcepto,
                    text: data[key].desconcepto
            });

        });

        return registros_conceptosaprobarinc;

    })

    .fail(function() {
        alert("error");

    });

}

function recargarconceptosaprobarinc(criterio){


    var route = "/recargarconceptosaprobarinc/" + criterio;

    $.get(route, function(data) {

    })

    .done(function(data) {

        //cambio
        registros_conceptosaprobarinc = [];

        $.each(data, function(key, registro) {

            if (data == "") {

                registros[0] = "";

            } else {

                registros_conceptosaprobarinc.push({
                        id: data[key].codconcepto,
                        text: data[key].desconcepto
                });

            }

        });

    })

    .fail(function() {
        alert("error");

    });

}


////////////////////////////FUNCIONES/////////////////////////////


jQuery("#excelaprobarincidencia").on('click', function() {
   
    var route = "/excel/aprobarincidencia";

   
    var query = jQuery('#listar_aprobarincidencia').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});

jQuery("#pdfaprobarincidencia").on('click', function() {

    var estatus="fecha";
   
    var tipobusqueda= busquedaGeneral.tipo;
    //var buscar = busquedaGeneral.buscar;
    var dp_fedesde_aprobarincidencia=  busquedaGeneral.dp_fedesde_aprobarincidencia;
    var dp_fehasta_aprobarincidencia=  busquedaGeneral.dp_fehasta_aprobarincidencia;

    var codigoaprobarinc = busquedaGeneral.codigoaprobarinc;

    if(codigoaprobarinc==""){
       codigoaprobarinc="todos"; 
    }

    var buscar = jQuery('#listar_aprobarincidencia').DataTable().search();
    busquedaGeneral.buscar = buscar;

    if(buscar==""){
       buscar="todos"; 
    }

    var todas = $('input:radio[id=ra_todas_aprobarincidencia]:checked').val();
    var ingresadas = $('input:radio[id=ra_ingresadas_aprobarincidencia]:checked').val();
    var aprobadas = $('input:radio[id=ra_aprobadas_aprobarincidencia]:checked').val();
    var rechazadas = $('input:radio[id=ra_rechazadas_aprobarincidencia]:checked').val();

    if (todas=="") {
        estatus = "todos";
        //alert("todas"+estatus);
    } 
    

    if (ingresadas!= undefined) {
        estatus = "INGRESADO";
        //alert("ingresadas"+estatus);
    } 
   


    if (aprobadas!= undefined) {
        estatus = "APROBADO";
        //alert("aprobadas"+estatus);
    } 


    if (rechazadas!= undefined) {
        estatus = "RECHAZADO";
        //alert("rechazadas"+estatus);

    } 

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfaprobarincidencia/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_aprobarincidencia+"/"+dp_fehasta_aprobarincidencia+"/"+estatus+"/"+codigoaprobarinc;

    //alert(url);

    window.open(url,'_blank');

    desactivarCargando();

   
});



jQuery(function($) {

    setTimeout(function() {

        $('#s2_incidencia_aprobarincidencia').select2({

            closeOnSelect: true,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione la Incidencia",


            formatSelectionTooBig: function (limit) {
                return 'S\u00F3lo puede seleccionar un Registro';
            },

            formatNoMatches: function(options) {

                return 'No existen registros Asociados';

            },
            query: function(options) {

                if (options.term.length >= 2) {

                    //CAMBIO
                    registros_conceptosaprobarinc.length= 0;
                    registros_filtrados_conceptosaprobarinc.length= 0;

                    recargarconceptosaprobarinc(options.term);

                    setTimeout(function() {
                        options.callback({
                            results: registros_conceptosaprobarinc
                        })
                    }, 2000); 

                } else {

                    if (options.term != "") {

                        filtrar_data(registros_conceptosaprobarinc, options.term,registros_filtrados_conceptosaprobarinc);

                        options.callback({
                            results: data_registrosfiltrados 
                        })

                    } else {

                        options.callback({
                            results: registros_conceptosaprobarinc
                        })
                    }
                }

            }
        });

     }, 500);

});


