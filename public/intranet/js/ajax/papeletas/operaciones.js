
var busquedaGeneral = {
    dp_fedesde_papeletas: 'todos',
    dp_fehasta_papeletas: 'todos',
    tipo: "general",
    buscar: ""
};


function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_papeletas = 'todos';
    busquedaGeneral.dp_fehasta_papeletas = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function listarpapeletas() {

    //alert("pase");

    var route = "/listarpapeletas";

    var recargartablatotal = jQuery('#listar_papeletas').dataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                data.dp_fedesde_papeletas = busquedaGeneral.dp_fedesde_papeletas;
                data.dp_fehasta_papeletas = busquedaGeneral.dp_fehasta_papeletas;
                data.tipo = busquedaGeneral.tipo;
                //data.estatus = busquedaGeneral.estatus;
                //data.codigoaprobarinc = busquedaGeneral.codigoaprobarinc;
                
            }

        },

        columns: [

            {
                data: 'idpapeleta',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idpapeleta" value="' + data + '" class="flat-blue">' + data + '';
                }
            },
            {
                className: 'text-center',
                name: 'solicitadapor',
                data: 'solicitadapor'
            },
            {
                className: 'text-center',
                name: 'fechasolicitud',
                data: 'fechasolicitud'
            },
            {
                className: 'text-center',
                name: 'horasolicitud',
                data: 'horasolicitud'
                
            },
           /*{
                className: 'text-center',
                name: 'tiposolicitud',
                render: function(data, type, row) {

                    var tiposolicitud= row.tiposolicitud;
                    //alert(codigoconcepto);

                    if(tiposolicitud=="V"){
                          //alert(mediajornada);
                          var tipo="VACACION";
                          return tipo;
                    }
                    if(tiposolicitud=="D"){
                          //alert(mediajornada);
                          var tipo="DIA";
                          return tipo;
                        
                       
                    }
                    if(tiposolicitud=="H"){
                          //alert(mediajornada);
                          var tipo="HORA";
                          return tipo;
                        
                       
                    }

                    
                }
                
            },*/
            {
                className: 'text-center',
                name: 'tiposolicitud',
                data: 'tiposolicitud'
                
            },
            
            {
                className: 'text-center',
                name: 'descripcionsolicitud',
                data: 'descripcionsolicitud'
                
            },
            {
                className: 'text-center',
                name: 'fechaaprobado',
                data: 'fechaaprobado'
                
            },
            {
                className: 'text-center',
                name: 'horaaprobado',
                data: 'horaaprobado'
                
            },
            {
                className: 'text-center',
                name: 'observacionaprobado',
                data: 'observacionaprobado'
                
            }
        ],

        language: lenguaje

    });

}


$("#btn_papeletas_buscar").on('click', function() {


    $("#msj-fechamayor-papeletas").fadeOut();
    $("#msj-seleccionarregistro-papeletas").fadeOut();
    
    var dp_fedesde_papeletas = $("#dp_fedesde_papeletas").val();
    var dp_fehasta_papeletas = $("#dp_fehasta_papeletas").val();


    var validarfechavalor = validar_fechas(dp_fedesde_papeletas , dp_fehasta_papeletas);

    if (validarfechavalor == 0) {


        $("#msj-fechamayor-papeletas").fadeIn();

        return false;

    } 
    else 
    {

            busquedaGeneral.dp_fedesde_papeletas = dp_fedesde_papeletas;
            busquedaGeneral.dp_fehasta_papeletas = dp_fehasta_papeletas;
            busquedaGeneral.tipo = "especifico";
            //busquedaGeneral.codigoaprobarinc = codigoaprobarinc;
            //busquedaGeneral.estatus = estatus;
            busquedaGeneral.buscar = "";


            jQuery('#listar_papeletas').DataTable().search('');
            jQuery('#listar_papeletas').DataTable().ajax.reload();
 
    }

});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_papeletas_limpiar").on('click', function() {


    var limpiar = 'todos';

    $("#msj-fechamayor-papeletas").fadeOut();
    $("#msj-seleccionarregistro-papeletas").fadeOut();
  

    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_papeletas").val(fechahoy);
    $("#dp_fehasta_papeletas").val(fechahoy);

    reestablecerBusquedaGeneral();

    jQuery('#listar_papeletas').DataTable().search('');
    jQuery('#listar_papeletas').DataTable().ajax.reload();



});

////////////////////////////FUNCIONES/////////////////////////////


jQuery("#pdfpapeletas").on('click', function() {

    $("#msj-fechamayor-papeletas").fadeOut();
    $("#msj-seleccionarregistro-papeletas").fadeOut();

    var valorregistropapeleta= $('input:radio[name=idpapeleta]:checked').val();

    if (valorregistropapeleta==undefined){

        $("#msj-seleccionarregistro-papeletas").fadeIn();
        return false;
    }
    else{

        activarCargando();

        //var route = "/pdf/marcaasistenciarepo/";


        var url = "/pdfimprimirboletas/"+valorregistropapeleta;

        window.open(url,'_blank');

        desactivarCargando();


    }

    //alert(dp_fedesde_marcaasistencia);
   
});


jQuery(function($) {

    setTimeout(function() {

     }, 500);

});


