
$("#btn_gestioncorreo_crear").on('click', function() {

     ocultarmensajesmodalescrear();

    $("#nb_casa").val('');
    $("#tx_representante").val('');
    $("#tx_correo").val('');

    $("#btnmodalaceptar_crcasa").removeAttr('disabled');

    $('#casa_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crcasa").on('click', function() {

    var casa = $("#nb_casa").val();
    var representante = $("#nb_representante").val();
    var correo = $("#tx_correo").val();

    var route = "/crearCasa";
    var token = $("#token").val();

    $("#msj-success-crcasa").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcasa").fadeOut();

    $("#btnmodalaceptar_crcasa").attr("disabled", "disabled");


    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {casa:casa,
            representante:representante,
            correo: correo},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crcasa").fadeIn();
                $("#btnmodalaceptar_crgestioncasa").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#casa_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crgestioncasa").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});



$("#btn_casa_editar").on('click', function() {

     ocultarmensajesmodaleseditar();
    
    //  $("#nb_edcasa").val();
    //  $("#nb_edrepresentante").val();
    //   $("#tx_edcorreo").val();

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#casa_editar').modal({
        backdrop: 'static',
        keyboard: false
    });

    
    var route = "/obtenerCasa/" + $('input:radio[name=id_casa]:checked').val();

    $(".modal-body input").val("");

    $.get(route, function (data) {
        $("#nb_edcasa").val(data[0].nb_casa);
        $("#nb_edrepresentante").val(data[0].nb_representante);
        $("#tx_edcorreo").val(data[0].tx_correo);
    })
        .fail(function () {
            alert("error");
        });

});

$("#btnmodalaceptar_edcasa").on('click', function () {

    ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_edcasa").attr("disabled", "disabled");
    var route = "/editarCasa"

    $.post(route, {
        id: $('input:radio[name=id_casa]:checked').val(),
        casa: $("#nb_edcasa").val(),
        representante: $("#nb_edrepresentante").val(),
        correo: $("#tx_edcorreo").val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.msj-error-edcorreo').text(msj.responseJSON.nombre);
                $('.msj-error-edcorreo').fadeIn();
                $("#btnmodalaceptar_edcasa").removeAttr('disabled');
            }
        })

        .done(function (msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edcasa").fadeIn();
                listar();
                setTimeout(function () {
                    $('#casa_editar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_edcasa").removeAttr('disabled');
            } else {
                $("#msj-success-edexistente").fadeIn();
                $("#btnmodalaceptar_edcasa").removeAttr('disabled');
            }
        });
})


$("#btn_casa_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminar();

   $("#tx_correo").val('');

   $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

   $('#casa_eliminar').modal({
       backdrop: 'static',
       keyboard: false
   });

   var route = "/obtenerCasa/" + $('input:radio[name=id_casa]:checked').val();

   $(".modal-body input").val("");

   $.get(route, function (data) {
       $("#nb_elcasa").val(data[0].nb_casa);
       $("#nb_elrepresentante").val(data[0].nb_representante);
       $("#tx_elcorreo").val(data[0].tx_correo);
   })
       .fail(function () {
           alert("error");
       });

});


$("#btnmodalaceptar_elcasa").on('click', function () {


     ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_elcasa").attr("disabled", "disabled");

    var route = "/eliminarCasa"

    $.post(route, {
        id: $('input:radio[name=id_casa]:checked').val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            $("#btnmodalaceptar_elcasa").removeAttr('disabled');
            // var errornombre = msj.responseJSON.nombre;

            // if (errornombre != undefined) {
            //     $('.msj-error-elcorreo').text(msj.responseJSON.nombre);
            //     $('.msj-error-elcorreo').fadeIn();
            //     $("#btnmodalaceptar_elcasa").removeAttr('disabled');
            // }
        })

        .done(function (msj) {

                $("#msj-success-elcasa").fadeIn();
                listar();
                setTimeout(function () {
                    $('#casa_eliminar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_elcasa").removeAttr('disabled');
        });
})



var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_casa').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_casa').dataTable( {
  
          ajax: {
              url:'/listarCasa',
              dataSrc: ""
          },
          columns: [
               {   
                  data:'id_casa',
                  render: function (data,type,row) {
                      return '<input type="radio" checked="" name="id_casa" value="'+data+'" class="flat-blue">'+data+'';    
                  }
              },
              {
                name: 'nb_casa',
                data: 'nb_casa'
              },
              {
                  name: 'nb_representante',
                  data: 'nb_representante'
              },
              {
                  name: 'tx_correo',
                  data: 'tx_correo'
              }
          ],
          language : lenguaje
    }); 
    
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crcasa").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcasa").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-success-edcasa").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edcasa").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elcasa").fadeOut();
    $("#msj-error-elcasa").fadeOut();
 }