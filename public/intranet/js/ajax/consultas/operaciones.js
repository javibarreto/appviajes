
registros_personalconsultas = [];
registros_filtrados_personalconsultas = [];

var busquedaGeneral = {
    dp_fedesde_consultas: 'todos',
    dp_fehasta_consultas: 'todos',
    tipo: "general",
    personalconsultas: "",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_consultas = 'todos';
    busquedaGeneral.dp_fehasta_consultas = 'todos';
    busquedaGeneral.personalconsultas = "";
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});

function cargarpersonalsupervisorconsultas(){

    registros_personalconsultas= [];

    //console.log(registros);

    var route = "/cargarpersonalsupervisorconsultas";

    $.get(route, function(data) {

    })

    .done(function(data) {


        $.each(data, function(key, registro) {

            //alert(data[key].dni);

            registros_personalconsultas.push({
                    id: data[key].dni,
                    text: data[key].nombre
            });

        });

        return registros_personalconsultas;

    })

    .fail(function() {
        alert("error");

    });

}


function listarconsultas() {

    //alert("pase");

    var route = "/listarconsultas";

    var tipo="general"

    var recargartablatotal = jQuery('#listar_consultas').dataTable({
        
        "lengthMenu": [10],
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                data.dp_fedesde_consultas = busquedaGeneral.dp_fedesde_consultas;
                data.dp_fehasta_consultas = busquedaGeneral.dp_fehasta_consultas;
                data.tipo = busquedaGeneral.tipo;
                data.personalconsultas = busquedaGeneral.personalconsultas;

            }

        },

        columns: [
            
            {
                data: 'id',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idconsulta" value="' + data + '" class="flat-blue">' + data + '';
                }
            },

            {
               className: 'text-left',
                data: 'nomperson'
            },
            {
                className: 'text-left',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'fecha',
                data: 'fecha'
                
            },
            {
                className: 'text-center',
                name: 'hora',
                data: 'hora'
                
            },

            {
                className: 'text-center',
                name: 'personaresp',
                data: 'personaresp'
                
            },

            {
                className: 'text-center',
                name: 'respuesta',
                data: 'respuesta'
                
            },

            {
                className: 'text-center',
                name: 'respondido',
                data: 'respondido'
                
            }
        
        ],

        language: lenguaje

    });

    

}


$("#btn_consultas_buscar").on('click', function() {


    $("#msj-fechamayor-consultas").fadeOut();
    $("#msj-noexistenregistros-consultas").fadeOut();
    $("#msj-fechadesdemesactual-consultas").fadeOut();
    $("#msj-fechahastamesactual-consultas").fadeOut();
    $("#msj-fechahastadiaactual-consultas").fadeOut();
    
    var dp_fedesde_consultas = $("#dp_fedesde_consultas").val();
    var dp_fehasta_consultas = $("#dp_fehasta_consultas").val();


    var validarfechavalor = validar_fechas(dp_fedesde_consultas , dp_fehasta_consultas);

    var fullDate = new Date()

    var dia = ((fullDate.getDate().length) === 1) ? (fullDate.getDate()) : '0' + (fullDate.getDate());
    var mes = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);



    var fechaactual = fullDate.getFullYear() + "-" + mes + "-" + dia;

    var validarfechadesdeconsultas = validar_fechasmesactual(dp_fedesde_consultas, fechaactual);

    var validarfechahastaconsultas = validar_fechasmesactual(dp_fehasta_consultas, fechaactual);

    
    var personalconsultas = $("#s2_personalconsultas").val();

    //alert(personalconsultas);

    if((personalconsultas=="")||(personalconsultas==null)){
        personalconsultas="todos"; 
    }


    if (validarfechavalor == 0) {


        $("#msj-fechamayor-consultas").fadeIn();

        return false;

    }
    
    //SE INHABILITO LA VALIDACION QUE TIENE LA BUSQUEDA
    //SE AGREGO ESTE ELSE
    else{

        busquedaGeneral.dp_fedesde_consultas = dp_fedesde_consultas;
        busquedaGeneral.dp_fehasta_consultas = dp_fehasta_consultas;
        busquedaGeneral.tipo = "especifico";
        busquedaGeneral.personalconsultas = personalconsultas;
        busquedaGeneral.buscar = "";

        var tipo="especifico"
    
        jQuery('#listar_consultas').DataTable().search('');
        jQuery('#listar_consultas').DataTable().ajax.reload();

    } 
});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_consultas_limpiar").on('click', function() {


    var limpiar = 'todos';

    $("#msj-fechamayor-consultas").fadeOut();
    $("#msj-noexistenregistros-consultas").fadeOut();
    $("#msj-fechadesdemesactual-consultas").fadeOut();
    $("#msj-fechahastamesactual-consultas").fadeOut();
    $("#msj-fechahastadiaactual-consultas").fadeOut();

    $("#s2id_s2_personalconsultas li.select2-search-choice").remove();
    $("#s2_personalconsultas").val("");

    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_consultas").val(fechahoy);
    $("#dp_fehasta_consultas").val(fechahoy);

    reestablecerBusquedaGeneral();

    var tipo="general"

    jQuery('#listar_consultas').DataTable().search('');
    jQuery('#listar_consultas').DataTable().ajax.reload();

});

$("#btn_consultas_crear").on('click', function() {

    ocultarmensajesmodalesindexconsultas();
    ocultarmensajesmodalescrearconsultas();

    var blanco = "";
    $("#textarea_descripcion_crconsultas").val(blanco);

    $("#btnmodalaceptar_crconsultas").removeAttr('disabled');
    $("#btnmodalcancelar_crconsultas").removeAttr('disabled');


    $('#consultas_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});

$("#btnmodalaceptar_crconsultas").on('click', function() {

    ocultarmensajesmodalesvalidacioncrconsultas();

    $("#btnmodalaceptar_crconsultas").attr("disabled", "disabled");

    var descripcion = $("#textarea_descripcion_crconsultas").val();

    if (descripcion == "") {

        $("#msj-descripcion-crconsultas").fadeIn();
        $('#consultas_crvalidacion').modal('show');
        $("#btnmodalaceptar_crconsultas").removeAttr('disabled');
        return false;

    } 

    var route = "/ingresarconsultas";
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        type: 'POST',
        dataType: 'json',

        data: {
            descripcion: descripcion
        },
        success: function(msj) {

            if (msj.mensaje == 'creado') {

                //setTimeout(function() {
                
                $("#msj-success-crconsultas").fadeIn();
                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#consultas_crear').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_consultas').DataTable().search('');
                jQuery('#listar_consultas').DataTable().ajax.reload();

               
                //}, 1500);
            } else {
                $('#msj-error-crconsultas').text(msj.error);
                $("#msj-error-crconsultas").removeClass('hidden');
                $("#btnmodalaceptar_crconsultas").removeAttr('disabled');
            }

        },
        error: function(msj) {
        }
    });

});


$("#btn_responder_crear").on('click', function() {

    ocultarmensajesmodalesindexconsultas();
    ocultarmensajesmodalescrearresponder();

    var valorregistroconsulta= $('input:radio[name=idconsulta]:checked').val();


    if (valorregistroconsulta==undefined){

        $("#msj-seleccionarregistro-consultas").fadeIn();
        return false;
    }
    else{

        var route="/obtenerdatosconsulta/"+$('input:radio[name=idconsulta]:checked').val();

        //alert(route);
     
        $.get(route , function(data) {
          
        }) 

        .fail(function() {
            alert( "error" );
        })

        .done(function(data) {

            var valorrespondido= data.respondido;
            var valorusuarioactual=data.usuarioactual;
            var valorusuarioconsulta=data.usuarioconsulta;

            //alert(valorestado);

            if (valorrespondido=="S"){

                $("#msj-respuestanopermitida-consultas").fadeIn();
                return false;
            }
            else{

                if (valorusuarioactual==valorusuarioconsulta){

                    $("#msj-respuestasupervisornopermitida-consultas").fadeIn();
                    return false;
                } 

                else{

                    var blanco = "";
                    $("#textarea_descripcion_crresponder").val(blanco);

                    $("#btnmodalaceptar_crresponder").removeAttr('disabled');
                    $("#btnmodalcancelar_crresponder").removeAttr('disabled');

                    $("#btnmodalaceptar_crresponder").attr("data-editar",data.id);

                    $('#responder_crear').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                }  
            }

        });
    } 

});

$("#btnmodalaceptar_crresponder").on('click',function(){

    ocultarmensajesmodalescrearresponder();

    $("#btnmodalaceptar_crresponder").attr("disabled", "disabled");

    var descripcion = $("#textarea_descripcion_crresponder").val();

    if (descripcion == "") {

        $("#msj-descripcion-crresponder").fadeIn();
        $('#responder_crvalidacion').modal('show');
        $("#btnmodalaceptar_crresponder").removeAttr('disabled');
        return false;

    } 
    
    var route="/actualizarrespuesta"
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',

        data:{
          codigo: $("#btnmodalaceptar_crresponder").attr("data-editar"),
          respuesta: descripcion
          
        },

        success:function(msj){
          
          $("#msj-success-crresponder").fadeIn();
         
          //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
          setTimeout(function () {
             $("#responder_crear").modal('hide');
          }, 1500);

          reestablecerBusquedaGeneral();
          jQuery('#listar_consultas').DataTable().search('');
          jQuery('#listar_consultas').DataTable().ajax.reload();
        },
        
        error:function(msj){

        }

    });
 
})


////////////////////////////FUNCIONES/////////////////////////////

function ocultarmensajesmodalesindexconsultas() {

    $("#msj-fechamayor-consultas").fadeOut();
    $("#msj-fechadesdemesactual-consultas").fadeOut();
    $("#msj-fechahastamesactual-consultas").fadeOut();
    $("#msj-fechahastadiaactual-consultas").fadeOut();
    $("#msj-noexistenregistros-consultas").fadeOut();
    $("#msj-respuestanopermitida-consultas").fadeOut();
    $("#msj-respuestasupervisornopermitida-consultas").fadeOut();
    $("#msj-seleccionarregistro-consultas").fadeOut();   
 
}

function ocultarmensajesmodalescrearconsultas(){

    $("#msj-success-crconsultas").fadeOut();
    $("#msj-error-crconsultas").fadeOut();
 
}

function ocultarmensajesmodalescrearresponder(){

    $("#msj-success-crresponder").fadeOut();
    $("#msj-error-crresponder").fadeOut();
 
}

function ocultarmensajesmodalesvalidacioncrconsultas(){

    $("#msj-descripcion-crconsultas").fadeOut();
}

function ocultarmensajesmodalesvalidacioncrresponder(){

    $("#msj-descripcion-crresponder").fadeOut();
}


jQuery("#excelmarcaasistenciarepo").on('click', function() {
   
    var route = "/excel/marcaasistenciarepo";

   
    var query = jQuery('#listar_marcaasistencia_repo').DataTable().search();

    //console.log(query);

    busquedaGeneral.buscar = query;

    activarCargando();

    jQuery.post(route,busquedaGeneral, function(response, status, xhr) {
            try {
                var a = document.createElement("a");
                a.href = response.file;
                a.download = response.name;
                document.body.appendChild(a);
                a.click();
                a.remove();

            } catch (ex) {
                console.log(ex);
            }

        })
        .fail(function() {
            console.log("Error descargar excel");
        }).always(function() {
            desactivarCargando();
        });

});

jQuery("#pdfmarcaasistenciarepo").on('click', function() {

    var estatus="fecha";
   
    var tipobusqueda= busquedaGeneral.tipo;
    //var buscar = busquedaGeneral.buscar;
    var dp_fedesde_marcaasistencia=  busquedaGeneral.dp_fedesde_marcaasistencia;
    var dp_fehasta_marcaasistencia=  busquedaGeneral.dp_fehasta_marcaasistencia;

    var buscar = jQuery('#listar_marcaasistencia_repo').DataTable().search();
    busquedaGeneral.buscar = buscar;

    if(buscar==""){
       buscar="todos"; 
    }

    //alert()

    //alert(dp_fedesde_marcaasistencia);

    activarCargando();

    //var route = "/pdf/marcaasistenciarepo/";


    var url = "/pdfmarcaasistenciarepo/"+tipobusqueda+"/"+buscar+"/"+dp_fedesde_marcaasistencia+"/"+dp_fehasta_marcaasistencia+"/"+estatus;


    window.open(url,'_blank');

    desactivarCargando();

   
});


jQuery(function($) {

    setTimeout(function() {

        $('#s2_personalconsultas').select2({

            closeOnSelect: true,
            maximumSelectionSize: 1,
            multiple: true,
            placeholder: "Por favor seleccione la Persona",


            formatSelectionTooBig: function (limit) {
                return 'S\u00F3lo puede seleccionar un Registro';
            },

            formatNoMatches: function(options) {

                return 'No existen registros Asociados';

            },
            query: function(options) {

                if (options.term.length >= 2) {

                    //CAMBIO
                    registros_personalconsultas.length= 0;
                    registros_filtrados_personalconsultas.length= 0;

                    recargarpersonalsupervisorconsultas(options.term);

                    setTimeout(function() {
                        options.callback({
                            results: registros_personalconsultas
                        })
                    }, 2000); 

                } else {

                    if (options.term != "") {

                        filtrar_data(registros_personalconsultas, options.term,registros_filtrados_personalconsultas);

                        options.callback({
                            results: data_registrosfiltrados 
                        })

                    } else {

                        options.callback({
                            results: registros_personalconsultas
                        })
                    }
                }

            }
        });

     }, 500);

});


