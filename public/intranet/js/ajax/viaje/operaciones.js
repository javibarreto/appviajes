
$("#btn_viaje_crear").on('click', function() {
    

     ocultarmensajesmodalescrear();

    $("#co_viaje").val('');
    $("#nu_plazas").val('');
    $("#nb_origen").val('');
    $("#nb_destino").val('');
    $("#nu_precio").val('');

    $("#btnmodalaceptar_crviaje").removeAttr('disabled');

    $('#viaje_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crviaje").on('click', function() {

    var nombre = $("#co_viaje").val();
    var plazas = $("#nu_plazas").val();
    var origen = $("#nb_origen").val();
    var destino = $("#nb_destino").val();
    var precio = $("#nu_precio").val();

 

    var route = "/crearViaje";
    var token = $("#token").val();

    $("#msj-success-crviaje").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crviaje").fadeOut();

    $("#btnmodalaceptar_crviaje").attr("disabled", "disabled");

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {nombre:nombre,
            plazas:plazas,
            origen:origen,
            destino:destino,
            precio: precio},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crviaje").fadeIn();
                $("#btnmodalaceptar_crgestionviaje").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#viaje_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crgestionviaje").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});



$("#btn_viaje_editar").on('click', function() {

     ocultarmensajesmodaleseditar();
    
    //  $("#nb_edcasa").val();
    //  $("#nb_edrepresentante").val();
    //   $("#tx_edcorreo").val();

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#viaje_editar').modal({
        backdrop: 'static',
        keyboard: false
    });

    
    var route = "/obtenerViaje/" + $('input:radio[name=id_viaje]:checked').val();

    $(".modal-body input").val("");

    $.get(route, function (data) {
        $("#co_edviaje").val(data[0].co_viaje);
        $("#nu_edplazas").val(data[0].nu_plazas);
        $("#nb_edorigen").val(data[0].nb_origen);
        $("#nb_eddestino").val(data[0].nb_destino);
        $("#nu_edprecio").val(data[0].nu_precio);

       

    })
        .fail(function () {
            alert("error");
        });

});

$("#btnmodalaceptar_edviaje").on('click', function () {

    ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_edviaje").attr("disabled", "disabled");
    var route = "/editarViaje"

    $.post(route, {

        id: $('input:radio[name=id_viaje]:checked').val(),
        nombre: $("#co_edviaje").val(),
        plazas: $("#nu_edplazas").val(),
        origen: $("#nb_edorigen").val(),
        destino: $("#nb_eddestino").val(),
        precio: $("#nu_edprecio").val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.msj-error-edcorreo').text(msj.responseJSON.nombre);
                $('.msj-error-edcorreo').fadeIn();
                $("#btnmodalaceptar_edviaje").removeAttr('disabled');
            }
        })

        .done(function (msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edviaje").fadeIn();
                listar();
                setTimeout(function () {
                    $('#viaje_editar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_edviaje").removeAttr('disabled');
            } else {
                $("#msj-success-edexistente").fadeIn();
                $("#btnmodalaceptar_edviaje").removeAttr('disabled');
            }
        });
})


$("#btn_viaje_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminar();

   $("#tx_correo").val('');

   $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

   $('#viaje_eliminar').modal({
       backdrop: 'static',
       keyboard: false
   });

   var route = "/obtenerViaje/" + $('input:radio[name=id_viaje]:checked').val();

   $(".modal-body input").val("");

   $.get(route, function (data) {
        $("#co_elviaje").val(data[0].co_viaje);
        $("#nu_elplazas").val(data[0].nu_plazas);
        $("#nb_elorigen").val(data[0].nb_origen);
        $("#nb_eldestino").val(data[0].nb_destino);
        $("#nu_elprecio").val(data[0].nu_precio);
   })
       .fail(function () {
           alert("error");
       });

});


$("#btnmodalaceptar_elviaje").on('click', function () {


     ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_elviaje").attr("disabled", "disabled");

    var route = "/eliminarViaje"

    $.post(route, {
        id: $('input:radio[name=id_viaje]:checked').val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            $("#btnmodalaceptar_elviaje").removeAttr('disabled');
            // var errornombre = msj.responseJSON.nombre;

            // if (errornombre != undefined) {
            //     $('.msj-error-elcorreo').text(msj.responseJSON.nombre);
            //     $('.msj-error-elcorreo').fadeIn();
            //     $("#btnmodalaceptar_elcasa").removeAttr('disabled');
            // }
        })

        .done(function (msj) {

                $("#msj-success-elviaje").fadeIn();
                listar();
                setTimeout(function () {
                    $('#viaje_eliminar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_elviaje").removeAttr('disabled');
        });
})



var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_viaje').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_viaje').dataTable( {
  
          ajax: {
              url:'/listarViajes',
              dataSrc: ""
          },
          columns: [
            {   
                data:'id_viaje',
                render: function (data,type,row) {
                    return '<input type="radio" checked="" name="id_viaje" value="'+data+'" class="flat-blue">'+data+'';    
                }
            },
            {
              name: 'co_viaje',
              data: 'co_viaje'
            },
            {
                name: 'nu_plazas',
                data: 'nu_plazas'
            },
            {
                name: 'nb_origen',
                data: 'nb_origen'
            },
            {
                name: 'nb_destino',
                data: 'nb_destino'
            },
            {
              name: 'nu_precio',
              data: 'nu_precio'
            }
            ],
          language : lenguaje
    }); 
    
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crviaje").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crviaje").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-success-edviaje").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edviaje").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elviaje").fadeOut();
    $("#msj-error-elviaje").fadeOut();
 }