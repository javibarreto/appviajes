
$("#btn_competidor_crear").on('click', function() {

     ocultarmensajesmodalescrear();

    $("#nb_competidor").val('');
    $("#nu_peso").val('');
    $("#id_casa").val('');

    $("#btnmodalaceptar_crcompetidor").removeAttr('disabled');

    $('#competidor_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crcompetidor").on('click', function() {

    var competidor = $("#nb_competidor").val();
    var peso = $("#nu_peso").val();
    var casa = $("#id_casa").val();

    var route = "/crearCompetidor";
    var token = $("#token").val();

    $("#msj-success-crcompetidor").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcompetidor").fadeOut();

    $("#btnmodalaceptar_crcompetidor").attr("disabled", "disabled");


    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {competidor:competidor,
            peso:peso,
            casa: casa},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crcompetidor").fadeIn();
                $("#btnmodalaceptar_crcompetidor").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#competidor_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crcompetidor").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcompetidor').text(msj.error);
                $("#msj-error-crcompetidor").removeClass('hidden');
                $("#btnmodalaceptar_crcompetidor").removeAttr('disabled');
            }
        }
    });
});



$("#btn_competidor_editar").on('click', function() {

     ocultarmensajesmodaleseditar();
    
    //  $("#nb_edcasa").val();
    //  $("#nb_edrepresentante").val();
    //   $("#tx_edcorreo").val();

    $("#btnmodalaceptar_edcompetidor").removeAttr('disabled');

    $('#competidor_editar').modal({
        backdrop: 'static',
        keyboard: false
    });

    
    var route = "/obtenerCompetidor/" + $('input:radio[name=id_competidor]:checked').val();

    $(".modal-body input").val("");

    $.get(route, function (data) {
        $("#nb_edcompetidor").val(data[0].nb_competidor);
        $("#nu_edpeso").val(data[0].peso);
        $("#id_edcasa").val(data[0].id_casa);
    })
        .fail(function () {
            alert("error");
        });

});

$("#btnmodalaceptar_edcompetidor").on('click', function () {

    ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_edcompetidor").attr("disabled", "disabled");
    var route = "/editarCompetidor"

    $.post(route, {
        id:  $('input:radio[name=id_competidor]:checked').val(),
        competidor: $("#nb_edcompetidor").val(),
        peso: $("#nu_edpeso").val(),
        casa: $("#id_edcasa").val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.msj-error-edcompetidor').text(msj.responseJSON.nombre);
                $('.msj-error-edcompetidor').fadeIn();
                $("#btnmodalaceptar_edcompetidor").removeAttr('disabled');
            }
        })

        .done(function (msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edcompetidor").fadeIn();
                listar();
                setTimeout(function () {
                    $('#competidor_editar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_edcompetidor").removeAttr('disabled');
            } else {
                $("#msj-error-edexistente").fadeIn();
                $("#btnmodalaceptar_edcompetidor").removeAttr('disabled');
            }
        });
})


$("#btn_competidor_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminar();

   $("#tx_competidor").val('');

   $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

   $('#competidor_eliminar').modal({
       backdrop: 'static',
       keyboard: false
   });

   var route = "/obtenerCompetidor/" + $('input:radio[name=id_competidor]:checked').val();

   $(".modal-body input").val("");

   $.get(route, function (data) {
       $("#nb_elcompetidor").val(data[0].nb_competidor);
       $("#nu_elpeso").val(data[0].peso);
       $("#id_elcasa").val(data[0].id_casa);
   })
       .fail(function () {
           alert("error");
       });

});


$("#btnmodalaceptar_elcompetidor").on('click', function () {


     ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_elcompetidor").attr("disabled", "disabled");

    var route = "/eliminarCompetidor"

    $.post(route, {
        id: $('input:radio[name=id_competidor]:checked').val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            $("#btnmodalaceptar_elcompetidor").removeAttr('disabled');
            // var errornombre = msj.responseJSON.nombre;

            // if (errornombre != undefined) {
            //     $('.msj-error-elcorreo').text(msj.responseJSON.nombre);
            //     $('.msj-error-elcorreo').fadeIn();
            //     $("#btnmodalaceptar_elcompetidor").removeAttr('disabled');
            // }
        })

        .done(function (msj) {

                $("#msj-success-elcompetidor").fadeIn();
                listar();
                setTimeout(function () {
                    $('#competidor_eliminar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_elcompetidor").removeAttr('disabled');
        });
})



var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_competidor').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_competidor').dataTable( {
  
          ajax: {
              url:'/listarCompetidor',
              dataSrc: ""
          },
          columns: [
               {   
                  data:'id_competidor',
                  render: function (data,type,row) {
                      return '<input type="radio" checked="" name="id_competidor" value="'+data+'" class="flat-blue">'+data+'';    
                  }
              },
              {
                name: 'id_casa',
                data: 'id_casa'
            },
              {
                name: 'nb_competidor',
                data: 'nb_competidor'
              },
              {
                  name: 'peso',
                  data: 'peso'
              }
          ],
          language : lenguaje
    }); 
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crcompetidor").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crcompetidor").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-success-edcompetidor").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edcompetidor").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elcompetidor").fadeOut();
    $("#msj-error-elcompetidor").fadeOut();
 }