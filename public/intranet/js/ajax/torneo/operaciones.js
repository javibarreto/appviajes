var id_fila = 1;



$("#btn_torneo_crear").on('click', function() {

    ocultarmensajesmodalescrear();

//    $("#nb_casa").val('');
//    $("#tx_representante").val('');
//    $("#tx_correo").val(''); 

   $("#btnmodalaceptar_crtorneo").removeAttr('disabled');

   $('#torneo_crear').modal({
       backdrop: 'static',
       keyboard: false
   });

});






$(document.body).on('click', '#detalle_torneo', function() {

    var route = "/detalle_torneo/" + $('input:radio[name=id_torneo]:checked').val();
    
    $("#torneo_detalle > tbody").html("");
 
   $('#torneo_detalle').modal({
       backdrop: 'static',
       keyboard: false
   });

});







$("#btnmodalaceptar_crtorneo").on('click', function() {

    var numeroFilas = $("#listar_detalle_crtorneo tr").length;

    var numero_par = numeroFilas % 2
    if (numero_par == 0){
        alert("el numero de participantes debe ser par")
        return
    }else{

    var fecha = $("#dp_fe_torneo").val();
    var valor_peso = $("#nu_peso").val();
    var valor_casa = $("#id_casa").val();

    var detalle_torneo = new Object();

    detalle_torneo.casa = [];
    detalle_torneo.competidor = [];
    detalle_torneo.peso = [];


    var i = 0;
    var j = 0;


    for (var i = 0; i <= id_fila; i++) {

        //SE DEFINE LOS IDENTIFICADORES
        var casa = "#casa" + i;
        var competidor = "#competidor" + i;
        var peso = "#peso" + i;

        //SE EXTRAEN LOS VALORES
        var valor_casa = $(casa).val();
        var valor_competidor = $(competidor).val();
        var valor_peso = $(peso).val();

        if ((valor_competidor != undefined) && (valor_casa != undefined)) {
            detalle_torneo.casa[j] = valor_casa;
            detalle_torneo.competidor[j] = valor_competidor;
            detalle_torneo.peso[j] = valor_peso;
            j++;
        }

    }

    var route = "/crearTorneo";
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {fecha:fecha,
            peso:valor_peso,
            competidores:numeroFilas,
            detalle_torneo:detalle_torneo
        },

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crtorneo").fadeIn();
                $("#btnmodalaceptar_crtorneo").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#torneo_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crtorneo").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crtorneo').text(msj.error);
                $("#msj-error-crtorneo").removeClass('hidden');
                $("#btnmodalaceptar_crtorneo").removeAttr('disabled');
            }
        }
    });
}
});



$(document.body).on('change', '#sp_competidor', function() {
   
     id_fila++;

     var valor_competidor = $("#sp_competidor").val()
     
     if (valor_competidor == "") {
        return
        }
      var casa = $("#sp_casa option:selected").html()
      var competidor = $("#sp_competidor option:selected").html()
     

    $('#listar_detalle_crtorneo').append(
        '<tr id="fila' + id_fila + '"><td style="display:none">' +
        '</td><td><center><input type="text" id="casa' + id_fila + '"  style="text-align:center"  disabled="" value="' + casa + '" />'   +
        '</td><td><center><input type="text" id="competidor' + id_fila + '"  style="text-align:center" disabled="" value="' + competidor + '" />' +
        '</td><td><center><input type="text" disabled="" id="peso' + id_fila + '" style="text-align:center" disabled="" value="' + valor_competidor + '" />' +
        '</td><td align="center">' + '<button type="button"  onclick="removerfilas(' + id_fila + ')" id="botoneliminar_cr' + id_fila + '" class="btn btn-danger"> <span class="glyphicon glyphicon-remove"></span></button>' +
        '</td></tr>');
});


var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_torneo').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_torneo').dataTable( {
  
          ajax: {
              url:'/listarTorneo',
              dataSrc: ""
          },
          columns: [
               {   
                  data:'id_torneo',
                  render: function (data,type,row) {
                      return '<input type="radio" checked="" name="id_torneo" value="'+data+'" class="flat-blue">'+data+'';    
                  }
              },
              {
                name: 'fe_torneo',
                data: 'fe_torneo'
              },
              {
                  name: 'nu_competidores',
                  data: 'nu_competidores'
              },
              {
                  name: 'peso_dif',
                  data: 'peso_dif'
              },
              {
                'defaultContent': "<center> <a href=''  id='detalle_torneo' class='detalleTorneo' data-toggle='modal' data-target='#detalle-modal'> <span class='glyphicon glyphicon-plus left-side-side'></span></a>"
            }
          ],
          language : lenguaje
    }); 
    
  }


  function ocultarmensajesmodalescrear() {

    $("#msj-success-crtorneo").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-success-nupar").fadeOut();
    $("#msj-error-crtorneo").fadeOut();
}


function removerfilas(id_fila) {

    var identificadorfilas = "#fila" + id_fila;

    $(identificadorfilas).remove();


}

$(document.body).on('change', '#sp_casa', function() {

     var id = $("#sp_casa").val()
     var route = "/cargarCompetidores/"+id
     console.log(route)

     $.get(route, function(data) {


             var select = $("#sp_competidor");
             var option = '';
             option += '<option value=' + "" + '>' + "Por favor seleccione un registro" + '</option>';

             $.each(data, function(key) {
                 option += '<option value=' + data[key].peso + '>' + data[key].nb_competidor + '</option>';
             });

             select.empty();
             select.val([]);
             select.append(option);
             select.selectpicker('refresh');


         })
         .fail(function() {
             alert("error");
         });

}); 