
$("#btnmodalaceptar_crmarcarasistencia").on('click', function () {


    var blanco = "";
    var ipcliente = "";

    ocultarmensajesmodalesmarcacionasistencia();

    $(".modal-body input").val("");

    var token = $("#token").val();

    $("#btnmodalaceptar_crmarcarasistencia").attr("disabled", "disabled");

    $("#btnmodalaceptar_marcarasistencia").attr("disabled", "disabled");

    
    $.getJSON('https://api.ipify.org?format=json', function(data){

    var ipcliente= data.ip;                      

        var route = "/buscar_marcacion_dniipmarcacion/"+ipcliente;

        $.get(route, function (data) {
        })

        .done(function (data) {

            if (data.buscarmarcacion=='MARCACIONDNIIP ENCONTRADA') {

              $("#msj-marcacionregistradamismaip-login").fadeIn();
              $("#marcacionasistencia_validacion").modal("show");
              $("#btnmodalaceptar_crmarcarasistencia").removeAttr('disabled');
              $("#btnmodalaceptar_marcarasistencia").removeAttr('disabled');
              return false;

            }
            else{

                var route = "/registrar_asistenciamarcacion";
                var token = $("#token").val();
                                                          
              $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',

                    data: {
                      ipcliente:ipcliente
                    },

                success: function (msj) {
                            
                    activarCargandoMarcacionAsistencia();
                    if (msj.mensaje=='asistenciaregistrada') {

                        if (msj.correoempleado!='') {

                            var route = "/enviarcorreoasistencia";
                            var token = $("#token").val();

                            var correoempleado= msj.correoempleado;

                            var datosempresa= "";

                            var datosempleado= msj.datosempleado;

                            var fechaasistencia= msj.fechaasistencia;

                            var horaasistencia= msj.horaasistencia;

                            var registradodesde= msj.registradodesde;

                            var dni= msj.dni;
                            
                            var nombreempleado= msj.nombreempleado;

                            var asunto= asuntocorreoasistencia();
                           
                            var textdescripcion= textodescripcionasistencia();

                            var textdatosempresa="";

                            var textdatosempleado=textodatosempleadoasistencia();

                            var textfechaasistencia=textofechaasistencia();

                            var texthoraasistencia=textohoraasistencia();

                            var textasistenciacreadadesde= textoasistenciacreadadesde();

                            //alert(idsolicitud+descripsolicitud+descripconcepto+generadapor+asunto+mensaje+correosupervisor)
                            $.ajax({
                                url: route,
                                headers: {
                                    'X-CSRF-TOKEN': token
                                },
                                type: 'POST',
                                dataType: 'json',

                                data: {
                                    correoempleado:correoempleado,
                                    asunto: asunto,
                                    textodescripcion: textdescripcion,
                                    textodatosempresa: textdatosempresa,
                                    textodatosempleado: textdatosempleado,
                                    textofechaasistencia:textfechaasistencia,
                                    textohoraasistencia:texthoraasistencia,
                                    datosempresa:datosempresa,
                                    datosempleado:datosempleado,
                                    fechaasistencia:fechaasistencia,
                                    horaasistencia:horaasistencia,
                                    textoasistenciacreadadesde:textasistenciacreadadesde,
                                    creadodesde:registradodesde,
                                    dni:dni,
                                    nombreempleado:nombreempleado
                                },
                                success: function(msj) {

                                    $("#btnmodalaceptar_crmarcarasistencia").removeAttr('disabled');
                                    $("#btnmodalaceptar_marcarasistencia").removeAttr('disabled');

                                    $('#txt_marcacionasis_dni').val(msj.dni);
                                    $('#txt_marcacionasis_nombreempleado').val(msj.nombreempleado);
                                    $('#txt_marcacionasis_fecha').val(msj.fecha);
                                    $('#txt_marcacionasis_hora').val(msj.hora);
                                    $('#txt_marcacionasis_dispositivo').val(msj.registradodesde);
                                    

                                    $("#msj-success-marcacionasistencia").fadeIn();
                                    desactivarCargandoMarcacionAsistencia();
                                   
                                },
                                error: function(msj) {
                                }
                            });

                        }//FIN CORREO DEL EMPLEADO INGRESADO
                        else{

                              $("#btnmodalaceptar_crmarcarasistencia").removeAttr('disabled');
                              $("#btnmodalaceptar_marcarasistencia").removeAttr('disabled');

                              $('#txt_marcacionasis_dni').val(msj.dni);
                              $('#txt_marcacionasis_nombreempleado').val(msj.nombreempleado);
                              $('#txt_marcacionasis_fecha').val(msj.fechaasistencia);
                              $('#txt_marcacionasis_hora').val(msj.horaasistencia);
                              $('#txt_marcacionasis_dispositivo').val(msj.registradodesde);

                              $("#msj-success-marcacionasistencia").fadeIn();
                              desactivarCargandoMarcacionAsistencia();

                        }//FIN CORREO DEL EMPLEADO NO INGRESADO


                    }

                },

                error: function (msj) {

                }
            }); 

            }

        }) //CIERRE BUSCAR MARCACION CON DNI-IP
                              
        .fail(function () {
            alert("error");
        })

    });//FIN DE LA LLAMADA DE LA API PARA SABER LA IP DEL CLIENTE

});


function abrirmodalmarcarasistencia(){

    ocultarmensajesmodalesmarcacionasistencia();

    $('.modal-dialog').stop().animate({ scrollTop: 0 }, 500);

    $(".modal-body input").val("");

    $('#marcarasistencia_crear').modal({backdrop: 'static', keyboard: false});
    
}

function ocultarmensajesmodalesmarcacionasistencia(){

    $("#msj-success-marcacionasistencia").fadeOut();
}

function activarCargandoMarcacionAsistencia() {
    //alert("pase por aqui");
    var cargando = $("#loadmarcacionasistencia");
    var procesandoinfo = $("#procesainfomarcacionmarcacionasistencia");
    procesandoinfo.show();
    cargando.button('loadmarcacionasistencia');
    cargando.show();

}

function desactivarCargandoMarcacionAsistencia() {
    $("#loadmarcacionasistencia").hide();
    $("#procesainfomarcacionmarcacionasistencia").hide();
}



