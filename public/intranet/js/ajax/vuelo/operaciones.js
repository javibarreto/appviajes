
$(document.body).on('change', '#sp_cedula', function() {

    var cedula = $("#sp_cedula").val()
    var route = "/obtenerViajeroInfo/" + cedula;

    $.get(route, function (data) {
        $("#nb_viajero").val(data[0].nb_viajero);
        $("#fe_nacimiento").val(data[0].fe_nacimiento);
        $("#nu_telefono").val(data[0].nu_telefono);
    })
        .fail(function () {
            alert("Seleccione un registro");
        });
});

$(document.body).on('change', '#sp_codigo', function() {

    var viaje = $("#sp_codigo").val()
    var route = "/obtenerViajeInfo/" + viaje;
    console.log(route)

    $.get(route, function (data) {
        $("#nu_plazas").val(data[0].nu_plazas);
        $("#nb_origen").val(data[0].nb_origen);
        $("#nb_destino").val(data[0].nb_destino);
        $("#nu_precio").val(data[0].nu_precio);

    })
        .fail(function () {
            alert("Seleccione un registro");
    });
});


$("#btn_vuelo_crear").on('click', function() {
    
     ocultarmensajesmodalescrear();

    $("#co_vuelo").val('');
    $("#nu_plazas").val('');
    $("#nb_origen").val('');
    $("#nb_destino").val('');
    $("#nu_precio").val('');

    $("#btnmodalaceptar_crvuelo").removeAttr('disabled');

    $('#vuelo_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crvuelo").on('click', function() {

    var cedula = $("#sp_cedula").val();
    var nombre = $("#nb_viajero").val();
    var feNacimiento = $("#fe_nacimiento").val();
    var telefono = $("#nu_telefono").val();
    var codigo =  $("#sp_codigo").val();
    var origen = $("#nb_origen").val();
    var destino = $("#nb_destino").val();
    var precio = $("#nu_precio").val();
 

    var route = "/crearVuelo";
    var token = $("#token").val();

    $("#msj-success-crvuelo").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crvuelo").fadeOut();

    $("#btnmodalaceptar_crvuelo").attr("disabled", "disabled");

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {nombre:nombre,
            cedula:cedula,
            feNacimiento:feNacimiento,
            telefono:telefono,
            codigo:codigo,
            origen:origen,
            destino:destino,
            precio: precio},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crvuelo").fadeIn();
                $("#btnmodalaceptar_crgestionvuelo").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#vuelo_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crgestionvuelo").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});




var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_vuelo').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_vuelo').dataTable( {
  
        ajax: {
            url:'/listarVuelos',
            dataSrc: ""
        },
        columns: [
          {   
              data:'id_vuelo',
              render: function (data,type,row) {
                  return '<input type="radio" checked="" name="id_vuelo" value="'+data+'" class="flat-blue">'+data+'';    
              }
          },
          {
            name: 'nu_ci',
            data: 'nu_ci'
          },
          {
              name: 'nb_viajero',
              data: 'nb_viajero'
          },
          {
              name: 'fe_nacimiento',
              data: 'fe_nacimiento'
          },
          {
            name: 'nu_telefono',
            data: 'nu_telefono'
          },
          {
            name: 'co_viaje',
            data: 'co_viaje'
          },
          {
            name: 'nb_origen',
            data: 'nb_origen'
        },
        {
            name: 'nb_destino',
            data: 'nb_destino'
        },
        {
            name: 'nu_precio',
            data: 'nu_precio'
          }
          ],
        language : lenguaje
  }); 
    
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crvuelo").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crvuelo").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-success-edvuelo").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edvuelo").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elvuelo").fadeOut();
    $("#msj-error-elvuelo").fadeOut();
 }