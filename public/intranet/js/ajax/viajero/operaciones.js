
$("#btn_gestioncorreo_crear").on('click', function() {
    

     ocultarmensajesmodalescrear();

    $("#nb_viajero").val('');
    $("#nu_cedula").val('');
    $("#fe_nacimiento").val('');
    $("#nu_telefono").val('');
    $("#tx_correo").val('');

    $("#btnmodalaceptar_crviajero").removeAttr('disabled');

    $('#viajero_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});




$("#btnmodalaceptar_crviajero").on('click', function() {

    var nombre = $("#nb_viajero").val();
    var cedula = $("#nu_cedula").val();
    var feNacimiento = $("#fe_nacimiento").val();
    var telefono = $("#nu_telefono").val();
    var correo = $("#tx_correo").val();

 

    var route = "/crearViajero";
    var token = $("#token").val();

    $("#msj-success-crviajero").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crviajero").fadeOut();

    $("#btnmodalaceptar_crviajero").attr("disabled", "disabled");

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {nombre:nombre,
            cedula:cedula,
            feNacimiento:feNacimiento,
            telefono:telefono,
            correo: correo},

        success: function (msj) {

            if (msj.mensaje == 'creado') {

                $("#msj-success-crviajero").fadeIn();
                $("#btnmodalaceptar_crgestionviajero").removeAttr('disabled');
                listar();
                setTimeout(function () {
                    $('#viajero_crear').modal('hide')
                }, 1500);
            } else {
                $("#msj-success-existente").fadeIn();
                $("#btnmodalaceptar_crgestionviajero").removeAttr('disabled');
            }
        },
        error: function (msj) {
            var errornombre = msj.responseJSON.error;

            if (errornombre != undefined) {
                $('#msj-error-crcorreo').text(msj.error);
                $("#msj-error-crcorreo").removeClass('hidden');
                $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');
            }
        }
    });
});



$("#btn_viajero_editar").on('click', function() {

     ocultarmensajesmodaleseditar();
    
    //  $("#nb_edcasa").val();
    //  $("#nb_edrepresentante").val();
    //   $("#tx_edcorreo").val();

    $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

    $('#viajero_editar').modal({
        backdrop: 'static',
        keyboard: false
    });

    
    var route = "/obtenerViajero/" + $('input:radio[name=id_viajero]:checked').val();

    $(".modal-body input").val("");

    $.get(route, function (data) {
        $("#nb_edviajero").val(data[0].nb_viajero);
        $("#nu_edcedula").val(data[0].nu_ci);
        $("#fe_ednacimiento").val(data[0].fe_nacimiento);
        $("#nu_edtelefono").val(data[0].nu_telefono);
        $("#tx_edcorreo").val(data[0].tx_correo);

    })
        .fail(function () {
            alert("error");
        });

});

$("#btnmodalaceptar_edviajero").on('click', function () {

    ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_edviajero").attr("disabled", "disabled");
    var route = "/editarViajero"

    $.post(route, {

        id: $('input:radio[name=id_viajero]:checked').val(),
        nombre: $("#nb_edviajero").val(),
        cedula: $("#nu_edcedula").val(),
        feNacimiento: $("#fe_ednacimiento").val(),
        telefono: $("#nu_edtelefono").val(),
        correo: $("#tx_edcorreo").val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            var errornombre = msj.responseJSON.nombre;

            if (errornombre != undefined) {
                $('.msj-error-edcorreo').text(msj.responseJSON.nombre);
                $('.msj-error-edcorreo').fadeIn();
                $("#btnmodalaceptar_edviajero").removeAttr('disabled');
            }
        })

        .done(function (msj) {
            if (msj.mensaje == 'actualizado') {

                $("#msj-success-edviajero").fadeIn();
                listar();
                setTimeout(function () {
                    $('#viajero_editar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_edviajero").removeAttr('disabled');
            } else {
                $("#msj-success-edexistente").fadeIn();
                $("#btnmodalaceptar_edviajero").removeAttr('disabled');
            }
        });
})


$("#btn_viajero_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminar();

   $("#tx_correo").val('');

   $("#btnmodalaceptar_crgestioncorreo").removeAttr('disabled');

   $('#viajero_eliminar').modal({
       backdrop: 'static',
       keyboard: false
   });

   var route = "/obtenerViajero/" + $('input:radio[name=id_viajero]:checked').val();

   $(".modal-body input").val("");

   $.get(route, function (data) {
        $("#nb_elviajero").val(data[0].nb_viajero);
        $("#nu_elcedula").val(data[0].nu_ci);
        $("#fe_elnacimiento").val(data[0].fe_nacimiento);
        $("#nu_eltelefono").val(data[0].nu_telefono);
        $("#tx_elcorreo").val(data[0].tx_correo);
   })
       .fail(function () {
           alert("error");
   });

});


$("#btnmodalaceptar_elviajero").on('click', function () {


     ocultarmensajesmodaleseditar();

    $("#btnmodalaceptar_elviajero").attr("disabled", "disabled");

    var route = "/eliminarViajero"

    $.post(route, {
        id: $('input:radio[name=id_viajero]:checked').val(),
        "_token": $("#token").val()
    }, function () {
        //alert( "success" );
    })
        .fail(function (msj) {
            $("#btnmodalaceptar_elviajero").removeAttr('disabled');
            // var errornombre = msj.responseJSON.nombre;

            // if (errornombre != undefined) {
            //     $('.msj-error-elcorreo').text(msj.responseJSON.nombre);
            //     $('.msj-error-elcorreo').fadeIn();
            //     $("#btnmodalaceptar_elcasa").removeAttr('disabled');
            // }
        })

        .done(function (msj) {

                $("#msj-success-elviajero").fadeIn();
                listar();
                setTimeout(function () {
                    $('#viajero_eliminar').modal('hide')
                }, 1500);
                $("#btnmodalaceptar_elviajero").removeAttr('disabled');
        });
})



var listar = function(){

    var eliminartabla_marcas = jQuery('#listar_viajero').dataTable().fnDestroy();
    var recargartabla_marcas = jQuery('#listar_viajero').dataTable( {
  
          ajax: {
              url:'/listarViajero',
              dataSrc: ""
          },
          columns: [
            {   
                data:'id_viajero',
                render: function (data,type,row) {
                    return '<input type="radio" checked="" name="id_viajero" value="'+data+'" class="flat-blue">'+data+'';    
                }
            },
            {
              name: 'nu_ci',
              data: 'nu_ci'
            },
            {
                name: 'nb_viajero',
                data: 'nb_viajero'
            },
            {
                name: 'fe_nacimiento',
                data: 'fe_nacimiento'
            },
            {
              name: 'nu_telefono',
              data: 'nu_telefono'
            },
            {
              name: 'tx_correo',
              data: 'tx_correo'
            }
            ],
          language : lenguaje
    }); 
    
  }


function ocultarmensajesmodalescrear() {

    $("#msj-success-crviajero").fadeOut();
    $("#msj-success-existente").fadeOut();
    $("#msj-error-crviajero").fadeOut();
}

function ocultarmensajesmodaleseditar() {

   $("#msj-success-edviajero").fadeOut();
    $("#msj-success-edexistente").fadeOut();
    $("#msj-error-edviajero").fadeOut();
}


function ocultarmensajesmodaleseliminar() {

    $("#msj-success-elviajero").fadeOut();
    $("#msj-error-elviajero").fadeOut();
 }