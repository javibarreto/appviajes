/*!
 * bootstrap-selectpro v1.12.4 (http://silviomoreto.github.io/bootstrap-selectpro)
 *
 * Copyright 2013-2017 bootstrap-selectpro
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-selectpro/blob/master/LICENSE)
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(root["jQuery"]);
  }
}(this, function (jQuery) {

(function ($) {
  $.fn.selectpickerpro.defaults = {
    noneSelectedText: '没有选中任何项',
    noneResultsText: '没有找到匹配项',
    countSelectedText: '选中{1}中的{0}项',
    maxOptionsText: ['超出限制 (最多选择{n}项)', '组选择超出限制(最多选择{n}组)'],
    multipleSeparator: ', ',
    selectAllText: '全选',
    deselectAllText: '取消全选'
  };
})(jQuery);


}));
