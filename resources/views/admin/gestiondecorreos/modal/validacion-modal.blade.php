{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="solicitudvacacion_crvalidacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Solicitud Vacacion</h4>
            </div>
            <div class="modal-body">

            <div id="msj-fechasigualesvalidacion-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
            <strong>La Fecha Desde y la Fecha Hasta no deben ser Iguales</strong>
            </div>

            <div id="msj-fechadesdevalidacion-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
            <strong>Debe Seleccionar La Fecha Desde para realizar la Solicitud</strong>
            </div>

            <div id="msj-fechahastavalidacion-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
            <strong>Debe Seleccionar La Fecha Hasta para realizar la Solicitud</strong>
            </div>

            <div id="msj-fechadesdemayor-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
            <strong>La Fecha Desde No puede ser mayor que la Fecha Hasta</strong>
            </div>

            <div id="msj-diassolicitados-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
            <strong>Los Dias Solicitados son Mayores a los Dias Pendientes de Vacacion </strong>
            </div>

            <div id="msj-correosupervisornovalido-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Correo del Supervisor es Incorrecto</strong>
            </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 