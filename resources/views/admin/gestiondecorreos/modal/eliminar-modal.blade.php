
{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="solicitudvacacion_eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Elimine el registro seleccionado</h4>
            </div>
            
           
            <div id="msj-success-elcorreo" class=" msj-success alert alert-success alert-dismissible" role="" >
                <strong> Correo Eliminado Correctamente</strong>
            </div>
           
            <div id="msj-error-elcorreo" class="alert alert-warning alert-dismissible" role="" >
                <strong></strong>
            </div>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

        

                 <div class="form-group">
                    <label>Correo</label>&nbsp;&nbsp;
                    <input type="mail" value="" id="tx_elcorreo" style="width:300px" disabled="">
                 </div>

               
            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_elgestioncorreo" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_elsolicitudvacacion" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}
