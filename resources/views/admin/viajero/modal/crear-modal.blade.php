{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="viajero_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos</h4>
            </div>
            <div id="msj-success-crviajero" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Viajero Agregado Correctamente.</strong>
            </div>
            <div id="msj-success-existente" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Este viajero ya existe</strong>
            </div>
            <div id="msj-error-crviajero" class="alert alert-warning alert-dismissible" role="">
                <strong></strong>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion
                    Por Favor Espere</label></b>
            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="nb_viajero">Nombre</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_viajero">
                </div>
                <div class="form-group">
                    <label for="nu_cedula">Cedula</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nu_cedula">
                </div>
                <div class="form-group">
                    <label for="fe_nacimiento">Fecha de Nacimiento</label>&nbsp;&nbsp;
                    <input class="form-control" type="date" value="" id="fe_nacimiento">
                </div>
                <div class="form-group">
                    <label for="nu_telefono">Telefono</label>&nbsp;&nbsp;
                    <input class="form-control" type="number" value="" id="nu_telefono">
                </div>
                <div class="form-group">
                    <label for="tx_correo">Correo</label>&nbsp;&nbsp;
                    <input class="form-control" type="mail" value="" id="tx_correo">
                </div>
            </div>
            <div class="modal-footer">
                <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crviajero" class="btn btn-primary"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crviajero" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

               