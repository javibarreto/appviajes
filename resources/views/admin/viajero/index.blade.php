@extends('admin.layout.admin')
@section('content')

    <!-- datatables -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- datatables -->

    <!-- Datepicker -->
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.css">

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">

    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- Datepicker Files -->

    @include('alerts.errors')

    <div id="msj-seleccionarregistroeditar-solicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Editar </strong>
    </div>

    <div id="msj-seleccionarregistroeliminar-solicitudvacacion" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Eliminar </strong>
    </div>
    

    @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Gestión de Viajeros
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Bienvenido</a></li>
            <li class="active">Gestión de Viajeros</li>
        </ol>

    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                        <div class="box-body">

                            <div class="margin">

                                <div class="btn-group">
                                    <button type="button" id="btn_gestioncorreo_crear" class="btn btn-primary"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-plus"></span> Crear
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @include('admin.viajero.modal.crear-modal')
                                </div>

                                 <div class="btn-group">
                                    <button type="button" id="btn_viajero_editar" class="btn btn-warning"
                                            data-toggle="" data-target=""><span
                                                class="glyphicon glyphicon-edit"></span> Editar
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;

                                    @include('admin.viajero.modal.editar-modal')
                                </div>

                                <div class="btn-group">
                                    <button type="button" id="btn_viajero_eliminar" class="btn btn-danger"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-minus"></span> Eliminar
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <!-- Modal -->
                                    @include('admin.viajero.modal.eliminar-modal')
                                </div>


                                <div class="btn-group">
                                    <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                                    </button>
                                </div>
                                <br><br>

                        </div><!-- /.col -->
                    </div><!-- /. row -->
                </div><!-- /. row -->
     <section class="content-header">
        <div class="row" >
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Listado de Viajeros</h3>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive" >
                   <table id="listar_viajero" class="table table-condensed table-hover" style="width:100%" cellspacing="0" >

                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cedula</th>
                            <th>Nombre</th>
                            <th>Fecha de Nacimiento</th>
                            <th>Telefono</th>
                            <th>Correo</th>
                        </tr>

                        </thead>
                    </table>
                </div><!-- /.box-body -->

            </div><!-- /.box -->

    </section>
    </section><!-- /.content -->

@endsection
@section('scripts')


    <!-- datepicker -->
    {!!Html::script('intranet/js/picker/bootstrap-datetimepicker.js')!!}
    {!!Html::script('intranet/js/picker/locales/bootstrap-datetimepicker.es.js')!!}
    {!!Html::script('intranet/css/datepicker/js/bootstrap-datepicker.js')!!}
    <!-- datepicker -->

    <!-- datatables -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- datatables -->


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js"></script>


    {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
   
    {!!Html::script('intranet/js/ajax/viajero/operaciones.js')!!}


    <script type="text/javascript">

        var lenguaje = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }


        }

        jQuery.noConflict();

        jQuery(document).ready(function () {
            listar();
        });


        
    </script>

@endsection