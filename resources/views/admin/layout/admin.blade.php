<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

        <title>Sistema de Viajes</title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {!!Html::style('intranet/css/bootstrap.min.css')!!}
        <!-- font Awesome -->
       <!-- {!!Html::style('intranet/css/font-awesome.min.css')!!} -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        {!!Html::style('intranet/css/ionicons.min.css')!!}
        <!-- Morris chart -->
       <!-- {!!Html::style('intranet/css/morris/morris.css')!!} -->

        <!-- {!!Html::style('intranet/css/jvectormap/jquery-jvectormap-1.2.2.css')!!} -->

         <!-- fullCalendar -->
      <!--  {!!Html::style('intranet/css/fullcalendar/fullcalendar.css')!!} -->
        <!-- Daterange picker -->
       <!-- {!!Html::style('intranet/css/daterangepicker/daterangepicker-bs3.css')!!}-->
        <!-- bootstrap wysihtml5 - text editor -->
      <!--  {!!Html::style('intranet/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}-->
        <!-- Theme style -->
        {!!Html::style('intranet/css/AdminLTE.css')!!}

        <!-- APLICA EL ESTILO AL SELECT -->
      <!--   {!!Html::style('intranet/css/select/css/bootstrap-select.css')!!} -->

      <!--   {!!Html::style('intranet/css/producto/css/bootstrap-selectpro.css')!!} -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="/" class="">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                     <!--<p align="right-side">_________________________________________________________</p>-->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->

                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                </a>
                <div class="navbar-left">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class=""></i>
                                <span>Sistema de Viajes<i class=""></i></span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>

                                <span>Usuario <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
 
                                        <?php

                                            if($fotopersona!=""){

                                                echo '<img src="data:image/jpeg;base64,'.base64_encode($fotopersona).'"  class="img-circle" alt="User Image"/> ';
                                            }
                                            else{

                                                echo '<img src="img/fotoblanco.png"  class="img-circle" alt="User Image"/> ';

                                            }
                                        ?>
                                   
                                    <p>
                                        Usuario -  {!!ucwords(Auth::user()->co_usuario)!!} {{$personaaccesa}}
                                        <small></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="">
                                        <a  href="{!!URL::to('/logout')!!}" class="btn btn-primary  btn-flat">Cerrar Sesion</a> 

                            
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    
                        <div class="user-panel">
                        <div class="pull-left image">

                        <?php

                            if($fotopersona!=""){

                                echo '<img src="data:image/jpeg;base64,'.base64_encode($fotopersona).'"  class="img-circle" alt="User Image"/> ';
                            }
                            else{

                                echo '<img src="img/fotoblanco.png"  class="img-circle" alt="User Image"/> ';
                            }

                        ?>

                        </div>
                        <div class="pull-left info">
                            <p>Bienvenido, {!!ucwords(Auth::user()->co_usuario)!!} {{$personaaccesa}}
                            </p>
                            
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="{!!URL::to('gestioncorreos')!!}"<i class="fa fa-tasks"></i>&nbsp;&nbsp;Gestión de Correos</a>
                                </li>
                                <li>
                            <a href="{!!URL::to('enviocorreos')!!}"<i class="fa fa-tasks"></i>&nbsp;&nbsp;Envío de Correos</a>
                            
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i>
                                    <span>Mantenimiento</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="{!!URL::to('casa')!!}"><i class="fa fa-tasks"></i>Casa</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('competidor')!!}"><i class="fa fa-tasks"></i>Competidores</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('viajero')!!}"><i class="fa fa-tasks"></i>Viajeros</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('viajes')!!}"><i class="fa fa-tasks"></i>Viajes</a>
                                </li>
                            </ul>
                        <li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i>
                                    <span>Procesos</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="{!!URL::to('vuelos')!!}"><i class="fa fa-tasks"></i>Vuelos</a>
                                </li>
                                <li>
                                    <a href="{!!URL::to('torneo')!!}"><i class="fa fa-tasks"></i>Torneos</a>
                                </li>
                            </ul>
                        <li>
                    </ul>    
             </section>
                <!-- /.sidebar -->
            </aside>

            @include('admin.marcarasistencia.modal.crear-modal')
            @include('admin.marcarasistencia.modal.validacion-modal')

            <!-- Right side column. Contains the navbar and content of the page -->
            
           <div id="principalPanel">
                <aside class="right-side">
                    @yield('content') 
                </aside><!-- /.right-side -->
           </div>

        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        

        
        <!-- jQuery UI 1.10.3 -->
        {!!Html::script('intranet/js/jquery-ui-1.10.3.min.js')!!}
        <!-- Bootstrap -->
        {!!Html::script('intranet/js/bootstrap.min.js')!!}
        <!-- Morris.js charts -->
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
        <!-- {!!Html::script('intranet/js/plugins/morris/morris.min.js')!!} -->
         <!-- Sparkline -->
        <!-- {!!Html::script('intranet/js/plugins/sparkline/jquery.sparkline.min.js')!!} -->
        <!-- jvectormap -->
        <!-- {!!Html::script('intranet/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}  -->
        <!-- {!!Html::script('intranet/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!} -->
         <!-- fullCalendar -->
        <!--  {!!Html::script('intranet/js/plugins/fullcalendar/fullcalendar.min.js')!!} -->
          <!-- jQuery Knob Chart -->
        <!--  {!!Html::script('intranet/js/plugins/jqueryKnob/jquery.knob.js')!!} -->
        <!-- daterangepicker -->
       <!-- {!!Html::script('intranet/js/plugins/daterangepicker/daterangepicker.js')!!} -->
        <!-- Bootstrap WYSIHTML5 -->
      <!--  {!!Html::script('intranet/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!} -->
        <!-- iCheck -->
        {!!Html::script('intranet/js/plugins/iCheck/icheck.min.js')!!}
        <!-- AdminLTE App -->
        {!!Html::script('intranet/js/AdminLTE/app.js')!!}
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        {!!Html::script('intranet/js/AdminLTE/dashboard.js')!!}

          <!-- APLICA EL ESTILO AL SELECT -->
    <!--    {!!Html::script('intranet/css/select/js/bootstrap-select.js')!!} -->

     <!--   {!!Html::script('intranet/css/producto/js/bootstrap-selectpro.js')!!} -->


         
         @section('scripts')

           
                {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
               

              


        <script type="text/javascript">


                 
               

        </script>
        
        
        

        @show    
    </body>
</html>