<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

	<table style="padding:10px; margin:0 auto; border-collapse: collapse;">
    <tr>    
        <td style="background-color: #ecf0f1">
                <div style="color: #34495e; margin: 4% 10% 2%;font-family: sans-serif;">
                    <h2 style="color: #3498db; margin: 0 0 7px; text-align: center">Solicitud Editada Zincron Web</h2>
                </div>

                <div style="width:500px;height:200px">  
                    
                    <ul style="color:#000000 font-size: 15px;  margin: 10px 0">
                        <li>{!!$textonumerosolicitud!!} <b>{!!$numerosolicitud!!}</b></li>
                        
                       @if(($tiposolicitud == "dia")||($tiposolicitud == "vacacion"))
                          <li>{!!$textofechadesdesolicitud!!} <b>{!!$fechadesdesolicitud!!}</b></li>
                          <li>{!!$textofechahastasolicitud!!} <b>{!!$fechahastasolicitud!!}</b></li>
                            
                        @endif

                        @if($tiposolicitud == "hora")
                           <li>{!!$textofechadesdesolicitud!!} <b>{!!$fechadesdesolicitud!!}</b></li>
                        @endif
                      

                        <li>{!!$textoconcepto!!} <b>{!!$descripcionconcepto!!}</b></li>
                        <li>{!!$textodescripcion!!} <b>{!!$descripcion!!}</b></li>
                        <li>{!!$textoelaboradapor!!} <b>{!!$generadapor!!}</b></li>
                    </ul>

                    <ul style="color:#000000 font-size: 15px;  margin: 10px 0">
                    	<b><label>IMPORTANTE!!!!!!</label></b>
						<br>
                        <li>{!!$textoaprobadarechazada!!}</li>
                    </ul>


                </div>
        </td>
    </tr>
    </table>

    <div style="width: 100%; text-align: left">
            <ul style="font-size: 20px;  margin: 10px 0 color:#000000">
                 <b><label>Este e-mail se ha generado por un sistema automatico.Por favor,
                 no responda a este e-mail directamente.</label></b>
            </ul>
    </div>  
	
</body>
</html>
