<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>

    <p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Buenas<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Envío la información General y lo que necesitas para iniciar.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Gracias por comunicarse para saber de nuestros servicios en Danke du<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Le ofrecemos la asesoría y&nbsp;<b><u>le buscamos un contrato por Au Pair, Voluntariado o un Contrato de Trabajo y Estudio Gratis por Ausbildung en Alemania</u></b>.&nbsp; Para aplicar debe tener al menos un certificado de idioma Alemán&nbsp;A1.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">CÓMO INICIAR?.&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. LLenar la Planilla<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp; Enviar el Curriculum Vitae en Español y Alemán. (Si no tienen quien traduzca, lo mandamos a traducir&nbsp;nosotros son 20$)<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Pagar el anticipo de la Asesoría de 150 Euros o equivalente en dólares (Pueden pagar por parte o fraccionado. Pagar por Western Union). Transferencia&nbsp; Bancaria para Alemania, Venezuela, Ecuador y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Puerto Rico (Informar para pasarle la cuenta)<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b>Las características de los programas&nbsp; son (DEBE SELECCIONAR UNO SOLO):</b><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;-<b>&nbsp;Au Pair&nbsp;</b>&nbsp;de 1 año extendible a 1 año y medio. Remunerado (le pagan un sueldo), incluye vivienda, transporte, alimentación, estudio de alemán&nbsp;&nbsp; + póliza&nbsp;de seguro de 30.000 Euros que eso lo paga ellos,&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; no Ud. Aquí&nbsp;se tiene una Visa inmediata de 1 año.&nbsp;&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;-<b>&nbsp;Voluntariado</b>&nbsp; de 1 año extendible a 1 año y medio. Remunerado, puede ser con vivienda si es del área social&nbsp; + póliza&nbsp;de seguro de 30.000 Euros que eso lo paga ellos, no Ud. Aquí&nbsp;se tiene una Visa&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; inmediata de 1 año.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;-<b>&nbsp;Ausbildung:</b><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Podrá pedir una Visa por 3 años&nbsp;+ 1 año adicional. Después allá puede cambiarla por una visa de Estudio o Trabajo y quedarse con Residencia en Alemania. Incluye:<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Contrato de Trabajo en una empresa del área&nbsp; relacionada a lo que estudió en el Ausbildung<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Le pagan los ingresos con incremento por cada año. Mínimo 700 Euros mas o menos, máximo 2500 Euros&nbsp; depende de la empresa<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Póliza de Seguro con cobertura de 30.000 Euros (Eso lo paga la empresa)<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- El horario de trabajo es 2 días estudia y 3 Trabaja o puede ser trabaja medio turno y medio turno estudia o&nbsp; &nbsp;dos meses estudia y dos trabaja. (Depende de la empresa)<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Condición: Debe estudiar porque es un Contrato Dual<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;<b>- Médicos</b>: En el caso de los Médicos es particular, hay opciones sin conocer el idioma Alemán o que tengan el Inglés, hay voluntariados en el área de salud, hay visas para especialización y cursos de&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; alemán según la zona de Alemania.&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Favor indicar si son Médicos Graduados para atender a cada persona en forma individual.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">De acuerdo a lo solicitado anexo a la presente remito los formularios para poder avanzar en los requerimientos de su programa.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Por favor en casos específicos, lo vamos tramitando a medida que se haga la entrega de los&nbsp;documentos&nbsp;correspondientes anexos debidamente respondidos.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Voy atendiendo cada caso en forma personalizada.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">A partir de que contraten con nosotros, nos encargamos de ubicar&nbsp; la Familia Anfitriona o empresa para el Ausbildung o voluntariado y lugar de vivir, ese es nuestro trabajo. Nosotros enviamos las solicitudes y hacemos el diseño de tu Currículum, más su expediente para la Visa, Ud. debe&nbsp; sólo enviar los documentos que solicite escaneado hasta que le corresponda la entrevista de trabajo&nbsp; directamente.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Todas las dudas las atendemos sin falta, hasta que esté en Alemania.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 10.5pt; font-family: Arial, sans-serif; color: rgb(77, 81, 86);">Hay que hacer un cronograma de Viaje para coordinar los tiempos.</span><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Toda la INVERSIÓN de asesoría, pasaje, visa y examen de alemán lo recupera de 2 a 3 meses de trabajo en Alemania.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Los que tienen familiares, se&nbsp; realiza una asesoría por caso, para los esposos e hijos, a través de una Visa de Integración y le explicamos el caso de los niños con colegio, etc...<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Eso implica una inversión, recuerden que Alemania<b>&nbsp;no quiere cargas</b>&nbsp;<b>sociales&nbsp;</b>sino&nbsp;<b><u>gente que estudie y trabaje en el país</u></b>&nbsp;y que genere impuestos, lejos de la mentalidad de Latina.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b><span style="color: blue;">Que el dinero no sea un límite para su VIDA.</span></b><b><span style="font-family: &quot;Segoe UI Emoji&quot;, sans-serif; color: blue;">☺</span></b><b><span style="color: blue;">&nbsp;HAY QUE SER INTELIGENTE.</span></b><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Siempre a la Orden&nbsp;&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">ANEXOS:<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;- Planilla de Datos básicos para llenar<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;- Metodología de estudio autodidacta del alemán<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp; &nbsp; l- Presupuesto de asesoría si va a optar por alguno de los planes<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Agradecemos revisar el material enviado y llenar el formulario enviándonos un correo indicando su caso particular, de manera de programar una cita telefónica para aclarar las dudas con relación a su caso.<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b><span style="color: yellow; background: red;">NOTA: SE LES AGRADECE ENVIAR SUS&nbsp;TELÉFONOS PARA INCLUIRLOS EN EL GRUPO DE WHATSAPP PROYECTO ALEMANIA -gratis enviamos información de Estudios, Libros, YouTube, profesores de Alemán, Clases de Alemán por Internet, etc.</span></b><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b><u><span style="font-family: Arial, sans-serif;">FORMA DE PAGO:</span></u></b><span style="color: rgb(80, 0, 80);"><o:p></o:p></span></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="font-size: 9pt; font-family: Verdana, sans-serif;">Para iniciar los trámites correspondientes se requiere el abono del 50% de los honorarios, pudiendo el monto restante pagarlo por cuotas fijadas por las partes durante todo el tiempo que dure el trámite, en tal sentido, dejamos a su disposición nuestra cuenta bancaria:</span><span style="color: rgb(80, 0, 80);">&nbsp;&nbsp;<o:p></o:p></span></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Puede hacer:<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">- Envío&nbsp;en su moneda local por&nbsp; Western Union hacia Alemania o Ecuador<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">- Transferencia Bancaria para Alemania, Venezuela, Ecuador y Puerto Rico (Informar por cual forma de pago le conieve)&nbsp; &nbsp;&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b>PAGO POR</b>&nbsp;<b>ECUADOR</b>&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Aura Rodriguez CI 0962316378&nbsp;<b><u>Banco Pichincha.</u></b>&nbsp;Cuenta Corriente&nbsp; 2203993207&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><o:p>&nbsp;</o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b>PAGO POR</b>&nbsp;<b>VENEZUELA</b><o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Aura Rodriguez CI 6.305.535&nbsp;<b><u>&nbsp;Banco de Venezuela.</u></b>&nbsp;Cuenta Corriente&nbsp; 0102 0667 7500 0008 6956<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;">Pago Móvil:&nbsp; Teléfono 0424.8793335 - CI 6.305 535 - Bco Venezuela&nbsp;<o:p></o:p></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><span style="color: rgb(80, 0, 80);">&nbsp;</span></p><p style="font-family: &quot;Times New Roman&quot;; font-size: medium;"></p><p class="MsoNormal" style="font-family: &quot;Times New Roman&quot;; font-size: medium;"><b>PAGO POR ALEMANIA:</b><b><span style="color: rgb(80, 0, 80);">&nbsp;&nbsp;</span></b><b><u><span style="color: blue;">CON SU MONEDA LOCAL NO HAY PROBLEMA&nbsp;</span></u></b></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222;mso-no-proof:yes"><!--[if gte vml 1]><v:shape
 id="_x0000_i1025" type="#_x0000_t75" style='width:133.5pt;height:132.75pt;
 visibility:visible'>
 <v:imagedata src="https://drive.google.com/uc?id=1mxty0cS1KMb-FljYbo6vFPYlAovWeEGO&amp;export=download"></v:imagedata>
</v:shape><![endif]--></span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222"><o:p></o:p></span></p><p class="MsoNormal"></p>
    <p class="MsoNormal"><b><u><span
                    style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue"><br></span></u></b></p>
    <img src="img/fotos/cuenta.png" class="" alt="cuenta"/>
    </p>
    <p class="MsoNormal"><b><u><span
                    style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue"><br></span></u></b></p>
    <img src="img/fotos/presentacion.png" class="" alt="presentacion"/>
    </p>
    <p><i><span style="font-family: Tahoma, sans-serif; border: 1pt none windowtext; padding: 0cm;">Esta comunicación
                contiene
                información que es confidencial y también puede contener información
                privilegiada. Es para uso exclusivo del destinatario/s. Si usted no es el
                destinatario/s tenga en cuenta que cualquier distribución, copia o uso de esta
                comunicación o la información que contiene está estrictamente prohibida. Si
                usted ha recibido esta comunicación por error por favor notifíquese por correo
                electrónico o por teléfono.</span></i>
        <o:p></o:p>
    </p>
    <p><span style="font-style:inherit;font-variant:inherit;font-weight:inherit;
font-stretch:inherit;line-height:inherit;color:inherit"><span style="font-style:
inherit;font-variant:inherit;font-weight:inherit;font-stretch:inherit;
line-height:inherit;color:inherit"><i><span style="font-style:inherit;
font-variant:inherit;font-weight:inherit;font-stretch:inherit;line-height:inherit;
color:inherit"><span lang="DE"
                            style="font-family: Tahoma, sans-serif; border: 1pt none windowtext; padding: 0cm;">Diese
                            E-Mail enthält vertrauliche und/oder rechtlich
                            geschützte Informationen.Wenn Sie nicht der richtige Adressat sind, oder diese
                            E-Mail irrtümlich erhalten haben, informieren Sie bitte den Absender und
                            löschen Sie diese Mail. Das unerlaubte Kopieren sowie die unbefugte Weitergabe
                            dieser E-Mail und der darin enthaltenen Informationen sind nicht
                            gestattet.</span></span></i></span>
            <o:p></o:p>
        </span></p>
    <p><span style="font-style:inherit;font-variant:inherit;font-weight:inherit;
font-stretch:inherit;line-height:inherit;color:inherit"><i><span style="font-style:inherit;
font-variant:inherit;font-weight:inherit;font-stretch:inherit;line-height:inherit;
color:inherit"><span lang="DE" style="font-family: Tahoma, sans-serif; border: 1pt none windowtext; padding: 0cm;"><br>
                        <!--[if !supportLineBreakNewLine]--><br>
                        <!--[endif]-->
                    </span></span></i>
            <o:p></o:p>
        </span></p>
    <p><span style="font-style:inherit;font-variant:inherit;font-weight:inherit;
font-stretch:inherit;line-height:inherit;color:inherit"><i><span style="font-style:inherit;
font-variant:inherit;font-weight:inherit;font-stretch:inherit;line-height:inherit;
color:inherit"><span lang="DE" style="font-family: Tahoma, sans-serif; border: 1pt none windowtext; padding: 0cm;"><br>
                        <!--[if !supportLineBreakNewLine]--><br>
                        <!--[endif]-->
                    </span></span></i>
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">Saludos Cordiales</span></i><span
            style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">&nbsp;</span></p>
    <p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">&nbsp;</span></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Aura Liz Rodríguez
                    Mola</span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Economista&nbsp;</span></i></b><span
            style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Telef. +58
                    424.8793335&nbsp;&nbsp;</span></i></b><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Consultor y Asesor del
                    Proyecto Alemania&nbsp;&nbsp;</span></i></b><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal">

    </p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">DANKE D.U.</span></i></b></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><br></span></i></b></p>
     <p class="MsoNormal"><b><u><span
                    style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue"><br></span></u></b></p>
    <img src="img/fotos/logoDanke.png" class="" alt="cuenta"/>
    </p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><br></span></i></b></p>
    <p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><br></span></i></b><span
            style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">
            <o:p></o:p>
        </span></p>
    <p class="MsoNormal"><b><u><span
                    style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue"><br></span></u></b>
        <o:p></o:p>
    </p>

  </body>

</html>