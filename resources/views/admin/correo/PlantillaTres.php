<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>

<p class="MsoNormal" style="margin-bottom:12.0pt">Este es el correo de las CONFERENCIAS QUE SON CADA 15 DIAS Y
QUE CAMBIO LOS DATOS<o:p></o:p></p><p class="MsoNormal">Buenas&nbsp;<o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">Gracias
por formar parte de&nbsp;<b><span style="font-size:13.5pt;mso-fareast-font-family:
&quot;Times New Roman&quot;">Proyecto Alemania</span></b>&nbsp;y su interés por&nbsp; saber de nuestros servicios
en&nbsp;<b>Danke D.U.</b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">EL
SABADO&nbsp;HARE OTRO WEBINAR<o:p></o:p></p><p class="MsoNormal"><b><i><u><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:red">CONFERENCIA SOBRE COMO ESTUDIAR, TRABAJAR Y VIVIR EN ALEMANIA</span></u></i></b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
 o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
 stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="_x0000_i1026" type="#_x0000_t75" style='width:24pt;
 height:24pt;visibility:visible'>
 <v:imagedata src="cid:ii_kf7da1ga1"/>
</v:shape><![endif]--><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">Le
ofrecemos la asesoría y&nbsp;<b><u>le buscamos un contrato por Au Pair,
Voluntariado o un Contrato de Trabajo y Estudio Gratis por Ausbildung en
Alemania</u></b>.&nbsp; Para aplicar debe tener al menos un certificado de
idioma Alemán&nbsp;A1.<o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">OFRECEMOS
:<o:p></o:p></p><p class="MsoNormal"><b><u><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:red">Asesoría personalizada por Zoom de 1 hora, Costo: 10$&nbsp;</span></u></b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">&nbsp;Pago
por Paypal,&nbsp; Wester Union o Transferencia&nbsp; Bancaria para Alemania.<o:p></o:p></p><p class="MsoNormal">Explicamos:<o:p></o:p></p><p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.Cómo
resolver su caso personal en forma personalizada.<o:p></o:p></p><p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.
Si está conforme con contratar los servicios,&nbsp; nos encargamos de
ubicar&nbsp; la Familia Anfitriona o empresa para el Ausbildung o voluntariado
y lugar de vivir, ese es nuestro trabajo.&nbsp;<o:p></o:p></p><p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.
Nosotros enviamos las solicitudes&nbsp;<o:p></o:p></p><p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.&nbsp;Hacemos
el diseño de tu Currículum<o:p></o:p></p><p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.&nbsp;Asesoría
para su expediente para la Visa.<o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">Todas
las dudas las atendemos sin falta, hasta que esté en Alemania.<o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:red">Hay que hacer un
cronograma de Viaje para coordinar los tiempos.</span><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:blue">Que el dinero no sea un límite para su VIDA.</span></b><b><span style="font-family:&quot;Segoe UI Emoji&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Segoe UI Emoji&quot;;color:blue">☺</span></b><b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue">&nbsp;HAY QUE SER
INTELIGENTE.</span></b><o:p></o:p></p><p class="MsoNormal">Siempre
a la Orden&nbsp;&nbsp;<o:p></o:p></p><p class="MsoNormal">&nbsp;<o:p></o:p></p><p class="MsoNormal"><b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:yellow;background:red">NOTA: SE LES AGRADECE ENVIAR SUS&nbsp;TELÉFONOS
PARA INCLUIRLOS EN EL GRUPO DE WHATSAPP PROYECTO ALEMANIA -gratis enviamos
información de Estudios, Libros, YouTube, profesores de Alemán, Clases de
Alemán por Internet, etc.</span></b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;">Saludos Cordiales</span></i><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Aura Liz Rodríguez Mola</span></i></b><o:p></o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Economista&nbsp;</span></i></b><o:p></o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Telef. +58 424.8793335 / Whatsapp +593
962879189&nbsp;</span></i></b><o:p></o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">Consultor y Asesor del Proyecto
Alemania&nbsp;&nbsp;</span></i></b><o:p></o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;">DANKE D.U.</span></i></b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-no-proof:yes"><!--[if gte vml 1]><v:shape id="_x0000_i1025"
 type="#_x0000_t75" style='width:133.5pt;height:132.75pt;visibility:visible'>
 <v:imagedata src="https://drive.google.com/uc?id=1mxty0cS1KMb-FljYbo6vFPYlAovWeEGO&amp;export=download"/>
</v:shape><![endif]--></span></i></b><o:p></o:p></p><p class="MsoNormal"><o:p>&nbsp;</o:p></p><p class="MsoNormal">



  </body>

</html>