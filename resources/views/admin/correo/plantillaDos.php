<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>

<p class="MsoNormal">Buenas<o:p></o:p></p>

<p class="MsoNormal">&nbsp;<o:p></o:p></p>

<p class="MsoNormal">Gracias
por comunicarse para saber de nuestros servicios en Danke du<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b>Asesoría:</b> para postularse como<u> <b>Médicos&nbsp;en Alemania&nbsp;</b></u><o:p></o:p></p>

<p class="MsoNormal"><b>Documentos
Iniciales:&nbsp;</b>
&nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal">1.&nbsp;
LLenar la Planilla<o:p></o:p></p>

<p class="MsoNormal">2.&nbsp;
Enviar el Curriculum Vitae en Español y Alemán. (Si no tienen quien traduzca,
lo mandamos a traducir&nbsp;nosotros son 20$)<o:p></o:p></p>

<p class="MsoNormal">&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><b>Inversión por Asesoría:&nbsp;</b><o:p></o:p></p>

<p class="MsoNormal"><b><i><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:blue">Costo total: 300&nbsp;€.&nbsp;</span></i></b> &nbsp; <span style="color:red">(50% Para iniciar proceso)&nbsp;</span>&nbsp;<b><i><span style="color:blue">&nbsp;&nbsp;150€&nbsp;&nbsp;Euros o equivalente en dóla<u>res</u></span></i></b><o:p></o:p></p>

<p class="MsoNormal"><b>Forma de Pago:&nbsp;</b><b><i><u><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:blue"> Western Union o Transferencia&nbsp; Bancaria
para Alemania, Venezuela, Ecuador y Puerto Rico (Informar para pasarle la
cuenta)</span></u></i></b><o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal">Aquí&nbsp;te
remito la información de lo conversado, en el caso de los&nbsp;Médicos&nbsp;o
Estudiantes de Medicina.<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b>-&nbsp;Médicos</b>: En el caso de los&nbsp;Médicos&nbsp;es
particular, hay opciones sin conocer el idioma Alemán o que tengan el Inglés,
hay visas para especialización y cursos de alemán según la zona de Alemania.
Los&nbsp;médicos&nbsp;son recibidos en Alemania en algunas localidades y
comienzan a estudiar el idioma, mientras le dan trabajos como internistas&nbsp;
mientras terminan de homologar sus documentos y sacar el nivel de Alemán B2 y
el Certificado de Idioma para Médicos.&nbsp;<o:p></o:p></p>

<p class="MsoNormal">A
los médicos se les reconoce:<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.
Todos sus años de Servicio&nbsp;<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.
No tiene límite de edad&nbsp; &nbsp;<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.
Pueden viajar con su familia de una vez, se solicita la Visa con grupo
familiar&nbsp;<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.
Sus ingresos son en función al Tabulador de Sueldos para Médicos (Mayor en
función a años de Experiencia y Especialidades o títulos adicionales)<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.
El tabulador de sueldos de Médicos es igual en cualquier región de Alemania.<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.
Les pueden pagar el curso de Alemán mientras trabajan.<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.
Pueden trabajar mientras homologan el título<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.
Le dan crédito para casa y vehículo inmediato, ya que tienen contrato de
trabajo asegurado<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9.
Pueden solicitar la Visa Azul de residencia altamente cualificada.<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.
Les dan capacitación constante&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
 o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
 stroked="f">
 <v:stroke joinstyle="miter"></v:stroke>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"></v:f>
  <v:f eqn="sum @0 1 0"></v:f>
  <v:f eqn="sum 0 0 @1"></v:f>
  <v:f eqn="prod @2 1 2"></v:f>
  <v:f eqn="prod @3 21600 pixelWidth"></v:f>
  <v:f eqn="prod @3 21600 pixelHeight"></v:f>
  <v:f eqn="sum @0 0 1"></v:f>
  <v:f eqn="prod @6 1 2"></v:f>
  <v:f eqn="prod @7 21600 pixelWidth"></v:f>
  <v:f eqn="sum @8 21600 0"></v:f>
  <v:f eqn="prod @7 21600 pixelHeight"></v:f>
  <v:f eqn="sum @10 21600 0"></v:f>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"></v:path>
 <o:lock v:ext="edit" aspectratio="t"></o:lock>
</v:shapetype><v:shape id="_x0000_i1028" type="#_x0000_t75" style='width:24pt;
 height:24pt;visibility:visible'>
 <v:imagedata src="cid:ii_kdj3wbyt0"></v:imagedata>
</v:shape><![endif]-->&nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal">Nota:
Otra alternativa para los que están en cero con el idioma o están en el área de
salud que no son&nbsp;médicos, es primero un Voluntariado en un área&nbsp; de
Salud&nbsp; sólo como trampolín&nbsp;para la Visa y una vez estando en Alemania
solicitar un cargo en un Hospital mientras homologan.&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b><u><span style="font-size:13.5pt;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:red">HOMOLOGACIONES DE TÍTULOS PROFESIONALES:</span></u></b><span style="font-size:13.5pt;mso-fareast-font-family:&quot;Times New Roman&quot;;color:red">&nbsp;</span><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:red">&nbsp;</span><o:p></o:p></p>

<p class="MsoNormal" style="vertical-align:baseline"><span style="font-family:
&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;"><span style="color:inherit;font-size:inherit;font-style:inherit;font-variant-ligatures:inherit;
font-variant-caps:inherit;font-weight:inherit">De conformidad a lo solicitado,
en DankeD.U.&nbsp;</span></span>podemos<span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;"><span style="color:inherit;font-size:inherit;font-style:inherit;font-variant-ligatures:inherit;
font-variant-caps:inherit;font-weight:inherit">&nbsp;tramitar su homologación
en Alemania, conformar su curriculum y carta de presentación.</span></span><o:p></o:p></p>

<p class="MsoNormal" style="vertical-align:baseline"><b>Asesoría:&nbsp;</b><span style="font-family: inherit, serif;"><span style="font-style:inherit;font-variant-ligatures:inherit;
font-variant-caps:inherit;font-weight:inherit">&nbsp;comprende el apoyo&nbsp; durante
su aplicación a distintos tipos de empresas.&nbsp;</span></span><o:p></o:p></p>

<p class="MsoNormal" style="vertical-align:baseline"><b>Documentos
Iniciales:</b><o:p></o:p></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family: inherit, serif;">Fotocopia de su pasaporte o DNI.</span><span style="font-family:
&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Dirección
actual.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Dirección
del lugar de trabajo en Alemania o ciudad donde le gustaría vivir.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Curriculum
en Alemán<o:p></o:p></span></p>

<p class="MsoNormal" style="vertical-align:baseline"><span style="font-family:
&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;"><span style="color:inherit;font-size:inherit;font-style:inherit;font-variant-ligatures:inherit;
font-variant-caps:inherit;font-weight:inherit">Nuestra Asesoría incluye:&nbsp;&nbsp;</span></span><br>
<!--[if !supportLineBreakNewLine]--><br>
<!--[endif]--><o:p></o:p></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Revisión
de carta de presentación en Alemán y traducción en caso de requerirse.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Ajuste
del currículum al modelo alemán.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:47.25pt;text-indent:-18.0pt;mso-list:l1 level1 lfo2;tab-stops:list 36.0pt;
vertical-align:baseline"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:11.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp; </span></span><!--[endif]--><span style="font-family:&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;">Asesoría
y mejora para las entrevistas laborales.<o:p></o:p></span></p>

<p class="MsoNormal" style="vertical-align:baseline"><b><u><span style="font-size: 9pt; font-family: Verdana, sans-serif;">Inversión por HOMOLOGACIÓN:&nbsp;</span></u></b><b><i><u><span style="font-size:9.0pt;font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:blue">Asesoría, apoyo y gestión durante todo el proceso
del&nbsp;Anerkannte hasta su profesionalización:&nbsp;</span></u></i></b><o:p></o:p></p>

<p class="MsoNormal" style="vertical-align:baseline"><b><i><u><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue">Médicos y
Odontólogos :&nbsp; 680&nbsp; €.&nbsp;</span></u></i></b><b><i><u><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:red"> &nbsp; (50% Para
iniciar proceso)</span></u></i></b><o:p></o:p></p>

<p class="MsoNormal" style="vertical-align:baseline"><span style="font-family:
&quot;inherit&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

<p class="MsoNormal"><b>Tiempo:</b>&nbsp;La aprobación de la
homologación dura aproximadamente 6 meses a 1 año por lo menos en el caso de
los médicos.&nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b>COSTOS
DEL VIAJE ESTIMADO:</b><o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
Pasaje. Depende del país origen (Toman un vuelo hasta&nbsp; Madrid (600 € ),
luego toman un vuelo con una línea Europea hacia Alemania (40 € ).<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Costo del Examen de Alemán&nbsp;para la certificación (más o menos 90 € )<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
Costo de la Visa&nbsp; (No se cuanto está en cada país aprox. 75 € )<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
Llevarte en mano por lo menos 2000 € por persona adulta&nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- La
Asesoría inicial son 300 €&nbsp; ( Pagando&nbsp; 50% para iniciar&nbsp; y&nbsp;
van entregando documentos&nbsp; &nbsp;avanzando en el proceso)<o:p></o:p></p>

<p class="MsoNormal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
Hay que hacer un cronograma de Viaje para coordinar los tiempos. <o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal">La
Asesoría incluye: documentación y presentación de carpetas tanto para la visa
como para postularse en el lugar de trabajo y apoyarlos en Alemania al llegar.
Incluso se puede buscar las alternativas de vivienda, cuentas bancarias,
transporte, colegio etc..<o:p></o:p></p>

<p class="MsoNormal"><b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#5F6368">&nbsp;</span></b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#4D5156">&nbsp;</span><o:p></o:p></p>

<p class="MsoNormal"><b><u><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:red">ES UNA INVERSIÓN, SACAR LA
CUENTA: EL PRIMER&nbsp; MES DE TRABAJO EN ALEMANIA RECUPERAN LA
INVERSIÓN.&nbsp;</span></u></b><o:p></o:p></p>

<p class="MsoNormal"><span style="font-size: 10.5pt; font-family: Arial, sans-serif;">&nbsp;</span><o:p></o:p></p>

<p class="MsoNormal">&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><b><i><span style="font-size:13.5pt;font-family:&quot;Georgia&quot;,serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:red">Que el dinero no sea un
límite para su VIDA.</span></i></b><b><i><span style="font-size:13.5pt;
font-family:&quot;Segoe UI Emoji&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Segoe UI Emoji&quot;;color:red">☺</span></i></b><b><i><span style="font-size:13.5pt;font-family:&quot;Georgia&quot;,serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:red">&nbsp;HAY QUE SER INTELIGENTE.</span></i></b><o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b><u><span style="font-family: Arial, sans-serif;">FORMA DE PAGO:</span></u></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#500050"><o:p></o:p></span></p>

<p class="MsoNormal"><span style="font-size: 9pt; font-family: Verdana, sans-serif;">&nbsp;</span><o:p></o:p></p>

<p class="MsoNormal">1.
Envío&nbsp;en su moneda local por&nbsp; Western Union hacia Alemania o Ecuador<o:p></o:p></p>

<p class="MsoNormal">2.
Transferencia Bancaria para Alemania, Venezuela, Ecuador y Puerto Rico
(Informar por cual forma de pago le conieve)&nbsp; &nbsp;&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b>PAGO POR</b>&nbsp;<b>ECUADOR</b>&nbsp;<o:p></o:p></p>

<p class="MsoNormal">Aura Rodriguez CI 0962316378&nbsp;<b><u>Banco Pichincha.</u></b>&nbsp;Cuenta
Corriente&nbsp; 2203993207&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal"><b>PAGO POR</b>&nbsp;<b>VENEZUELA</b><o:p></o:p></p>

<p class="MsoNormal">Aura Rodriguez CI 6.305.535&nbsp;<b><u>&nbsp;Banco de Venezuela.</u></b>&nbsp;Cuenta
Corriente&nbsp; 0102 0667 7500 0008 6956<o:p></o:p></p>

<p class="MsoNormal">Pago Móvil:&nbsp; Teléfono 0424.8793335 - CI 6.305 535 - Bco
Venezuela&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#500050">&nbsp;</span></p>

<p class="MsoNormal"><b>PAGO POR ALEMANIA:</b><b><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#500050">&nbsp;&nbsp;</span></b><o:p></o:p></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#500050;mso-no-proof:yes"><!--[if gte vml 1]><v:shape id="_x0000_i1027"
 type="#_x0000_t75" style='width:203.25pt;height:181.5pt;visibility:visible'>
 <v:imagedata src="cid:ii_khs9xuea0"></v:imagedata>
</v:shape><![endif]--></span><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#500050"><o:p></o:p></span></p>

<p class="MsoNormal">&nbsp; CON SU MONEDA LOCAL:<span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#500050">&nbsp;</span><o:p></o:p></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#500050;mso-no-proof:yes"><!--[if gte vml 1]><v:shape id="_x0000_i1026"
 type="#_x0000_t75" style='width:210pt;height:213.75pt;visibility:visible'>
 <v:imagedata src="cid:ii_kgqnqv2e3"></v:imagedata>
</v:shape><![endif]--></span><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#500050"><o:p></o:p></span></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

<p class="MsoNormal">&nbsp;<o:p></o:p></p>

<p class="MsoNormal"><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">Saludos Cordiales</span></i><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">&nbsp;</span></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">&nbsp;</span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Aura Liz Rodríguez
Mola</span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Economista&nbsp;</span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Telef. +58
424.8793335&nbsp;&nbsp;</span></i></b><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Consultor y Asesor del
Proyecto Alemania&nbsp;&nbsp;</span></i></b><span style="mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">DANKE D.U.</span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><o:p></o:p></span></p>

<p class="MsoNormal"><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222">&nbsp;</span></p>

<p class="MsoNormal"><b><i><span style="font-family:&quot;Verdana&quot;,sans-serif;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222;mso-no-proof:yes"><!--[if gte vml 1]><v:shape
 id="_x0000_i1025" type="#_x0000_t75" style='width:133.5pt;height:132.75pt;
 visibility:visible'>
 <v:imagedata src="https://drive.google.com/uc?id=1mxty0cS1KMb-FljYbo6vFPYlAovWeEGO&amp;export=download"></v:imagedata>
</v:shape><![endif]--></span></i></b><span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
color:#222222"><o:p></o:p></span></p><p class="MsoNormal"></p>
 <p class="MsoNormal"><b><u><span
                    style="mso-fareast-font-family:&quot;Times New Roman&quot;;color:blue"><br></span></u></b></p>
    <img src="img/fotos/logoDanke.png" class="" alt="cuenta"/>
    </p>
</body>

</html>