{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="vuelo_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos</h4>
            </div>
            <div id="msj-success-crvuelo" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Vuelo Agregado Correctamente.</strong>
            </div>
            <div id="msj-success-existente" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Este Vuelo ya existe</strong>
            </div>
            <div id="msj-error-crvuelo" class="alert alert-warning alert-dismissible" role="">
                <strong></strong>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion
                    Por Favor Espere</label></b>
            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            <div class="modal-body">
                
                <div class="form-group">
                <label for="sp_cedula">Cedula</label>&nbsp;&nbsp;
                    <select id="sp_cedula" class="form-control">
                    <option value="">Seleccione la cedula de un viajero</option>
                        @foreach($viajeros as $viajero)
                        <option value={{$viajero->nu_ci}}>{{$viajero->nu_ci}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nb_viajero">Nombre</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_viajero" disabled="">
                </div>
                <div class="form-group">
                    <label for="sp_codigo">Codigo</label>&nbsp;&nbsp;
                    <select id="sp_codigo" class="form-control">
                    <option value="">Seleccione el codigo de un viaje</option>
                        @foreach($viajes as $viaje)
                        <option value={{$viaje->co_viaje}}>{{$viaje->co_viaje}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nu_plazas">Numero de Plazas</label>&nbsp;&nbsp;
                    <input class="form-control" type="number" value="" id="nu_plazas" disabled="">
                </div>
                <div class="form-group">
                    <label for="nb_origen">Origen</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_origen" disabled="">
                </div>
                <div class="form-group">
                    <label for="nb_destino">Destino</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_destino" disabled="">
                </div>
                <div class="form-group">
                    <label for="nu_precio">Precio</label>&nbsp;&nbsp;
                    <input class="form-control" type="number" value="" id="nu_precio" disabled="">
                </div>
                <input class="form-control" type="hidden" value="" id="fe_nacimiento" disabled="">
                <input class="form-control" type="hidden" value="" id="nu_telefono" disabled="">
            </div>
            <div class="modal-footer">
                <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crvuelo" class="btn btn-primary"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crvuelo" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}
