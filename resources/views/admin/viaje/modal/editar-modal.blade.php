{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="viaje_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos</h4>
            </div>
            <div id="msj-success-edviaje" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Viaje Editato Correctamente</strong>
            </div>
            <div id="msj-success-edexistente" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Esta Viaje ya existe</strong>
            </div>
            <div id="msj-error-edviaje" class="alert alert-warning alert-dismissible" role="">
                <strong></strong>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion
                    Por Favor Espere</label></b>
            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="co_edviaje">Codigo</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="co_edviaje">
                </div>
                <div class="form-group">
                    <label for="nu_edplazas">Numero de Plazas</label>&nbsp;&nbsp;
                    <input class="form-control" type="number" value="" id="nu_edplazas">
                </div>
                <div class="form-group">
                    <label for="nb_edorigen">Origen</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_edorigen">
                </div>
                <div class="form-group">
                    <label for="nb_eddestino">Destino</label>&nbsp;&nbsp;
                    <input class="form-control" type="text" value="" id="nb_eddestino">
                </div>
                <div class="form-group">
                    <label for="nu_edprecio">Precio</label>&nbsp;&nbsp;
                    <input class="form-control" type="number" value="" id="nu_edprecio">
                </div>
            </div>
            <div class="modal-footer">
                <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_edviaje" class="btn btn-primary"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_edviaje" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}