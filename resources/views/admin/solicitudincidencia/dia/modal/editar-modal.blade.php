{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="solicitudincdia_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"solicitudincdia>&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modifique los datos del registro seleccionado</h4>
            </div>
             <div id="msj-success-edsolicitudincdia" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Solicitud Incidencia Modificada Correctamente.</strong>
        </div>
        <div id="msj-error-edsolicitudincdia" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfoed">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="loaded"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                 <div class="btn-group">
                    <label>Fecha Desde</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control" id="dp_fedesde_edsolicitudincdia">
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;


                 <div class="btn-group">
                    <label>Fecha Hasta</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control " id="dp_fehasta_edsolicitudincdia"> 
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="btn-group" >                         
                     <label for="">Conceptos</label> </b>
                     <div class="form-group">
                        <input id="sp_conceptos_edsolicitudincdia" multiple="multiple"   class="form-control"   style="width:270px" >
                     </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="btn-group">
                    <label>N° Documento</label> </b>
                    <div class="form-group">
                        <div class="input-group">
                        <input type="" size="22" class="form-control" style="text-align: left"
                                   id="tx_numerodocumento_edsolicitudincdia" placeholder="">
                        </div>
                    </div>
                </div>&nbsp;


                <div class="form-group">
                        <label>Descripcion</label>
                        <textarea id="textarea_descripcion_edsolicitudincdia" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder="" maxlength="60"></textarea>
                </div>

                 <input type="checkbox" checked="" id="check_enviarcorreo_edsolicitudincdia" value="">Enviar Correo Electronico

                 </br>
                 </br>

                 <div class="form-group">
                    <label>Correo Supervisor</label>&nbsp;&nbsp;
                    <input type="text" value="" id="txtoculto_correosupervisor_edsolicitudincdia">
                </div>
 
            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_edsolicitudincdia" data-editarsolicitudincdia=""class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_edsolicitudincdia" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>