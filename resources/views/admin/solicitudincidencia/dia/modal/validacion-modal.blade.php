{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="solicitudincdia_crvalidacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Incidencia Dia</h4>
            </div>
            <div class="modal-body">

                <div id="msj-fechadesdevalidacion-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Seleccionar La Fecha Desde para realizar la Solicitud</strong>
                </div>

                <div id="msj-fechahastavalidacion-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Seleccionar La Fecha Hasta para realizar la Solicitud</strong>
                </div>

                <div id="msj-fechadesdemayor-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>La Fecha Desde No puede ser mayor que la Fecha Hasta</strong>
                </div>

                <div id="msj-seleccionarconcepto-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Seleccionar El Concepto para realizar la Solicitud</strong>
                </div>

                <div id="msj-solicitudconceptoingresada-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Concepto Seleccionado Possee una Solicitud Ingresada</strong>
                </div>

                <div id="msj-correosupervisornovalido-crsolicitudincdia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Correo del Supervisor es Incorrecto</strong>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 