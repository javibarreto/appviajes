{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="solicitudvacacion_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos Solicitud Vacacion</h4>
            </div>
            
            <div id="msj-success-crsolicitudvacacion" class=" msj-success alert alert-success alert-dismissible" role="" >
                <strong> Solicitud Vacacion Agregada Correctamente.</strong>
            </div>
        
            <div id="msj-error-crsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" >
                <strong></strong>
            </div>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                 <div class="btn-group">
                    <label>Fecha Desde</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control" id="dp_fedesde_crsolicitudvacacion">
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;


                 <div class="btn-group">
                    <label>Fecha Hasta</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control" id="dp_fehasta_crsolicitudvacacion"> 
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;


                <input type="checkbox"  id="check_mediajornada_crsolicitudvacacion" value="" >Media Jornada</br>

                <div class="btn-group">
                    <label>N° Documento</label> </b>
                    <div class="form-group">
                        <div class="input-group">
                        <input type="" size="22" class="form-control" style="text-align: left"
                                   id="tx_numerodocumento_crsolicitudvacacion" placeholder="Introduzca Numero Documento">
                        </div>
                    </div>
                </div>&nbsp;


                <div class="form-group">


                    <div class="form-group">
                            <label>Descripcion</label>
                            <textarea id="textarea_descripcion_crsolicitudvacacion" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder="" maxlength="60"></textarea>
                    </div>
                </div>
             

                 <input type="checkbox" checked="" id="check_enviarcorreo_solicitudvacacion" value="">Enviar Correo Electronico
                 </br>
                 </br>

                 <div class="form-group">
                    <label>Correo Supervisor</label>&nbsp;&nbsp;
                    <input type="text" value="" id="txtoculto_correosupervisor_solicitudvacacion">
                 </div>

                 <input type="hidden" value="" id="txtoculto_diaspendientes_solicitudvacacion">

               
            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crsolicitudvacacion" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crsolicitudvacacion" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    });

</script>