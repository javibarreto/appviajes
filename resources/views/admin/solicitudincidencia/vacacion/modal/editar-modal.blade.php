{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="solicitudvacacion_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modifique los datos del registro seleccionado</h4>
            </div>

             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><label style="display: none" id="procesainfoed">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="loaded"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
             
            <div id="msj-success-edsolicitudvacacion" class=" msj-success alert alert-success alert-dismissible" role="" >
                <strong> Solicitud Vacacion Modificada Correctamente.</strong>
            </div>
        
            <div id="msj-error-edsolicitudvacacion" class="alert alert-warning alert-dismissible" role="" >
              <strong></strong>
            </div>

        
            
        <div class="modal-body">

                 <div class="btn-group">
                    <label>Fecha Desde</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control datepicker1" id="dp_fedesde_edsolicitudvacacion">
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;


                 <div class="btn-group">
                    <label>Fecha Hasta</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control datepicker1" id="dp_fehasta_edsolicitudvacacion"> 
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;

                <input type="checkbox"  id="check_mediajornada_edsolicitudvacacion" disabled="" value="">Media Jornada</br>

                <div class="btn-group">
                    <label>N° Documento</label> </b>
                    <div class="form-group">
                        <div class="input-group">
                        <input type="" size="22" class="form-control" style="text-align: left"
                                   id="tx_numerodocumento_edsolicitudvacacion" placeholder="">
                        </div>
                    </div>
                </div>&nbsp;

                <div class="form-group">
                        <label>Descripcion</label>
                        <textarea id="textarea_descripcion_edsolicitudvacacion" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder="" maxlength="60"></textarea>
                </div>

              <input type="checkbox" checked="" id="check_enviarcorreo_edsolicitudvacacion" value="">Enviar Correo Electronico

              </br>
              </br>

              <div class="form-group">
                  <label>Correo Supervisor</label>&nbsp;&nbsp;
                  <input type="text" value="" id="txtoculto_correosupervisor_edsolicitudvacacion">
              </div>

             <input type="hidden" value="" id="txtoculto_diaspendientes_edsolicitudvacacion">

               
            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_edsolicitudvacacion" data-editarsolicitudvacion="" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_edsolicitudvacacion" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

       $('.datepicker1').datepicker({
        //format: "dd/mm/yyyy",
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });


    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>