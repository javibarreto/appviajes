{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="solicitudinchora_crvalidacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Incidencia Hora</h4>
            </div>
            <div class="modal-body">

                <div id="msj-fechadesdevalidacion-crsolicitudinchora" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Seleccionar La Fecha para realizar la Solicitud</strong>
                </div>

                 <div id="msj-seleccionarconcepto-crsolicitudinchora" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Seleccionar El Concepto para realizar la Solicitud</strong>
                </div>

                <div id="msj-solicitudconceptoingresada-crsolicitudinchora" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Concepto Seleccionado Possee una Solicitud Ingresada</strong>
                </div>

                <div id="msj-correosupervisornovalido-crsolicitudinchora" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Correo del Supervisor es Incorrecto</strong>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 