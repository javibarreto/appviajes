{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="solicitudinchora_eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Elimine el Registro Seleccionado</h4>
                
            </div>
            <div id="msj-success-elsolicitudinchora" class=" msj-successeliminar alert alert-success alert-dismissible" role="" >
                     <strong> Solicitud Incidencia Eliminada Correctamente.</strong>
            </div>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfoel">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="loadel"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>


            <div class="modal-body">
                 <h4><p align="center">Desea Eliminar el Registro?</p></h4>
            </div>

            &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="" id="check_enviarcorreo_elsolicitudinchora" value="">Enviar Correo Electronico
            </br>
            </br>

            <div class="form-group">
                 &nbsp;&nbsp;&nbsp;&nbsp;<label>Correo Supervisor</label>&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="" id="txtoculto_correosupervisor_elsolicitudinchora">
            </div>

            <div class="modal-footer">
                <button type="button" id="btnmodalaceptar_elsolicitudinchora" data-eliminarsolicitudinchora="" class="btn btn-primary">
                    <span class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
                <button type="button" id="btnmodalcancelar_elsolicitudinchora" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 