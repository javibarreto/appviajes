{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="torneo_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:725px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos</h4>
            </div>
            <div id="msj-success-crtorneo" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Torneo Agregado Correctamente</strong>
            </div>
            <div id="msj-success-existente" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> Ya existe un Torneo para este mes</strong>
            </div>
            <div id="msj-success-nupar" class=" msj-success alert alert-success alert-dismissible" role="">
                <strong> El número de contrincantes debe ser pares</strong>
            </div>
            <div id="msj-error-crtorneo" class="alert alert-warning alert-dismissible" role="">
                <strong></strong>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion
                    Por Favor Espere</label></b>
            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">

                <div class="btn-group" >
                    <label>Fecha Desde</label> </br>
                    <div class="form-group">
                        <input type="text" class="form-control datepickerfedesde"
                                id="dp_fe_torneo" value="{{$fecha}}">
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   

                    <div class="btn-group">
                        <label for="">Dif peso (gr)</label> </b>
                        <div class="form-group">
                            <div class="input-group">
                                <input id="nu_peso" class="form-control" size="10" placeholder=""
                                       type="text" value="10">
                            </div>
                        </div>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <div class="btn-group">
                        <label for="">Casas</label> </b>
                        <div class="form-group">
                        <select id="sp_casa" class="form-control" style="width:295px">
                            @foreach($casas as $casa)
                            <option value={{$casa->id_casa}}>{{$casa->nb_casa}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                    <div class="btn-group">
                        <label for="">Competidores</label> </b>
                        <div class="form-group">
                        <select id="sp_competidor" class="form-control" style="width:295px" placeholder="Por favor seleccione un registro">
                           
                        </select>
                        </div>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

               
                <div class="panel-group">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><b>Listado de torneos</b></h3>
                        </div>
                        <div class="box-body table-responsive"
                             style="margin:0px; padding:6px; width:100%; height:290px; overflow:auto">

                            <table id="listar_detalle_crtorneo" class="table table-bordered table-hover">

                                <thead>
                                <tr>
                                    <th id="casa" style="text-align: center">Casa</th>
                                    <th id="competidor" style="text-align: center">Competidor</th>
                                    <th id="peso" style="text-align: center">Peso (gr)</th>
                                    <th id="eliminar" style="text-align: center">Eliminar</th>
                                </tr>
                                </thead>

                                <tbody id="listar_detalle_crventa_body">
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                        

                        
                    </div><!-- /.box -->
                </div>
            </div>
                <input type="hidden" value="" id="txtoculto_diaspendientes_solicitudvacacion">
            </div>
            <div class="modal-footer">
                <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crtorneo" class="btn btn-primary"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crtorneo" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}