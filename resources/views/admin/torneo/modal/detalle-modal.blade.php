{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="torneo_detalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:725px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Detalle del Torneo</h4>
            </div>
           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion
                    Por Favor Espere</label></b>
            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                    data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">

                <div class="panel-group">
                        <div class="box-body table-responsive"
                             style="margin:0px; padding:6px; width:100%; height:290px; overflow:auto">

                            <table id="listar_detalle_crtorneo" class="table table-bordered table-hover">

                                <thead>
                                <tr>
                                    <th id="cacontrincante" style="text-align: center">Contrincante</th>
                                    <th id="retador" style="text-align: center">Retador</th>
                                    <th id="peso" style="text-align: center"> Diferencia Peso (gr)</th>
                                   
                                </tr>
                                </thead>

                                <tbody id="listar_detalle_crventa_body">
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_detalle" class="btn btn-primary"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_detalle" class="btn btn-danger"
                        data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}