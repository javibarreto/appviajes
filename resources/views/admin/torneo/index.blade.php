@extends('admin.layout.admin')
@section('content')



    <!-- datatables -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- datatables -->

    <!-- Datepicker -->
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.css">

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">

    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- Datepicker Files -->

    
    @include('alerts.errors')
    <div id="msj-seleccionarregistroeditar-torneo" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Editar </strong>
    </div>

    <div id="msj-seleccionarregistroeliminar-torneo" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Eliminar </strong>
    </div>
    
    <div id="msj-soloeditarsolicitud-torneo" class=" alert alert-warning alert-dismissible" role="" style="display:none">
            <strong> Solo se puede Editar las Solicitudes con Estatus Ingresado.</strong>
    </div>

    <div id="msj-soloeliminarsolicitud-torneo" class=" alert alert-warning alert-dismissible" role="" style="display:none">
            <strong> Solo se puede Eliminar las Solicitudes con Estatus Ingresado.</strong>
    </div>


    <div id="msj-fechamayor-torneo" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> La Fecha Desde No Puede Ser Mayor a la Fecha Hasta </strong>
     </div>
     
    @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Torneos
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Bienvenido</a></li>
            <li><a>Procesos</a></li>
            <li><a>Solicitud Incidencia</a></li>
            <li class="active">Por Dia</li>
        </ol>

    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">

                        <div class="box-body">

                            <div class="margin">

                                <div class="btn-group">
                                    <button type="button" id="btn_torneo_crear" class="btn btn-primary"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-plus"></span> Crear
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @include('admin.torneo.modal.crear-modal')
                                    @include('admin.torneo.modal.detalle-modal')
                                </div>


                                <div class="btn-group">
                                    <a id="pdftorneo" href="#" class="">
                                        <img src="../../img/archivo_pdf.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>

                                <!-- Modal <div class="btn-group">
                                    <a id="exceltorneo" href="#" class="">
                                        <img src="../../img/archivo_excel.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>-->
                                 <div class="btn-group">
                                    <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                                    </button>
                                </div>
                                <br><br>

                                <div class="panel-group" >
                                    <div class="box box-primary" >
                                        <div class="box-header" >
                                            <h3 class="box-title"><b>Criterio de Busqueda</b></h3>
                                        </div>
                                        <div class="box-body">


                                            <div class="btn-group" >
                                                <label>Fecha Desde</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfedesde"
                                                           id="dp_fedesde_torneo" value="{{$fecha}}">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="btn-group" >
                                                <label>Fecha Hasta</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfehasta" value="{{$fecha}}"
                                                           id="dp_fehasta_torneo">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="btn-group" >
                                                <label>Nª de competidores</label> </br>
                                                <div class="form-group">
                                                    <input id="s2_incidencia_torneo" type="text"   class="form-control"   style="width:270px">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="content">
                                                <div class="btn-group">
                                                    <button type="button" id="btn_torneo_buscar" class="btn btn-success"><span
                                                                class="glyphicon glyphicon-search"></span> Buscar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" id="btn_torneo_limpiar" class="btn btn-info"><span
                                                                class="glyphicon glyphicon-refresh"></span> Limpiar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
                </div><!-- /. row -->
    <section class="content-header">
        <div class="row" >
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Listado Solicitud Incidencia Dia</h3>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive" >
                   <table id="listar_torneo" class="table table-condensed table-hover" style="width:100%" cellspacing="0" >

                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>N° competidores</th>
                            <th>Diferencia de peso</th>
                            <th>Detalle</th>
                        </tr>

                        </thead>
                    </table>
                </div><!-- /.box-body -->

            </div><!-- /.box -->
    </section>

    </section><!-- /.content -->

    

@endsection
@section('scripts')

 

    <!-- datepicker -->
    {!!Html::script('intranet/js/picker/bootstrap-datetimepicker.js')!!}
    {!!Html::script('intranet/js/picker/locales/bootstrap-datetimepicker.es.js')!!}
    {!!Html::script('intranet/css/datepicker/js/bootstrap-datepicker.js')!!}
    <!-- datepicker -->

    <!-- datatables -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- datatables -->


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js"></script>


    {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
    {!!Html::script('intranet/js/ajax/torneo/operaciones.js')!!}


    <script type="text/javascript">

    

        var lenguaje = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

        jQuery.noConflict();

        jQuery(document).ready(function () {

            listar();
        });


        $('#dp_fedesde_torneo').datepicker(
          {
               format: "yyyy-mm-dd"
          }
        )


        .on('changeDate', function(ev){ 
                //alert("pase por aqui");                
                $('#dp_fedesde_torneo').datepicker('hide');
        }); 

        $('#dp_fehasta_torneo').datepicker(
          {
               format: "yyyy-mm-dd"
          }
        )


        .on('changeDate', function(ev){ 
                //alert("pase por aqui");                
                $('#dp_fehasta_torneo').datepicker('hide');
        }); 


        $('#dp_fedesde_crtorneo').datepicker(
            {
             language: "es",
             autoclose: true,
             format: "yyyy-mm-dd"
            }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fedesde_crtorneo').datepicker('hide');
        }); 


        $('#dp_fehasta_crtorneo').datepicker(
            {
             
             language: "es",
             autoclose: true,
             format: "yyyy-mm-dd"
            
            }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fehasta_crtorneo').datepicker('hide');
        }); 


        $('#dp_fedesde_edtorneo').datepicker(
            {
             
             language: "es",
             autoclose: true,
             format: "yyyy-mm-dd"
            
            }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fedesde_edtorneo').datepicker('hide');
        }); 


        $('#dp_fehasta_edtorneo').datepicker(
            {
             
             language: "es",
             autoclose: true,
             format: "yyyy-mm-dd"
            
            }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fehasta_edtorneo').datepicker('hide');
        }); 

        
        

       //INICIALIZA LOS VALORES DEL BUTTON

       //$("#ra_todas_solicitudincdia").iCheck('check');
  
    </script>



@endsection