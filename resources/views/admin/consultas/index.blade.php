@extends('admin.layout.admin')
@section('content')



    <!-- datatables -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- datatables -->

    <!-- Datepicker -->
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.css">

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">

    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- Datepicker Files -->

    @include('alerts.errors')

    <div id="msj-fechamayor-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
        <strong> La Fecha Desde No Puede Ser Mayor a la Fecha Hasta </strong>
    </div>

    <div id="msj-fechadesdemesactual-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
        <strong> Solo Puede Consultar el Mes Actual Verifique Fecha Desde</strong>
    </div>

     <div id="msj-fechahastamesactual-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
        <strong> Solo Puede Consultar el Mes Actual Verifique Fecha Hasta</strong>
    </div>

     <div id="msj-fechahastadiaactual-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
        <strong> Solo Puede Consultar Hasta el Dia Actual Verifique Fecha Hasta</strong>
    </div>

    <div id="msj-noexistenregistros-consultas" class=" alert alert-warning alert-dismissible" role="" style="display:none">
        <strong> No existen registros para realizar esta operación.</strong>
    </div>

    <div id="msj-respuestanopermitida-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Consulta Respondida </strong>
    </div>

    <div id="msj-respuestasupervisornopermitida-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Usuario No Autorizado para Responder la Consulta</strong>
    </div>

    <div id="msj-seleccionarregistro-consultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para realizar esta Operacion </strong>
    </div>


    @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Consultas
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Bienvenido</a></li>
            <li class="active">Consultas</li>
        </ol>

    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">

                        <div class="box-body">

                            <div class="margin">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                                <div class="btn-group">
                                    <button type="button" id="btn_consultas_crear" class="btn btn-primary"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-plus"></span> Crear
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @include('admin.consultas.modal.crear-modal')
                                    @include('admin.consultas.modal.validacion-modal')
                                </div>

                                @if(Auth::user()->supervisor == "SI")

                                <div class="btn-group">
                                        <button type="button" id="btn_responder_crear" class="btn btn-warning"  data-toggle="modal" data-target="#Editar"><span class="glyphicon glyphicon-edit"></span> Responder</button>  &nbsp;&nbsp;&nbsp;&nbsp; 

                                        @include('admin.consultas.modal.responder-modal')
                                        @include('admin.consultas.modal.validacionresponder-modal') 

                                </div>

                                @endif 

                                <!--<div class="btn-group">
                                    <a id="pdfconsultas" href="#" class="">
                                        <img src="../../img/archivo_pdf.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>-->

                                <!--<div class="btn-group">
                                    <a id="excelmarcaasistenciarepo" href="#" class="">
                                        <img src="../../img/archivo_excel.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>-->

                                <div class="btn-group">
                                    <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                                    </button>
                                </div>
                                <br><br>

                                <div class="panel-group"  >
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            <h3 class="box-title"><b>Criterio de Busqueda</b></h3>
                                        </div>
                                        <div class="box-body">


                                            <div class="btn-group" >
                                                <label>Fecha Desde</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfedesde"
                                                           id="dp_fedesde_consultas" value="{{$fecha}}">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="btn-group">
                                                <label>Fecha Hasta</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfehasta"
                                                           value="{{$fecha}}"
                                                           id="dp_fehasta_consultas">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                             @if(Auth::user()->supervisor == "SI")
                                            <div class="btn-group" >
                                                <label>Personal</label> </br>
                                                <div class="form-group">
                                                    <input id="s2_personalconsultas" multiple="multiple"   class="form-control"   style="width:270px">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            @endif 

                                            <div class="content">
                                                <div class="btn-group">
                                                    <button type="button" id="btn_consultas_buscar"
                                                            class="btn btn-success"><span
                                                                class="glyphicon glyphicon-search"></span> Buscar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" id="btn_consultas_limpiar"
                                                            class="btn btn-info"><span
                                                                class="glyphicon glyphicon-refresh"></span> Limpiar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>   
                    </div>
                </div>
            </div>
        </div><!-- /. row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Listado Consultas</h3>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="listar_consultas" class="table table-condensed table-hover" style="width:100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>DESCRIPCION</th>
                            <th>FECHA</th>
                            <th>HORA</th>
                            <th>RESPONDIDO POR</th>
                            <th>RESPUESTA</th>
                            <th>RESPONDIDO</th>
                        </tr>
                        
                        </thead>

                  
                    </table>
                </div>
            </div>
        </div>   
    </section>

    <tfoot>




@endsection
@section('scripts')

    <!-- datepicker -->
    {!!Html::script('intranet/css/datepicker/js/bootstrap-datepicker.js')!!}
    <!-- datepicker -->

    <!-- datatables -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- datatables -->


     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js"></script>

    {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
    {!!Html::script('intranet/js/ajax/consultas/operaciones.js')!!}
    

    <script type="text/javascript">

        var lenguaje = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },

            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },

        }


        jQuery.noConflict();

        jQuery(document).ready(function () {

        listarconsultas();
        cargarpersonalsupervisorconsultas();

        
        }); 



        $('#dp_fedesde_consultas').datepicker(
            {

                format: "yyyy-mm-dd" 
            }
        )


            .on('changeDate', function (ev) {

                //alert("pase por aqui");                
                $('#dp_fedesde_consultas').datepicker('hide');
            });


        $('#dp_fehasta_consultas').datepicker(
            {


                format: "yyyy-mm-dd"
               
            }
        )


            .on('changeDate', function (ev) {

                //alert("pase por aqui");                
                $('#dp_fehasta_consultas').datepicker('hide');
            });


       // $('input:radio[id=rb_pago1_crventa]')[0].checked = true;

      //  $('input:radio[id=rb_pago2_crventa]')[0].checked = false;

 
    </script>



@endsection