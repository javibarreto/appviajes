{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="consultas_crvalidacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Consultas</h4>
            </div>
            <div class="modal-body">

                <div id="msj-descripcion-crconsultas" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Ingresar La Descripcion para realizar la Consulta</strong>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 