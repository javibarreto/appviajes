{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="consultas_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    box-body table-responsive

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"Consultas>&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos de la Consulta</h4>
            </div>
             <div id="msj-success-crconsultas" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Consultas Agregada Correctamente.</strong>
        </div>
        <div id="msj-error-crconsultas" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                <div class="form-group">
                        <label>Descripcion</label>
                        <textarea id="textarea_descripcion_crconsultas" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="4" placeholder="" maxlength=""></textarea>
                </div>

            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crconsultas" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crconsultas" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>