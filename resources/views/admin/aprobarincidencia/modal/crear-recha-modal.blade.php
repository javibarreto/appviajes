{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="rechazar_incidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Datos Rechazo</h4>
            </div>
             <div id="msj-success-rechazarincidencia" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Incidencia Rechazada Correctamente.</strong>
        </div>
        <div id="msj-error-rechazarincidencia" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfoed">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="loaded"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                <div class="form-group">
                     <label for="">Detalle Concepto</label >
                </div>
                
                <div class="form-group" > 

                     <label for="">Codigo</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <label for="">Descripcion</label> </b>
                     <div class="form-group">
                       
                            <input type="" size="3" class="" style="text-align: center"
                                   id="codigoconcepto_rechazarincidencia" value="" placeholder="" disabled="true">&nbsp;&nbsp;&nbsp;&nbsp;
                      
                    
                            <input type="" size="50" class="" style="text-align: left"
                                   id="descripconcepto_rechazarincidencia" value="" placeholder="" disabled="true">
                     </div>

                </div> &nbsp;&nbsp;&nbsp;&nbsp;


                <div class="form-group">
                        <label>Descripcion Solictud</label>
                        <textarea id="textarea_descripcion_rechazarincidencia" disabled=""onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder="" ></textarea>
                </div>

                <div class="btn-group">
                    <label>N° Documento</label> </b>
                    <div class="form-group">
                        <div class="input-group">
                        <input type="" size="22" class="form-control" style="text-align: left"
                                   id="tx_numerodocumento_rechazarincidencia" placeholder="" disabled="" >
                        </div>
                    </div>
                </div>&nbsp;

                <div class="form-group">
                        <label>Observacion Rechazo</label>
                        <textarea id="textarea_observacion_rechazarincidencia" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder=""></textarea>
                </div>

                <input type="checkbox" checked="" id="check_enviarcorreo_rechazarincidencia" value="">Enviar Notificacion

                </br>
                </br>

                <div class="form-group">
                    <label>Correo Solicitante</label>&nbsp;&nbsp;
                    <input type="text" value="" id="txtoculto_correosolicitante_rechazar">
                </div>

            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_rechazarincidencia" data-rechazarincidencia="" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_rechazarincidencia" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">


    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>