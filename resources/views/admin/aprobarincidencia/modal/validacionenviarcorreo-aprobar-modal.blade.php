{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="aprobarincidencia_validacioncorreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Aprobar</h4>
            </div>
             <div class="modal-body">
               <div id="msj-solicitantevacio-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Solicitante No cuenta con Correo Electronico desea Aprobar la Solicitud?</strong>
                </div>
            </div>
           

            <div class="modal-footer">
                <button type="button" id="btnmodalaceptar_aprobarincidenciacorreo" data-aprobarincidenciacorreo="" class="btn btn-primary">
                    <span class="glyphicon glyphicon-ok"></span> Aceptar
                </button>

                <button type="button" id="btnmodalcancelar_aprobarincidenciacorreo" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 