{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="revisar_incidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Datos Revision</h4>
            </div>
             <div id="msj-success-revisarincidencia" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Incidencia Revisada Correctamente.</strong>
        </div>
        <div id="msj-error-revisarincidencia" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainforevi">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="loadrevi"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                <div class="form-group">
                     <label for="">Detalle Concepto</label >
                </div>
                
                <div class="form-group" > 

                     <label for="">Codigo</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <label for="">Descripcion</label> </b>
                     <div class="form-group">
                       
                            <input type="" size="3" class="" style="text-align: center"
                                   id="codigoconcepto_revisarincidencia" value="" placeholder="" disabled="true">&nbsp;&nbsp;&nbsp;&nbsp;
                      
                    
                            <input type="" size="50" class="" style="text-align: left"
                                   id="descripconcepto_revisarincidencia" value="" placeholder="" disabled="true">
                     </div>

                </div> &nbsp;&nbsp;&nbsp;&nbsp;


                <div class="form-group">
                        <label>Descripcion Solictud</label>
                        <textarea id="textarea_descripcion_revisarincidencia" disabled=""onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder="" ></textarea>
                </div>

                <div class="btn-group">
                    <label>N° Documento</label> </b>
                    <div class="form-group">
                        <div class="input-group">
                        <input type="" size="22" class="form-control" style="text-align: left"
                                   id="tx_numerodocumento_revisarincidencia" placeholder="" disabled="">
                        </div>
                    </div>
                </div>&nbsp;

                <div class="form-group">
                        <label>Observacion Revision</label>
                        <textarea id="textarea_observacion_revisarincidencia" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" class="form-control" rows="2" placeholder=""></textarea>
                </div>

                 <input type="checkbox" checked="" id="check_enviarcorreo_revisarincidencia" value="">Enviar Correo Electronico

                </br>
                </br>

                <div class="form-group">
                    <label>Correo Solicitante</label>&nbsp;&nbsp;
                    <input type="text" value="" id="txtoculto_correosolicitante_revisar">
                </div>

               
            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_revisarincidencia" data-revisarincidencia="" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_revisarincidencia" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">


    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>