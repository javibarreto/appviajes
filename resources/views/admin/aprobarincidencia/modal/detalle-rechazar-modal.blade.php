{!!Form::open()!!}

<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

<div class="modal fade" id="detmodalrechazarincidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:850px">>
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Informacion Rechazo</h4>
            </div>
            <div class="modal-body">
                <div rol="form">
                    <div class="panel-group">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Detalle Rechazo Solicitud</b></h3>
                            </div>
                            <div class="box-body table-responsive"
                                 style="margin:0px; padding:6px;width:100%;height:280px;overflow:auto">
                                <table id="datosrechazarincidencia" class="table table-bordered table-hover">
                                   
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Fecha</th>
                                            <th style="text-align: center">Hora</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </table>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}
