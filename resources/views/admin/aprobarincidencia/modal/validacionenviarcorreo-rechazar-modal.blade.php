{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="rechazarincidencia_validacioncorreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Rechazar</h4>
            </div>
             <div class="modal-body">
               <div id="msj-solicitantevacio-rechazarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>El Solicitante No cuenta con Correo Electronico desea Rechazar la Solicitud?</strong>
                </div>
            </div>
           

            <div class="modal-footer">
                <button type="button" id="btnmodalaceptar_rechazarincidenciacorreo" data-aprobarincidenciacorreo="" class="btn btn-primary">
                    <span class="glyphicon glyphicon-ok"></span> Aceptar
                </button>

                <button type="button" id="btnmodalcancelar_rechazarincidenciacorreo" class="btn btn-danger" data-dismiss="modal"s><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                </button>

            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 