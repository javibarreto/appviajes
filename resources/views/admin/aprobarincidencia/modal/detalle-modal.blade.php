{!!Form::open()!!}

<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

<div class="modal fade" id="detmodaloperaaprobarincidencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document" style="width:850px">>
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Informacion Operaciones</h4>
            </div>
            <div class="modal-body">
                <div rol="form">
                    <div class="panel-group">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Detalle Operaciones Aprobacion-Rechazo</b></h3>
                            </div>
                            <div class="box-body table-responsive"
                                 style="margin:0px; padding:6px;width:100%;height:280px;overflow:auto">
                                <table id="datosoperaaprobarincidencia" class="table table-bordered table-hover">
                                   
                                        <thead>
                                        <tr>
                                            <th style="text-align: center">Operacion</th>
                                            <th style="text-align: center">Acceso Desde</th>
                                            <th style="text-align: center">Latitud</th>
                                            <th style="text-align: center">Longitud</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </table>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}
