@extends('admin.layout.admin')
@section('content')



    <!-- datatables -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- datatables -->

    <!-- Datepicker -->
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.css">

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">

    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- Datepicker Files -->

    
    @include('alerts.errors')
    <div id="msj-noexistenregistros-aprobarincidencia" class=" alert alert-warning alert-dismissible" role="" style="display:none">
            <strong> No existen registros para realizar esta operación.</strong>
    </div>

    <div id="msj-seleccionarregistro-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Aprobar Incidencia </strong>
    </div>

    <div id="msj-seleccionarregistro-rechazarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Rechazar Incidencia </strong>
    </div>

    <div id="msj-seleccionarregistro-revisarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong>Debe Seleccionar un Registro para Revisar Incidencia </strong>
    </div>

    <div id="msj-soloaprobarsolicitud-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> Solo se puede Aprobar las Solicitudes con Estatus Ingresado o Revisado. </strong>
    </div>

    <div id="msj-solorechazarsolicitud-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> Solo se puede Rechazar las Solicitudes con Estatus Ingresado o Revisado. </strong>
    </div>

    <div id="msj-solorevisarsolicitud-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> Solo se puede Revisar las Solicitudes con Estatus Ingresado. </strong>
    </div>

    <div id="msj-solorevisardocumentosolicitud-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> Solo se puede Revisar las Solicitudes que no tengo Numero de Documento Asignado. </strong>
    </div>


    <div id="msj-fechamayor-aprobarincidencia" class="alert alert-warning alert-dismissible" role="" style="display:none">
             <strong> La Fecha Desde No Puede Ser Mayor a la Fecha Hasta </strong>
     </div>
     
    @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Aprobar Incidencia
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Bienvenido</a></li>
            <li><a>Procesos</a></li>
            <li class="active">Aprobar Incidencia</li>
        </ol>

    </section>


    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                        <div class="box-body">

                            <div class="margin">

                                <div class="btn-group">
                                    <button type="button" id="btn_aprobar_incidencia" class="btn btn-primary"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-plus"></span> Aprobar
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                     @include('admin.aprobarincidencia.modal.crear-modal')
                                      @include('admin.aprobarincidencia.modal.validacionenviarcorreo-aprobar-modal')
                                     @include('admin.aprobarincidencia.modal.detalle-modal')
                                     @include('admin.aprobarincidencia.modal.detalle-aprobar-modal')
                                     @include('admin.aprobarincidencia.modal.detalle-rechazar-modal')
                                     @include('admin.aprobarincidencia.modal.detalle-revisar-modal')

                                </div>

                                    <div class="btn-group">
                                    <button type="button" id="btn_revisar_incidencia" class="btn btn-warning"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-plus"></span> Revisar
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                     @include('admin.aprobarincidencia.modal.crear-revi-modal')
                                     @include('admin.aprobarincidencia.modal.validacion-revi-modal')
                                     @include('admin.aprobarincidencia.modal.validacionenviarcorreo-revisar-modal')
                                    

                                </div>


                                <div class="btn-group">
                                    <button type="button" id="btn_rechazar_incidencia" class="btn btn-danger"
                                            data-toggle="modal" data-target=""><span
                                                class="glyphicon glyphicon-minus"></span> Rechazar
                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                     @include('admin.aprobarincidencia.modal.crear-recha-modal')
                                     @include('admin.aprobarincidencia.modal.validacion-recha-modal')
                                     @include('admin.aprobarincidencia.modal.validacion-aprob-modal')
                                     @include('admin.aprobarincidencia.modal.validacionenviarcorreo-rechazar-modal')
                                      

                                    <!-- Modal -->
                               
                                
                                </div>

                                <div class="btn-group">
                                    <a id="pdfaprobarincidencia" href="#" class="">
                                        <img src="../../img/archivo_pdf.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>

                               <!-- <div class="btn-group">
                                    <a id="excelaprobarincidencia" href="#" class="">
                                        <img src="../../img/archivo_excel.png" align="" width="" height="" class=""
                                             alt=""
                                             align="">
                                    </a>
                                </div>-->
                                 <div class="btn-group">
                                    <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                                    </button>
                                </div>
                                <br><br>

                                <div class="panel-group" >
                                    <div class="box box-primary" >
                                        <div class="box-header" >
                                            <h3 class="box-title"><b>Criterio de Busqueda</b></h3>
                                        </div>
                                        <div class="box-body">


                                            <div class="btn-group" >
                                                <label>Fecha Desde</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfedesde"
                                                           id="dp_fedesde_aprobarincidencia" value="{{$fecha}}">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <div class="btn-group" >
                                                <label>Fecha Hasta</label> </br>
                                                <div class="form-group">
                                                    <input type="text" class="form-control datepickerfehasta" value="{{$fecha}}"
                                                           id="dp_fehasta_aprobarincidencia">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                            <div class="btn-group" >
                                                <label>Incidencia</label> </br>
                                                <div class="form-group">
                                                    <input id="s2_incidencia_aprobarincidencia" multiple="multiple"   class="form-control"   style="width:270px">
                                                </div>
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;



                                            <div class="btn-group">
                                                 <input type="radio" class="" id="ra_todas_aprobarincidencia" name="solicitudvacacion" value=""/> Todas
                                                &nbsp;&nbsp;
                                                 <input type="radio" class="" id="ra_ingresadas_aprobarincidencia" name="solicitudvacacion" value=""/> Ingresadas
                                                &nbsp;&nbsp;
      
                                                <input type="radio" class="" id="ra_aprobadas_aprobarincidencia" name="solicitudvacacion" value=""/> Aprobadas
                                                &nbsp;&nbsp;
                                               
                                                <input type="radio" class="" id="ra_rechazadas_aprobarincidencia" name="solicitudvacacion" value=""/> Rechazadas&nbsp;&nbsp;

                                                <input type="radio" class="" id="ra_revisadas_aprobarincidencia" name="solicitudvacacion" value=""/> Revisadas&nbsp;&nbsp;
                                               
                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                         
                                            <div class="content">
                                                <div class="btn-group">
                                                    <button type="button" id="btn_aprobarincidencia_buscar" class="btn btn-success"><span
                                                                class="glyphicon glyphicon-search"></span> Buscar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" id="btn_aprobarincidencia_limpiar" class="btn btn-info"><span
                                                                class="glyphicon glyphicon-refresh"></span> Limpiar
                                                    </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /. row -->
                </div><!-- /. row -->
    <section class="content-header">
        <div class="row" >
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Listado Aprobacion Incidencia</h3>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive" >
                   <table id="listar_aprobarincidencia" class="table table-condensed table-hover" style="width:100%" cellspacing="0" >

                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            <th>Concepto</th>
                            <th>Descripcion</th>
                            <th>Documento</th>
                            <th>Solicitud Elaborada</th>
                            <th>Det.Aprob</th>
                            <th>Det.Rechaz</th>
                            <th>Det.Revi</th>
                            <th>Observacion</th>
                            <th>Operaciones</th>
                            <th>Procesada En Sistema</th>
                            <th>Estatus</th>
                        </tr>

                        </thead>
                    </table>
                </div><!-- /.box-body -->

            </div><!-- /.box -->


   
   
    </section>
    </section><!-- /.content -->

    

@endsection
@section('scripts')

 

    <!-- datepicker -->
    {!!Html::script('intranet/js/picker/bootstrap-datetimepicker.js')!!}
    {!!Html::script('intranet/js/picker/locales/bootstrap-datetimepicker.es.js')!!}
    {!!Html::script('intranet/css/datepicker/js/bootstrap-datepicker.js')!!}
    <!-- datepicker -->

    <!-- datatables -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- datatables -->


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js"></script>


    {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
    {!!Html::script('intranet/js/ajax/aprobarincidencia/operaciones.js')!!}


    <script type="text/javascript">

    

        var lenguaje = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

       
    
        jQuery.noConflict();

        jQuery(document).ready(function () {

            ListarAprobarIncidencia();
            cargarconceptosaprobarinc();

        });

       

        $('#dp_fedesde_aprobarincidencia').datepicker(
          {
             
               format: "yyyy-mm-dd"
            
          }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fedesde_aprobarincidencia').datepicker('hide');
        }); 

      
       
        
        $('#dp_fehasta_aprobarincidencia').datepicker(
          {
               format: "yyyy-mm-dd"
            
          }
        )


        .on('changeDate', function(ev){ 
                
                //alert("pase por aqui");                
                $('#dp_fehasta_aprobarincidencia').datepicker('hide');
        }); 


       //INICIALIZA LOS VALORES DEL BUTTON

       $("#ra_ingresadas_aprobarincidencia").iCheck('check');

    </script>



@endsection