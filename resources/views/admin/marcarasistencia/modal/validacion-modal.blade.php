{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="marcacionasistencia_validacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Marcacion Asistencia</h4>
            </div>
            <div class="modal-body">

                    <div id="msj-marcacionregistradamismaip-login" class="alert alert-warning alert-dismissible" role="" style="display:inline" align="justify">
                        <strong>Marcacion no permitida: Dispositivo usado por otro Usuario</strong>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button id=""  type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}


