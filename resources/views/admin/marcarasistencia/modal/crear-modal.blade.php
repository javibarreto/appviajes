{!!Form::open()!!}

<!-- EFECTO DEL BOTON LOAD-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- FIN EFECTO DEL BOTON LOAD -->

<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="marcarasistencia_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:400px; overflow-y: auto; margin: 7px auto 0 auto;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Marcar Asistencia</h4>
            </div>
           
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" 
                id="procesainfomarcacionmarcacionasistencia">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button  style="display: none" type="button" class="btn btn-success btn-lg " 
                id="loadmarcacionasistencia"
                      data-loadmarcacionasistencia-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            
            </div>
        
            <div id="msj-success-marcacionasistencia" class=" msj-success alert alert-success alert-dismissible" role="" >
                <strong> Marcacion Registrada Correctamente.</strong>
            </div>

            <div class="modal-body">

                <p id="" style="color:#FF0000"> Importante!!! La Marcacion sera revisada y verificada por el personal que labora en el area de Recursos Humanos.</p>  

                <div rol="form">
                        <label for="">DNI</label>
                        <input type="text" class="form-control" id="txt_marcacionasis_dni" onkeypress="" maxlength="" placeholder="" disabled=""> 
                </div>

                <div rol="form">
                        <label for="">EMPLEADO</label>
                        <input type="text" class="form-control" id="txt_marcacionasis_nombreempleado" onkeypress="" maxlength="" placeholder="" disabled=""> 
                </div>

                <div rol="form">
                        <label for="">FECHA</label>
                        <input type="text" class="form-control" id="txt_marcacionasis_fecha" onkeypress="" maxlength="" placeholder="" disabled=""> 
                </div>

                <div rol="form">
                        <label for="">HORA</label>
                        <input type="text" class="form-control" id="txt_marcacionasis_hora" onkeypress="" maxlength="" placeholder="" disabled=""> 
                </div>

                <div rol="form">
                        <label for="">REGISTRADO DESDE</label>
                        <input type="text" class="form-control" id="txt_marcacionasis_dispositivo" onkeypress="" maxlength="" placeholder="" disabled=""> 
                </div>

            </div>

            <div class="modal-footer">
                 
                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crmarcarasistencia" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> MarcarAsistencia
                    </button>
                    <button type="button" id="btnmodalaceptar_marcarasistencia" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                </div>
            </div>


        </div>
    </div>
</div>
{!!Form::close()!!}


