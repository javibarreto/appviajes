<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Listado de Total de Ventas</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12 form-group">
            <h2 class="page-header">Listado de Ventas</h2>
            <table  border="1" class="col-lg-12">

                <thead>
                <tr>
                    <th>Sucursal</th>
                    <th>Total de Ventas</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lista as $fila)
                    <tr>
                        <td class="no">{{$fila->sucursal}}</td>

                        <td class="no">{{$fila->cantidad}}</td>
                      
                    </tr>

                @endforeach
                </tbody>
            </table>

        </div>
    </div>

</body>
</html>